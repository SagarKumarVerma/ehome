﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Runtime.InteropServices
Imports UltraiDMS.EHomeDemo

'Namespace EHomeDemo
Public Class HCEHomeCMS
    Public Const ENUM_UNKNOWN As Integer = -1
    Public Const ENUM_DEV_ON As Integer = 0
    Public Const ENUM_DEV_OFF As Integer = 1
    Public Const ENUM_DEV_ADDRESS_CHANGED As Integer = 2
    Public Const ENUM_DEV_AUTH As Integer = 3
    Public Const ENUM_DEV_SESSIONKEY As Integer = 4
    Public Const ENUM_DEV_DAS_REQ As Integer = 5
    Public Const ENUM_DEV_SESSIONKEY_REQ As Integer = 6
    Public Const ENUM_DEV_DAS_REREGISTER As Integer = 7
    Public Const MAX_DEVNAME_LEN As Integer = 32
    Public Const DEMO_CHANNEL_TYPE_INVALID As Integer = -1
    Public Const DEMO_CHANNEL_TYPE_ANALOG As Integer = 0
    Public Const DEMO_CHANNEL_TYPE_IP As Integer = 1
    Public Const DEMO_CHANNEL_TYPE_ZERO As Integer = 2
    Public Const MAX_SERIALNO_LEN As Integer = 128
    Public Const MAX_PHOMENUM_LEN As Integer = 32
    Public Const MAX_DEVICE_NAME_LEN As Integer = 32
    Public Const NET_EHOME_GET_GPS_CFG As Integer = 20
    Public Const NET_EHOME_SET_GPS_CFG As Integer = 21
    Public Const NET_EHOME_GET_PIC_CFG As Integer = 22
    Public Const NET_EHOME_SET_PIC_CFG As Integer = 23
    Public Const MAX_EHOME_PROTOCOL_LEN As Integer = 1500
    Public Const IPADDRESS_LENGTH As Integer = 128
    Public Const CONFIG_GET_PARAM_XML As String = "<Params>" & vbCrLf & "<ConfigCmd>{0}</ConfigCmd>" & vbCrLf & "<ConfigParam1>{1}</ConfigParam1>" & vbCrLf & "<ConfigParam2>{2}</ConfigParam2>" & vbCrLf & "<ConfigParam3>{3}</ConfigParam3>" & vbCrLf & "<ConfigParam4>{4}</ConfigParam4>" & vbCrLf & "</Params>" & vbCrLf
    Public Const CONFIG_SET_PARAM_XML As String = "<Params>" & vbCrLf & "<ConfigCmd>{0}</ConfigCmd>" & vbCrLf & "<ConfigParam1>{1}</ConfigParam1>" & vbCrLf & "<ConfigParam2>{2}</ConfigParam2>" & vbCrLf & "<ConfigParam3>{3}</ConfigParam3>" & vbCrLf & "<ConfigParam4>{4}</ConfigParam4>" & vbCrLf & "<ConfigXML>{5}</ConfigXML>" & vbCrLf & "</Params>" & vbCrLf

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_CONFIG
        Public pCondBuf As IntPtr
        Public dwCondSize As UInteger
        Public pInBuf As IntPtr
        Public dwInSize As UInteger
        Public pOutBuf As IntPtr
        Public dwOutSize As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=40, ArraySubType:=UnmanagedType.U1)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_DEVICE_INFO
        Public dwSize As Integer
        Public dwChannelNumber As UInteger
        Public dwChannelAmount As UInteger
        Public dwDevType As UInteger
        Public dwDiskNumber As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_SERIALNO_LEN)>
        Public sSerialNumber As Byte()
        Public dwAlarmInPortNum As UInteger
        Public dwAlarmInAmount As UInteger
        Public dwAlarmOutPortNum As UInteger
        Public dwAlarmOutAmount As UInteger
        Public dwStartChannel As UInteger
        Public dwAudioChanNum As UInteger
        Public dwMaxDigitChannelNum As UInteger
        Public dwAudioEncType As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_SERIALNO_LEN)>
        Public sSIMCardSN As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_PHOMENUM_LEN)>
        Public sSIMCardPhoneNum As Byte()
        Public dwSupportZeroChan As UInteger
        Public dwStartZeroChan As UInteger
        Public dwSmartType As UInteger
        Public wDevClass As UShort
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=158)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_DEV_REG_INFO
        Public dwSize As Integer
        Public dwNetUnitType As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=HCEHomePublic.MAX_DEVICE_ID_LEN)>
        Public byDeviceID As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=24)>
        Public byFirmwareVersion As Byte()
        Public struDevAdd As HCEHomePublic.NET_EHOME_IPADDRESS
        Public dwDevType As Integer
        Public dwManufacture As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32)>
        Public byPassWord As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=HCEHomePublic.NET_EHOME_SERIAL_LEN)>
        Public sDeviceSerial As Byte()
        Public byReliableTransmission As Byte
        Public byWebSocketTransmission As Byte
        Public bySupportRedirect As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=6)>
        Public byDevProtocolVersion As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=HCEHomePublic.MAX_MASTER_KEY_LEN)>
        Public bySessionKey As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=27)>
        Public byRes As Byte()

        Public Sub Init()
            byDeviceID = New Byte(HCEHomePublic.MAX_DEVICE_ID_LEN - 1) {}
            byFirmwareVersion = New Byte(23) {}
            byPassWord = New Byte(31) {}
            sDeviceSerial = New Byte(HCEHomePublic.NET_EHOME_SERIAL_LEN - 1) {}
            byDevProtocolVersion = New Byte(5) {}
            bySessionKey = New Byte(HCEHomePublic.MAX_MASTER_KEY_LEN - 1) {}
            byRes = New Byte(26) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_IPADDRESS
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=HCEHomeCMS.IPADDRESS_LENGTH)>
        Public szIP As String
        Public wPort As UShort
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)>
        Public byRes As Byte()

        Public Sub Init()
            byRes = New Byte(1) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_DEV_REG_INFO_V12
        Public struRegInfo As HCEHomeCMS.NET_EHOME_DEV_REG_INFO
        Public struRegAddr As HCEHomeCMS.NET_EHOME_IPADDRESS
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DEVNAME_LEN)>
        Public sDevName As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=224)>
        Public byRes As Byte()

        Public Sub Init()
            struRegInfo.Init()
            struRegAddr.Init()
				sDevName = New Byte(MAX_DEVNAME_LEN - 1){}
            byRes = New Byte(223) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_BLACKLIST_SEVER
        Public struAdd As HCEHomePublic.NET_EHOME_IPADDRESS
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=HCEHomePublic.NAME_LEN)>
        Public byServerName As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=HCEHomePublic.NAME_LEN)>
        Public byUserName As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=HCEHomePublic.NAME_LEN)>
        Public byPassWord As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64)>
        Public byRes As Byte()

        Public Sub Init()
            struAdd.Init()
            byServerName = New Byte(HCEHomePublic.NAME_LEN - 1) {}
            byUserName = New Byte(HCEHomePublic.NAME_LEN - 1) {}
            byPassWord = New Byte(HCEHomePublic.NAME_LEN - 1) {}
            byRes = New Byte(63) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_SERVER_INFO
        Public dwSize As Integer
        Public dwKeepAliveSec As Integer
        Public dwTimeOutCount As Integer
        Public struTCPAlarmSever As HCEHomePublic.NET_EHOME_IPADDRESS
        Public struUDPAlarmSever As HCEHomePublic.NET_EHOME_IPADDRESS
        Public dwAlarmServerType As Integer
        Public struNTPSever As HCEHomePublic.NET_EHOME_IPADDRESS
        Public dwNTPInterval As Integer
        Public struPictureSever As HCEHomePublic.NET_EHOME_IPADDRESS
        Public dwPicServerType As Integer
        Public struBlackListServer As NET_EHOME_BLACKLIST_SEVER
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()

        Public Sub Init()
            struTCPAlarmSever.Init()
            struUDPAlarmSever.Init()
            struNTPSever.Init()
            struPictureSever.Init()
            struBlackListServer.Init()
            byRes = New Byte(127) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_SERVER_INFO_V50
        Public dwSize As Integer
        Public dwKeepAliveSec As Integer
        Public dwTimeOutCount As Integer
        Public struTCPAlarmSever As HCEHomePublic.NET_EHOME_IPADDRESS
        Public struUDPAlarmSever As HCEHomePublic.NET_EHOME_IPADDRESS
        Public dwAlarmServerType As Integer
        Public struNTPSever As HCEHomePublic.NET_EHOME_IPADDRESS
        Public dwNTPInterval As Integer
        Public struPictureSever As HCEHomePublic.NET_EHOME_IPADDRESS
        Public dwPicServerType As Integer
        Public struBlackListServer As NET_EHOME_BLACKLIST_SEVER
        Public struRedirectSever As HCEHomePublic.NET_EHOME_IPADDRESS
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64)>
        Public byClouldAccessKey As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64)>
        Public byClouldSecretKey As Byte()
        Public byClouldHttps As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=383)>
        Public byRes As Byte()

        Public Sub Init()
            struTCPAlarmSever.Init()
            struUDPAlarmSever.Init()
            struNTPSever.Init()
            struPictureSever.Init()
            struBlackListServer.Init()
            struRedirectSever.Init()
            byClouldAccessKey = New Byte(63) {}
            byClouldSecretKey = New Byte(63) {}
            byRes = New Byte(382) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_CMS_LISTEN_PARAM
        Public struAddress As HCEHomePublic.NET_EHOME_IPADDRESS
        Public fnCB As DEVICE_REGISTER_CB
        Public pUserData As IntPtr
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PREVIEWINFO_IN
        Public iChannel As Integer
        Public dwStreamType As Integer
        Public dwLinkMode As Integer
        Public struStreamSever As HCEHomePublic.NET_EHOME_IPADDRESS

        Public Sub Init()
            struStreamSever.Init()
        End Sub
    End Structure

    Public Structure NET_EHOME_PREVIEWINFO_IN_V11
        Public iChannel As Integer
        Public dwStreamType As Integer
        Public dwLinkMode As Integer
        Public struStreamSever As HCEHomePublic.NET_EHOME_IPADDRESS
        Public byDelayPreview As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=31)>
        Public byRes As Byte()

        Public Sub Init()
            struStreamSever.Init()
            byRes = New Byte(30) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PREVIEWINFO_OUT
        Public lSessionID As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PUSHSTREAM_IN
        Public dwSize As Integer
        Public lSessionID As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()

        Public Sub Init()
            byRes = New Byte(127) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PUSHSTREAM_OUT
        Public dwSize As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()

        Public Sub Init()
            byRes = New Byte(127) {}
        End Sub
    End Structure

    Public Delegate Function DEVICE_REGISTER_CB(ByVal lUserID As Integer, ByVal dwDataType As Integer, ByVal pOutBuffer As IntPtr, ByVal dwOutLen As UInteger, ByVal pInBuffer As IntPtr, ByVal dwInLen As UInteger, ByVal pUser As IntPtr) As Boolean
    Public Delegate Sub fVoiceDataCallBack(ByVal iVoiceHandle As Int32, ByVal pRecvDataBuffer As Char(), ByVal dwBufSize As UInteger, ByVal dwEncodeType As UInteger, ByVal byAudioFlag As Byte, ByVal pUser As IntPtr)

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_VOICETALK_PARA
        Public bNeedCBNoEncData As Integer
        Public cbVoiceDataCallBack As fVoiceDataCallBack
        Public dwEncodeType As UInteger
        Public pUser As IntPtr
        Public byVoiceTalk As Byte
        Public byDevAudioEnc As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=62)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_VOICE_TALK_IN
        Public dwVoiceChan As UInteger
        Public struStreamSever As HCEHomePublic.NET_EHOME_IPADDRESS
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_VOICE_TALK_OUT
        Public lSessionID As Int32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PUSHVOICE_IN
        Public dwSize As UInteger
        Public lSessionID As Int32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PUSHVOICE_OUT
        Public dwSize As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_XML_CFG
        Public pCmdBuf As IntPtr
        Public dwCmdLen As UInteger
        Public pInBuf As IntPtr
        Public dwInSize As UInteger
        Public pOutBuf As IntPtr
        Public dwOutSize As UInteger
        Public dwSendTimeOut As UInteger
        Public dwRecvTimeOut As UInteger
        Public pStatusBuf As IntPtr
        Public dwStatusSize As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=24)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_XML_REMOTE_CTRL_PARAM
        Public dwSize As UInteger
        Public lpInbuffer As IntPtr
        Public dwInBufferSize As UInteger
        Public dwSendTimeOut As UInteger
        Public dwRecvTimeOut As UInteger
        Public lpOutBuffer As IntPtr
        Public dwOutBufferSize As UInteger
        Public lpStatusBuffer As IntPtr
        Public dwStatusBufferSize As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=16)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PTXML_PARAM
        Public pRequestUrl As IntPtr
        Public dwRequestUrlLen As UInteger
        Public pCondBuffer As IntPtr
        Public dwCondSize As UInteger
        Public pInBuffer As IntPtr
        Public dwInSize As UInteger
        Public pOutBuffer As IntPtr
        Public dwOutSize As UInteger
        Public dwReturnedXMLLen As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_REMOTE_CTRL_PARAM
        Private dwSize As UInteger
        Private lpCondBuffer As IntPtr
        Private dwCondBufferSize As UInteger
        Private lpInbuffer As IntPtr
        Private dwInBufferSize As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32)>
        Private byRes As Byte()
    End Structure

		'时间参数
		<StructLayout(LayoutKind.Sequential)>
		Public Structure NET_EHOME_TIME
			Public wYear As Int16 '年
			Public byMonth() As Byte '月
			Public byDay() As Byte '日
			Public byHour() As Byte '时
			Public byMinute() As Byte '分
			Public bySecond() As Byte '秒
			Public byRes1() As Byte
			Public wMSecond As Int16 '毫秒
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := 2)>
			Public byRes() As Byte '保留
		End Structure

		<StructLayout(LayoutKind.Sequential)>
		Public Structure NET_EHOME_REC_FILE_COND
			Public dwChannel As Int32 '通道号，从1开始
			Public dwRecType As Int32
			Public struStartTime As NET_EHOME_TIME '开始时间
			Public struStopTime As NET_EHOME_TIME '结束时间
			Public dwStartIndex As Int32 '查询起始位置
			Public dwMaxFileCountPer As Int32 '单次搜索最大文件个数，最大文件个数，需要确定实际网络环境，建议最大个数为8
			Public byLocalOrUTC() As Byte '0-struStartTime和struStopTime中，表示的是设备本地时间，即设备OSD时间  1-struStartTime和struStopTime中，表示的是UTC时间
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := 8)>
			Public byRes() As Byte '保留
		End Structure


		'查询接口
		Public Const MAX_FILE_NAME_LEN As Integer = 100
		'录像文件信息
		<StructLayout(LayoutKind.Sequential)>
		Public Structure NET_EHOME_REC_FILE
			Public dwSize As Int32
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := MAX_FILE_NAME_LEN)>
			Public sFileName() As Char '文件名
			Public struStartTime As NET_EHOME_TIME '文件的开始时间
			Public struStopTime As NET_EHOME_TIME '文件的结束时间
			Public dwFileSize As Int32 '文件的大小
			Public dwFileMainType As Int32 '录像文件主类型
			Public dwFileSubType As Int32 '录像文件子类型
			Public dwFileIndex As Int32 '录像文件索引
			Public byTimeDiffH() As Byte 'struStartTime、struStopTime与国际标准时间（UTC）的时差（小时），-12 ... +14,0xff表示无效
			Public byTimeDiffM() As Byte 'struStartTime、struStopTime与国际标准时间（UTC）的时差（分钟），-30,0, 30, 45, 0xff表示无效
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := 126)>
			Public byRes() As Byte
		End Structure

		Public Structure NET_EHOME_STOPPLAYBACK_PARAM
			Public lSessionID As Int32
			Public lHandle As Int32
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := 120)>
			Public byRes As Byte
		End Structure

		<StructLayout(LayoutKind.Sequential)>
		Public Structure PLAYBACK_BY_NAME
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := MAX_FILE_NAME_LEN)>
			Public szFileName() As Char '回放的文件名
			Public dwSeekType As Int32 '0-按字节长度计算偏移量  1-按时间（秒数）计算偏移量
			Public dwFileOffset As Int32 '文件偏移量，从哪个位置开始下载，如果dwSeekType为0，偏移则以字节计算，为1则以秒数计算
			Public dwFileSpan As Int32 '下载的文件大小，为0时，表示下载直到该文件结束为止，如果dwSeekType为0，大小则以字节计算，为1则以秒数计算
		End Structure

		<StructLayout(LayoutKind.Sequential)>
		Public Structure PLAYBACK_BY_TIME
			Public struStartTime As NET_EHOME_TIME ' 按时间回放的开始时间
			Public struStopTime As NET_EHOME_TIME ' 按时间回放的结束时间
			Public byLocalOrUTC() As Byte '0-设备本地时间，即设备OSD时间  1-UTC时间
			Public byDuplicateSegment() As Byte 'byLocalOrUTC为1时无效 0-重复时间段的前段 1-重复时间段后端
		End Structure

		<StructLayout(LayoutKind.Sequential)>
		Public Structure NET_EHOME_PLAYBACK_INFO_IN
			Public dwSize As Int32
			Public dwChannel As Int32 '回放的通道号
			Public byPlayBackMode() As Byte '回放下载模式 0－按名字 1－按时间
			Public byStreamPackage() As Byte '回放码流类型，设备端发出的码流格式 0－PS（默认） 1－RTP
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := 2)>
			Public byRes() As Byte

			<StructLayout(LayoutKind.Explicit)>
			Public Structure unionPlayBackMode
				<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := 512)>
				<FieldOffsetAttribute(0)>
				Public byLen() As Byte
				<FieldOffsetAttribute(0)>
				Public struPlayBackbyName As PLAYBACK_BY_NAME
				<FieldOffsetAttribute(0)>
				Public struPlayBackbyTime As PLAYBACK_BY_TIME
			End Structure
			Public struStreamSever As NET_EHOME_IPADDRESS '流媒体地址
		End Structure


		<StructLayout(LayoutKind.Sequential)>
		Public Structure NET_EHOME_PLAYBACK_INFO_OUT
			Public lSessionID As Int32 '目前协议不支持，返回-1
			Public lHandle As Int32 '设置了回放异步回调之后，该值为消息句柄，回调中用于标识
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := 124)>
			Public byRes() As Byte
		End Structure

		Public Const ENUM_SEARCH_TYPE_ERR As Integer = -1
		Public Const ENUM_SEARCH_RECORD_FILE As Integer = 0 '查找录像文件
		Public Const ENUM_SEARCH_PICTURE_FILE As Integer = 1 '查找图片文件
		Public Const ENUM_SEARCH_FLOW_INFO As Integer = 2 '流量查询
		Public Const ENUM_SEARCH_DEV_LOG As Integer = 3 '设备日志查询
		Public Const ENUM_SEARCH_ALARM_HOST_LOG As Integer = 4 '报警主机日志查询

		Public Const ENUM_GET_NEXT_STATUS_SUCCESS As Integer = 1000 '成功读取到一条数据，处理完本次数据后需要再次调用FindNext获取下一条数据
		Public Const ENUM_GET_NETX_STATUS_NO_FILE As Integer = 1001 '没有找到一条数据
		Public Const ENUM_GET_NETX_STATUS_NEED_WAIT As Integer = 1002 '数据还未就绪，需等待，继续调用FindNext函数
		Public Const ENUM_GET_NEXT_STATUS_FINISH As Integer = 1003 '数据全部取完
		Public Const ENUM_GET_NEXT_STATUS_FAILED As Integer = 1004 '出现异常
		Public Const ENUM_GET_NEXT_STATUS_NOT_SUPPORT As Integer = 1005 '设备不支持该操作，不支持的查询类型




		#Region "HCISUPCMS.dll function definition"

		'SDK初始化
		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_Init() As Boolean
		End Function

		'SDK反初始化
		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_Fini() As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_GetLastError() As Integer
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_GetBuildVersion() As Long
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StartListen(ByRef lpCMSListenPara As NET_EHOME_CMS_LISTEN_PARAM) As Integer
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StopListen(ByVal iHandle As Int32) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_ForceLogout(ByVal lUserID As Int32) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_SetLogToFile(ByVal iLogLevel As Int32, ByVal szLogDir() As Char, ByVal bAutoDel As Boolean) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_SetSDKLocalCfg(ByVal enumType As HCEHomePublic.NET_EHOME_LOCAL_CFG_TYPE, ByVal pInBuff As IntPtr) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_GetSDKLocalCfg(ByVal enumType As HCEHomePublic.NET_EHOME_LOCAL_CFG_TYPE, ByVal POutBuff As IntPtr) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StartGetRealStream(ByVal lUserID As Int32, ByRef pPreviewInfoIn As HCEHomeCMS.NET_EHOME_PREVIEWINFO_IN, ByRef pPreviewInfoOut As HCEHomeCMS.NET_EHOME_PREVIEWINFO_OUT) As Boolean 'lUserID由SDK分配的用户ID，由设备注册回调时fDeviceRegisterCallBack返回
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StartGetRealStreamV11(ByVal lUserID As Int32, ByRef pPreviewInfoIn As NET_EHOME_PREVIEWINFO_IN_V11, ByRef pPreviewInfoOut As NET_EHOME_PREVIEWINFO_OUT) As Boolean 'iUserID由SDK分配的用户ID，由设备注册回调时fDeviceRegisterCallBack返回
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StopGetRealStream(ByVal lUserID As Int32, ByVal lSessionID As Int32) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StartPushRealStream(ByVal lUserID As Int32, ByRef pPushInfoIn As NET_EHOME_PUSHSTREAM_IN, ByRef pPushInfoOut As NET_EHOME_PUSHSTREAM_OUT) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_GetDevConfig(ByVal lUserID As Integer, ByVal dwCommand As UInteger, ByRef lpConfig As HCEHomeCMS.NET_EHOME_CONFIG, ByVal dwConfigSize As UInteger) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_SetDevConfig(ByVal lUserID As Int32, ByVal dwCommand As Int32, ByRef lpConfig As NET_EHOME_CONFIG, ByVal dwConfigSize As Int32) As Boolean
		End Function


		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StartVoiceTalk(ByVal lUserID As Int32, ByVal dwVoiceChan As UInteger, ByRef pVoiceTalkPara As NET_EHOME_VOICETALK_PARA) As Integer
		End Function
		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StartVoiceWithStmServer(ByVal lUserID As Int32, ByRef lpVoiceTalkIn As NET_EHOME_VOICE_TALK_IN, ByRef lpVoiceTalkOut As NET_EHOME_VOICE_TALK_OUT) As Boolean
		End Function
		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StartPushVoiceStream(ByVal lUserID As Int32, ByRef lpPushParamIn As NET_EHOME_PUSHVOICE_IN, ByRef lpPushParamOut As NET_EHOME_PUSHVOICE_OUT) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StopVoiceTalk(ByVal iVoiceHandle As Int32) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StopVoiceTalkWithStmServer(ByVal lUserID As Int32, ByVal lSessionID As Int32) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_SendVoiceTransData(ByVal iVoiceHandle As Int32, ByVal pSendBuf() As Char, ByVal dwBufSize As UInteger) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_XMLConfig(ByVal lUserID As Int32, ByVal pXmlCfg As IntPtr, ByVal dwConfigSize As Int32) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_RemoteControl(ByVal lUserID As Int32, ByVal dwCommand As Int32, ByRef lpCtrlParam As NET_EHOME_REMOTE_CTRL_PARAM) As Boolean
		End Function

    <DllImport("HCISUPCMS.dll")>
    Public Shared Function NET_ECMS_XMLRemoteControl(ByVal lUserID As Int32, ByVal lpCtrlParam As IntPtr, ByVal dwCtrlSize As Int32) As Boolean
		 End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_GetPTXMLConfig(ByVal iUserID As Int32, ByVal lpPTXMLParam As IntPtr) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_PutPTXMLConfig(ByVal iUserID As Int32, ByVal lpPTXMLParam As IntPtr) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_PostPTXMLConfig(ByVal iUserID As Int32, ByVal lpPTXMLParam As IntPtr) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_DeletePTXMLConfig(ByVal lUserID As Int32, ByVal lpPTXMLParam As IntPtr) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_SetDeviceSessionKey(ByRef pDeviceKey As HCEHomePublic.NET_EHOME_DEV_SESSIONKEY) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_GetDeviceSessionKey(ByRef pDeviceKey As HCEHomePublic.NET_EHOME_DEV_SESSIONKEY) As Boolean
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StartListenProxy(ByRef pStruProxyParam As HCEHomePublic.NET_EHOME_PT_PARAM) As Integer
		End Function

		<DllImport("HCISUPCMS.dll")>
		Public Shared Function NET_ECMS_StopListenProxy(ByVal lListenHandle As Int32, ByVal dwProxyType As Int32) As Boolean
		End Function

		'回放
		<DllImport("HCEHomeStream.dll")>
		Public Shared Function NET_ECMS_StartFindFile_V11(ByVal lUserID As Int32, ByVal lSearchType As Int32, ByVal pFindCond As IntPtr, ByVal dwCondSize As Int32) As Int32
		End Function
		<DllImport("HCEHomeStream.dll")>
		Public Shared Function NET_ECMS_FindNextFile_V11(ByVal lHandle As Int32, ByVal pFindData As IntPtr, ByVal dwDataSize As Int32) As Int32
		End Function
		<DllImport("HCEHomeStream.dll")>
		Public Shared Function NET_ECMS_StopFindFile(ByVal lHandle As Int32) As Int32
		End Function
		<DllImport("HCEHomeStream.dll")>
		Public Shared Function NET_ECMS_StartPlayBack(ByVal lUserID As Int32, ByVal pPlayBackInfoIn As IntPtr, ByVal pPlayBackInfoOut As IntPtr) As Boolean
		End Function
		<DllImport("HCEHomeStream.dll")>
		Public Shared Function NET_ECMS_StopPlayBack(ByVal iUserID As Int32, ByVal pStopParam As IntPtr) As Boolean
		End Function

		#End Region

	End Class
'End Namespace
