﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Runtime.InteropServices
Imports TINYXMLTRANS
Imports UltraiDMS.EHomeDemo
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Windows.Forms
Imports EHomeDemo.Public
Imports Newtonsoft.Json
Imports System.IO
Imports System.Net
Imports System.Xml

Partial Public Class CMSAlarm
    Inherits Form

#Region "dll region"
    <DllImport("User32.dll", EntryPoint:="PostMessage")>
    Public Shared Function PostMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Integer
    End Function
    <DllImport("User32.dll", EntryPoint:="SendMessage")>
    Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Integer
    End Function
#End Region
    Public m_bUseAccessList As Boolean = False
    Public ehomeMsgCallback As HCEHomeAlarm.EHomeMsgCallBack = Nothing
    Public m_struAccessSecure As New HCEHomePublic.NET_EHOME_LOCAL_ACCESS_SECURITY()
    Private g_formList As DeviceLogList = DeviceLogList.Instance()

    Public m_struServInfo As New HCEHomeCMS.NET_EHOME_SERVER_INFO_V50()

    Public ptrTcpAlarm As HCEHomeAlarm.EHomeMsgCallBack = Nothing
    Public ptrMqtt As HCEHomeAlarm.EHomeMsgCallBack = Nothing
    Public m_lUdpAlarmHandle As Integer
    Public m_lTcpAlarmHandle As Integer
		Public Shared m_lEhome50AlarmHandle As Integer = -1

    Public m_struAmsAddr As New HCEHomePublic.NET_EHOME_AMS_ADDRESS()
    Private Delegate Sub UpdatePictureBoxControl(ByVal AlarmPath As List(Of String))


    Private Shared g_AlarmInfoList As New CMSAlarm()

    Public m_csLocalIP As String = String.Empty
    Public m_wAlarmServerMqttPort As UShort = 0
    Public Shared m_stAccessDeviceList(63) As ACCESS_DEVICE_INFO
    Private m_logListHandle As IntPtr
    Public Structure ACCESS_DEVICE_INFO
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=12, ArraySubType:=UnmanagedType.U1)>
        Public sSerialNumber() As Byte '设备序列号
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.U1)>
        Public sIdentifyCode() As Byte '设备验证码
    End Structure


    Public Shared VisibleBox As New PictureBox()
    Public Shared ThemalBox As New PictureBox()
    Public Shared AlarmInfoBox As New RichTextBox()
    Public Shared ISAPIAlarmBox As New RichTextBox()



    Public Sub New()
        'InitializeComponent()
        'm_logListHandle = g_formList.Handle
        'VisibleBox = CmsPic
        'AlarmInfoBox = AlarmBox
        'ISAPIAlarmBox = TemperatureInfo
        'ThemalBox = ThemalPicBox
        'AddHandler Me.UpdatePictureBox, AddressOf ReadImage
    End Sub

    Public Shared Function Instance() As CMSAlarm
        Return g_AlarmInfoList
    End Function


		Public Shared Sub ReadImageFromLocal(ByVal AlarmPath As String)
			VisibleBox.Image = Image.FromFile(AlarmPath)
		End Sub

    Public Shared Sub ReadThemalImageFromLocal(ByVal AlarmPath As String)
        ThemalBox.Image = Image.FromFile(AlarmPath)
    End Sub


    Public Shared Sub ShowCmsAlarmInfo(ByRef AlarmInfo As String)
        If AlarmInfoBox.Text IsNot Nothing Then
            AlarmInfoBox.Clear()
            AlarmInfoBox.Text = AlarmInfo
        End If
    End Sub
    Public Shared Sub ShowISAPIAlarmInfo(ByRef AlarmInfo As String)
        If AlarmInfoBox.Text IsNot Nothing Then
            ISAPIAlarmBox.Clear()
            ISAPIAlarmBox.Text = AlarmInfo
        End If
    End Sub

    ''Close CMs Alarm
    'Private Sub CloseAlarmBtn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseAlarmBtn.Click
    '	If Not Me.CloseAlarmBtn.IsHandleCreated Then Return

    '	If -1 <> m_lEhome50AlarmHandle Then
    '		If HCEHomeAlarm.NET_EALARM_StopListen(m_lEhome50AlarmHandle) = 0 Then
    '			g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 3, "NET_EALARM_StopListen Failed")
    '		Else
    '			g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_EALARM_StopListen Success")
    '		End If
    '		m_lEhome50AlarmHandle = -1
    '	End If
    '	m_struAmsAddr.dwSize = Marshal.SizeOf(m_struAmsAddr)
    '	m_struAmsAddr.byEnable = 0

    '	Dim struAmsAddrPtr As IntPtr = Marshal.AllocHGlobal(m_struAmsAddr.dwSize)

    '	If Not HCEHomeCMS.NET_ECMS_SetSDKLocalCfg(HCEHomePublic.NET_EHOME_LOCAL_CFG_TYPE.AMS_ADDRESS, struAmsAddrPtr) Then
    '		g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 3, "Close cms-alarm Failed")
    '	Else
    '		g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 1, "Close cms-alarm Succ")
    '	End If
    'End Sub

End Class

