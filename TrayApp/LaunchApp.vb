﻿
'Use only ONE of these Main methods.
Imports System.IO

Public Module LaunchApp

    Public Sub Main()
        'Turn visual styles back on
        Try
            AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf MyHandler 'for global exception handling

            Application.EnableVisualStyles()
            'Run the application using AppContext
            Dim appcount As Integer = 0
            For Each clsProcess As Process In Process.GetProcesses
                If clsProcess.ProcessName = "UltraiDMS" Then
                    appcount = appcount + 1
                End If
            Next
            If appcount > 1 Then
                MessageBox.Show("Application Already Running")
                Application.Exit()
                Return
            End If
            EHomeForm.Show()
            Application.Run(New AppContext)
        Catch ex As Exception
            Try
                Dim fs As FileStream = New FileStream("GlobalTry.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & ex.Message.Trim(vbNullChar) & " " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
                sw.Flush()
                sw.Close()
            Catch
            End Try
        End Try
        ''You can also run the application using a main form
        'Application.Run(New MainForm)

        ''Or in a default context with no user interface at all
        'Application.Run()
    End Sub


    Private Sub MyHandler(ByVal sender As Object, ByVal args As UnhandledExceptionEventArgs)
        Dim e As Exception = CType(args.ExceptionObject, Exception)
        'Console.WriteLine(("MyHandler caught : " + e.Message))
        Try
            Dim fs As FileStream = New FileStream("Global.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.[End])
            sw.WriteLine(vbLf & e.Message.Trim(vbNullChar) & " " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
            sw.Flush()
            sw.Close()
        Catch
        End Try
    End Sub

    'Public Sub Main(ByVal cmdArgs() As String)
    '    Application.EnableVisualStyles()

    '    Dim UseTray As Boolean = False

    '    For Each Cmd As String In cmdArgs
    '        If Cmd.ToLower = "-tray" Then
    '            UseTray = True
    '            Exit For
    '        End If
    '    Next

    '    If UseTray Then
    '        Application.Run(New AppContext)
    '    Else
    '        Application.Run(New MainForm)
    '    End If
    'End Sub

    'Public Function Main() As Integer
    'End Function

    'Public Function Main(ByVal cmdArgs() As String) As Integer
    'End Function

End Module
