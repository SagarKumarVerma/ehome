﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Runtime.InteropServices
Imports UltraiDMS.EHomeDemo

'Namespace EHomeDemo
Friend Class HCEHomeSS
        Public Const MAX_PATH As Integer = 260
        Public Const MAX_KMS_USER_LEN As Integer = 512
        Public Const MAX_KMS_PWD_LEN As Integer = 512
        Public Const MAX_CLOUD_AK_SK_LEN As Integer = 64
        Public Const MAX_URL_LEN_SS As Integer = 4096
		Public Delegate Function EHomeSSRWCallBack(ByVal iHandle As Integer, ByVal byAct As Byte, ByVal pFileName() As Char, ByVal pFileBuf As IntPtr, ByVal dwFileLen As IntPtr, ByVal pFileUrl As IntPtr, ByVal pUser As IntPtr) As Boolean
		Public Delegate Function EHomeSSStorageCallBack(ByVal iHandle As Integer, ByVal pFileName As IntPtr, ByVal pFileBuf As IntPtr, ByVal dwFileLen As Integer, ByVal pFilePath As IntPtr, ByVal pUser As IntPtr) As Boolean
		Public Delegate Function EHomeSSMsgCallBack(ByVal iHandle As Integer, ByVal enumType As HCEHomeSS.NET_EHOME_SS_MSG_TYPE, ByVal pOutBuffer As IntPtr, ByVal dwOutLen As Integer, ByVal pInBuffer As IntPtr, ByVal dwInLen As Integer, ByVal pUser As IntPtr) As Boolean


		Public Delegate Function EHomeSSRWCallBackEx(ByVal iHandle As Integer, ByRef pRwParam As NET_EHOME_SS_RW_PARAM, ByRef pExStruct As NET_EHOME_SS_EX_PARAM) As Boolean


        Public Enum NET_EHOME_SS_INIT_CFG_TYPE
            NET_EHOME_SS_INIT_CFG_SDK_PATH = 1
            NET_EHOME_SS_INIT_CFG_CLOUD_TIME_DIFF
            NET_EHOME_SS_INIT_CFG_PUBLIC_IP_PORT
        End Enum

        Public Enum NET_EHOME_SS_MSG_TYPE
            NET_EHOME_SS_MSG_TOMCAT = 1 'Tomcat回调函数
            NET_EHOME_SS_MSG_KMS_USER_PWD 'KMS用户名密码校验
            NET_EHOME_SS_MSG_CLOUD_AK 'EHome5.0存储协议AK回调
        End Enum

        Public Enum NET_EHOME_SS_CLIENT_TYPE
            NET_EHOME_SS_CLIENT_TYPE_TOMCAT = 1 'Tomcat图片上传客户端
            NET_EHOME_SS_CLIENT_TYPE_VRB 'VRB图片上传客户端
            NET_EHOME_SS_CLIENT_TYPE_KMS 'KMS图片上传客户端
            NET_EHOME_SS_CLIENT_TYPE_CLOUD 'EHome5.0存储协议客户端
        End Enum


        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_EHOME_SS_TOMCAT_MSG
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_URL_LEN_SS)>
            Public szDevUri As String
            Public dwPicNum As Integer
            Public pPicURLs As IntPtr
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64)>
            Public byRes() As Byte
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_EHOME_SS_LISTEN_HTTPS_PARAM
            Public byHttps As Byte
            Public byVerifyMode As Byte
            Public byCertificateFileType As Byte
            Public byPrivateKeyFileType As Byte
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_PATH)>
            Public szUserCertificateFile As String
            <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_PATH)>
            Public szUserPrivateKeyFile As String
            Public dwSSLVersion As Integer
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=360)>
            Public byRes3() As Byte

            Public Sub Init()
                byRes3 = New Byte(359) {}
            End Sub
        End Structure
        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_EHOME_SS_CLIENT_PARAM
            Public enumType As HCEHomeSS.NET_EHOME_SS_CLIENT_TYPE '图片上传客户端类型
            Public struAddress As HCEHomePublic.NET_EHOME_IPADDRESS '图片服务器地址
            Public byHttps As Byte '是否启用HTTPs
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=63)>
            Public byRes() As Byte
            Public Sub Init()
                struAddress.Init()
                byRes = New Byte(62) {}
            End Sub
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_EHOME_SS_RW_PARAM
            Public pFileName As IntPtr
            Public pFileBuf As IntPtr
            Public dwFileLen As IntPtr
            Public pFileUrl As IntPtr
            Public pUser As IntPtr
            Public byAct As Byte
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=63)>
            Public byRes() As Byte
            Public Sub Init()
                byRes = New Byte(62) {}
            End Sub
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_EHOME_SS_CLOUD_PARAM
            Public pPoolId As IntPtr 'poolId 资源池id
            Public byPoolIdLength As Byte 'poolid 长度
            Public dwErrorCode As Integer '云存储错误码
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=503)>
            Public byRes() As Byte
            Public Sub Init()
                byRes = New Byte(502) {}
            End Sub
        End Structure
        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_EHOME_SS_TOMCAT_PARAM
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=512)>
            Public byRes() As Byte
            Public Sub Init()
                byRes = New Byte(511) {}
            End Sub
        End Structure


        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_EHOME_SS_KMS_PARAM
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=512)>
            Public byRes() As Byte
            Public Sub Init()
                byRes = New Byte(511) {}
            End Sub
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure NET_EHOME_SS_VRB_PARAM
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=512)>
            Public byRes() As Byte
            Public Sub Init()
                byRes = New Byte(511) {}
            End Sub
        End Structure



        <StructLayout(LayoutKind.Sequential)>
		Public Structure NET_EHOME_ESS_STORE_UNION
            Public struCloud As NET_EHOME_SS_CLOUD_PARAM
            Public struTomcat As NET_EHOME_SS_TOMCAT_PARAM
            Public struKms As NET_EHOME_SS_KMS_PARAM
            Public struVrb As NET_EHOME_SS_VRB_PARAM
        End Structure

        <StructLayoutAttribute(LayoutKind.Sequential)>
        Public Structure NET_EHOME_SS_EX_PARAM
            Public byProtoType As Byte '存储协议类型 1-云存储，2-tomcat，3-kms，4-vrb
            <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=23)>
            Public byRes() As Byte
			Public unionStoreInfo As NET_EHOME_ESS_STORE_UNION
            Public Sub Init()
                byRes = New Byte(22) {}
            End Sub
        End Structure
		<StructLayoutAttribute(LayoutKind.Sequential)>
		Public Structure NET_EHOME_SS_LISTEN_PARAM
			Public struAddress As HCEHomePublic.NET_EHOME_IPADDRESS
			<MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst := HCEHomeSS.MAX_KMS_USER_LEN)>
			Public szKMS_UserName As String
			<MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst := HCEHomeSS.MAX_KMS_PWD_LEN)>
			Public szKMS_Password As String
			Public fnSStorageCb As EHomeSSStorageCallBack '图片服务器信息存储回调函数
			Public fnSSMsgCb As EHomeSSMsgCallBack '图片服务器信息Tomcat回调函数
			<MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst := HCEHomeSS.MAX_CLOUD_AK_SK_LEN)>
			Public szAccessKey As String
			<MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst := HCEHomeSS.MAX_CLOUD_AK_SK_LEN)>
			Public szSecretKey As String
			Public pUserData As IntPtr
			Public byHttps As Byte
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := 3)>
			Public byRes1() As Byte
			Public fnSSRWCb As EHomeSSRWCallBack
			'Security Mode
			Public fnSSRWCbEx As EHomeSSRWCallBackEx
			Public bySecurityMode As Byte
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := 51)>
			Public byRes() As Byte
			Public Sub Init()
				byRes1 = New Byte(2){}
				byRes = New Byte(50){}
			End Sub
		End Structure

		<StructLayoutAttribute(LayoutKind.Sequential)>
		Public Structure PicPath
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := 100)>
			Public Path() As Byte
			Public Sub Init()
				Path = New Byte(99){}
			End Sub
		End Structure
    <DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_HAMSHA256(ByVal pSrc As IntPtr, ByVal pSecretKey As IntPtr, ByVal pSingatureOut() As Byte, ByVal dwSingatureLen As Integer) As Boolean
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_Init() As Boolean
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_SetLogToFile(ByVal iLogLevel As Integer, ByVal strLogDir() As Char, ByVal bAutoDel As Boolean) As Integer
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_SetListenHttpsParam(ByRef pSSHttpsParam As HCEHomeSS.NET_EHOME_SS_LISTEN_HTTPS_PARAM) As Boolean
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_SetSDKInitCfg(ByVal enumType As HCEHomeSS.NET_EHOME_SS_INIT_CFG_TYPE, ByVal lpInBuff As IntPtr) As Boolean
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_StartListen(ByRef pSSListenParam As HCEHomeSS.NET_EHOME_SS_LISTEN_PARAM) As Integer
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_CreateClient(ByRef pClientParam As HCEHomeSS.NET_EHOME_SS_CLIENT_PARAM) As Integer
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_ClientSetTimeout(ByVal lHandle As Integer, ByVal dwSendTimeout As Integer, ByVal dwRecvTimeout As Integer) As Boolean
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_ClientSetParam(ByVal lHandle As Integer, ByVal strParamName As String, ByVal strParamVal As String) As Boolean
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_ClientDoUpload(ByVal lHandle As Integer, ByVal strUrl As IntPtr, ByVal dwUrlLen As Integer) As Boolean
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_DestroyClient(ByVal lHandle As Integer) As Boolean
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_StopListen(ByVal lListenHandle As Integer) As Boolean
		End Function

		<DllImport("HCISUPSS.dll")>
		Public Shared Function NET_ESS_Fini() As Boolean
		End Function
    End Class
'End Namespace
