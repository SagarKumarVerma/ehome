﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Runtime.InteropServices
Imports UltraiDMS.EhomeSDK
Imports UltraiDMS.EHomeDemo

'Namespace EHomeDemo
Public Class GlobalDefinition
    Public Const MAX_DEVICES As Integer = 1024
    Public Const MAX_CHAN_NUM_DEMO As Integer = 256
    Public Const MAX_OUTPUTS As Integer = 64
    Public Const MAX_LISTEN_NUM As Integer = 10
    Public LoginDevIp As String = ""
    Public LoginDevPort As Int16 = 0

    Public Structure LISTEN_INFO
        Public struIP As NET_EHOME_IPADDRESS
        Public iLinkType As Int32
        Public lHandle As Int32
    End Structure

    Public Const TREE_ALL As Integer = 0
    Public Const DEVICE_LOGOUT As Integer = 1
    Public Const DEVICE_LOGIN As Integer = 2
    Public Const DEVICE_FORTIFY As Integer = 3
    Public Const DEVICE_ALARM As Integer = 4
    Public Const DEVICE_FORTIFY_ALARM As Integer = 5
    Public Const CHAN_ORIGINAL As Integer = 6
    Public Const CHAN_PLAY As Integer = 7
    Public Const CHAN_RECORD As Integer = 8
    Public Const CHAN_PLAY_RECORD As Integer = 9
    Public Const TREE_ALL_T As Integer = 0
    Public Const DEVICETYPE As Integer = 1
    Public Const CHANNELTYPE As Integer = 2
    Public Const WM_USER As Integer = &H400
    Public Const WM_ADD_LOG As Integer = WM_USER + 1
    Public Const WM_ADD_DEV As Integer = WM_USER + 2
    Public Const WM_DEL_DEV As Integer = WM_USER + 3
    Public Const WM_CHANGE_CHANNEL_ITEM_IMAGE As Integer = WM_USER + 4
    Public Const WM_PROC_EXCEPTION As Integer = WM_USER + 5
    Public Const WM_CHANGE_IP As Integer = WM_USER + 6
    Public Const ALARM_INFO_T As Integer = 0
    Public Const OPERATION_SUCC_T As Integer = 1
    Public Const OPERATION_FAIL_T As Integer = 2
    Public Const PLAY_SUCC_T As Integer = 3
    Public Const PLAY_FAIL_T As Integer = 4

    Public Enum DEMO_CHANNEL_TYPE
        DEMO_CHANNEL_TYPE_INVALID = -1
        DEMO_CHANNEL_TYPE_ANALOG = 0
        DEMO_CHANNEL_TYPE_IP = 1
        DEMO_CHANNEL_TYPE_ZERO
    End Enum

    Public Structure STRU_CHANNEL_INFO
        Public iDeviceIndex As Integer
        Public iChanIndex As Integer
        Public iSessionID As Integer
        Public dwStreamType As Integer
        Public dwLinkMode As Integer
        Public lPreviewHandle As Integer
        Public iChanType As DEMO_CHANNEL_TYPE
        Public iChannelNO As Integer
        Public bEnable As Boolean
        Public dwImageType As Integer
        Public struIP As HCEHomePublic.NET_EHOME_IPADDRESS
        Public iPlayWndIndex As Integer
        Public m_hWnd As IntPtr
        Public bPlay As Boolean

        Public Sub Init()
            iDeviceIndex = -1
            iChanIndex = -1
            iSessionID = -1
            dwStreamType = 0
            dwLinkMode = 0
            lPreviewHandle = -1
            iChannelNO = -1
            bEnable = False
            iChanType = DEMO_CHANNEL_TYPE.DEMO_CHANNEL_TYPE_INVALID
            dwImageType = CHAN_ORIGINAL
            struIP.szIP = New Char(127) {}
            struIP.byRes = New Byte(31) {}
            struIP.wPort = 8000
            iPlayWndIndex = -1
            m_hWnd = IntPtr.Zero
            bPlay = False
        End Sub
    End Structure
    Public Structure LOCAL_DEVICE_INFO
        Public iDeviceIndex As Integer
        Public iLoginID As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=256, ArraySubType:=UnmanagedType.U1)>
        Public byDeviceID As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.U1)>
        Public byPassword As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64, ArraySubType:=UnmanagedType.U1)>
        Public byFirmwareVersion As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.U1)>
        Public byDevLocalIP As Byte()
        Public wDevLocalPort As Int16
        Public wManuFacture As Int16
        Public dwDevType As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=50, ArraySubType:=UnmanagedType.U1)>
        Public bySerialNumber As Byte()
        Public dwDeviceChanNum As UInteger
        Public dwStartChan As UInteger
        Public dwDiskNum As UInteger
        Public dwAlarmInNum As UInteger
        Public dwAlarmOutNum As UInteger
        Public dwAudioNum As UInteger
        Public dwAnalogChanNum As UInteger
        Public dwIPChanNum As UInteger
        Public dwZeroChanNum As UInteger
        Public dwZeroChanStart As UInteger
			<MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst := 12, ArraySubType :=UnmanagedType.U1)>
        Public sDeviceSerial As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8, ArraySubType:=UnmanagedType.U1)>
        Public sIdentifyCode As Byte()
        Public bPlayDevice As Boolean
        Public dwVersion As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_CHAN_NUM_DEMO, ArraySubType:=UnmanagedType.Struct)>
        Public struChanInfo As STRU_CHANNEL_INFO()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.U1)>
        Public byEhomeKey As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=16, ArraySubType:=UnmanagedType.U1)>
        Public bySessionKey As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64, ArraySubType:=UnmanagedType.U1)>
        Public byClouldSecretKey As Byte()

        Public Sub Init()
            iDeviceIndex = -1
            iLoginID = -1
            byDeviceID = New Byte(255) {}
            byPassword = New Byte(31) {}
            byFirmwareVersion = New Byte(63) {}
            byDevLocalIP = New Byte(127) {}
            wDevLocalPort = 0
            wManuFacture = 0
            dwDevType = 0
            bySerialNumber = New Byte(51) {}
            dwDeviceChanNum = 0
            dwStartChan = 0
            dwDiskNum = 0
            dwAlarmInNum = 0
            dwAlarmOutNum = 0
            dwAudioNum = 0
            dwAnalogChanNum = 0
            dwIPChanNum = 0
            dwZeroChanNum = 0
            dwZeroChanStart = 0
            bPlayDevice = False
            sDeviceSerial = New Byte(11) {}
            sIdentifyCode = New Byte(7) {}
            dwVersion = 0
				struChanInfo = New STRU_CHANNEL_INFO(MAX_CHAN_NUM_DEMO - 1){}
            byEhomeKey = New Byte(31) {}
            bySessionKey = New Byte(15) {}
            byClouldSecretKey = New Byte(63) {}
        End Sub
    End Structure
    Public Function convEncode(ByVal write As String, ByVal fromEncode As String, ByVal toEncode As String) As String
        Dim From, [To] As Encoding
        From = Encoding.GetEncoding(fromEncode)
        [To] = Encoding.GetEncoding(toEncode)
        Dim temp As Byte() = From.GetBytes(write)
        Dim temp1 As Byte() = Encoding.Convert(From, [To], temp)
        Return [To].GetString(temp1)
    End Function
End Class
'End Namespace
