﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EHomeForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EHomeForm))
        Me.LabelStatus = New System.Windows.Forms.Label()
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.m_cmbLocalIP = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.textServerIP = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TimerDeviceCommand = New System.Windows.Forms.Timer(Me.components)
        Me.TimerLoadActiveDevices = New System.Windows.Forms.Timer(Me.components)
        Me.TimerPortCheck = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'LabelStatus
        '
        Me.LabelStatus.AutoSize = True
        Me.LabelStatus.Location = New System.Drawing.Point(13, 88)
        Me.LabelStatus.Name = "LabelStatus"
        Me.LabelStatus.Size = New System.Drawing.Size(0, 13)
        Me.LabelStatus.TabIndex = 18
        '
        'btnConnect
        '
        Me.btnConnect.Location = New System.Drawing.Point(106, 65)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(75, 23)
        Me.btnConnect.TabIndex = 17
        Me.btnConnect.Text = "Connect"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'm_cmbLocalIP
        '
        Me.m_cmbLocalIP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.m_cmbLocalIP.FormattingEnabled = True
        Me.m_cmbLocalIP.Items.AddRange(New Object() {"0.0.0.0"})
        Me.m_cmbLocalIP.Location = New System.Drawing.Point(106, 12)
        Me.m_cmbLocalIP.Name = "m_cmbLocalIP"
        Me.m_cmbLocalIP.Size = New System.Drawing.Size(136, 21)
        Me.m_cmbLocalIP.TabIndex = 15
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "System IP"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(256, 63)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 14
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        Me.Button3.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(256, 5)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(256, 34)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 10000
        '
        'textServerIP
        '
        Me.textServerIP.Location = New System.Drawing.Point(106, 39)
        Me.textServerIP.Name = "textServerIP"
        Me.textServerIP.Size = New System.Drawing.Size(136, 20)
        Me.textServerIP.TabIndex = 19
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Server IP"
        '
        'TimerDeviceCommand
        '
        Me.TimerDeviceCommand.Interval = 5000
        '
        'TimerLoadActiveDevices
        '
        Me.TimerLoadActiveDevices.Interval = 900000
        '
        'TimerPortCheck
        '
        Me.TimerPortCheck.Interval = 600000
        '
        'EHomeForm
        '
        Me.AcceptButton = Me.btnConnect
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(356, 104)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.textServerIP)
        Me.Controls.Add(Me.LabelStatus)
        Me.Controls.Add(Me.btnConnect)
        Me.Controls.Add(Me.m_cmbLocalIP)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "EHomeForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ULtraiDMS"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LabelStatus As Label
    Friend WithEvents btnConnect As Button
    Public WithEvents m_cmbLocalIP As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Timer1 As Timer
    Friend WithEvents textServerIP As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TimerDeviceCommand As Timer
    Friend WithEvents TimerLoadActiveDevices As Timer
    Friend WithEvents TimerPortCheck As Timer
End Class
