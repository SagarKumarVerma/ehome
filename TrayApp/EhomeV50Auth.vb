﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Runtime.InteropServices
Imports UltraiDMS.EHomeDemo

Public Class EhomeV50Auth
    Public AuthDevIP As String
    Public AuthDevPort As Int16

    Public m_iDevID As Integer = -1
    Public temp As String = Nothing
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub EhomeV50Auth_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
    Private g_deviceTree As DeviceTree = DeviceTree.Instance()
    Public Sub CleanUpBytearray(ByRef source() As Byte, ByVal len As Integer)
        For i As Integer = 0 To len - 1
            source(i) = 0
        Next i
    End Sub
    '消息发送API  
    <DllImport("User32.dll", EntryPoint:="PostMessage")>
    Public Shared Function PostMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Integer
    End Function

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        m_iDevID = 0
        If m_iDevID >= 0 Then
            Dim strEhomeKey As String = "ehome111"
            Dim EhomeKeyLength As Integer = strEhomeKey.Length
            Dim byEhomeKey(EhomeKeyLength - 1) As Byte
            CleanUpBytearray(DeviceTree.g_struDeviceInfo(m_iDevID).byEhomeKey, 32)
            DeviceTree.StrToByteArray(byEhomeKey, strEhomeKey)
            Array.Copy(byEhomeKey, 0, DeviceTree.g_struDeviceInfo(m_iDevID).byEhomeKey, 0, EhomeKeyLength)

            If EHomeForm.g_bSS_Enable Then
                Dim ptrDeviceID As IntPtr = Marshal.AllocHGlobal(256)
                Marshal.Copy(DeviceTree.g_struDeviceInfo(m_iDevID).byDeviceID, 0, ptrDeviceID, 256)

                Dim ptrbyEhomeKey As IntPtr = Marshal.AllocHGlobal(32)
                Marshal.Copy(DeviceTree.g_struDeviceInfo(m_iDevID).byEhomeKey, 0, ptrbyEhomeKey, 32)

                'IntPtr ptrClouldSecretKey = Marshal.AllocHGlobal(64);
                Dim ClouldSecretKey(255) As Byte
                If HCEHomeSS.NET_ESS_HAMSHA256(ptrDeviceID, ptrbyEhomeKey, ClouldSecretKey, 255) Then
                    'Marshal.Copy(ptrClouldSecretKey, DeviceTree.g_struDeviceInfo[m_iDevID].byClouldSecretKey, 0, 64);
                    Array.Copy(ClouldSecretKey, DeviceTree.g_struDeviceInfo(m_iDevID).byClouldSecretKey, 64)
                End If
                Marshal.FreeHGlobal(ptrDeviceID)
                Marshal.FreeHGlobal(ptrbyEhomeKey)
                'Marshal.FreeHGlobal(ptrClouldSecretKey);
            End If
        Else
            Dim lpTemp As New GlobalDefinition.LOCAL_DEVICE_INFO()
            lpTemp.Init()
            lpTemp.dwVersion = 5
            Dim DevID() As Byte = System.Text.Encoding.Default.GetBytes("Ehome11")
            Dim IDLength As Integer = DevID.Length
            If IDLength > 256 Then
                'MessageBox.Show("DevID length is exceeding 256")
                EHomeForm.TraceService("DevID length is exceeding 256", "Line 67")
            End If
            Array.Copy(DevID, lpTemp.byDeviceID, IDLength)

            Dim EhomeKey() As Byte = System.Text.Encoding.Default.GetBytes("ehome111")
            Dim EhomeLength As Integer = EhomeKey.Length
            If EhomeLength > 32 Then
                'MessageBox.Show("EhomeKey length is exceeding 32")
                EHomeForm.TraceService("EhomeKey length is exceeding 32", "Line 75")
            End If
            Array.Copy(EhomeKey, lpTemp.byEhomeKey, EhomeLength)
            Dim dwSize As Integer = Marshal.SizeOf(GetType(GlobalDefinition.LOCAL_DEVICE_INFO))
            Dim ptrtemp As IntPtr = Marshal.AllocHGlobal(dwSize)
            Marshal.StructureToPtr(lpTemp, ptrtemp, False)
            Dim mes As New Message()
            mes.Msg = EHomeForm.WM_ADD_DEV
            mes.LParam = ptrtemp
            g_deviceTree.ProDevStatu(mes)

            'PostMessage(EHomeDemo.m_treeHandle, EHomeDemo.WM_ADD_DEV, ptrtemp, ptrtemp);
        End If
        Me.DialogResult = DialogResult.OK

    End Sub
End Class