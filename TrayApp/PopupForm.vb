﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text

Public Class PopupForm
    Dim servername As String
    Dim DB As String
    Public Shared ConnectionString As String
    Dim con As SqlConnection
    Dim con1 As OleDbConnection
    Public Sub New()
        InitializeComponent()
        Icon = My.Resources.TrayIcon
    End Sub
    Private Sub CancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
    Private Sub CloseAppButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DialogResult = Windows.Forms.DialogResult.Abort
        Me.Close()
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        CheckDBText()
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = "select * from tblUser where USER_R='admin'"
        If servername = "Access" Then
            adapAc = New OleDbDataAdapter(sSql, con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, con)
            adapS.Fill(Rs)
        End If
        If TextBox1.Text.Trim.ToUpper = Rs.Tables(0).Rows(0)("PASSWORD").ToString.Trim.ToUpper Then
            Me.DialogResult = Windows.Forms.DialogResult.Abort
            'Me.Close()

            'StopPreviewListen()
            'Dim struAmsAddr As New HCEHomePublic.NET_EHOME_AMS_ADDRESS()
            'struAmsAddr.dwSize = Marshal.SizeOf(struAmsAddr)
            'struAmsAddr.byEnable = 2
            'Dim ptrAmsAddr As IntPtr = Marshal.AllocHGlobal(struAmsAddr.dwSize)
            'Marshal.StructureToPtr(struAmsAddr, ptrAmsAddr, False)
            'HCEHomeCMS.NET_ECMS_SetSDKLocalCfg(HCEHomePublic.NET_EHOME_LOCAL_CFG_TYPE.AMS_ADDRESS, ptrAmsAddr)
            'Marshal.FreeHGlobal(ptrAmsAddr)
            'HCEHomeAlarm.NET_EALARM_StopListen(m_lUdpAlarmHandle)
            'HCEHomeAlarm.NET_EALARM_StopListen(m_lAlarmHandle)
            'HCEHomeAlarm.NET_EALARM_StopListen(m_lCmsAlarm)
            'HCEHomeAlarm.NET_EALARM_Fini()
            'HCEHomeCMS.NET_ECMS_StopListen(0)
            'HCEHomeCMS.NET_ECMS_Fini()
            'HCEHomeStream.NET_ESTREAM_Fini()
            'Application.Exit()

            Application.Exit()
        Else
            MessageBox.Show("Incorrect Password")
        End If
    End Sub
    Private Sub CheckDBText()
        If Not System.IO.File.Exists("db.txt") Then
            File.Create("db.txt").Dispose()
        End If
        Application.DoEvents()
        Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        Dim sr As StreamReader = New StreamReader(fs)
        Dim str As String
        Dim str1() As String
        'Dim dbname As String
        Do While sr.Peek <> -1
            str = sr.ReadLine
            str1 = str.Split(",")
            servername = str1(0)
        Loop

        sr.Close()
        fs.Close()
        If servername = "" Then
            Me.Hide()
            'XtraDB.ShowDialog()
            Exit Sub
        End If

        If servername = "Access" Then
            ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\HTDB.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\HTDB.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            con1 = New OleDbConnection(ConnectionString)

            Try
                con1.Open()
                con1.Close()
            Catch ex As Exception
                Me.Hide()
                'XtraDB.ShowDialog()
                Exit Sub
            End Try

        Else
            Try
                DB = str1(1)
                If str1(2) = "Win" Then
                    ConnectionString = "Data Source=" & servername & ";Initial Catalog=" & str1(1) & ";Integrated Security=True"
                    ConnectionString = "Data Source=" & servername & ";Initial Catalog=" & str1(1) & ";Integrated Security=True;MultipleActiveResultSets=true;"
                    'SQLUserId = ""
                    'SQLPassword = ""
                Else
                    ConnectionString = "Data Source=" & servername & ";Initial Catalog=" & str1(1) & ";User Id=" & str1(3) & ";Password=" & str1(4) & ";"
                    ConnectionString = "Data Source=" & servername & ";Initial Catalog=" & str1(1) & ";User Id=" & str1(3) & ";Password=" & str1(4) & ";MultipleActiveResultSets=true;"
                    'SQLUserId = str1(3)
                    'SQLPassword = str1(4)
                End If
                con = New SqlConnection(ConnectionString)

                Try
                    con.Open()
                    con.Close()
                Catch ex As Exception
                    Me.Hide()
                    Exit Sub
                End Try
                Dim sSql As String
                Try
                    sSql = "If COL_LENGTH('schemaName.UserFace', 'FaceByte') IS NULL " &
                                   "BEGIN  ALTER TABLE UserFace ADD FaceByte image End"
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    Dim cmd As SqlCommand
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                Catch ex As Exception
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End Try

                Try
                    sSql = "If COL_LENGTH('schemaName.MachineRawPunch', 'Temperature') IS NULL " &
                                    "BEGIN  ALTER TABLE MachineRawPunch ADD Temperature varchar(MAX),MaskStatus varchar(MAX),IsAbnomal varchar(MAX) End"
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    Dim cmd As SqlCommand
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                Catch ex As Exception
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End Try


            Catch ex As Exception
                Me.Hide()
                Exit Sub
            End Try
        End If
    End Sub
    Private Sub ClosePassword_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        ' Me.Close()
    End Sub
    Private Sub PopupForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox1.Text = ""
        TextBox1.Select()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim m_csCmd As String = "SETDEVICECONFIG"
        Dim m_szCommand() As Byte = New Byte((1024) - 1) {}
        Dim m_szInputBuffer() As Byte = New Byte((1500) - 1) {}
        Dim m_szOutBuffer() As Byte = New Byte(((1024 * 10)) - 1) {}
        Dim m_struCfg As HCEHomeCMS.NET_EHOME_XML_CFG = New HCEHomeCMS.NET_EHOME_XML_CFG
        Dim m_lUserID As Integer = 0
        Dim m_strInputXml As String = "<?xml version='1.0' encoding='UTF-8'?>" &
"<PPVSPMessage>" &
"<Version>5.0</Version>" &
"<Sequence>1</Sequence>" &
"<CommandType>REQUEST</CommandType>" &
"<Method>CONTROL</Method>" &
"<Command>REBOOT</Command>" &
"</PPVSPMessage>"
        Dim strTemp As String = m_csCmd
        m_szCommand = Encoding.UTF8.GetBytes(strTemp)
        m_struCfg.pCmdBuf = Marshal.AllocHGlobal(1024)
        Marshal.Copy(m_szCommand, 0, m_struCfg.pCmdBuf, m_szCommand.Length)
        strTemp = m_strInputXml
        m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
        m_struCfg.pInBuf = Marshal.AllocHGlobal((1024 * 10))
        Marshal.Copy(m_szInputBuffer, 0, m_struCfg.pInBuf, m_szInputBuffer.Length)
        m_struCfg.pOutBuf = Marshal.AllocHGlobal((1024 * 10))
        Dim i As Integer = 0
        Do While (i < (1024 * 10))
            Marshal.WriteByte(m_struCfg.pOutBuf, i, 0)
            i = (i + 1)
        Loop

        'Marshal.Copy(m_szOutBuffer, 0, m_struCfg.pInBuf, m_szOutBuffer.Length);
        'm_struCfg.pOutBuf = Marshal.StringToHGlobalAnsi(m_strOutputXml);
        m_struCfg.dwCmdLen = CType(m_szCommand.Length, UInteger)
        m_struCfg.dwInSize = CType(strTemp.Length, UInteger)
        m_struCfg.dwOutSize = CType((1024 * 10), UInteger)
        m_struCfg.byRes = New Byte((24) - 1) {}
        Dim dwSize As Integer = 0
        Dim ptrCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(m_struCfg))
        Marshal.StructureToPtr(m_struCfg, ptrCfg, False)
        dwSize = Marshal.SizeOf(GetType(HCEHomeCMS.NET_EHOME_XML_CFG))
        If Not HCEHomeCMS.NET_ECMS_XMLConfig(m_lUserID, ptrCfg, dwSize) Then
            'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_XMLConfig")
            Marshal.FreeHGlobal(m_struCfg.pCmdBuf)
            Marshal.FreeHGlobal(m_struCfg.pInBuf)
            Marshal.FreeHGlobal(m_struCfg.pOutBuf)
            Marshal.FreeHGlobal(ptrCfg)
            Return
        Else
            'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_XMLConfig")
            strTemp = ""
            strTemp = Marshal.PtrToStringAnsi(m_struCfg.pOutBuf, CType(m_struCfg.dwOutSize, Integer))
            'richTextBoxOutput.Text = strTemp
            Marshal.FreeHGlobal(m_struCfg.pCmdBuf)
            Marshal.FreeHGlobal(m_struCfg.pInBuf)
            Marshal.FreeHGlobal(m_struCfg.pOutBuf)
            Marshal.FreeHGlobal(ptrCfg)
            Return
        End If
    End Sub
End Class