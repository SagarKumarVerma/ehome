﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Runtime.InteropServices

'Namespace EHomeDemo
Public Class HCEHomePublic
		'''*****************全局错误码 begin*********************


		'音对讲库错误码
		Public Const NET_AUDIOINTERCOM_OK As Integer = 600 '无错误
		Public Const NET_AUDIOINTECOM_ERR_NOTSUPORT As Integer = 601 '不支持
		Public Const NET_AUDIOINTECOM_ERR_ALLOC_MEMERY As Integer = 602 '内存申请错误
		Public Const NET_AUDIOINTECOM_ERR_PARAMETER As Integer = 603 '参数错误
		Public Const NET_AUDIOINTECOM_ERR_CALL_ORDER As Integer = 604 '调用次序错误
		Public Const NET_AUDIOINTECOM_ERR_FIND_DEVICE As Integer = 605 '未发现设备
		Public Const NET_AUDIOINTECOM_ERR_OPEN_DEVICE As Integer = 606 '不能打开设备诶
		Public Const NET_AUDIOINTECOM_ERR_NO_CONTEXT As Integer = 607 '设备上下文出错
		Public Const NET_AUDIOINTECOM_ERR_NO_WAVFILE As Integer = 608 'WAV文件出错
		Public Const NET_AUDIOINTECOM_ERR_INVALID_TYPE As Integer = 609 '无效的WAV参数类型
		Public Const NET_AUDIOINTECOM_ERR_ENCODE_FAIL As Integer = 610 '编码失败
		Public Const NET_AUDIOINTECOM_ERR_DECODE_FAIL As Integer = 611 '解码失败
		Public Const NET_AUDIOINTECOM_ERR_NO_PLAYBACK As Integer = 612 '播放失败
		Public Const NET_AUDIOINTECOM_ERR_DENOISE_FAIL As Integer = 613 '降噪失败
		Public Const NET_AUDIOINTECOM_ERR_UNKOWN As Integer = 619 '未知错误
		'''*****************全局错误码 end*********************    
    Public Const ALARM_INFO_T As Integer = 0
    Public Const OPERATION_SUCC_T As Integer = 1
    Public Const OPERATION_FAIL_T As Integer = 2
    Public Const PLAY_SUCC_T As Integer = 3
    Public Const PLAY_FAIL_T As Integer = 4
    Public Const MAX_PASSWD_LEN As Integer = 32
    Public Const NAME_LEN As Integer = 32
    Public Const MAX_DEVICE_ID_LEN As Integer = 256
    Public Const NET_EHOME_SERIAL_LEN As Integer = 12
    Public Const MAX_MASTER_KEY_LEN As Integer = 16
    Public Const MAX_URL_LEN_SS As Integer = 4096

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_IPADDRESS
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128, ArraySubType:=UnmanagedType.U1)>
        Public szIP As Char()
        Public wPort As Int16
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)>
        Public byRes As Byte()

        Public Sub Init()
				wPort = 0
            szIP = New Char(127) {}
            byRes = New Byte(1) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_DEV_SESSIONKEY
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=HCEHomePublic.MAX_DEVICE_ID_LEN, ArraySubType:=UnmanagedType.U1)>
        Public sDeviceID As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=HCEHomePublic.MAX_MASTER_KEY_LEN, ArraySubType:=UnmanagedType.U1)>
        Public sSessionKey As Byte()

			Public Sub Init()
				sDeviceID = New Byte(HCEHomePublic.MAX_DEVICE_ID_LEN - 1){}
				sSessionKey = New Byte(HCEHomePublic.MAX_MASTER_KEY_LEN - 1){}
			End Sub
		End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_ZONE
        Private dwX As Int32
        Private dwY As Int32
        Private dwWidth As Int32
        Private dwHeight As Int32
    End Structure

    Public Enum NET_EHOME_LOCAL_CFG_TYPE
        UNDEFINE = -1
        ACTIVE_ACCESS_SECURITY = 0
        AMS_ADDRESS = 1
        SEND_PARAM = 2
    End Enum

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_LOCAL_ACCESS_SECURITY
        Public dwSize As UInt32
        Public byAccessSecurity As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=127)>
        Public byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_AMS_ADDRESS
        Public dwSize As Int32
        Public byEnable As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)>
        Public byRes1 As Byte()
        Public struAddress As NET_EHOME_IPADDRESS
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32)>
        Public byRes2 As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_SEND_PARAM
        Public dwSize As Int32
        Public dwRecvTimeOut As Int32
        Public bySendTimes As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=127)>
        Public byRes2 As Byte()
    End Structure

    Public Structure LOCAL_LOG_INFO
        Public iLogType As Integer
        Public strTime As String
        Public strLogInfo As String
        Public strDevInfo As String
        Public strErrInfo As String
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PT_PARAM
        Public struIP As NET_EHOME_IPADDRESS
        Public byProtocolType As Byte
        Public byProxyType As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)>
        Public byRes As Byte()

        Public Sub Init()
            struIP = New NET_EHOME_IPADDRESS()
            struIP.Init()
            byRes = New Byte(1) {}
        End Sub
    End Structure

    Public Enum NET_CMS_ENUM_PROXY_TYPE
        ENUM_PROXY_TYPE_NETSDK = 0
        ENUM_PROXY_TYPE_HTTP
    End Enum
End Class
'End Namespace
