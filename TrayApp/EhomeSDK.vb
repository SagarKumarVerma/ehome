﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Runtime.InteropServices

'Namespace EHomeDemo
Public Class EhomeSDK
    Public Const CP_UTF8 As Integer = 65001
    Public Const CP_ACP As Integer = 0

    <StructLayout(LayoutKind.Sequential)>
    Public Structure PREVIEW_IFNO
        Public iDeviceIndex As Integer
        Public iChanIndex As Integer
        Public PanelNo As Byte
        Public lRealHandle As Integer
        Public hPlayWnd As IntPtr
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_IPADDRESS
        Public szIP As Byte()
        Public lPort As System.Int16
        Public byRes As Byte()

        Public Sub Init()
            szIP = New Byte(127) {}
            byRes = New Byte(1) {}
        End Sub
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_ALARM_MSG
        Private dwAlarmType As System.Int32
        Private pAlarmInfo As IntPtr
        Private dwAlarmInfoLen As System.Int32
        Private pXmlBuf As IntPtr
        Private dwXmlBufLen As System.Int32
        Private sSerialNumber As String
        Private byDataType As Byte
        Private byRes As Byte()

        Public Sub Init()
            byRes = New Byte(2) {}
        End Sub
    End Structure

    Public Delegate Function MSGCallBack(ByVal iHandle As Long, ByRef pAlarmMsg As NET_EHOME_ALARM_MSG, ByVal pUser As IntPtr) As Boolean

    Public Structure NET_EHOME_ALARM_LISTEN_PARAM
        Private struAddress As NET_EHOME_IPADDRESS
        Private fnMsgCb As MSGCallBack
        Private pUserData As IntPtr
        Private byProtocolType As Byte
        Private byUseCmsPort As Byte
        Private byUseThreadPool As Byte
        Private byRes As Byte()
    End Structure

    Public Enum NET_EHOME_LOCAL_CFG_TYPE
        UNDEFINE = -1
        ACTIVE_ACCESS_SECURITY = 0
        AMS_ADDRESS
        SEND_PARAM
    End Enum

		<DllImport("kernel32.dll")>
		Public Shared Function MultiByteToWideChar(ByVal CodePage As Integer, ByVal dwFlags As Integer, ByVal lpMultiByteStr As String, ByVal cchMultiByte As Integer, <MarshalAs(UnmanagedType.LPWStr)> ByVal lpWideCharStr As String, ByVal cchWideChar As Integer) As Integer
		End Function

		<DllImport("Kernel32.dll")>
		Public Shared Function WideCharToMultiByte(ByVal CodePage As UInteger, ByVal dwFlags As UInteger, <[In], MarshalAs(UnmanagedType.LPWStr)> ByVal lpWideCharStr As String, ByVal cchWideChar As Integer, <[Out], MarshalAs(UnmanagedType.LPStr)> ByVal lpMultiByteStr As StringBuilder, ByVal cbMultiByte As Integer, ByVal lpDefaultChar As IntPtr, ByVal lpUsedDefaultChar As IntPtr) As Integer
		End Function

End Class
'End Namespace
