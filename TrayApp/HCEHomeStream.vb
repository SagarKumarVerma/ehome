﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Runtime.InteropServices

'Namespace EHomeDemo
Public Class HCEHomeStream
    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_NEWLINK_CB_MSG
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=HCEHomePublic.MAX_DEVICE_ID_LEN)>
        Public szDeviceID As Byte()
        Public iSessionID As Long
        Public dwChannelNo As Long
        Public byStreamType As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)>
        Public byRes1 As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=HCEHomePublic.NET_EHOME_SERIAL_LEN)>
        Public sDeviceSerial As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=112)>
        Public byRes As Byte()

        Public Sub Init()
            szDeviceID = New Byte(HCEHomePublic.MAX_DEVICE_ID_LEN - 1) {}
            byRes1 = New Byte(2) {}
            sDeviceSerial = New Byte(HCEHomePublic.NET_EHOME_SERIAL_LEN - 1) {}
            byRes = New Byte(111) {}
        End Sub
    End Structure

    Public Delegate Function PREVIEW_NEWLINK_CB(ByVal lLinkHandle As Integer, ByRef pNewLinkCBMsg As NET_EHOME_NEWLINK_CB_MSG, ByVal pUserData As IntPtr) As Boolean

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_LISTEN_PREVIEW_CFG
        Public struIPAdress As HCEHomePublic.NET_EHOME_IPADDRESS
        Public fnNewLinkCB As PREVIEW_NEWLINK_CB
        Public pUser As IntPtr
        Public byLinkMode As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=127)>
        Public byRes As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PREVIEW_CB_MSG
        Public byDataType As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)>
        Public byRes1 As Byte()
        Public pRecvdata As IntPtr
        Public dwDataLen As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes2 As Byte()
    End Structure

    Public Delegate Sub PREVIEW_DATA_CB(ByVal iPreviewHandle As Integer, ByRef pPreviewCBMsg As NET_EHOME_PREVIEW_CB_MSG, ByVal pUserData As IntPtr)

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PREVIEW_DATA_CB_PARAM
        Public fnPreviewDataCB As PREVIEW_DATA_CB
        Public pUserData As IntPtr
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PLAYBACK_NEWLINK_CB_INFO
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=HCEHomePublic.MAX_DEVICE_ID_LEN)>
        Public szDeviceID As Char()
        Public lSessionID As Int32
        Public dwChannelNo As Int32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=HCEHomePublic.NET_EHOME_SERIAL_LEN)>
        Public sDeviceSerial As Char()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=108)>
        Public byRes As Byte()
    End Structure

    Public Delegate Function PLAYBACK_NEWLINK_CB(ByVal lPlayBackLinkHandle As Int32, ByRef pNewLinkCBInfo As NET_EHOME_PLAYBACK_NEWLINK_CB_INFO, ByVal pUserData As IntPtr) As Boolean

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PLAYBACK_LISTEN_PARAM
        Private struIPAdress As HCEHomePublic.NET_EHOME_IPADDRESS
        Private fnNewLinkCB As PLAYBACK_NEWLINK_CB
        Private pUserData As IntPtr
        Private byLinkMode As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=127)>
        Private byRes As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PLAYBACK_DATA_CB_INFO
        Private dwType As Int32
        Private pData As Byte()
        Private dwDataLen As Int32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Private byRes As Byte()
    End Structure

    Public Delegate Function PLAYBACK_DATA_CB(ByVal iPlayBackLinkHandle As Int32, ByRef pDataCBInfo As NET_EHOME_PLAYBACK_DATA_CB_INFO, ByVal pUserData As IntPtr) As Boolean

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PLAYBACK_DATA_CB_PARAM
        Private fnPlayBackDataCB As PLAYBACK_DATA_CB
        Private pUserData As IntPtr
        Private byStreamFormat As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=127)>
        Private byRes As Byte()
    End Structure

    Public Const EHOME_PREVIEW_EXCEPTION As Integer = &H102
    Public Const EHOME_PLAYBACK_EXCEPTION As Integer = &H103
    Public Const EHOME_AUDIOTALK_EXCEPTION As Integer = &H104
    Public Const NET_EHOME_SYSHEAD As Integer = 1
    Public Const NET_EHOME_STREAMDATA As Integer = 2
    Public Const NET_EHOME_DEVICEID_LEN As Integer = 256
    Public Const NET_EHOME_SERIAL_LEN As Integer = 12
    Public Delegate Function NET_ESTREAM_StartListenPlayBack(ByRef pListenParam As NET_EHOME_PLAYBACK_LISTEN_PARAM) As Boolean
    Public Delegate Function NET_ESTREAM_SetPlayBackDataCB(ByVal iPlayBackLinkHandle As Int32, ByRef pDataCBParam As NET_EHOME_PLAYBACK_DATA_CB_PARAM) As Boolean
    Public Delegate Function NET_ESTREAM_StopPlayBack(ByVal iPlayBackLinkHandle As Int32) As Boolean
    Public Delegate Function NET_ESTREAM_StopListenPlayBack(ByVal iPlaybackListenHandle As Int32) As Boolean

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_VOICETALK_NEWLINK_CB_INFO
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NET_EHOME_DEVICEID_LEN)>
        Public szDeviceID As Byte()
        Public dwEncodeType As Int32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NET_EHOME_SERIAL_LEN)>
        Public sDeviceSerial As Char()
        Public dwAudioChan As Int32
        Public lSessionID As Int32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()
    End Structure

    Public Delegate Function VOICETALK_NEWLINK_CB(ByVal lHandle As Int32, ByRef pNewLinkCBInfo As NET_EHOME_VOICETALK_NEWLINK_CB_INFO, ByVal pUserData As IntPtr) As Boolean

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_LISTEN_VOICETALK_CFG
        Public struIPAdress As HCEHomePublic.NET_EHOME_IPADDRESS
        Public fnNewLinkCB As VOICETALK_NEWLINK_CB
        Public pUser As IntPtr
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_VOICETALK_DATA_CB_INFO
        Public pData As Byte()
        Public dwDataLen As Int32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()
    End Structure

    Public Delegate Function VOICETALK_DATA_CB(ByVal lHandle As Int32, ByRef pDataCBInfo As NET_EHOME_VOICETALK_DATA_CB_INFO, ByVal pUserData As IntPtr) As Boolean

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_VOICETALK_DATA_CB_PARAM
        Public fnVoiceTalkDataCB As VOICETALK_DATA_CB
        Public pUserData As IntPtr
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure NET_EHOME_VOICETALK_DATA
        Public pSendBuf As Byte()
        Public dwDataLen As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=128)>
        Public byRes As Byte()
    End Structure

    Public Delegate Sub fExceptionCallBack(ByVal dwType As Integer, ByVal iUserID As Integer, ByVal iHandle As Integer, ByVal pUser As IntPtr)
    Public Declare Function NET_ESTREAM_Init Lib "HCEHomeStream.dll" () As Boolean
    Public Declare Function NET_ESTREAM_Fini Lib "HCEHomeStream.dll" () As Boolean
    Public Declare Function NET_ESTREAM_GetLastError Lib "HCEHomeStream.dll" () As Boolean
    Public Declare Function NET_ESTREAM_SetExceptionCallBack Lib "HCEHomeStream.dll" (ByVal dwMessage As Int32, ByVal hWnd As Int32, ByVal fExCB As fExceptionCallBack, ByVal pUser As IntPtr) As Boolean
    Public Declare Function NET_ESTREAM_SetLogToFile Lib "HCEHomeStream.dll" (ByVal iLogLevel As Int32, ByVal strLogDir As String, ByVal bAutoDel As Boolean) As Boolean
    '��H,�
    Public Declare Function NET_ESTREAM_GetBuildVersion Lib "HCEHomeStream.dll" () As Boolean
    Public Declare Function NET_ESTREAM_StartListenPreview Lib "HCEHomeStream.dll" (ByRef pListenParam As NET_EHOME_LISTEN_PREVIEW_CFG) As Int32
    'ref NET_EHOME_LISTEN_PREVIEW_CFG pListenParam
    Public Declare Function NET_ESTREAM_StopListenPreview Lib "HCEHomeStream.dll" (ByVal iListenHandle As Int32) As Boolean
    Public Declare Function NET_ESTREAM_StopPreview Lib "HCEHomeStream.dll" (ByVal iPreviewHandle As Int32) As Boolean
    Public Declare Function NET_ESTREAM_SetPreviewDataCB Lib "HCEHomeStream.dll" (ByVal iHandle As Int32, ByVal pStruCBParam As IntPtr) As Boolean
    Public Declare Function NET_ESTREAM_StartListenVoiceTalk Lib "HCEHomeStream.dll" (ByRef pListenParam As NET_EHOME_LISTEN_VOICETALK_CFG) As Int32
    Public Declare Function NET_ESTREAM_StopListenVoiceTalk Lib "HCEHomeStream.dll" (ByVal lListenHandle As Int32) As Boolean
    Public Declare Function NET_ESTREAM_SetVoiceTalkDataCB Lib "HCEHomeStream.dll" (ByVal lHandle As Int32, ByRef pStruCBParam As NET_EHOME_VOICETALK_DATA_CB_PARAM) As Boolean
    Public Declare Function NET_ESTREAM_SendVoiceTalkData Lib "HCEHomeStream.dll" (ByVal lHandle As Int32, ByRef pVoicTalkData As NET_EHOME_VOICETALK_DATA) As Integer
    Public Declare Function NET_ESTREAM_StopVoiceTalk Lib "HCEHomeStream.dll" (ByVal lHandle As Int32) As Boolean
    Public Declare Function NET_ESTREAM_SetSDKLocalCfg Lib "HCEHomeStream.dll" (ByVal enumType As EhomeSDK.NET_EHOME_LOCAL_CFG_TYPE, ByVal lpInBuff As IntPtr) As Boolean
    Public Declare Function NET_ESTREAM_GetSDKLocalCfg Lib "HCEHomeStream.dll" (ByVal enumType As EhomeSDK.NET_EHOME_LOCAL_CFG_TYPE, ByVal lpOutBuff As IntPtr) As Boolean
End Class
'End Namespace
