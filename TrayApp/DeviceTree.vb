﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports System.Data.SqlClient
'Imports Microsoft.VisualBasic
Imports System.Threading
Imports UltraiDMS.EHomeDemo

'Namespace EHomeDemo
'Partial
Public Class DeviceTree
    'Inherits UserControl
    Public Shared treeView1 As System.Windows.Forms.TreeView = New System.Windows.Forms.TreeView
    Public Shared g_struDeviceInfo As GlobalDefinition.LOCAL_DEVICE_INFO() = New GlobalDefinition.LOCAL_DEVICE_INFO(GlobalDefinition.MAX_DEVICES - 1) {}
    Public m_strPanelInfo As EhomeSDK.PREVIEW_IFNO() = New EhomeSDK.PREVIEW_IFNO(GlobalDefinition.MAX_DEVICES - 1) {}
    Public m_ConvertModel As ConvertModel = New ConvertModel()
    Public Shared g_deviceTree As DeviceTree = New DeviceTree()
    Public m_hCurDeviceItem As TreeNode = New TreeNode()
    Public m_hCurChanItem As TreeNode = New TreeNode()
    Public m_RootNode As TreeNode = New TreeNode("device")
    Public Const MAX_DEVICES As Integer = 1024
    'Public Shared g_formList As DeviceLogList = DeviceLogList.Instance()
    'Public Shared m_PreviewPanel As PreviewPanel() = PreviewPanel.Instance()
    Public m_CurPreviewPanel As Panel() = New Panel(3) {}
    Public m_iCurWndNum As Integer
    Public m_iCurWndIndex As Integer
    Public m_iCurDeviceIndex As Integer
    Public m_iCurChanIndex As Integer
    Public m_hWnd As IntPtr
    Public m_bPlaying As Boolean
    Public m_CurNodeArgs As TreeNodeMouseClickEventArgs
    Public Shared m_cslocalIP As String
    Public Shared m_lport As Int16
    Public Shared m_CurPanel As Panel
    Public g_bSS_Enable As Boolean = True
    Public Declare Function PostMessage Lib "User32.dll" Alias "PostMessage" (ByVal hWnd As IntPtr, ByVal Msg As UInteger, ByVal wParam As UIntPtr, ByVal lParam As UIntPtr) As Integer
    Private Sub Initg_struDeviceInfo(ByRef g_struDeviceInfo As GlobalDefinition.LOCAL_DEVICE_INFO(), ByVal length As Integer)
        For i As Integer = 0 To length - 1
            g_struDeviceInfo(i).Init()
        Next
    End Sub
    Public Sub New()
        'treeImageListInit()
        treeView1.Nodes.Add(m_RootNode)
        treeView1.Nodes(0).ImageKey = "tree"
        treeView1.Nodes(0).SelectedImageIndex = 0
        Initg_struDeviceInfo(g_struDeviceInfo, MAX_DEVICES)
        m_bPlaying = False
    End Sub
    Public Shared Function Instance() As DeviceTree
        Return g_deviceTree
    End Function
    Public Sub MessageProcess(ByVal m As Message)
        DevStatusProcess(m)
    End Sub
    Public Sub DevStatusProcess(ByRef m As Message)
        If m.Msg = EHomeForm.WM_ADD_DEV Then
            addDevice(m.LParam)
            'EHomeForm.TraceServiceText("Add Device")
        ElseIf m.Msg = EHomeForm.WM_DEL_DEV Then
            deleteDevice(m.LParam)
            'EHomeForm.TraceServiceText("deleteDevice Device")
        ElseIf m.Msg = EHomeForm.WM_CHANGE_IP Then
            IPAddrChanged(m.WParam)
            'EHomeForm.TraceServiceText("IPAddrChanged Device")
        End If
    End Sub
    'Public Sub treeImageListInit()
    '    Me.treeView1.ImageList.Images.Add("tree", PropertyCollection.tree)
    '    Me.treeView1.ImageList.Images.Add("camera", Properties.Resources.camera)
    '    Me.treeView1.ImageList.Images.Add("Alarm", Properties.Resources.Alarm)
    '    Me.treeView1.ImageList.Images.Add("audio", Properties.Resources.audio)
    '    Me.treeView1.ImageList.Images.Add("bitmap_p", Properties.Resources.bitmap_p)
    '    Me.treeView1.ImageList.Images.Add("dev_alarm", Properties.Resources.dev_alarm)
    '    Me.treeView1.ImageList.Images.Add("fortify", Properties.Resources.fortify)
    '    Me.treeView1.ImageList.Images.Add("fortify_alarm", Properties.Resources.fortify_alarm)
    '    Me.treeView1.ImageList.Images.Add("IPChan", Properties.Resources.IPChan)
    '    Me.treeView1.ImageList.Images.Add("login", Properties.Resources.login)
    '    Me.treeView1.ImageList.Images.Add("logout", Properties.Resources.logout)
    '    Me.treeView1.ImageList.Images.Add("p_r_a", Properties.Resources.p_r_a)
    '    Me.treeView1.ImageList.Images.Add("play", Properties.Resources.play)
    '    Me.treeView1.ImageList.Images.Add("play_alarm", Properties.Resources.play_alarm)
    '    Me.treeView1.ImageList.Images.Add("playAndAlarm", Properties.Resources.playAndAlarm)
    'End Sub
    Public Sub deleteDevice(ByVal pstDevInfo As IntPtr)
        If IntPtr.Zero = pstDevInfo Then
            Return
        End If

        Dim iLoginID As Integer = CInt(Marshal.PtrToStructure(pstDevInfo, GetType(Integer)))
        HCEHomeCMS.NET_ECMS_ForceLogout(iLoginID)
        DelDev(iLoginID)
    End Sub
    Public Sub IPAddrChanged(ByVal pstDevInfo As IntPtr)
        If IntPtr.Zero = pstDevInfo Then
            Return
        End If

        Dim stDevInfo As GlobalDefinition.LOCAL_DEVICE_INFO = New GlobalDefinition.LOCAL_DEVICE_INFO()
        stDevInfo.Init()
        stDevInfo = CType(Marshal.PtrToStructure(pstDevInfo, GetType(GlobalDefinition.LOCAL_DEVICE_INFO)), GlobalDefinition.LOCAL_DEVICE_INFO)
        Dim iLoginID As Integer = stDevInfo.iLoginID

        Dim test As String = System.Text.Encoding.Default.GetString(stDevInfo.byDeviceID).Trim(vbNullChar)
        EHomeForm.TraceServiceText("IPAddrChanged " & test)

        DelDev(iLoginID)
        Return
    End Sub
    Public Sub DelDev(ByVal lLoginID As Int32)
        Dim iDeviceIndex As Integer = -1
        Dim iChanIndex As Integer = -1
        Dim i As Integer = 0
        Dim j As Integer = 0

        For i = 0 To GlobalDefinition.MAX_DEVICES - 1
            iDeviceIndex = i 'm_iCurDeviceIndex
            iChanIndex = m_iCurChanIndex

            If g_struDeviceInfo(iDeviceIndex).iDeviceIndex <> -1 Then
                If g_struDeviceInfo(iDeviceIndex).iLoginID = lLoginID Then
                    Dim iPlayWndIndex As Integer = g_struDeviceInfo(iDeviceIndex).struChanInfo(iChanIndex).iPlayWndIndex
                    If g_struDeviceInfo(iDeviceIndex).struChanInfo(iChanIndex).bPlay Then
                        'If Not m_PreviewPanel(iPlayWndIndex).StopPlay() Then
                        '    g_formList.AddLog(iDeviceIndex, GlobalDefinition.OPERATION_FAIL_T, 0, "StopPlay Failed")
                        'End If
                        g_struDeviceInfo(iDeviceIndex).struChanInfo(iChanIndex).bPlay = False
                        g_struDeviceInfo(iDeviceIndex).struChanInfo(iChanIndex).iPlayWndIndex = -1
                    End If
                    'Me.Refresh()
                    'm_CurPreviewPanel(iPlayWndIndex).BackColor = Color.Gray
                    'm_CurPreviewPanel(iPlayWndIndex).Refresh()
                    g_struDeviceInfo(iDeviceIndex).iDeviceIndex = -1
                    For j = 0 To GlobalDefinition.MAX_CHAN_NUM_DEMO - 1
                        g_struDeviceInfo(iDeviceIndex).struChanInfo(j).iDeviceIndex = -1
                        g_struDeviceInfo(iDeviceIndex).struChanInfo(j).iChanIndex = -1
                        g_struDeviceInfo(iDeviceIndex).struChanInfo(j).iChannelNO = -1
                    Next
                    If iDeviceIndex < DeviceTree.treeView1.Nodes.Item(0).Nodes.Count Then
                        Dim test As String = DeviceTree.treeView1.Nodes.Item(0).Nodes.Item(iDeviceIndex).Text.Trim(vbNullChar)
                        EHomeForm.TraceServiceText("Delete Device " & test)

                        DeviceTree.treeView1.Nodes.Item(0).Nodes.Item(iDeviceIndex).Remove()
                        DeviceTree.treeView1.Refresh()
                        Exit For
                    End If

                    'Exit For
                End If
            End If
        Next
        ''DeviceTree.treeView1.Nodes.Item(0).Nodes.Item(x)
        'For Each node As TreeNode In m_RootNode.Nodes 'original
        '    'For Each node As TreeNode In m_RootNode.Nodes.Item(0).Nodes
        '    Try
        '        If node.Index = iDeviceIndex Then
        '            'node.Remove()
        '            DeviceTree.treeView1.Nodes.Item(0).Nodes.Item(iDeviceIndex).Remove()
        '        End If
        '    Catch ex As Exception
        '        EHomeForm.TraceServiceText("Delete Device " & ex.Message)
        '    End Try
        'Next
        ''Me.Refresh()
    End Sub
    Public Shared Sub StrToByteArray(ByRef destination As Byte(), ByVal data As String)
        If data <> "" Then
            Dim source As Byte() = System.Text.Encoding.[Default].GetBytes(data)

            If source.Length > destination.Length Then
                'MessageBox.Show("The length of num is exceeding")
                EHomeForm.TraceService("The length of num is exceeding", "Device Tree Line 162")
            Else

                For i As Integer = 0 To source.Length - 1
                    destination(i) = source(i)
                Next
            End If
        End If
    End Sub
    Public Shared Function ByteToStr(ByRef source As Byte(), ByVal length As Integer) As String
        Dim temp As String = Nothing
        Dim len As Integer = 0

        For i As Integer = 0 To length - 1

            If source(i) <> 0 Then
                len += 1
            Else
                Exit For
            End If
        Next

        Dim res As Byte() = New Byte(len - 1) {}
        Array.Copy(source, 0, res, 0, len)
        temp = System.Text.Encoding.[Default].GetString(res)
        Return temp
    End Function
    Private Function iSByteArrayEmpty(ByRef array As Byte(), ByVal length As Integer) As Boolean
        Dim zeronumber As Integer = 0

        For i As Integer = 0 To length - 1

            If 0 = array(i) Then
                zeronumber += 1
            End If
        Next

        If length = zeronumber Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Shared Function isByteEqual(ByRef source As Byte(), ByRef dst As Byte(), ByVal length As Integer) As Boolean
        If length > source.Length OrElse length > dst.Length Then
            Return False
        End If

        Dim flag As Boolean = True

        For i As Integer = 0 To length - 1

            If source(i) <> dst(i) Then
                flag = False
                Exit For
            End If
        Next

        Return flag
    End Function
    'Public Sub addDevice(ByVal pstDevInfo As IntPtr)
    '    Dim i As Integer = 0
    '    Dim j As Integer = 0
    '    Dim stDevInfo As New GlobalDefinition.LOCAL_DEVICE_INFO()
    '    stDevInfo.Init()
    '    stDevInfo = CType(Marshal.PtrToStructure(pstDevInfo, GetType(GlobalDefinition.LOCAL_DEVICE_INFO)), GlobalDefinition.LOCAL_DEVICE_INFO)
    '    Dim deviceNode As New TreeNode()
    '    Dim channelNode As New TreeNode() '通道节点
    '    Dim strTemp As String = Nothing

    '    For i = 0 To GlobalDefinition.MAX_DEVICES - 1
    '        If g_struDeviceInfo(i).iDeviceIndex = -1 OrElse isByteEqual(stDevInfo.byDeviceID, g_struDeviceInfo(i).byDeviceID, 256) Then
    '            '这一步走的加密流程EhomeKeyC++中传的都是NULL，所以不判断
    '            Dim isEhomekeyempty As Boolean = iSByteArrayEmpty(g_struDeviceInfo(i).byEhomeKey, 32)
    '            If False = isEhomekeyempty Then
    '                g_struDeviceInfo(i).byEhomeKey.CopyTo(stDevInfo.byEhomeKey, 0)
    '            End If

    '            Dim isSessionKeyempty As Boolean = iSByteArrayEmpty(g_struDeviceInfo(i).bySessionKey, 16)
    '            If False = isSessionKeyempty Then
    '                g_struDeviceInfo(i).bySessionKey.CopyTo(stDevInfo.bySessionKey, 0)
    '            End If
    '            If g_bSS_Enable Then
    '                Dim ptrDeviceID As IntPtr = Marshal.AllocHGlobal(256)
    '                Marshal.Copy(stDevInfo.byDeviceID, 0, ptrDeviceID, 256)

    '                Dim ptrbyEhomeKey As IntPtr = Marshal.AllocHGlobal(32)
    '                Marshal.Copy(stDevInfo.byEhomeKey, 0, ptrbyEhomeKey, 32)

    '                Dim ClouldSecretKey(255) As Byte
    '                If HCEHomeSS.NET_ESS_HAMSHA256(ptrDeviceID, ptrbyEhomeKey, ClouldSecretKey, 255) Then
    '                    'Marshal.Copy(ptrClouldSecretKey,  stDevInfo.byClouldSecretKey,0, 64);
    '                    Array.Copy(ClouldSecretKey, stDevInfo.byClouldSecretKey, 64)
    '                End If
    '                Marshal.FreeHGlobal(ptrDeviceID)
    '                Marshal.FreeHGlobal(ptrbyEhomeKey)
    '                'Marshal.FreeHGlobal(ptrClouldSecretKey);
    '            End If

    '            'm_iCurDeviceIndex = i;
    '            Dim tempindex As Integer = g_struDeviceInfo(i).iDeviceIndex
    '            g_struDeviceInfo(i) = stDevInfo '注册成功, 拷贝设备信息
    '            g_struDeviceInfo(i).iDeviceIndex = tempindex

    '            Dim strDevID As String = Nothing
    '            If stDevInfo.dwVersion >= 4 Then
    '                If g_struDeviceInfo(i).iDeviceIndex < 0 Then
    '                    If False = iSByteArrayEmpty(g_struDeviceInfo(i).sDeviceSerial, 12) Then
    '                        strDevID = System.Text.Encoding.Default.GetString(g_struDeviceInfo(i).sDeviceSerial)
    '                        deviceNode = New TreeNode(strDevID)
    '                        'deviceNode.ImageKey = "login"
    '                        'deviceNode.SelectedImageIndex = 9
    '                        deviceNode.Text = strDevID
    '                        m_RootNode.Nodes.Add(deviceNode)
    '                    Else
    '                        If 5 = stDevInfo.dwVersion AndAlso stDevInfo.iLoginID < 0 Then
    '                            Dim strName As String = Nothing
    '                            strName = ByteToStr(g_struDeviceInfo(i).byDeviceID, 256) & "(offline)"
    '                            deviceNode = New TreeNode(strName)
    '                            'deviceNode.ImageKey = "login"
    '                            'deviceNode.SelectedImageIndex = 9
    '                            deviceNode.Text = strName
    '                            m_RootNode.Nodes.Add(deviceNode)
    '                        Else
    '                            'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 3, "version is larger than four, serial is null，show DeviceID!")
    '                            Dim strName As String = Nothing
    '                            strName = ByteToStr(g_struDeviceInfo(i).byDeviceID, 256)
    '                            deviceNode = New TreeNode(strName)
    '                            'deviceNode.ImageKey = "login"
    '                            'deviceNode.SelectedImageIndex = 9
    '                            deviceNode.Text = strName
    '                            m_RootNode.Nodes.Add(deviceNode)
    '                        End If
    '                    End If
    '                Else '给5.0设备未上线的上线   Go online for 5.0 devices that are not online
    '                    Dim flag As Boolean = False
    '                    For Each node As TreeNode In m_RootNode.Nodes
    '                        If node.Text.Contains("offline") Then
    '                            flag = True
    '                            If False = iSByteArrayEmpty(g_struDeviceInfo(i).sDeviceSerial, 12) Then
    '                                node.Text = ByteToStr(g_struDeviceInfo(i).sDeviceSerial, 12)
    '                                deviceNode = node
    '                            Else
    '                                node.Text = ByteToStr(g_struDeviceInfo(i).byDeviceID, 256)
    '                                deviceNode = node
    '                            End If
    '                            Exit For
    '                        End If
    '                    Next node
    '                    If False = flag Then
    '                        'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 3, "GET TREE ITEM FAIL")
    '                        Return
    '                    End If
    '                End If
    '            Else
    '                strDevID = System.Text.Encoding.Default.GetString(g_struDeviceInfo(i).byDeviceID)
    '                deviceNode = New TreeNode(strDevID)
    '                deviceNode.ImageKey = "login"
    '                deviceNode.SelectedImageIndex = 9
    '                deviceNode.Text = strDevID
    '                m_RootNode.Nodes.Add(deviceNode)
    '            End If

    '            '获取设备信息
    '            g_struDeviceInfo(i).iDeviceIndex = i
    '            If stDevInfo.iLoginID >= 0 Then
    '                Dim struDevInfo As New HCEHomeCMS.NET_EHOME_DEVICE_INFO()
    '                Dim struCfg As New HCEHomeCMS.NET_EHOME_CONFIG()
    '                struDevInfo.sSerialNumber = New Byte(HCEHomeCMS.MAX_SERIALNO_LEN - 1) {}
    '                struDevInfo.sSIMCardSN = New Byte(HCEHomeCMS.MAX_SERIALNO_LEN - 1) {}
    '                struDevInfo.sSIMCardPhoneNum = New Byte(HCEHomeCMS.MAX_PHOMENUM_LEN - 1) {}
    '                struDevInfo.byRes = New Byte(159) {}
    '                struDevInfo.dwSize = Marshal.SizeOf(struDevInfo)

    '                Dim ptrDevInfo As IntPtr = Marshal.AllocHGlobal(struDevInfo.dwSize)
    '                Marshal.StructureToPtr(struDevInfo, ptrDevInfo, False)

    '                struCfg.pOutBuf = ptrDevInfo
    '                struCfg.byRes = New Byte(39) {}
    '                struCfg.dwOutSize = CUInt(Math.Truncate(struDevInfo.dwSize))
    '                Dim dwConfigSize As UInteger = CUInt(Math.Truncate(Marshal.SizeOf(struCfg)))
    '                Dim ptrCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struCfg))
    '                If Not HCEHomeCMS.NET_ECMS_GetDevConfig(g_struDeviceInfo(i).iLoginID, EHomeExternalCommand.NET_EHOME_GET_DEVICE_INFO, struCfg, dwConfigSize) Then
    '                    Dim str As String = String.Format("{0}", CInt(Math.Truncate(HCEHomeCMS.NET_ECMS_GetLastError())))
    '                    Console.WriteLine("NET_ECMS_GetDevConfig failed, err={0}", CInt(Math.Truncate(HCEHomeCMS.NET_ECMS_GetLastError())))

    '                Else
    '                    struDevInfo = CType(Marshal.PtrToStructure(ptrDevInfo, GetType(HCEHomeCMS.NET_EHOME_DEVICE_INFO)), HCEHomeCMS.NET_EHOME_DEVICE_INFO)

    '                    '获取UTF8格式数据
    '                    Dim strDevInfo As String
    '                    strDevInfo = System.Text.Encoding.UTF8.GetString(struDevInfo.sSerialNumber)
    '                    Dim strDevInfoLen As Integer = strDevInfo.IndexOf(ControlChars.NullChar)
    '                    Dim strSIMCardSN As String = System.Text.Encoding.UTF8.GetString(struDevInfo.sSIMCardSN)
    '                    Dim strSIMCardSNLen As Integer = strSIMCardSN.IndexOf(ControlChars.NullChar)
    '                    Dim strSIMCardPhoneNum As String = System.Text.Encoding.UTF8.GetString(struDevInfo.sSIMCardPhoneNum)
    '                    Dim strSIMCardPhoneNumLen As Integer = strDevInfo.IndexOf(ControlChars.NullChar)

    '                    strTemp = String.Format("NET_EHOME_GET_DEVICE_INFO sSerialNumber= {0}", strDevInfo)
    '                    'g_formList.AddLog(g_struDeviceInfo(i).iDeviceIndex, GlobalDefinition.OPERATION_SUCC_T, 1, strTemp)
    '                    strTemp = String.Format("NET_EHOME_GET_DEVICE_INFO sSIMCardSN={0}", strSIMCardSN)
    '                    'g_formList.AddLog(g_struDeviceInfo(i).iDeviceIndex, GlobalDefinition.OPERATION_SUCC_T, 1, strTemp)
    '                    strTemp = String.Format("NET_EHOME_GET_DEVICE_INFO sSIMCardPhoneNum={0}", strSIMCardPhoneNum)
    '                    'g_formList.AddLog(g_struDeviceInfo(i).iDeviceIndex, GlobalDefinition.OPERATION_SUCC_T, 1, strTemp)
    '                End If
    '                Marshal.FreeHGlobal(ptrDevInfo)
    '                Marshal.FreeHGlobal(ptrCfg)

    '                g_struDeviceInfo(i).dwAlarmInNum = struDevInfo.dwAlarmInPortNum
    '                g_struDeviceInfo(i).dwAlarmOutNum = struDevInfo.dwAlarmOutPortNum
    '                g_struDeviceInfo(i).dwAnalogChanNum = struDevInfo.dwChannelNumber
    '                g_struDeviceInfo(i).dwDeviceChanNum = struDevInfo.dwChannelAmount
    '                g_struDeviceInfo(i).dwIPChanNum = struDevInfo.dwChannelAmount - struDevInfo.dwChannelNumber
    '                g_struDeviceInfo(i).dwAudioNum = struDevInfo.dwAudioChanNum

    '                '模拟通道
    '                For j = 0 To struDevInfo.dwChannelNumber - 1
    '                    '由于目前的通道无法判断是否可用，因此都当做可用
    '                    g_struDeviceInfo(i).struChanInfo(j).bEnable = True
    '                    '这是模拟通道
    '                    g_struDeviceInfo(i).struChanInfo(j).iChannelNO = j + 1
    '                    g_struDeviceInfo(i).struChanInfo(j).iChanType = HCEHomeCMS.DEMO_CHANNEL_TYPE_ANALOG
    '                    strTemp = String.Format("Camera{0}", g_struDeviceInfo(i).struChanInfo(j).iChannelNO)
    '                    channelNode = New TreeNode(strTemp)
    '                    channelNode.ImageKey = "camera"
    '                    channelNode.SelectedImageIndex = 1
    '                    deviceNode.Nodes.Add(channelNode)
    '                    g_struDeviceInfo(i).struChanInfo(j).iDeviceIndex = i
    '                    g_struDeviceInfo(i).struChanInfo(j).iChanIndex = j
    '                    'm_treeDeviceList.SetItemData(hChannel, CHANNELTYPE * 1000 + g_struDeviceInfo[i].struChanInfo[j].iChanIndex);
    '                Next j

    '                '然后添加IP通道
    '                Do While j < struDevInfo.dwChannelAmount
    '                    '由于目前的通道无法判断是否可用，因此都当做可用
    '                    g_struDeviceInfo(i).struChanInfo(j).bEnable = True
    '                    '这是IP通道
    '                    g_struDeviceInfo(i).struChanInfo(j).iChannelNO = CInt(Math.Truncate(struDevInfo.dwStartChannel + (j - struDevInfo.dwChannelNumber)))
    '                    g_struDeviceInfo(i).struChanInfo(j).iChanType = GlobalDefinition.DEMO_CHANNEL_TYPE.DEMO_CHANNEL_TYPE_IP
    '                    strTemp = String.Format("IPCamera{0}", g_struDeviceInfo(i).struChanInfo(j).iChannelNO)
    '                    channelNode = New TreeNode(strTemp)
    '                    channelNode.ImageKey = "camera"
    '                    channelNode.SelectedImageIndex = 1
    '                    deviceNode.Nodes.Add(channelNode)
    '                    g_struDeviceInfo(i).struChanInfo(j).iDeviceIndex = i
    '                    g_struDeviceInfo(i).struChanInfo(j).iChanIndex = j
    '                    'm_treeDeviceList.SetItemData(hChannel, CHANNELTYPE * 1000 + g_struDeviceInfo[i].struChanInfo[j].iChanIndex);
    '                    j += 1
    '                Loop

    '                '零通道处理逻辑
    '                g_struDeviceInfo(i).dwZeroChanNum = struDevInfo.dwSupportZeroChan ' SupportZeroChan:支持零通道的个数：0-不支持，1-支持1路，2-支持2路，以此类推
    '                g_struDeviceInfo(i).dwZeroChanStart = struDevInfo.dwStartZeroChan ' 零通道起始编号，默认10000
    '                Dim k As Integer = 0
    '                Do While k < CInt(Math.Truncate(struDevInfo.dwSupportZeroChan))
    '                    Dim nZeroChannelIndex As Integer = j + k
    '                    g_struDeviceInfo(i).struChanInfo(nZeroChannelIndex).bEnable = True

    '                    '这是零通道
    '                    g_struDeviceInfo(i).struChanInfo(nZeroChannelIndex).iChannelNO = CInt(Math.Truncate(struDevInfo.dwStartZeroChan + k))
    '                    g_struDeviceInfo(i).struChanInfo(nZeroChannelIndex).iChanType = GlobalDefinition.DEMO_CHANNEL_TYPE.DEMO_CHANNEL_TYPE_ZERO
    '                    strTemp = String.Format("ZeroChannel{0}", g_struDeviceInfo(i).struChanInfo(j).iChannelNO)
    '                    channelNode = New TreeNode(strTemp)
    '                    channelNode.ImageKey = "camera"
    '                    channelNode.SelectedImageIndex = 1
    '                    deviceNode.Nodes.Add(channelNode)
    '                    g_struDeviceInfo(i).struChanInfo(nZeroChannelIndex).iDeviceIndex = i
    '                    g_struDeviceInfo(i).struChanInfo(nZeroChannelIndex).iChanIndex = nZeroChannelIndex
    '                    'm_treeDeviceList.SetItemData(hChannel, CHANNELTYPE * 1000 + g_struDeviceInfo[i].struChanInfo[nZeroChannelIndex].iChanIndex);
    '                    k += 1
    '                Loop

    '                For j = 0 To GlobalDefinition.MAX_CHAN_NUM_DEMO - 1
    '                    Dim strIP As String = m_cslocalIP
    '                    Dim ip As System.Net.IPAddress = System.Net.IPAddress.Parse(strIP)
    '                    System.Net.IPAddress.NetworkToHostOrder(ip.Address)
    '                    strIP = ip.ToString()
    '                    strIP.CopyTo(0, g_struDeviceInfo(i).struChanInfo(j).struIP.szIP, 0, strIP.Length)
    '                Next j
    '                'm_PreviewPanel(i).g_struDeviceInfo(i) = g_struDeviceInfo(i)
    '            End If
    '            treeView1.ExpandAll()
    '            Exit For
    '        End If

    '    Next i
    'End Sub

    Public Sub addDevice(ByVal pstDevInfo As IntPtr)
        Dim i As Integer = 0
        Dim j As Integer = 0

        Dim stDevInfo As New GlobalDefinition.LOCAL_DEVICE_INFO()
        stDevInfo.Init()
        stDevInfo = DirectCast(Marshal.PtrToStructure(pstDevInfo, GetType(GlobalDefinition.LOCAL_DEVICE_INFO)), GlobalDefinition.LOCAL_DEVICE_INFO)

        Dim deviceNode As New TreeNode()
        '设备根节点的通道节点
        Dim channelNode As New TreeNode() '通道节点
        Dim strTemp As String = Nothing

        Dim test As String = System.Text.Encoding.Default.GetString(stDevInfo.byDeviceID).Trim(vbNullChar)
        EHomeForm.TraceServiceText("Add Device " & test)

        For i = 0 To GlobalDefinition.MAX_DEVICES - 1
            If g_struDeviceInfo(i).iDeviceIndex = -1 OrElse isByteEqual(stDevInfo.byDeviceID, g_struDeviceInfo(i).byDeviceID, 256) Then
                '这一步走的加密流程EhomeKeyC++中传的都是NULL，所以不判断
                Dim isEhomekeyempty As Boolean = iSByteArrayEmpty(g_struDeviceInfo(i).byEhomeKey, 32)
                If False = isEhomekeyempty Then
                    g_struDeviceInfo(i).byEhomeKey.CopyTo(stDevInfo.byEhomeKey, 0)
                End If

                Dim isSessionKeyempty As Boolean = iSByteArrayEmpty(g_struDeviceInfo(i).bySessionKey, 16)
                If False = isSessionKeyempty Then
                    g_struDeviceInfo(i).bySessionKey.CopyTo(stDevInfo.bySessionKey, 0)
                End If
                If g_bSS_Enable Then
                    Dim ptrDeviceID As IntPtr = Marshal.AllocHGlobal(256)
                    Marshal.Copy(stDevInfo.byDeviceID, 0, ptrDeviceID, 256)

                    Dim ptrbyEhomeKey As IntPtr = Marshal.AllocHGlobal(32)
                    Marshal.Copy(stDevInfo.byEhomeKey, 0, ptrbyEhomeKey, 32)

                    Dim ClouldSecretKey(255) As Byte
                    If HCEHomeSS.NET_ESS_HAMSHA256(ptrDeviceID, ptrbyEhomeKey, ClouldSecretKey, 255) Then
                        'Marshal.Copy(ptrClouldSecretKey,  stDevInfo.byClouldSecretKey,0, 64);
                        Array.Copy(ClouldSecretKey, stDevInfo.byClouldSecretKey, 64)
                    End If
                    Marshal.FreeHGlobal(ptrDeviceID)
                    Marshal.FreeHGlobal(ptrbyEhomeKey)
                    'Marshal.FreeHGlobal(ptrClouldSecretKey);
                End If

                'm_iCurDeviceIndex = i;
                Dim tempindex As Integer = g_struDeviceInfo(i).iDeviceIndex
                g_struDeviceInfo(i) = stDevInfo '注册成功, 拷贝设备信息
                g_struDeviceInfo(i).iDeviceIndex = tempindex

                Dim strDevID As String = Nothing
                If stDevInfo.dwVersion >= 4 Then
                    If g_struDeviceInfo(i).iDeviceIndex < 0 Then
                        If False = iSByteArrayEmpty(g_struDeviceInfo(i).sDeviceSerial, 12) Then
                            strDevID = System.Text.Encoding.Default.GetString(g_struDeviceInfo(i).sDeviceSerial)
                            deviceNode = New TreeNode(strDevID)
                            'deviceNode.ImageKey = "login"
                            'deviceNode.SelectedImageIndex = 9
                            deviceNode.Text = strDevID
                            m_RootNode.Nodes.Add(deviceNode)


                        Else
                            If 5 = stDevInfo.dwVersion AndAlso stDevInfo.iLoginID < 0 Then
                                Dim strName As String = Nothing
                                strName = ByteToStr(g_struDeviceInfo(i).byDeviceID, 256) & "(offline)"
                                deviceNode = New TreeNode(strName)
                                'deviceNode.ImageKey = "login"
                                'deviceNode.SelectedImageIndex = 9
                                deviceNode.Text = strName
                                m_RootNode.Nodes.Add(deviceNode)


                                setEhomeKey(i)
                            Else
                                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 3, "version is larger than four, serial is null，show DeviceID!")
                                Dim strName As String = Nothing
                                strName = ByteToStr(g_struDeviceInfo(i).byDeviceID, 256)
                                deviceNode = New TreeNode(strName)
                                'deviceNode.ImageKey = "login"
                                'deviceNode.SelectedImageIndex = 9
                                deviceNode.Text = strName
                                m_RootNode.Nodes.Add(deviceNode)

                            End If
                        End If
                    Else '给5.0设备未上线的上线
                        Dim flag As Boolean = False
                        For Each node As TreeNode In m_RootNode.Nodes
                            If node.Text.Contains("offline") Then
                                flag = True
                                If False = iSByteArrayEmpty(g_struDeviceInfo(i).sDeviceSerial, 12) Then
                                    node.Text = ByteToStr(g_struDeviceInfo(i).sDeviceSerial, 12)
                                    deviceNode = node
                                Else
                                    node.Text = ByteToStr(g_struDeviceInfo(i).byDeviceID, 256)
                                    deviceNode = node
                                End If
                                Exit For
                            End If
                        Next node
                        If False = flag Then
                            'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 3, "GET TREE ITEM FAIL")
                            Return
                        End If
                    End If
                Else
                    strDevID = System.Text.Encoding.Default.GetString(g_struDeviceInfo(i).byDeviceID)
                    deviceNode = New TreeNode(strDevID)
                    'deviceNode.ImageKey = "login"
                    'deviceNode.SelectedImageIndex = 9
                    deviceNode.Text = strDevID
                    m_RootNode.Nodes.Add(deviceNode)
                End If


                '    if (g_struDeviceInfo[i].sDeviceSerial.Length > 0)
                '    {
                '        strDevID = System.Text.Encoding.Default.GetString(g_struDeviceInfo[i].sDeviceSerial);
                '        if (g_struDeviceInfo[i].sDeviceSerial[0] == 0)
                '        {
                '            strDevID = System.Text.Encoding.Default.GetString(g_struDeviceInfo[i].byDeviceID);
                '        }
                '        deviceNode = new TreeNode(strDevID);
                '        deviceNode.ImageKey = "login";
                '        deviceNode.SelectedImageIndex = 9;
                '        deviceNode.Name = strDevID;
                '        m_RootNode.Nodes.Add(deviceNode);
                '    }
                '    else
                '    {
                '        strDevID = System.Text.Encoding.Default.GetString(g_struDeviceInfo[i].byDeviceID);
                '        deviceNode = new TreeNode(strDevID);
                '        deviceNode.Name = strDevID;
                '        m_RootNode.Nodes.Add(deviceNode);
                '    }
                '}
                'else
                '{
                '    strDevID = System.Text.Encoding.Default.GetString(g_struDeviceInfo[i].byDeviceID);
                '    deviceNode = new TreeNode(strDevID);
                '    deviceNode.Name = strDevID;
                '    m_RootNode.Nodes.Add(deviceNode);
                '}

                '获取设备信息
                g_struDeviceInfo(i).iDeviceIndex = i
                If stDevInfo.iLoginID >= 0 Then
                    Dim struDevInfo As New HCEHomeCMS.NET_EHOME_DEVICE_INFO()
                    Dim struCfg As New HCEHomeCMS.NET_EHOME_CONFIG()
                    struDevInfo.sSerialNumber = New Byte(HCEHomeCMS.MAX_SERIALNO_LEN - 1) {}
                    struDevInfo.sSIMCardSN = New Byte(HCEHomeCMS.MAX_SERIALNO_LEN - 1) {}
                    struDevInfo.sSIMCardPhoneNum = New Byte(HCEHomeCMS.MAX_PHOMENUM_LEN - 1) {}
                    struDevInfo.byRes = New Byte(159) {}
                    struDevInfo.dwSize = Marshal.SizeOf(struDevInfo)

                    Dim ptrDevInfo As IntPtr = Marshal.AllocHGlobal(struDevInfo.dwSize)
                    Marshal.StructureToPtr(struDevInfo, ptrDevInfo, False)

                    struCfg.pOutBuf = ptrDevInfo
                    struCfg.byRes = New Byte(39) {}
                    struCfg.dwOutSize = CUInt(struDevInfo.dwSize)
                    Dim dwConfigSize As UInteger = CUInt(Marshal.SizeOf(struCfg))
                    Dim ptrCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struCfg))
                    'If Not HCEHomeCMS.NET_ECMS_GetDevConfig(g_struDeviceInfo(i).iLoginID, EHomeExternalCommand.NET_EHOME_GET_DEVICE_INFO, struCfg, dwConfigSize) Then
                    '    Dim str As String = String.Format("{0}", CInt(Math.Truncate(HCEHomeCMS.NET_ECMS_GetLastError())))
                    '    Console.WriteLine("NET_ECMS_GetDevConfig failed, err={0}", CInt(Math.Truncate(HCEHomeCMS.NET_ECMS_GetLastError())))

                    'Else
                    '    struDevInfo = DirectCast(Marshal.PtrToStructure(ptrDevInfo, GetType(HCEHomeCMS.NET_EHOME_DEVICE_INFO)), HCEHomeCMS.NET_EHOME_DEVICE_INFO)

                    '    '获取UTF8格式数据
                    '    Dim strDevInfo As String
                    '    strDevInfo = System.Text.Encoding.UTF8.GetString(struDevInfo.sSerialNumber)
                    '    Dim strDevInfoLen As Integer = strDevInfo.IndexOf(ControlChars.NullChar)
                    '    Dim strSIMCardSN As String = System.Text.Encoding.UTF8.GetString(struDevInfo.sSIMCardSN)
                    '    Dim strSIMCardSNLen As Integer = strSIMCardSN.IndexOf(ControlChars.NullChar)
                    '    Dim strSIMCardPhoneNum As String = System.Text.Encoding.UTF8.GetString(struDevInfo.sSIMCardPhoneNum)
                    '    Dim strSIMCardPhoneNumLen As Integer = strDevInfo.IndexOf(ControlChars.NullChar)

                    '    'strTemp = String.Format("NET_EHOME_GET_DEVICE_INFO sSerialNumber= {0}", strDevInfo)
                    '    'g_formList.AddLog(g_struDeviceInfo(i).iDeviceIndex, GlobalDefinition.OPERATION_SUCC_T, 1, strTemp)
                    '    'strTemp = String.Format("NET_EHOME_GET_DEVICE_INFO sSIMCardSN={0}", strSIMCardSN)
                    '    'g_formList.AddLog(g_struDeviceInfo(i).iDeviceIndex, GlobalDefinition.OPERATION_SUCC_T, 1, strTemp)
                    '    'strTemp = String.Format("NET_EHOME_GET_DEVICE_INFO sSIMCardPhoneNum={0}", strSIMCardPhoneNum)
                    '    'g_formList.AddLog(g_struDeviceInfo(i).iDeviceIndex, GlobalDefinition.OPERATION_SUCC_T, 1, strTemp)
                    'End If
                    Marshal.FreeHGlobal(ptrDevInfo)
                    Marshal.FreeHGlobal(ptrCfg)

                    g_struDeviceInfo(i).dwAlarmInNum = struDevInfo.dwAlarmInPortNum
                    g_struDeviceInfo(i).dwAlarmOutNum = struDevInfo.dwAlarmOutPortNum
                    g_struDeviceInfo(i).dwAnalogChanNum = struDevInfo.dwChannelNumber
                    g_struDeviceInfo(i).dwDeviceChanNum = struDevInfo.dwChannelAmount
                    g_struDeviceInfo(i).dwIPChanNum = CInt(struDevInfo.dwChannelAmount) - struDevInfo.dwChannelNumber
                    g_struDeviceInfo(i).dwAudioNum = struDevInfo.dwAudioChanNum

                    '模拟通道
                    For j = 0 To struDevInfo.dwChannelNumber - 1
                        '由于目前的通道无法判断是否可用，因此都当做可用
                        g_struDeviceInfo(i).struChanInfo(j).bEnable = True
                        '这是模拟通道
                        g_struDeviceInfo(i).struChanInfo(j).iChannelNO = j + 1
                        g_struDeviceInfo(i).struChanInfo(j).iChanType = HCEHomeCMS.DEMO_CHANNEL_TYPE_ANALOG
                        strTemp = String.Format("Camera{0}", g_struDeviceInfo(i).struChanInfo(j).iChannelNO)
                        channelNode = New TreeNode(strTemp)
                        'channelNode.ImageKey = "camera"
                        'channelNode.SelectedImageIndex = 1
                        deviceNode.Nodes.Add(channelNode)
                        g_struDeviceInfo(i).struChanInfo(j).iDeviceIndex = i
                        g_struDeviceInfo(i).struChanInfo(j).iChanIndex = j
                        'm_treeDeviceList.SetItemData(hChannel, CHANNELTYPE * 1000 + g_struDeviceInfo[i].struChanInfo[j].iChanIndex);
                    Next j

                    '然后添加IP通道
                    Do While j < struDevInfo.dwChannelAmount
                        '由于目前的通道无法判断是否可用，因此都当做可用
                        g_struDeviceInfo(i).struChanInfo(j).bEnable = True
                        '这是IP通道
                        g_struDeviceInfo(i).struChanInfo(j).iChannelNO = CInt(struDevInfo.dwStartChannel + (j - struDevInfo.dwChannelNumber))
                        g_struDeviceInfo(i).struChanInfo(j).iChanType = GlobalDefinition.DEMO_CHANNEL_TYPE.DEMO_CHANNEL_TYPE_IP
                        strTemp = String.Format("IPCamera{0}", g_struDeviceInfo(i).struChanInfo(j).iChannelNO)
                        channelNode = New TreeNode(strTemp)
                        'channelNode.ImageKey = "camera"
                        'channelNode.SelectedImageIndex = 1
                        deviceNode.Nodes.Add(channelNode)
                        g_struDeviceInfo(i).struChanInfo(j).iDeviceIndex = i
                        g_struDeviceInfo(i).struChanInfo(j).iChanIndex = j
                        'm_treeDeviceList.SetItemData(hChannel, CHANNELTYPE * 1000 + g_struDeviceInfo[i].struChanInfo[j].iChanIndex);
                        j += 1
                    Loop

                    '零通道处理逻辑
                    g_struDeviceInfo(i).dwZeroChanNum = struDevInfo.dwSupportZeroChan ' SupportZeroChan:支持零通道的个数：0-不支持，1-支持1路，2-支持2路，以此类推
                    g_struDeviceInfo(i).dwZeroChanStart = struDevInfo.dwStartZeroChan ' 零通道起始编号，默认10000
                    Dim k As Integer = 0
                    Do While k < CInt(struDevInfo.dwSupportZeroChan)
                        Dim nZeroChannelIndex As Integer = j + k
                        g_struDeviceInfo(i).struChanInfo(nZeroChannelIndex).bEnable = True

                        '这是零通道
                        g_struDeviceInfo(i).struChanInfo(nZeroChannelIndex).iChannelNO = CInt(struDevInfo.dwStartZeroChan + k)
                        g_struDeviceInfo(i).struChanInfo(nZeroChannelIndex).iChanType = GlobalDefinition.DEMO_CHANNEL_TYPE.DEMO_CHANNEL_TYPE_ZERO
                        strTemp = String.Format("ZeroChannel{0}", g_struDeviceInfo(i).struChanInfo(j).iChannelNO)
                        channelNode = New TreeNode(strTemp)
                        'channelNode.ImageKey = "camera"
                        'channelNode.SelectedImageIndex = 1
                        deviceNode.Nodes.Add(channelNode)
                        g_struDeviceInfo(i).struChanInfo(nZeroChannelIndex).iDeviceIndex = i
                        g_struDeviceInfo(i).struChanInfo(nZeroChannelIndex).iChanIndex = nZeroChannelIndex
                        'm_treeDeviceList.SetItemData(hChannel, CHANNELTYPE * 1000 + g_struDeviceInfo[i].struChanInfo[nZeroChannelIndex].iChanIndex);
                        k += 1
                    Loop

                    For j = 0 To GlobalDefinition.MAX_CHAN_NUM_DEMO - 1
                        Dim strIP As String = m_cslocalIP
                        Dim ip As System.Net.IPAddress = System.Net.IPAddress.Parse(strIP)
                        System.Net.IPAddress.NetworkToHostOrder(ip.Address)
                        strIP = ip.ToString()
                        strIP.CopyTo(0, g_struDeviceInfo(i).struChanInfo(j).struIP.szIP, 0, strIP.Length)
                    Next j
                    'm_PreviewPanel(i).g_struDeviceInfo(i) = g_struDeviceInfo(i)

                End If
                treeView1.ExpandAll()
                Exit For
            End If

        Next i
    End Sub    
    Private Sub treeView1_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs)
        m_CurNodeArgs = e
        If 1 = e.Node.Level Then
            m_iCurDeviceIndex = e.Node.Index
            m_iCurChanIndex = 0
            g_struDeviceInfo(m_iCurDeviceIndex).struChanInfo(m_iCurChanIndex).iPlayWndIndex = m_iCurWndIndex
            'PlayChan(m_iCurDeviceIndex, m_iCurChanIndex, m_CurNodeArgs)
        ElseIf 2 = e.Node.Level Then
            m_iCurDeviceIndex = e.Node.Parent.Index
            m_iCurChanIndex = e.Node.Index
            'PlayChan(m_iCurDeviceIndex, m_iCurChanIndex, m_CurNodeArgs)
        End If
        'Me.Refresh()
    End Sub
    'Public Sub PlayChan(ByVal iDeviceIndex As Integer, ByVal iChanIndex As Integer, ByVal e As TreeNodeMouseClickEventArgs)
    '    If iDeviceIndex < 0 Then
    '        g_formList.AddLog(iDeviceIndex, GlobalDefinition.OPERATION_FAIL_T, 0, "DeviceTree.PlayChan iDeviceIndex < 0")
    '        Return
    '    End If

    '    Dim iPlayWndIndex As Integer = g_struDeviceInfo(iDeviceIndex).struChanInfo(iChanIndex).iPlayWndIndex

    '    If g_struDeviceInfo(iDeviceIndex).struChanInfo(iChanIndex).bPlay Then

    '        If Not m_PreviewPanel(iPlayWndIndex).StopPlay() Then
    '            g_formList.AddLog(iDeviceIndex, GlobalDefinition.OPERATION_FAIL_T, 0, "StopPlay Failed")
    '        End If

    '        g_struDeviceInfo(iDeviceIndex).struChanInfo(iChanIndex).bPlay = False
    '        g_struDeviceInfo(iDeviceIndex).struChanInfo(iChanIndex).iPlayWndIndex = -1
    '        e.Node.ImageKey = "camera"
    '        e.Node.SelectedImageIndex = 1
    '        m_CurPreviewPanel(iPlayWndIndex).BackColor = Color.Gray
    '        m_CurPreviewPanel(iPlayWndIndex).Refresh()
    '    Else
    '        g_struDeviceInfo(m_iCurDeviceIndex).struChanInfo(m_iCurChanIndex).iPlayWndIndex = m_iCurWndIndex

    '        If Not m_PreviewPanel(m_iCurWndIndex).StartPlay(iDeviceIndex, iChanIndex) Then
    '            g_formList.AddLog(iDeviceIndex, GlobalDefinition.OPERATION_FAIL_T, 0, "StartPlay Failed")
    '            Return
    '        End If

    '        g_struDeviceInfo(iDeviceIndex).struChanInfo(iChanIndex).bPlay = True
    '        e.Node.ImageKey = "play"
    '        e.Node.SelectedImageIndex = 12
    '    End If
    'End Sub
    'Public Sub PlayDevice(ByVal iDeviceIndex As Integer, ByVal iChannelIndex As Integer)
    '    If m_bPlaying Then

    '        If Not m_PreviewPanel(m_iCurWndIndex).StopPlay() Then
    '            g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 1, "StopPlay Failed")
    '            Return
    '        Else
    '            m_bPlaying = False
    '            Return
    '        End If
    '    End If

    '    m_PreviewPanel(m_iCurWndIndex).StartPlay(iDeviceIndex, iDeviceIndex)
    'End Sub
    Private Sub setEhomeKeyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Dim V50 As EhomeV50Auth

        'If m_iCurDeviceIndex >= 0 Then
        '    V50 = New EhomeV50Auth(m_iCurDeviceIndex)
        'Else
        '    V50 = New EhomeV50Auth()
        'End If

        'Dim globalDef As GlobalDefinition = New GlobalDefinition()
        'globalDef.LoginDevIp = m_cslocalIP
        'globalDef.LoginDevPort = m_lport
        'V50.ShowDialog()
    End Sub
    Private Sub treeView1_NodeMouseClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs)
        treeView1.SelectedNode = e.Node

        If e.Button = MouseButtons.Right AndAlso 1 = e.Node.Level Then
            m_iCurDeviceIndex = e.Node.Index
            'e.Node.ContextMenuStrip = treeEhomeKeyMenuStrip
            'treeEhomeKeyMenuStrip.Show(e.Location)
        End If
    End Sub
    Public Delegate Sub DelegateDev(ByVal m As Message)
    Public Sub ProDevStatu(ByVal mes As Message)
        If treeView1.InvokeRequired Then
            Dim delegateAddLog As [Delegate] = New DelegateDev(AddressOf ProDevStatu)
            treeView1.Invoke(delegateAddLog, mes)
        Else
            MessageProcess(mes)
        End If
    End Sub
    Private Sub SetDeviceIndex(ByVal index As Integer)
        m_iCurDeviceIndex = index
    End Sub

    'Private Sub setEhomeKey()
    '    Dim m_iDevID As Integer = 0
    '    For i = 0 To DeviceTree.treeView1.Nodes.Count - 1
    '        m_iDevID = i
    '        If m_iDevID >= 0 Then
    '            Dim strEhomeKey As String = "ehome111"
    '            Dim EhomeKeyLength As Integer = strEhomeKey.Length
    '            Dim byEhomeKey(EhomeKeyLength - 1) As Byte
    '            CleanUpBytearray(DeviceTree.g_struDeviceInfo(m_iDevID).byEhomeKey, 32)
    '            DeviceTree.StrToByteArray(byEhomeKey, strEhomeKey)
    '            Array.Copy(byEhomeKey, 0, DeviceTree.g_struDeviceInfo(m_iDevID).byEhomeKey, 0, EhomeKeyLength)

    '            If EHomeForm.g_bSS_Enable Then
    '                Dim ptrDeviceID As IntPtr = Marshal.AllocHGlobal(256)
    '                Marshal.Copy(DeviceTree.g_struDeviceInfo(m_iDevID).byDeviceID, 0, ptrDeviceID, 256)
    '                Dim ptrbyEhomeKey As IntPtr = Marshal.AllocHGlobal(32)
    '                Marshal.Copy(DeviceTree.g_struDeviceInfo(m_iDevID).byEhomeKey, 0, ptrbyEhomeKey, 32)
    '                'IntPtr ptrClouldSecretKey = Marshal.AllocHGlobal(64);
    '                Dim ClouldSecretKey(255) As Byte
    '                If HCEHomeSS.NET_ESS_HAMSHA256(ptrDeviceID, ptrbyEhomeKey, ClouldSecretKey, 255) Then
    '                    Array.Copy(ClouldSecretKey, DeviceTree.g_struDeviceInfo(m_iDevID).byClouldSecretKey, 64)
    '                End If
    '                Marshal.FreeHGlobal(ptrDeviceID)
    '                Marshal.FreeHGlobal(ptrbyEhomeKey)
    '            End If
    '        Else
    '            Dim lpTemp As New GlobalDefinition.LOCAL_DEVICE_INFO()
    '            lpTemp.Init()
    '            lpTemp.dwVersion = 5
    '            Dim DevID As Byte() = DeviceTree.g_struDeviceInfo(m_iDevID).byDeviceID ' System.Text.Encoding.[Default].GetBytes(textBoxDeviceID.Text.ToString())
    '            Dim IDLength As Integer = DevID.Length
    '            If IDLength > 256 Then
    '                'MessageBox.Show("DevID length is exceeding 256")
    '                EHomeForm.TraceService("DevID length is exceeding 256", "Device Tree Line 843")
    '            End If
    '            Array.Copy(DevID, lpTemp.byDeviceID, IDLength)
    '            Dim EhomeKey As Byte() = System.Text.Encoding.[Default].GetBytes("ehome111")
    '            Dim EhomeLength As Integer = EhomeKey.Length
    '            If EhomeLength > 32 Then
    '                EHomeForm.TraceService("EhomeKey length is exceeding 32", "Device Tree Line 850")
    '                'MessageBox.Show("EhomeKey length is exceeding 32")
    '            End If
    '            Array.Copy(EhomeKey, lpTemp.byEhomeKey, EhomeLength)
    '            Dim dwSize As Integer = Marshal.SizeOf(GetType(GlobalDefinition.LOCAL_DEVICE_INFO))
    '            Dim ptrtemp As IntPtr = Marshal.AllocHGlobal(dwSize)
    '            Marshal.StructureToPtr(lpTemp, ptrtemp, False)
    '            Dim mes As New Message()
    '            mes.Msg = EHomeForm.WM_ADD_DEV
    '            mes.LParam = ptrtemp
    '            g_deviceTree.ProDevStatu(mes)
    '        End If
    '    Next
    '    'Me.DialogResult = DialogResult.OK
    'End Sub
    Private Sub setEhomeKey(i)
        Dim m_iDevID As Integer = 0
        'For i = 0 To DeviceTree.treeView1.Nodes.Count - 1
        m_iDevID = i
        If m_iDevID >= 0 Then
            Dim strEhomeKey As String = "ehome111"
            Dim EhomeKeyLength As Integer = strEhomeKey.Length
            Dim byEhomeKey(EhomeKeyLength - 1) As Byte
            CleanUpBytearray(DeviceTree.g_struDeviceInfo(m_iDevID).byEhomeKey, 32)
            DeviceTree.StrToByteArray(byEhomeKey, strEhomeKey)
            Array.Copy(byEhomeKey, 0, DeviceTree.g_struDeviceInfo(m_iDevID).byEhomeKey, 0, EhomeKeyLength)

            If EHomeForm.g_bSS_Enable Then
                Dim ptrDeviceID As IntPtr = Marshal.AllocHGlobal(256)
                Marshal.Copy(DeviceTree.g_struDeviceInfo(m_iDevID).byDeviceID, 0, ptrDeviceID, 256)
                Dim ptrbyEhomeKey As IntPtr = Marshal.AllocHGlobal(32)
                Marshal.Copy(DeviceTree.g_struDeviceInfo(m_iDevID).byEhomeKey, 0, ptrbyEhomeKey, 32)
                'IntPtr ptrClouldSecretKey = Marshal.AllocHGlobal(64);
                Dim ClouldSecretKey(255) As Byte
                If HCEHomeSS.NET_ESS_HAMSHA256(ptrDeviceID, ptrbyEhomeKey, ClouldSecretKey, 255) Then
                    Array.Copy(ClouldSecretKey, DeviceTree.g_struDeviceInfo(m_iDevID).byClouldSecretKey, 64)
                End If
                Marshal.FreeHGlobal(ptrDeviceID)
                Marshal.FreeHGlobal(ptrbyEhomeKey)
            End If
        Else
            Dim lpTemp As New GlobalDefinition.LOCAL_DEVICE_INFO()
            lpTemp.Init()
            lpTemp.dwVersion = 5
            Dim DevID As Byte() = DeviceTree.g_struDeviceInfo(m_iDevID).byDeviceID ' System.Text.Encoding.[Default].GetBytes(textBoxDeviceID.Text.ToString())
            Dim IDLength As Integer = DevID.Length
            If IDLength > 256 Then
                'MessageBox.Show("DevID length is exceeding 256")
                EHomeForm.TraceService("DevID length is exceeding 256", "Device Tree Line 843")
            End If
            Array.Copy(DevID, lpTemp.byDeviceID, IDLength)
            Dim EhomeKey As Byte() = System.Text.Encoding.[Default].GetBytes("ehome111")
            Dim EhomeLength As Integer = EhomeKey.Length
            If EhomeLength > 32 Then
                EHomeForm.TraceService("EhomeKey length is exceeding 32", "Device Tree Line 850")
                'MessageBox.Show("EhomeKey length is exceeding 32")
            End If
            Array.Copy(EhomeKey, lpTemp.byEhomeKey, EhomeLength)
            Dim dwSize As Integer = Marshal.SizeOf(GetType(GlobalDefinition.LOCAL_DEVICE_INFO))
            Dim ptrtemp As IntPtr = Marshal.AllocHGlobal(dwSize)
            Marshal.StructureToPtr(lpTemp, ptrtemp, False)
            Dim mes As New Message()
            mes.Msg = EHomeForm.WM_ADD_DEV
            mes.LParam = ptrtemp
            g_deviceTree.ProDevStatu(mes)
        End If
        'Next
        'Me.DialogResult = DialogResult.OK
    End Sub
    Public Sub CleanUpBytearray(ByRef source As Byte(), ByVal len As Integer)
        For i As Integer = 0 To len - 1
            source(i) = 0
        Next
    End Sub
End Class
'End Namespace
