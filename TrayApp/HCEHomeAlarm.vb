﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Runtime.InteropServices
Imports UltraiDMS.EhomeSDK
Imports UltraiDMS.EHomeDemo

'Namespace EHomeDemo
Public Class HCEHomeAlarm
    Public Const NET_EHOME_DEVICEID_LEN As Integer = 256
    Public Const MAX_DEVICE_ID_LEN As Integer = 256
    Public Const NET_EHOME_SERIAL_LEN As Integer = 12
    Public Const MAX_TIME_LEN As Integer = 32
    Public Const MAX_REMARK_LEN As Integer = 64
    Public Const MAX_URL_LEN As Integer = 512
    Public Const MAX_UUID_LEN As Integer = 64
    Public Const CID_DES_LEN_EX As Integer = 256
    Public Const CID_DES_LEN As Integer = 32
    Public Const MAX_ALARM_STRUCT_LEN As Integer = 2048
    Public Const MAX_ALARM_ANSWER_LEN As Integer = 512
    Public Const MAX_ALARM_DESCRIPTION_LEN As Integer = 64
    Public Const MAX_ALARM_XML_LEN As Integer = 2048
    Public Const NAME_LEN As Integer = 32
    Public Const MAX_SERIAL_LEN As Integer = 12
    Public Const EHOME_STATUS_200 As Integer = 200
    Public Const EHOME_STATUS_400 As Integer = 400
    Public Const EHOME_STATUS_401 As Integer = 401
    Public Const EHOME_ALARM_UNKNOWN As Integer = 0
    Public Const EHOME_ALARM As Integer = 1
    Public Const EHOME_ALARM_HEATMAP_REPORT As Integer = 2
    Public Const EHOME_ALARM_FACESNAP_REPORT As Integer = 3
    Public Const EHOME_ALARM_GPS As Integer = 4
    Public Const EHOME_ALARM_CID_REPORT As Integer = 5
    Public Const EHOME_ALARM_NOTICE_PICURL As Integer = 6
    Public Const EHOME_ALARM_NOTIFY_FAIL As Integer = 7
    Public Const EHOME_ALARM_SELFDEFINE As Integer = 9
    Public Const EHOME_ALARM_ACS As Integer = 11
    Public Const EHOME_ISAPI_ALARM As Integer = 13 'Only Temperature Mode
    Public Const EHOME_ALARM_FACETEMP As Integer = 21 'Face Temperature Alarm

    Public Enum EN_ALARM_TYPE
        ALARM_TYPE_DISK_FULL = 0
        ALARM_TYPE_DISK_WRERROR
        ALARM_TYPE_VIDEO_LOST = 5
        ALARM_TYPE_EXTERNAL
        ALARM_TYPE_VIDEO_COVERED
        ALARM_TYPE_MOTION
        ALARM_TYPE_STANDARD_NOTMATCH
        ALARM_TYPE_SPEEDLIMIT_EXCEED
        ALARM_TYPE_PIR
        ALARM_TYPE_WIRELESS
        ALARM_TYPE_CALL_HELP
        ALARM_TYPE_DISARM
        ALARM_TYPE_STREAM_PRIVATE
        ALARM_TYPE_PIC_UPLOAD_FAIL
        ALARM_TYPE_LOCAL_REC_EXCEPTION
        ALARM_TYPE_UPGRADE_FAIL
        ALARM_TYPE_ILLEGAL_ACCESS
        ALARM_TYPE_SOUNDLIMIT_EXCEED = 80
        ALARM_TYPE_TRIFFIC_VIOLATION = 90
        ALARM_TYPE_ALARM_CONTROL
        ALARM_TYPE_FACE_DETECTION = 97
        ALARM_TYPE_DEFOUSE_DETECTION
        ALARM_TYPE_AUDIO_EXCEPTION
        ALARM_TYPE_SCENE_CHANGE
        ALARM_TYPE_TRAVERSE_PLANE
        ALARM_TYPE_ENTER_AREA
        ALARM_TYPE_LEAVE_AREA
        ALARM_TYPE_INTRUSION
        ALARM_TYPE_LOITER
        ALARM_TYPE_LEFT_TAKE
        ALARM_TYPE_CAR_STOP
        ALARM_TYPE_MOVE_FAST
        ALARM_TYPE_HIGH_DENSITY
        ALARM_TYPE_PDC_BY_TIME
        ALARM_TYPE_PDC_BY_FRAME
        ALARM_TYPE_LEFT
        ALARM_TYPE_TAKE
        ALARM_TYPE_ENV_LIMIT = 8800
        ALARM_TYPE_ENV_REAL_TIME
        ALARM_TYPE_ENV_EXCEPTION
        ALARM_TYPE_HIGH_TEMP = 40961
        ALARM_TYPE_ACC_EXCEPTION
    End Enum


    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_ALARM_INFO
        Public dwSize As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_TIME_LEN)>
        Public szAlarmTime As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DEVICE_ID_LEN)>
        Public szDeviceID As Byte()
        Public dwAlarmType As UInt32
        Public dwAlarmAction As UInt32
        Public dwVideoChannel As UInt32
        Public dwAlarmInChannel As UInt32
        Public dwDiskNumber As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_REMARK_LEN)>
        Public byRemark As Byte()
        Public byRetransFlag As Byte
        Public byTimeType As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)>
        Public byRes1 As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_TIME_LEN)>
        Public szAlarmUploadTime As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)>
        Public byRes As Byte()

			Public Sub Init()
				szAlarmTime = New Byte(MAX_TIME_LEN - 1){}
				szDeviceID = New Byte(MAX_DEVICE_ID_LEN - 1){}
				szAlarmUploadTime = New Byte(MAX_TIME_LEN - 1){}
				byRemark = New Byte(MAX_REMARK_LEN - 1){}
				byRes1 = New Byte(1){}
				byRes = New Byte(27){}
			End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_GPS_INFO
        Public dwSize As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_TIME_LEN)>
        Public bySampleTime As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DEVICE_ID_LEN)>
        Public byDeviceID As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)>
        Public byDivision As Byte()
        Public bySatelites As Byte
        Public byPrecision As Byte
        Public dwLongitude As UInt32
        Public dwLatitude As UInt32
        Public dwDirection As UInt32
        Public dwSpeed As UInt32
        Public dwHeight As UInt32
        Public byRetransFlag As Byte
        Public byLocateMode As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)>
        Public byRes1 As Byte()
        Public dwMileage As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=56)>
        Public byRes As Byte()

        Public Sub Init()
            bySampleTime = New Byte(MAX_TIME_LEN - 1) {}
            byDeviceID = New Byte(MAX_DEVICE_ID_LEN - 1) {}
            byDivision = New Byte(1) {}
            byRes1 = New Byte(1) {}
            byRes = New Byte(55) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_HEATMAP_VALUE
        Public dwMaxHeatMapValue As UInt32
        Public dwMinHeatMapValue As UInt32
        Public dwTimeHeatMapValue As UInt32
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_PIXEL_ARRAY_SIZE
        Public dwLineValue As UInt32
        Public dwColumnValue As UInt32
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_HEATMAP_REPORT
        Public dwSize As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DEVICE_ID_LEN)>
        Public byDeviceID As Char()
        Public dwVideoChannel As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_TIME_LEN)>
        Public byStartTime As Char()
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_TIME_LEN)>
        Public byStopTime As Char()
        Public struHeatmapValue As NET_EHOME_HEATMAP_VALUE
        Public struPixelArraySize As NET_EHOME_PIXEL_ARRAY_SIZE
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_URL_LEN)>
        Public byPixelArrayData As Char
        Public byRetransFlag As Byte
        Public byTimeType As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=62)>
        Public byRes As Byte
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_HUMAN_FEATURE
        Private byAgeGroup As Byte
        Private bySex As Byte
        Private byEyeGlass As Byte
        Private byRes As Byte
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_ZONE
        Private dwX As UInt32
        Private dwY As UInt32
        Private dwWidth As UInt32
        Private dwHeight As UInt32
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_FACESNAP_REPORT
        Private dwSize As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_DEVICE_ID_LEN)>
        Private byDeviceID As Char()
        Private dwVideoChannel As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_TIME_LEN)>
        Private byAlarmTime As Char()
        Private dwFacePicID As UInt32
        Private dwFaceScore As UInt32
        Private dwTargetID As UInt32
        Private struTarketZone As NET_EHOME_ZONE
        Private struFacePicZone As NET_EHOME_ZONE
        Private struHumanFeature As NET_EHOME_HUMAN_FEATURE
        Private dwStayDuration As UInt32
        Private dwFacePicLen As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_URL_LEN)>
        Private byFacePicUrl As Char()
        Private dwBackgroudPicLen As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_URL_LEN)>
        Private byBackgroudPicUrl As Char()
        Private byRetransFlag As Byte
        Private byTimeType As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=62)>
        Private byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_CID_PARAM
        Public dwUserType As UInt32
        Public lUserNo As Int32
        Public lZoneNo As Int32
        Public lKeyboardNo As Int32
        Public lVideoChanNo As Int32
        Public lDiskNo As Int32
        Public lModuleAddr As Int32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=NAME_LEN)>
        Public byUserName As String
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32)>
        Public byRes As Byte()

        Public Sub Init()
            byRes = New Byte(31) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_CID_INFO_INTERNAL_EX
        Public byRecheck As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)>
        Public byRes As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_UUID_LEN)>
        Public byUUID As String
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_URL_LEN)>
        Public byVideoURL As String
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_URL_LEN)>
        Public byCIDDescribeEx As String
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=256)>
        Public byRes1 As Byte()

        Public Sub Init()
            byRes = New Byte(2) {}
            byRes1 = New Byte(255) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_CID_INFO
        Public dwSize As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_DEVICE_ID_LEN)>
        Public byDeviceID As String
        Public dwCIDCode As UInt32
        Public dwCIDType As UInt32
        Public dwSubSysNo As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=CID_DES_LEN)>
        Public byCIDDescribe As String
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_TIME_LEN)>
        Public byTriggerTime As String
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_TIME_LEN)>
        Public byUploadTime As String
        Public struCIDParam As NET_EHOME_CID_PARAM
        Public byTimeDiffH As Byte
        Public byTimeDiffM As Byte
        Public byExtend As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)>
        Public byRes1 As Byte()
        Public pCidInfoEx As IntPtr
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=52)>
        Public byRes As Byte()

        Public Sub Init()
            struCIDParam.Init()
            byRes1 = New Byte(4) {}
            byRes = New Byte(51) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_NOTICE_PICURL
        Private dwSize As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_DEVICE_ID_LEN)>
        Private byDeviceID As Char()
        Private wPicType As UInt16
        Private wAlarmType As UInt16
        Private dwAlarmChan As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_TIME_LEN)>
        Private byAlarmTime As Char()
        Private dwCaptureChan As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_TIME_LEN)>
        Private byPicTime As Char()
        <MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst:=MAX_URL_LEN)>
        Private byPicUrl As Char()
        Private dwManualSnapSeq As UInt32
        Private byRetransFlag As Byte
        Private byTimeType As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=30)>
        Private byRes As Byte()
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_NOTIFY_FAIL_INFO
        Private dwSize As UInt32
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=MAX_DEVICE_ID_LEN)>
        Private byDeviceID As Char
        Private wFailedCommand As UInt16
        Private wPicType As UInt16
        Private dwManualSnapSeq As UInt32
        Private byRetransFlag As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=31)>
        Private byRes As Byte()
    End Structure

    Public Delegate Function EHomeMsgCallBack(ByVal lHandle As Int32, ByVal pAlarmMsg As IntPtr, ByVal pUser As IntPtr) As Boolean

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_ALARM_LISTEN_PARAM
        Public struAddress As HCEHomePublic.NET_EHOME_IPADDRESS
        Public fnMsgCb As EHomeMsgCallBack
        Public pUserData As IntPtr
        Public byProtocolType As Byte
        Public byUseCmsPort As Byte
        Public byUseThreadPool As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=29)>
        Public byRes As Byte()

        Public Sub Init()
            struAddress.Init()
            byRes = New Byte(28) {}
        End Sub
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_ALARM_MSG
        Public dwAlarmType As UInteger
        Public pAlarmInfo As IntPtr
        Public dwAlarmInfoLen As UInteger
        Public pXmlBuf As IntPtr
        Public dwXmlBufLen As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=NET_EHOME_SERIAL_LEN)>
			Public sSerialNumber() As Byte '设备序列号，用于进行Token认证
			Public pHttpUrl As IntPtr
			Public dwHttpUrlLen As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=12)>
        Public byRes As Byte()

        Public Sub Init()
            dwAlarmType = 0
            pAlarmInfo = IntPtr.Zero
            dwAlarmInfoLen = 0
            pXmlBuf = IntPtr.Zero
            dwXmlBufLen = 0
            sSerialNumber = New Byte(NET_EHOME_SERIAL_LEN - 1) {}
            byRes = New Byte(11) {}
        End Sub
    End Structure
    Public Enum AlarmType
        EHOME_ALARM_UNKNOWN = 0 '未知报警类型
        EHOME_ALARM = 1 'Ehome基本报警
        EHOME_ALARM_HEATMAP_REPORT = 2 '热度图报告
        EHOME_ALARM_FACESNAP_REPORT = 3 '人脸抓拍报告
        EHOME_ALARM_GPS = 4 'GPS信息上传
        EHOME_ALARM_CID_REPORT = 5 '报警主机CID告警上传
        EHOME_ALARM_NOTICE_PICURL = 6 '图片URL上报
        EHOME_ALARM_NOTIFY_FAIL = 7 '异步失败通知
        EHOME_ALARM_SELFDEFINE = 9 '自定义报警上传
        EHOME_ALARM_DEVICE_NETSWITCH_REPORT = 10 '设备网络切换上传
        EHOME_ALARM_ACS = 11 '门禁事件上报
        EHOME_ALARM_WIRELESS_INFO = 12 '无线网络信息上传
        EHOME_ISAPI_ALARM = 13 'ISAPI报警上传
        EHOME_INFO_RELEASE_PRIVATE = 14 '为了兼容信息发布产品的私有EHome协议报警（不再维护）
        EHOME_ALARM_MPDCDATA = 15 '车载设备的客流数据
    End Enum
    <StructLayoutAttribute(LayoutKind.Sequential)>
    Public Structure NET_EHOME_ALARM_LISTEN_PARAM_LOCAL
        Private struAlaramListenParam As NET_EHOME_ALARM_LISTEN_PARAM
        Private byAccessSecurity As Byte
    End Structure

    Public Structure NET_EHOME_AMS_ADDRESS
        Public dwSize As Integer
        Public byEnable As Byte '0-关闭CMS接收报警功能，1-开启CMS接收报警功能
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)>
        Public byRes1() As Byte
        Public struAddress As HCEHomePublic.NET_EHOME_IPADDRESS 'AMS本地回环地址
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32)>
        Public byRes2() As Byte
        Public Sub Init()
            dwSize = 0
            byEnable = 0
            byRes1 = New Byte(2) {}
            struAddress.Init()
        End Sub
    End Structure
		#Region "HCISUPAlarm.dll function definition"

		'SDK Init
		<DllImport("HCISUPAlarm.dll")>
		Public Shared Function NET_EALARM_Init() As Boolean
		End Function

		'SDK Fni 
		<DllImport("HCISUPAlarm.dll")>
		Public Shared Function NET_EALARM_Fini() As Boolean
		End Function

		<DllImport("HCISUPAlarm.dll")>
		Public Shared Function NET_EALARM_GetLastError() As Boolean
		End Function

		<DllImport("HCISUPAlarm.dll")>
		Public Shared Function NET_EALARM_SetLogToFile(ByVal iLogLevel As Integer, ByVal strLogDir() As Char, ByVal bAutoDel As Boolean) As Integer
		End Function

    '[DllImport(@"HCEHomeAlarm.dll")]
    'public static extern bool NET_EALARM_StopListen(ref NET_EHOME_ALARM_LISTEN_PARAM pAlarmListenParam);

		<DllImport("HCISUPAlarm.dll")>
		Public Shared Function NET_EALARM_GetBuildVersion() As Integer
		End Function

		<DllImport("HCISUPAlarm.dll")>
		Public Shared Function NET_EALARM_StartListen(ByRef pAlarmListenParam As NET_EHOME_ALARM_LISTEN_PARAM) As Integer
		End Function
		'Close Cms Alarm
		<DllImport("HCISUPAlarm.dll")>
		Public Shared Function NET_EALARM_StopListen(ByVal iListenHandle As Integer) As Integer
		End Function

		<DllImport("HCISUPAlarm.dll")>
		Public Shared Function NET_EALARM_SetSDKLocalCfg(ByVal enumType As HCEHomePublic.NET_EHOME_LOCAL_CFG_TYPE, ByVal lpInBuff As IntPtr) As Boolean
		End Function

		<DllImport("HCISUPAlarm.dll")>
		Public Shared Function NET_EALARM_SetDeviceSessionKey(ByRef pDeviceKey As HCEHomePublic.NET_EHOME_DEV_SESSIONKEY) As Boolean
		End Function
		#End Region
	End Class
'End Namespace
