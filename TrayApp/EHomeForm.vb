﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms
Imports System.Net
Imports System.Threading
Imports System.Globalization
Imports System.Xml
Imports System.Runtime.InteropServices
Imports UltraiDMS.EHomeDemo
Imports System.IO
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports Newtonsoft.Json
Imports System.Timers
Imports System.Net.Sockets
Imports System.Net.NetworkInformation

Public Class EHomeForm
#Region "Declaration"
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim adap As SqlDataAdapter
    Dim adapA As OleDbDataAdapter
    'Dim ds, RsCrEmp, RsEmpTemp As DataSet
    Dim EnableMaintimer As Boolean = False

    Dim servername As String
    Dim DB As String
    Public Shared ConnectionString As String
    'Dim con As SqlConnection
    'Dim con1 As OleDbConnection

    Dim m_csLocalIP As String
    Public Shared m_nPort As String = "7660"

    Dim timerOL As Timers.Timer
    Dim timerCommand As Timers.Timer
    Dim timerDeleteFiles As Timers.Timer
    Dim timerRestart As Timers.Timer  'LnT

    Dim timerSmartInfo As Timers.Timer 'for smart infocom

    Public Shared AutoSync As String = "N"
    Public Shared AlowVisitor As String = "Y"
    Public Shared DuplicateCheck As Integer = 0
    Public Shared PhotoRemoveDays As Integer = 0
    Public Shared AllowPhoto As String = "Y"
    Public Shared AttProcess As String = "N"
    Public Shared AutoDownloadLogs As String = "N"
    Public Shared AutoDownloadLogsTime As String = "01:00"
    Public Shared AllowSeconds As String = "N"

    'Public Declare Function PostMessage Lib "User32.dll" Alias "PostMessage" (ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Integer
    'Public Declare Function SendMessage Lib "User32.dll" Alias "SendMessage" (ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Integer


#Region "dll region"
    <DllImport("User32.dll", EntryPoint:="PostMessage")>
    Public Shared Function PostMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Integer
    End Function
    <DllImport("User32.dll", EntryPoint:="SendMessage")>
    Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Integer
    End Function
#End Region

    'Dim m_csLocalIP As String = Nothing
    'Dim m_nPort As Int16
    Dim m_lPort As Integer
    Dim m_hWnd As IntPtr
    'Dim m_hPreviewWnd As IntPtr
    Public m_bPlay As Boolean
    Public m_iPlayHandle As Integer
    Public m_iSessionID As Integer = -1
    Public m_iWndIndex As Integer
    Public m_iLoginID As Integer = -1
    Public m_iHandle As Integer = -1
    Public m_iChanelType As Integer
    Public m_iLinkHandle As Integer = -1
    Public m_iRealHandle As Integer = -1

    Private m_iCurWndNum As Integer 'screen split mode 1, 4, 9, 16, 25, 36
    Private m_iCurWndIndex As Integer 'current selected split window index, start from 0
    Public m_byUseCmsPort As Integer
    Public m_bListLogEnlarge As Boolean
    Public m_bListAlarmEnlarge As Boolean
    Public m_iMainType As Integer
    Public m_lAlarmHandle As Integer
    Public m_lCmsAlarm As Integer
    Public m_lUdpAlarmHandle As Integer
    Public m_lTcpAlarmHandle As Integer
    Public m_lEhome50AlarmHandle As Integer
    Public m_bUseAccessList As Boolean = False
    Public m_bAlarmed As Boolean = False
    Public Shared m_treeHandle As IntPtr
    Public m_count1 As Integer = 0
    Public Const MAX_PATH As Integer = 260
    Public SS_PATH As String = System.Environment.CurrentDirectory & "\StorageServer"
    Public SS_MESSAGE_PATH As String = System.Environment.CurrentDirectory & "\StorageServer\Message"
    Public SS_STORAGE_PATH As String = System.Environment.CurrentDirectory & "\StorageServer\Storage"  ' 
    Public PicServerPort As Integer = 0
    Public ptrTcpAlarm As HCEHomeAlarm.EHomeMsgCallBack = Nothing
    Public ptrMqtt As HCEHomeAlarm.EHomeMsgCallBack = Nothing
    Public Shared g_bSS_Enable As Boolean = True
    Public m_wAlarmServerMqttPort As UShort = 0
    Public Const DevOnServerAdd As Integer = 0
    Public Const StreamAdd As Integer = 1
    Public Const AudioAdd As Integer = 2
    Public Const DASAdd As Integer = 3
    Public Const PSSAdd As Integer = 4
    Public Const PIC_URI_LEN As Integer = 128
    Public Const WM_ADD_LOG As Integer = &H401 'add log
    Public Shared WM_ADD_DEV As Integer = 78
    Public Shared WM_LISTENED_ALARM As Integer = 79
    Public Shared WM_ACS_ALARM As Integer = 80
    Public Shared WM_DEL_DEV As Integer = &H403 'delete device'1027
    Public Const WM_CHANGE_CHANNEL_ITEM_IMAGE As Integer = &H404 'change channel node icon
    Public Const WM_PROC_EXCEPTION As Integer = &H405 'process exception
    Public Shared WM_CHANGE_IP As Integer = &H406 'ip address changed'1030
    Public Const MAX_DEVICES As Integer = 1024
    Public Delegate Function rmMsgCallBack(ByVal lHandle As Long, ByRef pAlarmMsg As HCEHomeAlarm.NET_EHOME_ALARM_MSG, ByVal pUser As IntPtr) As Boolean
    Dim m_struAmsAddr As HCEHomePublic.NET_EHOME_AMS_ADDRESS = New HCEHomePublic.NET_EHOME_AMS_ADDRESS
    Dim m_struAccessSecure As HCEHomePublic.NET_EHOME_LOCAL_ACCESS_SECURITY = New HCEHomePublic.NET_EHOME_LOCAL_ACCESS_SECURITY
    Dim m_struSendParam As HCEHomePublic.NET_EHOME_SEND_PARAM = New HCEHomePublic.NET_EHOME_SEND_PARAM
    Dim m_struCMSListenPara As HCEHomeCMS.NET_EHOME_CMS_LISTEN_PARAM = New HCEHomeCMS.NET_EHOME_CMS_LISTEN_PARAM
    Dim m_struServInfo As HCEHomeCMS.NET_EHOME_SERVER_INFO_V50 = New HCEHomeCMS.NET_EHOME_SERVER_INFO_V50
    'Dim m_struServInfo As New HCEHomeCMS.NET_EHOME_SERVER_INFO_V50()
    Dim m_ConvertModel As ConvertModel = New ConvertModel
    Public Shared m_stAccessDeviceList(63) As ACCESS_DEVICE_INFO
    Private Shared fnPREVIEW_DATA_CB_Func As New HCEHomeStream.PREVIEW_DATA_CB(AddressOf fnPREVIEW_DATA_CB)
    Dim fnPREVIEW_NEWLINK_CB_Func As HCEHomeStream.PREVIEW_NEWLINK_CB = Nothing
    Dim AlarmMsgCallBack_Func As HCEHomeAlarm.EHomeMsgCallBack = Nothing
    Dim EHOME_REGISTER_Func As HCEHomeCMS.DEVICE_REGISTER_CB = Nothing
    Dim SSMsgCallBack As HCEHomeSS.EHomeSSMsgCallBack = Nothing
    Dim StroageCallBack As HCEHomeSS.EHomeSSStorageCallBack = Nothing
    Dim ptrSSMsgCallBack As HCEHomeSS.EHomeSSMsgCallBack = Nothing
    Dim ptrStroageCallBack As HCEHomeSS.EHomeSSStorageCallBack = Nothing
    Dim m_bHttps As Boolean = False
    Dim doc As XmlDocument = New XmlDocument
    Dim m_hAlarmListenCB As Thread
    Dim m_AlarmListenCB As AlarmListenCB = New AlarmListenCB
    Dim m_count As Integer = 0
    Public m_lSSHandle As Integer = -1
    Dim PicServerip As String = Nothing
    'Dim g_formList As DeviceLogList = DeviceLogList.Instance
    Dim g_deviceTree As DeviceTree = DeviceTree.Instance
    'Dim g_AudioTalk As AudioTalk = New AudioTalk
    Dim m_logListHandle As IntPtr
    Dim m_byCmsSecureAccessType As Byte
    Dim m_byAlarmSecureAccessType As Byte
    Dim m_byStreamSecureAccessType As Byte
#End Region
    Public Shared IpAddressList As New List(Of IPAddress)()
    Public Shared ActiveSerialNumber() As String
    Public Sub GetLocalIP()
        IpAddressList.Clear()
        Dim hostName As String = Dns.GetHostName()
        Dim addressList() As IPAddress = Dns.GetHostAddresses(hostName)
        For Each ip As IPAddress In addressList
            If ip.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then '判断是否IP4协议
                Me.m_cmbLocalIP.Items.Add(ip)
                IpAddressList.Add(ip)
            End If
        Next ip
        m_cmbLocalIP.Items.Remove("0.0.0.0")
        Me.m_cmbLocalIP.SelectedIndex = 0
    End Sub
    'Public Sub GetLocalIP()
    '    'IpAddressList.Clear()
    '    Dim hostName As String = Dns.GetHostName
    '    Dim addressList() As IPAddress = Dns.GetHostAddresses(hostName)
    '    For Each ip As IPAddress In addressList
    '        If (ip.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork) Then
    '            m_csLocalIP = ip.ToString ' IpAddressList.Add(ip)
    '        End If
    '    Next
    'End Sub
    Public Structure ACCESS_DEVICE_INFO
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=12, ArraySubType:=UnmanagedType.U1)>
        Public sSerialNumber() As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32, ArraySubType:=UnmanagedType.U1)>
        Public sIdentifyCode() As Byte
    End Structure
    Public Enum NET_EHOME_LOCAL_CFG_TYPE

        UNDEFINE = -1

        ACTIVE_ACCESS_SECURITY = 0

        AMS_ADDRESS

        SEND_PARAM
    End Enum

    Public Const EHOME_ISAPI_ALARM As Integer = 13 'ISAPI Alarm Upload
    Public Const EHOME_ALARM_ACS As Integer = 11 'ACS Alarm
    Public Const EHOME_ALARM_FACETEMP As Integer = 21 'Face Temperature Upload

    Public Shared StrTcpServIP As String = String.Empty
    Public Shared StrUdpServIP As String = String.Empty
    Public Shared StrPicServIp As String = String.Empty
    Public Class RegionCoordinates
        Public Property positionX() As Integer
        Public Property positionY() As Integer
    End Class
    Public Class FaceTemperatureMeasurementEvent
        Public Property deviceName() As String
        Public Property serialNo() As Integer
        Public Property thermometryUnit() As String
        Public Property currTemperature() As Single
        Public Property isAbnomalTemperature() As Boolean
        Public Property RegionCoordinates() As RegionCoordinates
        Public Property mask() As String
        Public Property visibleLightURL() As String
        Public Property thermalURL() As String
    End Class
    Public Class FaceAlarmInfo
        Public Property ipAddress() As String
        Public Property portNo() As Integer
        Public Property channelID() As Integer
        Public Property dateTime() As String
        Public Property activePostCount() As Integer
        Public Property eventType() As String
        Public Property eventState() As String
        Public Property eventDescription() As String
        Public Property FaceTemperatureMeasurementEvent() As FaceTemperatureMeasurementEvent
    End Class
    Private Sub EHomeForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        GetLocalIP()
        CheckDBText()
        LoadActiveSerialNumber()
    End Sub
    Public Shared Sub TraceService(ByVal errmessage As String, ByVal source As String)
        errmessage = errmessage.Replace("'", "")
        Dim con As SqlConnection = New SqlConnection(ConnectionString)
        Try
            If (con.State <> ConnectionState.Open) Then
                con.Open()
            End If
            source = source & ("----Port: " & m_nPort)
            Dim s As String = "insert into ZKServiceErrorLog(ErrorMessage,ErrorSource,ErrorTime) values('" & errmessage & "','" & source & "',getdate())"
            Dim cmd As SqlCommand = New SqlCommand(s, con)
            cmd.CommandType = CommandType.Text
            Dim RowsCount As Integer = cmd.ExecuteNonQuery
            If (con.State <> ConnectionState.Closed) Then
                con.Close()
            End If
        Catch ex As Exception
            If (con.State <> ConnectionState.Closed) Then
                con.Close()
            End If
        End Try
    End Sub
    Private Sub CheckDBText()
        If Not System.IO.File.Exists("db.txt") Then
            File.Create("db.txt").Dispose()
        End If
        Application.DoEvents()
        Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        Dim sr As StreamReader = New StreamReader(fs)
        Dim str As String
        Dim str1() As String
        'Dim dbname As String
        Do While sr.Peek <> -1
            str = sr.ReadLine
            str1 = str.Split(",")
            servername = str1(0)
        Loop
        sr.Close()
        fs.Close()
        If servername = "" Then
            Me.Hide()
            'XtraDB.ShowDialog()
            Exit Sub
        End If
        Dim con As SqlConnection
        Dim con1 As OleDbConnection
        If servername = "Access" Then
            ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\HTDB.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\HTDB.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            con1 = New OleDbConnection(ConnectionString)
            Try
                con1.Open()
                con1.Close()
            Catch ex As Exception
                Me.Hide()
                'XtraDB.ShowDialog()
                Exit Sub
            End Try
        Else
            Try
                DB = str1(1)
                If str1(2) = "Win" Then
                    ConnectionString = "Data Source=" & servername & ";Initial Catalog=" & str1(1) & ";Integrated Security=True"
                    ConnectionString = "Data Source=" & servername & ";Initial Catalog=" & str1(1) & ";Integrated Security=True;MultipleActiveResultSets=true;"
                    'SQLUserId = ""
                    'SQLPassword = ""
                Else
                    ConnectionString = "Data Source=" & servername & ";Initial Catalog=" & str1(1) & ";User Id=" & str1(3) & ";Password=" & str1(4) & ";"
                    ConnectionString = "Data Source=" & servername & ";Initial Catalog=" & str1(1) & ";User Id=" & str1(3) & ";Password=" & str1(4) & ";MultipleActiveResultSets=true;pooling=true;connection lifetime=120;max pool size=1000;"
                    'SQLUserId = str1(3)
                    'SQLPassword = str1(4)
                End If
                con = New SqlConnection(ConnectionString)

                Try
                    con.Open()
                    con.Close()
                Catch ex As Exception
                    'Me.Hide()
                    MsgBox("Connection To database failed. Please Check db.txt")
                    Application.Exit()
                    Exit Sub
                End Try
                Dim sSql As String
                Try
                    sSql = "If COL_LENGTH('schemaName.UserFace', 'FaceByte') IS NULL " &
                                   "BEGIN  ALTER TABLE UserFace ADD FaceByte image End"
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    Dim cmd As SqlCommand
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                Catch ex As Exception
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End Try

                Try
                    sSql = "If COL_LENGTH('schemaName.MachineRawPunch', 'Temperature') IS NULL " &
                                    "BEGIN  ALTER TABLE MachineRawPunch ADD Temperature varchar(MAX),MaskStatus varchar(MAX),IsAbnomal varchar(MAX) End"
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    Dim cmd As SqlCommand
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                Catch ex As Exception
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End Try


                'NoOfCommandsAtTime
                sSql = "Select * from iDMSSetting"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    If ds.Tables(0).Rows.Count = 0 Then
                        sSql = "Insert into iDMSSetting (NoOfCommandsAtTime) values ('50')"
                        con.Open()
                        cmd = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        con.Close()
                    End If
                Catch ex As Exception

                End Try


                'ServerIP
                Try
                    sSql = "If COL_LENGTH('schemaName.iDMSSetting', 'ServerIP') IS NULL " &
                                    "BEGIN  ALTER TABLE iDMSSetting ADD ServerIP varchar(MAX) End"
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    Dim cmd As SqlCommand
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                Catch ex As Exception
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End Try

                'check ServerIP
                Try
                    Dim ds As DataSet = New DataSet
                    sSql = "select ServerIP from iDMSSetting "
                    adap = New SqlDataAdapter(sSql, con)
                    adap.Fill(ds)

                    If ds.Tables(0).Rows.Count > 0 Then
                        textServerIP.Text = ds.Tables(0).Rows(0)(0).ToString.Trim
                    Else
                        textServerIP.Text = m_cmbLocalIP.Text.Trim
                    End If
                Catch ex As Exception
                    textServerIP.Text = m_cmbLocalIP.Text.Trim
                End Try

                Try
                    sSql = "If COL_LENGTH('schemaName.devicecommands', 'Priority') IS NULL " &
                                    "BEGIN  ALTER TABLE devicecommands ADD Priority varchar(MAX) End"
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    Dim cmd As SqlCommand
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                Catch ex As Exception
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End Try

                'AutoSync
                sSql = "Select AutoSync from iDMSSetting"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    AutoSync = ds.Tables(0).Rows(0)("AutoSync").ToString.Trim
                Catch ex As Exception
                    sSql = "ALTER TABLE iDMSSetting add AutoSync varchar(1)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update iDMSSetting set AutoSync='N'"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                    AutoSync = "N"
                End Try


                'check FailedEntries
                sSql = "Select top 1 * from FailedEntries"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                Catch ex As Exception
                    Try
                        sSql = "create table FailedEntries (UserId varchar(50), DeviceID varchar(50), Content varchar(50), UpdatedOn datetime)"
                        con.Open()
                        Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        con.Close()
                    Catch ex1 As Exception
                    End Try
                End Try
                'end check FaildEntries

                'AlowVisitor
                sSql = "Select AlowVisitor from iDMSSetting"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    AlowVisitor = ds.Tables(0).Rows(0)("AlowVisitor").ToString.Trim
                Catch ex As Exception
                    sSql = "ALTER TABLE iDMSSetting add AlowVisitor varchar(1)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update iDMSSetting set AlowVisitor='Y'"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                    AlowVisitor = "Y"
                End Try

                'DuplicateCheck
                sSql = "Select DuplicateCheck from iDMSSetting"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    DuplicateCheck = Convert.ToInt64(ds.Tables(0).Rows(0)("DuplicateCheck").ToString.Trim)
                Catch ex As Exception
                    sSql = "ALTER TABLE iDMSSetting add DuplicateCheck int"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update iDMSSetting set DuplicateCheck=0"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                    DuplicateCheck = 0
                End Try

                'PhotoRemoveDays
                sSql = "Select PhotoRemoveDays from iDMSSetting"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    PhotoRemoveDays = Convert.ToInt64(ds.Tables(0).Rows(0)("PhotoRemoveDays").ToString.Trim)
                Catch ex As Exception
                    sSql = "ALTER TABLE iDMSSetting add PhotoRemoveDays int"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update iDMSSetting set PhotoRemoveDays=0"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                    PhotoRemoveDays = 0
                End Try


                'AllowPhoto
                sSql = "Select AllowPhoto from iDMSSetting"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    AllowPhoto = ds.Tables(0).Rows(0)("AllowPhoto").ToString.Trim
                Catch ex As Exception
                    sSql = "ALTER TABLE iDMSSetting add AllowPhoto varchar(1)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update iDMSSetting set AllowPhoto='Y'"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                    AllowPhoto = "Y"
                End Try


                'EventCheck
                sSql = "SELECT * FROM sys.tables WHERE name = 'EventCheck' AND type = 'U'"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    If ds.Tables(0).Rows.Count = 0 Then
                        sSql = "CREATE TABLE [dbo].[EventCheck]([SerialNo] [varchar](50) NULL,[EventDate] [datetime] NULL,[DeviceID] [varchar](50) NULL) ON [PRIMARY]"
                        con.Open()
                        Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        con.Close()
                    End If
                Catch ex As Exception
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End Try



                'UpdateFlag
                sSql = "Select top 1 UpdateFlag from Userdetail"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                Catch ex As Exception
                    sSql = "ALTER TABLE Userdetail add UpdateFlag char(1)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update Userdetail set UpdateFlag='O'"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End Try

                'IsDeleted
                sSql = "Select top 1 IsDeleted from Userdetail"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                Catch ex As Exception
                    sSql = "ALTER TABLE Userdetail add IsDeleted char(1)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    'sSql = "update Userdetail set UpdateFlag='O'"
                    'cmd = New SqlCommand(sSql, con)
                    'cmd.ExecuteNonQuery()
                    con.Close()
                End Try

                'LastLog
                Try
                    sSql = "If COL_LENGTH('schemaName.tblMachine', 'LastLog') IS NULL " &
                                    "BEGIN  ALTER TABLE tblMachine ADD LastLog datetime End"
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    Dim cmd As SqlCommand
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                Catch ex As Exception
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End Try


                'check CommandStatus
                sSql = "Select top 1 * from CommandStatus"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                Catch ex As Exception
                    Try
                        sSql = "create table CommandStatus (DeviceID varchar(50), CommandContent varchar(50), ProcessStatus varchar(max), UpdatedOn datetime)"
                        con.Open()
                        Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        con.Close()
                    Catch ex1 As Exception
                    End Try
                End Try
                'end check CommandStatus


                'check ActiveSerialNumber
                sSql = "Select * From sysobjects Where name ='ActiveSerialNumber'"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    If ds.Tables(0).Rows.Count = 0 Then
                        sSql = "CREATE TABLE [dbo].[ActiveSerialNumber](
                                [SerialNumber] [varchar](500) NOT NULL,
                                CONSTRAINT [PK_ActiveSerialNumber] PRIMARY KEY CLUSTERED 
                                (
                                [SerialNumber] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                ) ON [PRIMARY]"
                        con.Open()
                        Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()

                        sSql = "select SerialNumber from tblmachine"
                        ds = New DataSet
                        adap = New SqlDataAdapter(sSql, con)
                        adap.Fill(ds)
                        If ds.Tables(0).Rows.Count > 0 Then
                            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                                Try
                                    Dim EncSerial As String = EncyDcry.Encrypt(ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, True).Trim
                                    sSql = "Insert into ActiveSerialNumber (SerialNumber) values ('" & EncSerial & "')"
                                    cmd = New SqlCommand(sSql, con)
                                    cmd.ExecuteNonQuery()
                                Catch ex As Exception

                                End Try
                            Next
                        End If
                        con.Close()
                    End If
                Catch ex As Exception
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End Try
                'end check ActiveSerialNumber




                'dev_model
                sSql = "Select top 1 dev_model from tbl_fkdevice_status"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                Catch ex As Exception
                    sSql = "ALTER TABLE tbl_fkdevice_status add dev_model varchar(24)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End Try

                'dev_status_info
                sSql = "Select top 1 dev_status_info from tbl_fkdevice_status"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                Catch ex As Exception
                    sSql = "ALTER TABLE tbl_fkdevice_status add dev_status_info varchar(max)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End Try


                'AttProcess
                sSql = "Select AttProcess from iDMSSetting"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    AttProcess = ds.Tables(0).Rows(0)("AttProcess").ToString.Trim
                Catch ex As Exception
                    sSql = "ALTER TABLE iDMSSetting add AttProcess varchar(1)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update iDMSSetting set AttProcess='N'"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                    AttProcess = "N"
                End Try


                'QueryResult , ExecutedOn
                sSql = "Select top 1 QueryResult from DeviceCommands"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)

                Catch ex As Exception
                    sSql = "ALTER TABLE DeviceCommands add QueryResult varchar(1), ExecutedOn datetime"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End Try


                'SaveImage 
                sSql = "Select top 1 SaveImage from tblmachine"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                Catch ex As Exception
                    sSql = "ALTER TABLE tblmachine add SaveImage varchar(1)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update tblmachine set SaveImage='Y'"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End Try


                'AutoDownloadLogs
                sSql = "Select AutoDownloadLogs from iDMSSetting"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    AutoDownloadLogs = ds.Tables(0).Rows(0)("AutoDownloadLogs").ToString.Trim
                Catch ex As Exception
                    sSql = "ALTER TABLE iDMSSetting add AutoDownloadLogs varchar(1)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update iDMSSetting set AutoDownloadLogs='N'"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                    AutoDownloadLogs = "N"
                End Try


                'AutoDownloadLogsTime
                sSql = "Select AutoDownloadLogsTime from iDMSSetting"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    AutoDownloadLogsTime = ds.Tables(0).Rows(0)("AutoDownloadLogsTime").ToString.Trim
                Catch ex As Exception
                    sSql = "ALTER TABLE iDMSSetting add AutoDownloadLogsTime varchar(max)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update iDMSSetting set AutoDownloadLogsTime='01:00'"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                    AutoDownloadLogsTime = "01:00"
                End Try


                'DeviceType
                sSql = "Select DeviceType from tblMachine"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                Catch ex As Exception
                    sSql = "ALTER TABLE tblMachine add DeviceType varchar(50)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update tblMachine set DeviceType='Face'"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End Try


                'DumpUserAttendance
                sSql = "SELECT * FROM sys.tables WHERE name = 'DumpUserAttendance' AND type = 'U'"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    If ds.Tables(0).Rows.Count = 0 Then
                        sSql = "CREATE TABLE [dbo].[DumpUserAttendance](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceID] [varchar](50) NOT NULL,
	[UserID] [varchar](50) NOT NULL,
	[AttState] [varchar](50) NULL,
	[VerifyMode] [varchar](50) NULL,
	[WorkCode] [varchar](50) NULL,
	[AttDateTime] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[CTemp] [varchar](50) NULL,
	[FTemp] [varchar](50) NULL,
	[MaskStatus] [varchar](50) NULL,
	[IsAbnomal] [varchar](50) NULL,
	[io_workcode] [varchar](50) NULL,
	[verify_mode] [varchar](50) NULL,
	[io_mode] [varchar](50) NULL,
	[Upload] [bit] NULL,
	[UploadOn] [datetime] NULL,
 CONSTRAINT [PK_DumpUserAttendance_UserId] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[AttDateTime] ASC,
	[DeviceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
"
                        con.Open()
                        Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        con.Close()
                    End If
                Catch ex As Exception
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End Try


                'IsThermal 
                sSql = "Select top 1 IsThermal from tblmachine"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                Catch ex As Exception
                    sSql = "ALTER TABLE tblmachine add IsThermal varchar(1)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End Try


                'AllowSeconds
                sSql = "Select AllowSeconds from iDMSSetting"
                Try
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    Dim ds As DataSet = New DataSet
                    adap.Fill(ds)
                    AllowSeconds = ds.Tables(0).Rows(0)("AllowSeconds").ToString.Trim
                Catch ex As Exception
                    sSql = "ALTER TABLE iDMSSetting add AllowSeconds varchar(1)"
                    con.Open()
                    Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    sSql = "update iDMSSetting set AllowSeconds='N'"
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                    AllowSeconds = "N"
                End Try

            Catch ex As Exception
                Me.Hide()
                Exit Sub
            End Try
        End If
    End Sub
    Private Sub LoadActiveSerialNumber()
        Dim SrnoList As New List(Of String)()
        Dim con As SqlConnection = New SqlConnection(ConnectionString)
        Dim ds As DataSet
loadSerialNo: ds = New DataSet
        Dim sSql As String = "select SerialNumber from ActiveSerialNumber"
        Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
        adap.Fill(ds)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                SrnoList.Add(EncyDcry.Decrypt(ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, True).Trim)
                TraceServiceText(EncyDcry.Decrypt(ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, True).Trim)
            Next
            ActiveSerialNumber = SrnoList.Distinct.ToArray
        End If
    End Sub
#Region "member function region"
    Public Sub StopPreviewListen()
        'Dim i As Integer = 0
        'For i = 0 To GlobalDefinition.MAX_LISTEN_NUM - 1
        '    If g_struPreviewListen(i).lHandle <> -1 Then
        '        HCEHomeStream.NET_ESTREAM_StopListenPreview(g_struPreviewListen(i).lHandle)
        '        g_struPreviewListen(i).lHandle = -1
        '    End If
        'Next i
    End Sub
    Public Sub InitParamFromXML()
        Dim strTemp As String
        doc.Load("UltraiDMSValues.xml")
        Try
            Dim root As XmlNode = doc.DocumentElement.FirstChild
            m_struServInfo.dwKeepAliveSec = Integer.Parse(root.SelectSingleNode("//KeepAliveSeconds").InnerText)
            m_struServInfo.dwTimeOutCount = Integer.Parse(root.SelectSingleNode("//KeepAliveCount").InnerText)
            m_struServInfo.dwAlarmServerType = Integer.Parse(root.SelectSingleNode("//AlarmServerType").InnerText)
            strTemp = root.SelectSingleNode("//AlarmServerIP").InnerText 'm_csLocalIP
            StrTcpServIP = strTemp
            StrUdpServIP = strTemp
            strTemp.CopyTo(0, m_struServInfo.struTCPAlarmSever.szIP, 0, strTemp.Length)
            strTemp.CopyTo(0, m_struServInfo.struUDPAlarmSever.szIP, 0, strTemp.Length)
            m_struServInfo.struUDPAlarmSever.wPort = Short.Parse(root.SelectSingleNode("//AlarmServerUdpPort").InnerText)
            m_struServInfo.struTCPAlarmSever.wPort = Short.Parse(root.SelectSingleNode("//AlarmServerTcpPort").InnerText)
            m_wAlarmServerMqttPort = UShort.Parse(root.SelectSingleNode("//AlarmServerMqttPort").InnerText)
            m_byUseCmsPort = Integer.Parse(root.SelectSingleNode("//AlarmServerPortUseCms").InnerText)
            strTemp = root.SelectSingleNode("//NTPServerIP").InnerText
            strTemp.CopyTo(0, m_struServInfo.struNTPSever.szIP, 0, strTemp.Length)
            m_struServInfo.struNTPSever.wPort = Short.Parse(root.SelectSingleNode("//NTPServerPort").InnerText)
            m_struServInfo.dwNTPInterval = Integer.Parse(root.SelectSingleNode("//NTPInterval").InnerText)

            m_struServInfo.dwPicServerType = Integer.Parse(root.SelectSingleNode("//PictureServerType").InnerText)
            strTemp = root.SelectSingleNode("//PictureServerIP").InnerText ' m_csLocalIP
            StrPicServIp = strTemp
            PicServerip = strTemp
            strTemp.CopyTo(0, m_struServInfo.struPictureSever.szIP, 0, strTemp.Length)
            m_struServInfo.struPictureSever.wPort = Short.Parse(root.SelectSingleNode("//PictureServerPort").InnerText) '8898 '10003 '
            PicServerPort = Integer.Parse(root.SelectSingleNode("//PictureServerPort").InnerText)  '8898 '10003 ' 

            m_byCmsSecureAccessType = Convert.ToByte(root.SelectSingleNode("//CmsAccessSecurity").InnerText)
            m_byAlarmSecureAccessType = Convert.ToByte(root.SelectSingleNode("//AlarmAccessSecurity").InnerText)
            m_byStreamSecureAccessType = Convert.ToByte(root.SelectSingleNode("//StreamAccessSecurity").InnerText)

        Catch ep As Exception
            'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 0, ep.ToString())
        End Try
    End Sub
    Protected Overrides Sub DefWndProc(ByRef m As Message)
        Try
            If m.Msg = GlobalDefinition.WM_PROC_EXCEPTION Then
                Dim iHandle As Integer = DirectCast(Marshal.PtrToStructure(m.LParam, GetType(Integer)), Integer)
                If Not HCEHomeStream.NET_ESTREAM_StopPreview(iHandle) Then
                    'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 2, "OnWMProcException NET_ESTREAM_StopPreview failed")
                End If
            End If
            MyBase.DefWndProc(m)
        Catch ex As Exception

        End Try
    End Sub
    '}
    Public Sub fnPREVIEW_EXCEPTION_CB(ByVal dwType As Integer, ByVal iUserID As Integer, ByVal iHandle As Integer, ByVal pUser As IntPtr)
        Try
            If HCEHomeStream.EHOME_PREVIEW_EXCEPTION = dwType Then
                Dim str As String = String.Format("Preview exception, handle={0}", iHandle)
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 2, str)
                Dim pUserId As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(GetType(IntPtr)))
                Dim pHandle As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(GetType(IntPtr)))
                Marshal.StructureToPtr(iUserID, pUserId, False)
                PostMessage(m_hWnd, GlobalDefinition.WM_PROC_EXCEPTION, pUserId, pHandle)
            End If
        Catch ex As Exception

        End Try
    End Sub
    'public void StartPreviewListen()
    '{
    '    HCEHomeStream.NET_ESTREAM_SetExceptionCallBack(0, 0, fnPREVIEW_EXCEPTION_CB, m_hWnd);
    '}

    'Public Sub InitChildWindow()
    '    For i As Integer = 0 To GlobalDefinition.MAX_OUTPUTS - 1
    '        DeviceTree.m_PreviewPanel(i).m_cslocalIP = m_csLocalIP
    '        DeviceTree.m_PreviewPanel(i).m_nPort = m_nPort
    '    Next i
    '    DeviceTree.m_PreviewPanel(0).m_hWnd = Me.m_previewPanelOne.Handle
    '    DeviceTree.m_PreviewPanel(1).m_hWnd = Me.m_previewPanelTwo.Handle
    '    DeviceTree.m_PreviewPanel(2).m_hWnd = Me.m_previewPanelThree.Handle
    '    DeviceTree.m_PreviewPanel(3).m_hWnd = Me.m_previewPanelFour.Handle
    'End Sub
    Private Function SS_Message_Callback(ByVal iHandle As Integer, ByVal enumType As HCEHomeSS.NET_EHOME_SS_MSG_TYPE, ByVal pOutBuffer As IntPtr, ByVal dwOutLen As Integer, ByVal pInBuffer As IntPtr, ByVal dwInLen As Integer, ByVal pUser As IntPtr) As Boolean
        Try
            If enumType = HCEHomeSS.NET_EHOME_SS_MSG_TYPE.NET_EHOME_SS_MSG_TOMCAT Then
                Dim pTomcatMsg As HCEHomeSS.NET_EHOME_SS_TOMCAT_MSG = DirectCast(Marshal.PtrToStructure(pOutBuffer, GetType(HCEHomeSS.NET_EHOME_SS_TOMCAT_MSG)), HCEHomeSS.NET_EHOME_SS_TOMCAT_MSG)
                Dim szPicUri((4 * PIC_URI_LEN) - 1) As Char

                For i As Integer = 0 To pTomcatMsg.dwPicNum - 1
                    Dim temp As String = Marshal.PtrToStringAnsi(pTomcatMsg.pPicURLs)
                    temp &= (i * HCEHomeSS.MAX_URL_LEN_SS).ToString()
                    temp.CopyTo(0, szPicUri, i * PIC_URI_LEN, PIC_URI_LEN)
                Next i
                If Not File.Exists(SS_PATH) Then
                    File.Create(SS_PATH)
                End If
                If Not File.Exists(SS_MESSAGE_PATH) Then
                    File.Create(SS_MESSAGE_PATH)
                End If

                Dim strFilePath As String = SS_MESSAGE_PATH & "\tomcatOutput.txt"
                Dim byPicUri((4 * PIC_URI_LEN) - 1) As Byte
                Array.Copy(szPicUri, 0, byPicUri, 0, 4 * PIC_URI_LEN)
                Dim line As String = vbLf
                Dim byline() As Byte = System.Text.Encoding.Default.GetBytes(line)
                Using fs As New FileStream(strFilePath, FileMode.Create, FileAccess.Write)
                    Try
                        For i As Integer = 0 To pTomcatMsg.dwPicNum - 1
                            fs.Write(byPicUri, i * PIC_URI_LEN, PIC_URI_LEN)
                            fs.Write(byline, 0, byline.Length)
                        Next i
                        fs.Flush()
                        fs.Close()
                    Catch e As Exception
                        'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 0, e.ToString())
                    End Try
                End Using
            ElseIf enumType = HCEHomeSS.NET_EHOME_SS_MSG_TYPE.NET_EHOME_SS_MSG_KMS_USER_PWD Then
                Marshal.WriteByte(pInBuffer, 1)
            ElseIf enumType = HCEHomeSS.NET_EHOME_SS_MSG_TYPE.NET_EHOME_SS_MSG_CLOUD_AK Then
                Dim temp(127) As Byte
                Marshal.Copy(pOutBuffer, temp, 0, dwOutLen)
                For i As Integer = 0 To MAX_DEVICES - 1
                    If DeviceTree.g_struDeviceInfo(i).iDeviceIndex <> -1 Then
                        If DeviceTree.isByteEqual(DeviceTree.g_struDeviceInfo(i).byDeviceID, temp, 64) Then
                            Marshal.Copy(DeviceTree.g_struDeviceInfo(i).byClouldSecretKey, 0, pInBuffer, 64)
                            Exit For
                        End If
                    End If
                Next i
            End If
        Catch ex As Exception

        End Try
        Return True

    End Function
    '存储回调
    'Public Function SS_Storage_Callback(ByVal iHandle As Integer, ByVal pFileName As IntPtr, ByVal pFileBuf As IntPtr, ByVal dwFileLen As Integer, ByRef pFilePath As IntPtr, ByVal pUser As IntPtr) As Boolean
    '    Dim strFilename As String = Marshal.PtrToStringAnsi(pFileName)
    '    If "" = strFilename OrElse IntPtr.Zero = pFileBuf OrElse 0 = dwFileLen Then
    '        Return False
    '    End If
    '    'if(!File.Exists(SS_PATH))
    '    '{
    '    '    File.Create(SS_PATH);
    '    '}
    '    'if(!File.Exists(SS_STORAGE_PATH))
    '    '{
    '    '    File.Create(SS_STORAGE_PATH);
    '    '}

    '    If False = System.IO.Directory.Exists(SS_PATH) Then
    '        System.IO.Directory.CreateDirectory(SS_PATH)
    '    End If
    '    If False = System.IO.Directory.Exists(SS_STORAGE_PATH) Then
    '        System.IO.Directory.CreateDirectory(SS_STORAGE_PATH)
    '    End If

    '    strFilename.Replace(":"c, "_"c)
    '    'string Filepath = SS_STORAGE_PATH + "\\" + strFilename;
    '    Dim Filepath As String = SS_STORAGE_PATH & "/" & strFilename
    '    Try
    '        Using fs As New FileStream(Filepath, FileMode.OpenOrCreate, FileAccess.ReadWrite)
    '            Dim DataStream(dwFileLen - 1) As Byte
    '            Marshal.Copy(pFileBuf, DataStream, 0, dwFileLen)
    '            fs.Write(DataStream, 0, dwFileLen)
    '            fs.Flush()
    '            fs.Close()
    '        End Using
    '    Catch e As Exception
    '        'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 3, e.ToString())
    '    End Try
    '    Dim pPath As New HCEHomeSS.PicPath()
    '    pPath.Init()


    '    'Convert Filepath to byte []
    '    pPath.Path = System.Text.Encoding.Default.GetBytes(Filepath.PadRight(100, ControlChars.NullChar))
    '    pFilePath = Marshal.AllocHGlobal(Marshal.SizeOf(pPath))
    '    Marshal.StructureToPtr(pPath, pFilePath, True)

    '    'char[] cFilePath = Filepath.ToCharArray();
    '    'Marshal.Copy(cFilePath, 0, pFilePath, cFilePath.Length);
    '    Return True
    'End Function
    Public Function SS_Storage_Callback(ByVal iHandle As Integer, ByVal pFileName As IntPtr, ByVal pFileBuf As IntPtr, ByVal dwFileLen As Integer, ByVal pFilepath As IntPtr, ByVal pUser As IntPtr) As Boolean
        Try
            Dim strFilename As String = Marshal.PtrToStringAnsi(pFileName)
            Dim user As String = Marshal.PtrToStringAnsi(pUser)
            Dim filep As String = Marshal.PtrToStringAnsi(pFilepath)
            If "" = strFilename OrElse IntPtr.Zero = pFileBuf OrElse 0 = dwFileLen Then
                Return False
            End If
            If False = System.IO.Directory.Exists(SS_PATH) Then
                System.IO.Directory.CreateDirectory(SS_PATH)
            End If
            If False = System.IO.Directory.Exists(SS_STORAGE_PATH) Then
                System.IO.Directory.CreateDirectory(SS_STORAGE_PATH)
            End If
            strFilename.Replace(":"c, "_"c)

            Dim KMSTemp As String = "/home/config/pic/enrlFace"
            If strFilename.Contains("/home/config/pic/enrlFace") Then
                Dim KMSIndex As Integer = strFilename.IndexOf(KMSTemp)
                strFilename = strFilename.Substring(0, KMSIndex)
            ElseIf (strFilename.Contains(".jpg") OrElse strFilename.Contains(".png")) AndAlso Not (strFilename.Contains("_00.jpg")) Then
                'capture Face Through KMS Protocol，Filter .jpg or .png as well as DateTime  in Pic Name
                'in case of FileName is too long,
                strFilename = strFilename.Substring(0, strFilename.Length - 18)
            ElseIf strFilename.Contains("_00.jpg") Then
                strFilename = strFilename.Substring(0, strFilename.Length - 21)
            End If
            Dim Filepath As String = SS_STORAGE_PATH & "\" & strFilename
            If dwFileLen > 10000 Then 'added to delete tharmal image
                Try
                    Using fs As New FileStream(Filepath, FileMode.OpenOrCreate, FileAccess.Read Or FileAccess.Write)
                        Dim DataStream(dwFileLen - 1) As Byte
                        Marshal.Copy(pFileBuf, DataStream, 0, dwFileLen)
                        fs.Write(DataStream, 0, dwFileLen)
                        fs.Flush()
                        fs.Close()
                    End Using
                Catch e As Exception
                    'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 3, e.ToString())
                End Try
            End If
            Dim pPath As New HCEHomeSS.PicPath()
            pPath.Init()
            'Convert Filepath to byte []
            pPath.Path = System.Text.Encoding.Default.GetBytes(Filepath.PadRight(100, ControlChars.NullChar))
            Dim DemoPath As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(pPath))
            Marshal.StructureToPtr(pPath, pFilepath, False)
        Catch ex As Exception

        End Try
        Return True
    End Function
    Private Sub GetAddressByType(ByVal iType As Integer, ByVal dwVersion As Integer, ByRef pOutValue As IntPtr, ByVal dwOutLen As Integer, ByRef pSubValue As Integer, ByVal dwSubLen As Integer)
        Try
            Dim iMapType As Integer = -1
            Try
                Dim root As XmlNode = doc.SelectSingleNode("//AddressMap")
                iMapType = Integer.Parse(root.SelectSingleNode("//Enable").InnerText)
                If iMapType <> 1 AndAlso iMapType <> -1 AndAlso iMapType <> 0 Then
                    iMapType = -1
                ElseIf iMapType = 1 Then
                    iMapType = 1
                Else
                    iMapType = -1
                End If
            Catch

            End Try

            If DevOnServerAdd = iType Then
                FormatRegAddress(pOutValue, dwOutLen, dwVersion, iMapType, doc)
            ElseIf StreamAdd = iType Then
                GetStreamAddress(pOutValue, dwOutLen, pSubValue, dwSubLen, iMapType, doc)
            ElseIf AudioAdd = iType Then
                GetAudioAddress(pOutValue, dwOutLen, pSubValue, dwSubLen, iMapType, doc)
            ElseIf DASAdd = iType Then
                GetDasAddress(pOutValue, dwOutLen, pSubValue, dwSubLen, iMapType, doc)
            ElseIf PSSAdd = iType Then
                GetPicServerAddress(pOutValue, dwOutLen, pSubValue, dwSubLen, iMapType, doc)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ClearCharArr(ByRef source() As Char)
        For i As Integer = 0 To source.Length - 1
            Try
                'source(i) = "0"c
                source(i) = ControlChars.NullChar
            Catch ex As Exception

            End Try
        Next i
    End Sub
    Private Sub FormatRegAddress(ByRef pOutValue As IntPtr, ByVal dwOutLen As Integer, ByVal dwVersion As Integer, ByVal iMapType As Integer, ByRef doc As XmlDocument)
        Try
            Dim root As XmlNode = doc.SelectSingleNode("//AddressMap")
            Dim struServInfo As New HCEHomeCMS.NET_EHOME_SERVER_INFO_V50()
            struServInfo.Init()
            struServInfo = DirectCast(Marshal.PtrToStructure(pOutValue, GetType(HCEHomeCMS.NET_EHOME_SERVER_INFO_V50)), HCEHomeCMS.NET_EHOME_SERVER_INFO_V50)
            struServInfo = m_struServInfo
            Dim strTemp As String = Nothing
            If 5 = dwVersion Then
                struServInfo.struTCPAlarmSever.wPort = CShort(m_wAlarmServerMqttPort)
            End If

            If 1 = iMapType Then
                strTemp = doc.SelectSingleNode("//AddressMap//AlarmServerIP").InnerText
                ClearCharArr(struServInfo.struTCPAlarmSever.szIP)
                ClearCharArr(struServInfo.struUDPAlarmSever.szIP)
                strTemp.CopyTo(0, struServInfo.struTCPAlarmSever.szIP, 0, strTemp.Length)
                strTemp.CopyTo(0, struServInfo.struUDPAlarmSever.szIP, 0, strTemp.Length)
                'System.Buffer.BlockCopy(strTemp.ToCharArray(), 0, struServInfo.struTCPAlarmSever.szIP, 0, strTemp.ToCharArray().Length);
                'System.Buffer.BlockCopy(strTemp.ToCharArray(), 0, struServInfo.struUDPAlarmSever.szIP, 0, strTemp.ToCharArray().Length);

                struServInfo.struUDPAlarmSever.wPort = Short.Parse(doc.SelectSingleNode("//AddressMap//AlarmServerUdpPort").InnerText)
                struServInfo.struTCPAlarmSever.wPort = Short.Parse(doc.SelectSingleNode("//AddressMap//AlarmServerTcpPort").InnerText)

                If 5 = dwVersion Then
                    struServInfo.struTCPAlarmSever.wPort = Short.Parse(doc.SelectSingleNode("//AddressMap//AlarmServerMqttPort").InnerText)
                End If

                Dim strPicIp As String = doc.SelectSingleNode("//AddressMap//PictureServerIP").InnerText.ToString()

                'System.Buffer.BlockCopy(strPicIp.ToCharArray(), 0, struServInfo.struPictureSever.szIP, 0, strPicIp.ToCharArray().Length);
                ClearCharArr(struServInfo.struPictureSever.szIP)
                strPicIp.CopyTo(0, struServInfo.struPictureSever.szIP, 0, strPicIp.Length)

                struServInfo.struPictureSever.wPort = Short.Parse(doc.SelectSingleNode("//AddressMap//PictureServerPort").InnerText)
            End If

            Dim strptrTest As New String(struServInfo.struTCPAlarmSever.szIP, 0, 128)
            Dim ptrTest As IntPtr = CType(Marshal.StringToHGlobalAnsi(strptrTest), IntPtr)
            If IsNULLAddress(ptrTest) Then
                Dim LocalIP As String = m_csLocalIP 'm_csLocalIP
                System.Buffer.BlockCopy(LocalIP.ToCharArray(), 0, struServInfo.struTCPAlarmSever.szIP, 0, LocalIP.Length)

            End If
            Marshal.FreeHGlobal(ptrTest)

            strptrTest = New String(struServInfo.struUDPAlarmSever.szIP, 0, 128)
            ptrTest = CType(Marshal.StringToHGlobalAnsi(strptrTest), IntPtr)
            If IsNULLAddress(ptrTest) Then
                Dim LocalIP As String = m_csLocalIP 'm_csLocalIP
                System.Buffer.BlockCopy(LocalIP.ToCharArray(), 0, struServInfo.struUDPAlarmSever.szIP, 0, LocalIP.Length)
            End If
            Marshal.FreeHGlobal(ptrTest)

            Marshal.StructureToPtr(struServInfo, pOutValue, False)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetStreamAddress(ByRef pOutValue As IntPtr, ByVal dwOutLen As Integer, ByRef pSubValue As Integer, ByVal dwSubLen As Integer, ByVal iMapType As Integer, ByRef doc As XmlDocument)
        Try
            If -1 = iMapType OrElse 0 = iMapType Then

                pOutValue = Marshal.StringToHGlobalAnsi(m_csLocalIP)
            Else
                Dim StreamIP As String = doc.SelectSingleNode("//StreamServerIP").InnerText.ToString()
                Dim TempByte((StreamIP.Length * Len(New Char())) - 1) As Char
                System.Buffer.BlockCopy(StreamIP.ToCharArray(), 0, TempByte, 0, TempByte.Length)
                Marshal.Copy(TempByte, 0, pOutValue, TempByte.Length)

                Dim ServerPort As Integer = Integer.Parse(doc.SelectSingleNode("//StreamServerPort").InnerText)
                pSubValue = ServerPort
            End If
            If IsNULLAddress(pOutValue) Then
                Dim LocalIP As String = m_csLocalIP
                Dim IPtemp((LocalIP.Length * Len(New Char())) - 1) As Char
                System.Buffer.BlockCopy(LocalIP.ToCharArray(), 0, IPtemp, 0, LocalIP.Length)
                Marshal.Copy(IPtemp, 0, pOutValue, IPtemp.Length)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Function IsNULLAddress(ByVal pOutValue As IntPtr) As Boolean
        Try
            Dim temp As String = Marshal.PtrToStringAnsi(pOutValue)
            If 0 = temp.Length OrElse temp = "0.0.0.0" Then
                Return True
            End If
            Return False
        Catch ex As Exception

        End Try
    End Function
    Private Sub GetAudioAddress(ByRef pOutValue As IntPtr, ByVal dwOutLen As Integer, ByRef pSubValue As Integer, ByVal dwSubLen As Integer, ByVal iMapType As Integer, ByRef doc As XmlDocument)
        Try
            If -1 = iMapType OrElse 0 = iMapType Then
                pOutValue = Marshal.StringToHGlobalAnsi(m_csLocalIP)
            Else
                Dim AudioIP As String = doc.SelectSingleNode("//AudioServerIP").InnerText.ToString()
                Dim TempByte((AudioIP.Length * Len(New Char())) - 1) As Char
                System.Buffer.BlockCopy(AudioIP.ToCharArray(), 0, TempByte, 0, TempByte.Length)
                Marshal.Copy(TempByte, 0, pOutValue, TempByte.Length)

                Dim ServerPort As Integer = Integer.Parse(doc.SelectSingleNode("//AudioServerPort").InnerText)
                pSubValue = ServerPort
            End If

            If IsNULLAddress(pOutValue) Then
                Dim LocalIP As String = m_csLocalIP
                Dim IPtemp((LocalIP.Length * Len(New Char())) - 1) As Char
                System.Buffer.BlockCopy(LocalIP.ToCharArray(), 0, IPtemp, 0, LocalIP.Length)
                Marshal.Copy(IPtemp, 0, pOutValue, IPtemp.Length)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetDasAddress(ByRef pOutValue As IntPtr, ByVal dwOutLen As Integer, ByRef pSubValue As Integer, ByVal dwSubLen As Integer, ByVal iMapType As Integer, ByRef doc As XmlDocument)
        Try
            If -1 = iMapType OrElse 0 = iMapType Then
                pOutValue = Marshal.StringToHGlobalAnsi(m_csLocalIP)
            Else
                Dim DASIP As String = doc.SelectSingleNode("//DASIP").InnerText.ToString()
                'char[] TempByte = new char[DASIP.Length * sizeof(char)];
                'System.Buffer.BlockCopy(DASIP.ToCharArray(), 0, TempByte, 0, TempByte.Length);
                'Marshal.Copy(TempByte, 0, pOutValue, TempByte.Length);

                pOutValue = Marshal.StringToHGlobalAnsi(DASIP)
                Dim ServerPort As Integer = Integer.Parse(doc.SelectSingleNode("//DASPort").InnerText)
                pSubValue = ServerPort
            End If

            If IsNULLAddress(pOutValue) Then
                Dim LocalIP As String = m_csLocalIP
                Dim IPtemp((LocalIP.Length * Len(New Char())) - 1) As Char
                System.Buffer.BlockCopy(LocalIP.ToCharArray(), 0, IPtemp, 0, LocalIP.Length)
                Marshal.Copy(IPtemp, 0, pOutValue, IPtemp.Length)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetPicServerAddress(ByRef pOutValue As IntPtr, ByVal dwOutLen As Integer, ByRef pSubValue As Integer, ByVal dwSubLen As Integer, ByVal iMapType As Integer, ByRef doc As XmlDocument)
        Try
            If -1 = iMapType OrElse 0 = iMapType Then
                Dim LocalIP As String = m_csLocalIP
                Dim IPtemp(LocalIP.Length - 1) As Byte
                StrTobyte(IPtemp, LocalIP)
                Marshal.Copy(IPtemp, 0, pOutValue, IPtemp.Length)
            Else
                Dim strPicIp As String = doc.SelectSingleNode("//AddressMap//PictureServerIP").InnerText.ToString()
                'char[] TempByte = new char[strPicIp.Length * sizeof(char)];
                'System.Buffer.BlockCopy(strPicIp.ToCharArray(), 0, TempByte, 0, TempByte.Length);
                Dim TempByte(strPicIp.Length - 1) As Byte
                StrTobyte(TempByte, strPicIp)
                'pOutValue = Marshal.StringToHGlobalAnsi(strPicIp);
                Marshal.Copy(TempByte, 0, pOutValue, strPicIp.Length)

                Dim ServerPort As Integer = Integer.Parse(doc.SelectSingleNode("//AddressMap//PictureServerPort").InnerText)
                pSubValue = ServerPort
            End If

            If IsNULLAddress(pOutValue) Then
                Dim LocalIP As String = m_csLocalIP
                Dim IPtemp(LocalIP.Length - 1) As Byte
                StrTobyte(IPtemp, LocalIP)
                Marshal.Copy(IPtemp, 0, pOutValue, IPtemp.Length)
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub StrTobyte(ByRef des() As Byte, ByVal source As String)
        Try
            If source.Length <= 0 Then
                Return
            End If
            For i As Integer = 0 To source.Length - 1
                des(i) = AscW(source.Chars(i))
            Next i
        Catch ex As Exception

        End Try
    End Sub

    Public Sub InitLib()
        Try
            Dim csLogPath As String = System.Environment.CurrentDirectory & "\EHomeSdkLog\" ' "C:/EHomeSdkLog/"
            'Dim szLogPath(127) As Char
            Dim szLogPath(csLogPath.Length - 1) As Char
            csLogPath.CopyTo(0, szLogPath, 0, csLogPath.Length)
            Dim dwsize As Integer
            If g_bSS_Enable Then
                HCEHomeSS.NET_ESS_Init()
                HCEHomeSS.NET_ESS_SetLogToFile(3, szLogPath, True)
                If m_bHttps Then
                    Dim struHttpsParam As New HCEHomeSS.NET_EHOME_SS_LISTEN_HTTPS_PARAM()
                    struHttpsParam.Init()
                    struHttpsParam.byHttps = 1
                    struHttpsParam.byCertificateFileType = 0
                    struHttpsParam.byPrivateKeyFileType = 0

                    struHttpsParam.szUserCertificateFile = System.Environment.CurrentDirectory & "\Certificate\server.pem"
                    struHttpsParam.szUserPrivateKeyFile = System.Environment.CurrentDirectory & "\Certificate\server.pem"
                    struHttpsParam.dwSSLVersion = 0
                    HCEHomeSS.NET_ESS_SetListenHttpsParam(struHttpsParam)
                End If

                Dim struPublicAddress As New HCEHomePublic.NET_EHOME_IPADDRESS()
                struPublicAddress.Init()

                Dim PtrSzIp As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struPublicAddress))
                Marshal.StructureToPtr(struPublicAddress, PtrSzIp, False)
                Dim port As Integer = 0
                'INSTANT VB NOTE: The variable size was renamed since Visual Basic does not handle local variables named the same as class members well:
                Dim size_Conflict As Integer = Marshal.SizeOf(struPublicAddress.wPort)
                GetAddressByType(4, 0, PtrSzIp, 128, port, size_Conflict)

                struPublicAddress = DirectCast(Marshal.PtrToStructure(PtrSzIp, GetType(HCEHomePublic.NET_EHOME_IPADDRESS)), HCEHomePublic.NET_EHOME_IPADDRESS)

                struPublicAddress.wPort = Short.Parse(port.ToString())
                If 0 = struPublicAddress.wPort Then
                    struPublicAddress.wPort = m_struServInfo.struPictureSever.wPort
                End If
                Array.Copy(m_struServInfo.struPictureSever.szIP, 0, struPublicAddress.szIP, 0, 128) 'original
                'Array.Copy(struPublicAddress.szIP, 0, m_struServInfo.struPictureSever.szIP, 0, 128) 'nitin

                dwsize = Marshal.SizeOf(struPublicAddress)
                Dim ptrstruPublicAddress As IntPtr = Marshal.AllocHGlobal(dwsize)
                Marshal.StructureToPtr(struPublicAddress, ptrstruPublicAddress, False)
                HCEHomeSS.NET_ESS_SetSDKInitCfg(HCEHomeSS.NET_EHOME_SS_INIT_CFG_TYPE.NET_EHOME_SS_INIT_CFG_PUBLIC_IP_PORT, ptrstruPublicAddress)
                Marshal.FreeHGlobal(ptrstruPublicAddress)

                Dim struSSListenParam As New HCEHomeSS.NET_EHOME_SS_LISTEN_PARAM()
                struSSListenParam.Init()
                struSSListenParam.struAddress.Init()
                m_csLocalIP.CopyTo(0, struSSListenParam.struAddress.szIP, 0, m_csLocalIP.Length)
                struSSListenParam.struAddress.wPort = m_struServInfo.struPictureSever.wPort
                struSSListenParam.szKMS_UserName = "test"
                struSSListenParam.szKMS_Password = "12345"
                struSSListenParam.szAccessKey = "test"
                struSSListenParam.szSecretKey = "12345"
                '将安全模式关闭
                struSSListenParam.bySecurityMode = 1

                SSMsgCallBack = New HCEHomeSS.EHomeSSMsgCallBack(AddressOf SS_Message_Callback)
                struSSListenParam.fnSSMsgCb = SSMsgCallBack
                ptrSSMsgCallBack = SSMsgCallBack

                struSSListenParam.fnSSRWCbEx = Nothing

                StroageCallBack = New HCEHomeSS.EHomeSSStorageCallBack(AddressOf SS_Storage_Callback)
                struSSListenParam.fnSStorageCb = StroageCallBack
                ptrStroageCallBack = StroageCallBack

                struSSListenParam.pUserData = Me.Handle

                m_lSSHandle = HCEHomeSS.NET_ESS_StartListen(struSSListenParam)
                If -1 = m_lSSHandle Then
                    'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 3, "NET_ESS_StartListen Failed, port" & struSSListenParam.struAddress.wPort.ToString())
                Else
                    'g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 3, "NET_ESS_StartListen SUCC, port" & struSSListenParam.struAddress.wPort.ToString())
                End If
            End If
            Dim bRet As Boolean = False
            bRet = HCEHomeAlarm.NET_EALARM_Init()
            m_struAccessSecure.dwSize = CUInt(Marshal.SizeOf(m_struAccessSecure))
            m_struAccessSecure.byAccessSecurity = m_byAlarmSecureAccessType
            Dim ptrAccessSecure As IntPtr = Marshal.AllocHGlobal(CInt(m_struAccessSecure.dwSize))
            Marshal.StructureToPtr(m_struAccessSecure, ptrAccessSecure, False)

            If Not HCEHomeAlarm.NET_EALARM_SetSDKLocalCfg(HCEHomePublic.NET_EHOME_LOCAL_CFG_TYPE.ACTIVE_ACCESS_SECURITY, ptrAccessSecure) Then
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 2, "NET_EALARM_SetSDKLocalCfg ACTIVE_ACCESS_SECURITY Failed")
            Else
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_EALARM_SetSDKLocalCfg ACTIVE_ACCESS_SECURITY Success")
            End If

            If 1 = m_struServInfo.dwAlarmServerType Then
                Dim struTcpAlarmListenParam As New HCEHomeAlarm.NET_EHOME_ALARM_LISTEN_PARAM()
                struTcpAlarmListenParam.Init()
                Dim LocalIP As String = m_csLocalIP 'SelectIP.IpAddressList(0).ToString()
                LocalIP.CopyTo(0, struTcpAlarmListenParam.struAddress.szIP, 0, LocalIP.Length)
                struTcpAlarmListenParam.byProtocolType = 0
                struTcpAlarmListenParam.struAddress.wPort = m_struServInfo.struTCPAlarmSever.wPort
                ptrTcpAlarm = New HCEHomeAlarm.EHomeMsgCallBack(AddressOf AlarmMsgCallBack)
                struTcpAlarmListenParam.fnMsgCb = ptrTcpAlarm
                struTcpAlarmListenParam.pUserData = Me.Handle
                m_lTcpAlarmHandle = HCEHomeAlarm.NET_EALARM_StartListen(struTcpAlarmListenParam)
            End If

            Dim struAlarmListenParam As New HCEHomeAlarm.NET_EHOME_ALARM_LISTEN_PARAM()
            struAlarmListenParam.struAddress.Init()
            m_csLocalIP.CopyTo(0, struAlarmListenParam.struAddress.szIP, 0, m_csLocalIP.Length)
            ptrMqtt = New HCEHomeAlarm.EHomeMsgCallBack(AddressOf AlarmMsgCallBack)
            struAlarmListenParam.fnMsgCb = ptrMqtt
            struAlarmListenParam.pUserData = Me.Handle
            struAlarmListenParam.byUseCmsPort = 0
            struAlarmListenParam.byUseThreadPool = 0

            struAlarmListenParam.byProtocolType = 1
            struAlarmListenParam.struAddress.wPort = m_struServInfo.struUDPAlarmSever.wPort
            struAlarmListenParam.byUseCmsPort = CByte(m_byUseCmsPort)
            m_lUdpAlarmHandle = HCEHomeAlarm.NET_EALARM_StartListen(struAlarmListenParam)



            struAlarmListenParam.byUseCmsPort = 0
            struAlarmListenParam.byProtocolType = 2
            struAlarmListenParam.struAddress.wPort = CShort(m_wAlarmServerMqttPort)
            m_lEhome50AlarmHandle = HCEHomeAlarm.NET_EALARM_StartListen(struAlarmListenParam)

            'New
            If m_lEhome50AlarmHandle <> -1 Then
                CMSAlarm.m_lEhome50AlarmHandle = m_lEhome50AlarmHandle
            End If
            'New End

            If m_wAlarmServerMqttPort = m_struServInfo.struTCPAlarmSever.wPort Then

                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 0, "Conflict between 2.0/4.0 TCP alarm port and 5.0 alarm port !")
            End If
            If -1 = m_lUdpAlarmHandle Then
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 3, "NET_EALARM_StartListen Failed, port" & struAlarmListenParam.struAddress.wPort.ToString())
            Else
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 3, "NET_EALARM_StartListen succ, port" & struAlarmListenParam.struAddress.wPort.ToString())
            End If

            HCEHomeAlarm.NET_EALARM_SetLogToFile(3, szLogPath, True)
            HCEHomeAlarm.NET_EALARM_GetBuildVersion()


            bRet = HCEHomeCMS.NET_ECMS_Init()
            If Not bRet Then
                Dim str As String = String.Format("{0}", CInt(Math.Truncate(HCEHomeCMS.NET_ECMS_GetLastError())))
                Console.WriteLine("NET_ECMS_Init failed, err={0}", CInt(Math.Truncate(HCEHomeCMS.NET_ECMS_GetLastError())))
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_Init Failed")
            End If

            csLogPath.CopyTo(0, szLogPath, 0, csLogPath.Length)
            bRet = HCEHomeCMS.NET_ECMS_SetLogToFile(3, szLogPath, True)

            m_struAccessSecure.byAccessSecurity = m_byCmsSecureAccessType
            Marshal.StructureToPtr(m_struAccessSecure, ptrAccessSecure, False)
            If Not HCEHomeCMS.NET_ECMS_SetSDKLocalCfg(HCEHomePublic.NET_EHOME_LOCAL_CFG_TYPE.ACTIVE_ACCESS_SECURITY, ptrAccessSecure) Then
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_SetSDKLocalCfg ACTIVE_ACCESS_SECURITY Failed")
                Marshal.FreeHGlobal(ptrAccessSecure)
                Return
            Else
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_SetSDKLocalCfg ACTIVE_ACCESS_SECURITY Succeed")
            End If


            m_struAmsAddr.struAddress.Init()
            m_csLocalIP.CopyTo(0, m_struAmsAddr.struAddress.szIP, 0, m_csLocalIP.Length)
            m_struAmsAddr.struAddress.wPort = m_struServInfo.struUDPAlarmSever.wPort
            m_struAmsAddr.byEnable = CByte(m_byUseCmsPort)
            m_struAmsAddr.dwSize = CInt(Marshal.SizeOf(m_struAmsAddr))
            Dim ptrAmsAddr As IntPtr = Marshal.AllocHGlobal(CInt(m_struAmsAddr.dwSize))
            Marshal.StructureToPtr(m_struAmsAddr, ptrAmsAddr, False)

            If Not HCEHomeCMS.NET_ECMS_SetSDKLocalCfg(HCEHomePublic.NET_EHOME_LOCAL_CFG_TYPE.AMS_ADDRESS, ptrAmsAddr) Then
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 1, "open cms-alarm Failed 7333")
            Else
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 1, "open cms-alarm  Succ 7333")
            End If
            Marshal.FreeHGlobal(ptrAmsAddr)


            m_struSendParam.dwRecvTimeOut = 0
            m_struSendParam.bySendTimes = 2
            m_struSendParam.byRes2 = New Byte(126) {}
            m_struSendParam.dwSize = CInt(Marshal.SizeOf(m_struSendParam))
            Dim ptrSendParam As IntPtr = Marshal.AllocHGlobal(m_struSendParam.dwSize)
            Marshal.StructureToPtr(m_struSendParam, ptrSendParam, False)

            If Not HCEHomeCMS.NET_ECMS_SetSDKLocalCfg(HCEHomePublic.NET_EHOME_LOCAL_CFG_TYPE.SEND_PARAM, ptrSendParam) Then
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_SetSDKLocalCfg SEND_PARAM failed")
            Else
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_SetSDKLocalCfg SEND_PARAM succ")
            End If
            Marshal.FreeHGlobal(ptrSendParam)

            m_struCMSListenPara.struAddress.Init()
            m_csLocalIP.CopyTo(0, m_struCMSListenPara.struAddress.szIP, 0, m_csLocalIP.Length)
            m_struCMSListenPara.struAddress.wPort = m_nPort
            EHOME_REGISTER_Func = New HCEHomeCMS.DEVICE_REGISTER_CB(AddressOf EHOME_REGISTER)
            m_struCMSListenPara.fnCB = EHOME_REGISTER_Func
            m_struCMSListenPara.pUserData = Me.Handle
            m_struCMSListenPara.byRes = New Byte(31) {}
            dwsize = CInt(Marshal.SizeOf(m_struCMSListenPara))
            Dim ptrCMSListenPara As IntPtr = Marshal.AllocHGlobal(dwsize)
            Marshal.StructureToPtr(m_struCMSListenPara, ptrCMSListenPara, False)
            Dim iListen As Integer = -1
            iListen = HCEHomeCMS.NET_ECMS_StartListen(m_struCMSListenPara)
            Marshal.FreeHGlobal(ptrCMSListenPara)
            If iListen = -1 Then
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_StartListen Failed")
            Else
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_StartListen Succ")
            End If
            HCEHomeCMS.NET_ECMS_GetBuildVersion()

            bRet = HCEHomeStream.NET_ESTREAM_Init()
            bRet = HCEHomeStream.NET_ESTREAM_SetLogToFile(3, System.Environment.CurrentDirectory & "\EHomeSdkLog\", True)
            m_struAccessSecure.byAccessSecurity = m_byStreamSecureAccessType
            If Not HCEHomeStream.NET_ESTREAM_SetSDKLocalCfg(EhomeSDK.NET_EHOME_LOCAL_CFG_TYPE.ACTIVE_ACCESS_SECURITY, ptrAccessSecure) Then
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 2, "NET_ESTREAM_SetSDKLocalCfg ACTIVE_ACCESS_SECURITY Success")
            Else
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ESTREAM_SetSDKLocalCfg ACTIVE_ACCESS_SECURITY Success")
            End If
            Marshal.FreeHGlobal(ptrAccessSecure)
            HCEHomeStream.NET_ESTREAM_GetBuildVersion()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetAddr(ByVal csIP As String, ByVal nport As Int16)
        m_csLocalIP = csIP
        m_nPort = nport
    End Sub
    'Public Sub InitPreviewListenParam()
    '    Dim i As Integer = 0
    '    For i = 0 To GlobalDefinition.MAX_LISTEN_NUM - 1
    '        g_struPreviewListen(i).lHandle = -1
    '        g_struPreviewListen(i).struIP.Init()
    '        m_csLocalIP.CopyTo(0, g_struPreviewListen(i).struIP.szIP, 0, m_csLocalIP.Length)
    '    Next i

    '    g_struPreviewListen(0).struIP.wPort = 8000
    '    g_struPreviewListen(0).iLinkType = 0
    '    fnPREVIEW_NEWLINK_CB_Func = New HCEHomeStream.PREVIEW_NEWLINK_CB(AddressOf fnPREVIEW_NEWLINK_CB)
    '    Dim struListen As New HCEHomeStream.NET_EHOME_LISTEN_PREVIEW_CFG()
    '    struListen.struIPAdress.Init()
    '    struListen.struIPAdress = g_struPreviewListen(0).struIP
    '    struListen.fnNewLinkCB = fnPREVIEW_NEWLINK_CB_Func
    '    struListen.pUser = Me.Handle
    '    struListen.byLinkMode = 0
    '    struListen.byRes = New Byte(126) {}

    '    Dim dwSize As Int32 = CInt(Marshal.SizeOf(struListen))
    '    Dim iHandle As Int32 = HCEHomeStream.NET_ESTREAM_StartListenPreview(struListen)
    '    If iHandle > -1 Then
    '        'g_formList.AddLog(0, HCEHomePublic.OPERATION_SUCC_T, 2, "NET_ESTREAM_StartListenPreview Success")
    '        g_struPreviewListen(0).lHandle = iHandle
    '        m_iHandle = iHandle
    '    Else
    '        'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 2, "NET_ESTREAM_StartListenPreview Failed")
    '    End If
    'End Sub
#Region "call back function"
    Public Function EHOME_REGISTER(ByVal iUserID As Integer, ByVal dwDataType As Integer, ByVal pOutBuffer As IntPtr, ByVal dwOutLen As UInteger, ByVal pInBuffer As IntPtr, ByVal dwInLen As UInteger, ByVal pUser As IntPtr) As Boolean
        Try
            Dim dwSize As Integer = 0
            Dim struTemp As New GlobalDefinition.LOCAL_DEVICE_INFO()
            struTemp.Init()
            dwSize = Marshal.SizeOf(GetType(GlobalDefinition.LOCAL_DEVICE_INFO))
            Dim ptrTemp As IntPtr = Marshal.AllocHGlobal(dwSize)

            Dim iOutLen As Integer = 0
            Dim struDevInfo As New HCEHomeCMS.NET_EHOME_DEV_REG_INFO_V12()
            struDevInfo.Init()
            'INSTANT VB WARNING: VB does not allow comparing non-nullable value types with 'null' - they are never equal to 'null':
            'ORIGINAL LINE: if (pOutBuffer != null)
            If True Then
                If dwDataType = HCEHomeCMS.ENUM_DEV_ON OrElse HCEHomeCMS.ENUM_DEV_AUTH = dwDataType OrElse HCEHomeCMS.ENUM_DEV_SESSIONKEY = dwDataType OrElse HCEHomeCMS.ENUM_DEV_ADDRESS_CHANGED = dwDataType Then
                    struDevInfo = DirectCast(Marshal.PtrToStructure(pOutBuffer, GetType(HCEHomeCMS.NET_EHOME_DEV_REG_INFO_V12)), HCEHomeCMS.NET_EHOME_DEV_REG_INFO_V12)
                End If
            Else
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 2, "pOutBuffer is NULL")
            End If
            If HCEHomeCMS.ENUM_DEV_ON = dwDataType Then
                'INSTANT VB WARNING: VB does not allow comparing non-nullable value types with 'null' - they are never equal to 'null':
                'ORIGINAL LINE: if (pInBuffer == null)
                If False Then
                    Return False
                End If

                EnableMaintimer = True
                Dim SerialNumber As String = System.Text.Encoding.[Default].GetString(struDevInfo.struRegInfo.sDeviceSerial).Trim(vbNullChar)
                Dim LOCATION As String = struDevInfo.struRegInfo.struDevAdd.szIP
                LOCATION = LOCATION.Substring(0, LOCATION.IndexOf(vbNullChar))
                Dim Password As String = System.Text.Encoding.[Default].GetString(struDevInfo.struRegInfo.byPassWord).Trim(vbNullChar)
                Dim DeviceID As String = System.Text.Encoding.[Default].GetString(struDevInfo.struRegInfo.byDeviceID).Trim(vbNullChar)
                Dim FVersion As String = System.Text.Encoding.[Default].GetString(struDevInfo.struRegInfo.byFirmwareVersion).Trim(vbNullChar)
                Dim Protocol As String = System.Text.Encoding.[Default].GetString(struDevInfo.struRegInfo.byDevProtocolVersion).Trim(vbNullChar)
                Dim DeviceName As String = System.Text.Encoding.[Default].GetString(struDevInfo.sDevName).Trim(vbNullChar)
                If SerialNumber <> "" Then
                    Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
                    Dim cmd As SqlCommand
                    'Dim sSql As String = "Update tblMachine set UpdatedOn =getdate(), LOCATION='" & LOCATION.Trim(vbNullChar) & "' where SerialNumber='" & SerialNumber.Trim(vbNullChar) & "' " &
                    '    "IF @@ROWCOUNT=0 " &
                    '     "insert into tblMachine(ID_NO,ClientID,SerialNumber,DeviceMode,CompanyCode,mac_Address, UpdatedOn,port, LOCATION,SaveImage,DeviceType) values((select ID_NO=isnull(max(ID_NO),0) + 1 from TblMACHINE),1,'" & SerialNumber.Trim(vbNullChar) & "','HTSeries','C001','" & SerialNumber.Trim(vbNullChar) & "',getdate(),'" & m_lPort & "','" & LOCATION.Trim(vbNullChar) & "','Y','Face')"

                    'for smart and wonder
                    Dim sSql As String = "Update tblMachine set UpdatedOn =getdate() where SerialNumber='" & SerialNumber.Trim(vbNullChar) & "' " &
                        "IF @@ROWCOUNT=0 " &
                         "insert into tblMachine(ID_NO,ClientID,SerialNumber,DeviceMode,CompanyCode,mac_Address, UpdatedOn,port,SaveImage,DeviceType) values((select ID_NO=isnull(max(ID_NO),0) + 1 from TblMACHINE),1,'" & SerialNumber.Trim(vbNullChar) & "','HTSeries','C001','" & SerialNumber.Trim(vbNullChar) & "',getdate(),'" & m_lPort & "','Y','Face')"

                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    cmd = New SqlCommand(sSql, con)
                    Dim count As Integer = cmd.ExecuteNonQuery()


                    'sSql = "insert into DeviceCommands (SerialNumber,CommandContent,TransferToDevice,Executed,IsOnline,CreatedOn,StartDateATTLOG,EndDateATTLOG) values " &
                    '"('" & SerialNumber.Trim(vbNullChar) & "','ATTLOG','" & SerialNumber.Trim(vbNullChar) & "','0','1',getdate(),'" & Now.AddDays(-2).ToString("yyyy-MM-dd 00:00:00") & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "') "
                    'cmd = New SqlCommand(sSql, con)
                    'count = cmd.ExecuteNonQuery()


                    'sSql = "insert into DeviceCommands (SerialNumber,CommandContent,TransferToDevice,Executed,IsOnline,CreatedOn) values " &
                    '"('" & SerialNumber.Trim(vbNullChar) & "','flaginfo','" & SerialNumber.Trim(vbNullChar) & "','0','1',getdate()) "
                    'cmd = New SqlCommand(sSql, con)
                    'count = cmd.ExecuteNonQuery()


                    'sSql = "insert into DeviceCommands (SerialNumber,CommandContent,TransferToDevice,Executed,IsOnline,CreatedOn) values " &
                    '"('" & SerialNumber.Trim(vbNullChar) & "','CHECK','" & SerialNumber.Trim(vbNullChar) & "','0','1',getdate()) "
                    'cmd = New SqlCommand(sSql, con)
                    'count = cmd.ExecuteNonQuery()

                    sSql = "insert into DeviceCommands (SerialNumber,CommandContent,TransferToDevice,Executed,IsOnline,CreatedOn,Priority) values " &
                    "('" & SerialNumber.Trim(vbNullChar) & "','flagsetdatetime','" & SerialNumber.Trim(vbNullChar) & "','0','1',getdate(),0) "
                    cmd = New SqlCommand(sSql, con)
                    count = cmd.ExecuteNonQuery()

                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If

                    Try
                        If AutoDownloadLogs = "Y" Then
                            'sSql = "insert into DeviceCommands (SerialNumber,CommandContent,TransferToDevice,Executed,IsOnline,CreatedOn,StartDateATTLOG,EndDateATTLOG) values " &
                            '"('" & SerialNumber.Trim(vbNullChar) & "','ATTLOG','" & SerialNumber.Trim(vbNullChar) & "','0','1',getdate(),'" & Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00") & "','" & Now.ToString("yyyy-MM-dd 23:59:59") & "') "
                            'If con.State <> ConnectionState.Open Then
                            '    con.Open()
                            'End If
                            'cmd = New SqlCommand(sSql, con)
                            'count = cmd.ExecuteNonQuery()
                            'If con.State <> ConnectionState.Closed Then
                            '    con.Close()
                            'End If

                            sSql = "select LastLog from tblmachine where SerialNumber='" & SerialNumber.Trim(vbNullChar) & "'"
                            Dim ds As DataSet = New DataSet
                            Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                            adap.Fill(ds)
                            If ds.Tables(0).Rows(0)(0).ToString.Trim <> "" Then
                                Dim startdate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0)(0).ToString.Trim)
                                If Now.Subtract(startdate).Minutes > 15 Then
                                    sSql = "insert into DeviceCommands (SerialNumber,CommandContent,TransferToDevice,Executed,IsOnline,CreatedOn,StartDateATTLOG,EndDateATTLOG,Priority) values " &
                                "('" & SerialNumber.Trim(vbNullChar) & "','ATTLOG','" & SerialNumber.Trim(vbNullChar) & "','0','1',getdate(),'" & startdate.ToString("yyyy-MM-dd HH:mm:00") & "','" & Now.ToString("yyyy-MM-dd HH:mm:00") & "',3) "
                                    If con.State <> ConnectionState.Open Then
                                        con.Open()
                                    End If
                                    cmd = New SqlCommand(sSql, con)
                                    count = cmd.ExecuteNonQuery()
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If
                                End If
                            End If
                        End If
                    Catch ex As Exception

                    End Try



                End If


                m_ConvertModel.UTF82A(struDevInfo.struRegInfo.byDeviceID, struDevInfo.struRegInfo.byDeviceID, HCEHomePublic.MAX_DEVICE_ID_LEN, iOutLen)
                m_ConvertModel.UTF82A(struDevInfo.struRegInfo.sDeviceSerial, struDevInfo.struRegInfo.sDeviceSerial, HCEHomePublic.NET_EHOME_SERIAL_LEN, iOutLen)

                struDevInfo.struRegInfo.byDeviceID.CopyTo(struTemp.byDeviceID, 0)
                struTemp.iLoginID = iUserID
                struDevInfo.struRegInfo.sDeviceSerial.CopyTo(struTemp.sDeviceSerial, 0)


                Dim szDeviceSerial(HCEHomePublic.NET_EHOME_SERIAL_LEN) As Byte
                struDevInfo.struRegInfo.sDeviceSerial.CopyTo(szDeviceSerial, 0)
                Dim szSessionKey(HCEHomePublic.MAX_MASTER_KEY_LEN) As Byte
                struDevInfo.struRegInfo.bySessionKey.CopyTo(szSessionKey, 0)
                If struDevInfo.struRegInfo.byDevProtocolVersion(0) = AscW("2"c) Then
                    struTemp.dwVersion = 2
                ElseIf AscW("4"c) = struDevInfo.struRegInfo.byDevProtocolVersion(0) Then
                    struTemp.dwVersion = 4
                Else
                    struTemp.dwVersion = 5
                End If

                Dim struServInfo As New HCEHomeCMS.NET_EHOME_SERVER_INFO_V50()
                struServInfo.Init()
                struServInfo = DirectCast(Marshal.PtrToStructure(pInBuffer, GetType(HCEHomeCMS.NET_EHOME_SERVER_INFO_V50)), HCEHomeCMS.NET_EHOME_SERVER_INFO_V50)

                '新增修改的内容
                'New
                struServInfo = m_struServInfo
                'Dim strTCPAlarmIP As String = New String(m_struServInfo.struTCPAlarmSever.szIP).Trim(vbNullChar)
                'Dim strUDPAlarmIP As String = New String(m_struServInfo.struTCPAlarmSever.szIP).Trim(vbNullChar)
                'Dim strPicServIP As String = New String(m_struServInfo.struTCPAlarmSever.szIP).Trim(vbNullChar)
                Dim strTCPAlarmIP As String = StrTcpServIP
                Dim strUDPAlarmIP As String = StrUdpServIP
                'INSTANT VB NOTE: The variable strPicServIP was renamed since Visual Basic does not handle local variables named the same as class members well:
                Dim strPicServIP_Conflict As String = StrPicServIp

                Dim TCPAlarmIP() As Char = strTCPAlarmIP.ToCharArray()
                Dim UDPAlarmIP() As Char = strUDPAlarmIP.ToCharArray()
                Dim PicServIP() As Char = strPicServIP_Conflict.ToCharArray()


                ClearCharArr(struServInfo.struPictureSever.szIP)
                ClearCharArr(struServInfo.struTCPAlarmSever.szIP)
                ClearCharArr(struServInfo.struUDPAlarmSever.szIP)

                Array.Copy(TCPAlarmIP, struServInfo.struPictureSever.szIP, TCPAlarmIP.Length)
                Array.Copy(UDPAlarmIP, struServInfo.struTCPAlarmSever.szIP, UDPAlarmIP.Length)
                Array.Copy(PicServIP, struServInfo.struUDPAlarmSever.szIP, PicServIP.Length)
                'End New

                Dim TdwSize As Integer = Marshal.SizeOf(struServInfo)
                Dim ptrStruS As IntPtr = Marshal.AllocHGlobal(TdwSize)
                Marshal.StructureToPtr(struServInfo, ptrStruS, False)
                Dim value As Integer = 0
                GetAddressByType(0, 0, ptrStruS, TdwSize, value, 0)
                struServInfo = DirectCast(Marshal.PtrToStructure(ptrStruS, GetType(HCEHomeCMS.NET_EHOME_SERVER_INFO_V50)), HCEHomeCMS.NET_EHOME_SERVER_INFO_V50)
                If g_bSS_Enable Then
                    For i As Integer = 0 To MAX_DEVICES - 1
                        If DeviceTree.g_struDeviceInfo(i).iDeviceIndex <> -1 Then
                            If True = DeviceTree.isByteEqual(DeviceTree.g_struDeviceInfo(i).byDeviceID, struDevInfo.struRegInfo.byDeviceID, 256) Then
                                If m_bHttps Then
                                    struServInfo.byClouldHttps = 1
                                Else
                                    struServInfo.byClouldHttps = 0
                                End If
                                Array.Copy(struDevInfo.struRegInfo.byDeviceID, 0, struServInfo.byClouldAccessKey, 0, 64)
                                Array.Copy(DeviceTree.g_struDeviceInfo(i).byClouldSecretKey, 0, struServInfo.byClouldSecretKey, 0, 64)
                                Exit For
                            End If
                        End If
                    Next i
                End If

                If 5 = struTemp.dwVersion Then
                    struServInfo.struTCPAlarmSever.wPort = CShort(m_wAlarmServerMqttPort)
                End If

                Marshal.StructureToPtr(struServInfo, pInBuffer, False)


                If m_bUseAccessList Then
                    For i As Integer = 0 To 63
                        If struTemp.dwVersion >= 4 Then
                            If struDevInfo.struRegInfo.sDeviceSerial.Equals(m_stAccessDeviceList(i).sSerialNumber) Then
                                Marshal.StructureToPtr(struTemp, ptrTemp, False)
                                Dim mes As New Message()
                                mes.Msg = EHomeForm.WM_ADD_DEV
                                mes.LParam = ptrTemp
                                g_deviceTree.ProDevStatu(mes)
                                'PostMessage(m_treeHandle, EHomeForm.WM_ADD_DEV, ptrTemp, ptrTemp);
                                Return True
                            End If
                        Else
                            If struDevInfo.struRegInfo.byDeviceID.Equals(m_stAccessDeviceList(i).sSerialNumber) Then
                                Marshal.StructureToPtr(struTemp, ptrTemp, False)
                                Dim mes As New Message()
                                mes.Msg = EHomeForm.WM_ADD_DEV
                                mes.LParam = ptrTemp
                                g_deviceTree.ProDevStatu(mes)

                                'PostMessage(m_treeHandle, EHomeForm.WM_ADD_DEV, ptrTemp, ptrTemp);
                                Return True
                            End If
                        End If
                    Next i
                Else

                    Marshal.StructureToPtr(struTemp, ptrTemp, False)

                    Dim mes As New Message()
                    mes.Msg = EHomeForm.WM_ADD_DEV
                    mes.LParam = ptrTemp
                    g_deviceTree.ProDevStatu(mes)

                    'SetDeviceConfig(iUserID, DeviceID, "", "flagsetdatetime")

                    'PostMessage(m_treeHandle, EHomeForm.WM_ADD_DEV, ptrTemp, ptrTemp);
                    Return True
                End If

                Return False
            ElseIf HCEHomeCMS.ENUM_DEV_OFF = dwDataType Then

                Marshal.StructureToPtr(iUserID, ptrTemp, False)

                Dim mes As New Message()
                mes.Msg = EHomeForm.WM_DEL_DEV
                mes.LParam = ptrTemp
                g_deviceTree.ProDevStatu(mes)

                'PostMessage(m_treeHandle, EHomeForm.WM_DEL_DEV, ptrTemp, ptrTemp);
                Return False
            ElseIf HCEHomeCMS.ENUM_DEV_AUTH = dwDataType Then
                For i As Integer = 0 To MAX_DEVICES - 1
                    If -1 = DeviceTree.g_struDeviceInfo(i).iDeviceIndex Then
                        struDevInfo.struRegInfo.byDeviceID.CopyTo(struTemp.byDeviceID, 0)
                        struTemp.dwVersion = 5
                        'if (struDevInfo.struRegInfo.byDeviceID.Equals(m_stAccessDeviceList[i].sSerialNumber))
                        '{

                        Marshal.StructureToPtr(struTemp, ptrTemp, False)
                        Dim mes As New Message()
                        mes.Msg = EHomeForm.WM_ADD_DEV
                        mes.LParam = ptrTemp
                        g_deviceTree.ProDevStatu(mes)

                        'PostMessage(m_treeHandle, EHomeForm.WM_ADD_DEV, ptrTemp, ptrTemp);
                        '}
                        Exit For
                    Else
                        m_ConvertModel.UTF82A(struDevInfo.struRegInfo.byDeviceID, struDevInfo.struRegInfo.byDeviceID, HCEHomePublic.MAX_DEVICE_ID_LEN, iOutLen)
                        If Enumerable.SequenceEqual(DeviceTree.g_struDeviceInfo(i).byDeviceID, struDevInfo.struRegInfo.byDeviceID) Then
                            Dim temp(31) As Byte
                            DeviceTree.g_struDeviceInfo(i).byEhomeKey.CopyTo(temp, 0)
                            Marshal.Copy(temp, 0, pInBuffer, 32)
                            Exit For
                        End If
                    End If
                Next i
            ElseIf HCEHomeCMS.ENUM_DEV_SESSIONKEY = dwDataType Then
                Dim devSessionkey As New HCEHomePublic.NET_EHOME_DEV_SESSIONKEY()
                devSessionkey.Init()
                struDevInfo.struRegInfo.byDeviceID.CopyTo(devSessionkey.sDeviceID, 0)
                struDevInfo.struRegInfo.bySessionKey.CopyTo(devSessionkey.sSessionKey, 0)
                HCEHomeCMS.NET_ECMS_SetDeviceSessionKey(devSessionkey)
                HCEHomeAlarm.NET_EALARM_SetDeviceSessionKey(devSessionkey)
            ElseIf HCEHomeCMS.ENUM_DEV_DAS_REQ = dwDataType Then
                Dim szLocalIP As String = ""
                'IntPtr ptrLocalIP = Marshal.AllocHGlobal(128);
                'ptrLocalIP=Marshal.StringToHGlobalAnsi(szLocalIP);
                'szLocalIP = SelectIP.IpAddressList[0].ToString();
                Dim ptrLocalIP As IntPtr = IntPtr.Zero
                Dim dwPort As Integer = 0
                GetAddressByType(3, 0, ptrLocalIP, 128, dwPort, 4)
                szLocalIP = Marshal.PtrToStringAnsi(ptrLocalIP)
                If 0 = dwPort Then
                    dwPort = m_nPort
                End If
                Dim portTemp As String = dwPort.ToString()
                Dim strInBuffer As String = "{""Type"":""DAS"",""DasInfo"":{""Address"":""" & szLocalIP & """," & """Domain"":""test.ys7.com"",""ServerID"":""das_" & szLocalIP & "_" & portTemp & """,""Port"":" & portTemp & ",""UdpPort"":" & portTemp & "}}"

                Dim byTemp() As Byte = System.Text.Encoding.Default.GetBytes(strInBuffer)
                Marshal.Copy(byTemp, 0, pInBuffer, byTemp.Length)

            ElseIf HCEHomeCMS.ENUM_DEV_ADDRESS_CHANGED = dwDataType Then
                Dim byEhomeKey(31) As Byte
                For i As Integer = 0 To MAX_DEVICES - 1
                    If DeviceTree.g_struDeviceInfo(i).iDeviceIndex <> -1 Then
                        DeviceTree.g_struDeviceInfo(i).byEhomeKey.CopyTo(byEhomeKey, 0)
                        Exit For
                    End If
                Next i
                Marshal.StructureToPtr(struTemp, ptrTemp, False)
                Dim mes As New Message()
                mes.Msg = EHomeForm.WM_CHANGE_IP
                mes.LParam = ptrTemp
                g_deviceTree.ProDevStatu(mes)

                'PostMessage(m_treeHandle, EHomeForm.WM_CHANGE_IP, ptrTemp, ptrTemp);
                'g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 0, "IP changed,register again")



                m_ConvertModel.UTF82A(struDevInfo.struRegInfo.byDeviceID, struDevInfo.struRegInfo.byDeviceID, HCEHomePublic.MAX_DEVICE_ID_LEN, iOutLen)
                m_ConvertModel.UTF82A(struDevInfo.struRegInfo.sDeviceSerial, struDevInfo.struRegInfo.sDeviceSerial, HCEHomePublic.NET_EHOME_SERIAL_LEN, iOutLen)

                struDevInfo.struRegInfo.byDeviceID.CopyTo(struTemp.byDeviceID, 0)
                struTemp.iLoginID = iUserID
                struDevInfo.struRegInfo.sDeviceSerial.CopyTo(struTemp.sDeviceSerial, 0)


                Dim szDeviceSerial(HCEHomePublic.NET_EHOME_SERIAL_LEN) As Byte
                struDevInfo.struRegInfo.sDeviceSerial.CopyTo(szDeviceSerial, 0)
                If 2 = struDevInfo.struRegInfo.byDevProtocolVersion(0) Then
                    struTemp.dwVersion = 2
                ElseIf 4 = struDevInfo.struRegInfo.byDevProtocolVersion(0) Then
                    struTemp.dwVersion = 4
                Else
                    struTemp.dwVersion = 5
                End If

                If m_bUseAccessList Then
                    For i As Integer = 0 To 63
                        If struTemp.dwVersion >= 4 Then
                            If struDevInfo.struRegInfo.sDeviceSerial.Equals(m_stAccessDeviceList(i).sSerialNumber) Then

                                Marshal.StructureToPtr(struTemp, ptrTemp, False)
                                Dim mesg As New Message()
                                mesg.Msg = EHomeForm.WM_ADD_DEV
                                mesg.LParam = ptrTemp
                                g_deviceTree.ProDevStatu(mesg)
                                'PostMessage(m_treeHandle, EHomeForm.WM_ADD_DEV, ptrTemp, ptrTemp);
                                Return True
                            End If
                        Else
                            If struDevInfo.struRegInfo.byDeviceID.Equals(m_stAccessDeviceList(i).sSerialNumber) Then
                                Marshal.StructureToPtr(struTemp, ptrTemp, False)
                                Dim mesg As New Message()
                                mesg.Msg = EHomeForm.WM_ADD_DEV
                                mesg.LParam = ptrTemp
                                g_deviceTree.ProDevStatu(mesg)
                                'PostMessage(m_treeHandle, EHomeForm.WM_ADD_DEV, ptrTemp, ptrTemp);
                                Return True
                            End If
                        End If
                    Next i
                Else
                    Marshal.StructureToPtr(struTemp, ptrTemp, False)
                    Dim mesa As New Message()
                    mesa.Msg = EHomeForm.WM_ADD_DEV
                    mesa.LParam = ptrTemp
                    g_deviceTree.ProDevStatu(mesa)

                    'PostMessage(m_treeHandle, EHomeForm.WM_ADD_DEV, ptrTemp, ptrTemp);
                    Return True
                End If

                Return False
            End If
        Catch ex As Exception

        End Try
        Return True
    End Function
    '3, 0, ref szLocalIP, 128, ref dwPort, 4
    Public Function fnPREVIEW_NEWLINK_CB(ByVal iLinkHandle As Integer, ByRef pNewLinkCBMsg As HCEHomeStream.NET_EHOME_NEWLINK_CB_MSG, ByVal pUserData As IntPtr) As Boolean
        Try
            m_iLinkHandle = iLinkHandle
            m_iPlayHandle = iLinkHandle
            'Int32 dwConvertLen = 0;
            Dim struDataCB As New HCEHomeStream.NET_EHOME_PREVIEW_DATA_CB_PARAM()
            struDataCB.fnPreviewDataCB = fnPREVIEW_DATA_CB_Func
            struDataCB.pUserData = New IntPtr(m_iCurWndIndex)
            struDataCB.byRes = New Byte(127) {}
            Dim ptrDataCB As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struDataCB))
            Marshal.StructureToPtr(struDataCB, ptrDataCB, False)
            If Not HCEHomeStream.NET_ESTREAM_SetPreviewDataCB(iLinkHandle, ptrDataCB) Then
                Marshal.FreeHGlobal(ptrDataCB)
                Return False
            End If
            Marshal.FreeHGlobal(ptrDataCB)
        Catch ex As Exception

        End Try
        Return True
    End Function
    Public Shared Sub fnPREVIEW_DATA_CB(ByVal iPreviewHandle As Integer, ByRef pPreviewCBMsg As HCEHomeStream.NET_EHOME_PREVIEW_CB_MSG, ByVal pUserData As IntPtr)
        Try
            'bool bRet = false;
            'm_iRealHandle = iPreviewHandle;
            Dim iWinIndex As Integer = CInt(pUserData)
            'DeviceTree.m_PreviewPanel(iWinIndex).InputStreamData(pPreviewCBMsg.byDataType, pPreviewCBMsg.pRecvdata, pPreviewCBMsg.dwDataLen)
        Catch ex As Exception

        End Try
    End Sub
    Public Function IsInAccessDeviceList(ByVal pSerialNumber() As Byte) As Boolean
        Try
            If pSerialNumber Is Nothing Then
                Return False
            End If
            For i As Integer = 0 To 63
                If pSerialNumber.Equals(m_stAccessDeviceList(i).sSerialNumber) Then
                    Return True
                End If
            Next i
        Catch ex As Exception

        End Try
        Return False
    End Function
    Public Function AlarmMsgCallBack(ByVal iHandle As Integer, ByVal pAlarmMsg As IntPtr, ByVal pUser As IntPtr) As Boolean
        Try
            Dim struAlarmMsg As New HCEHomeAlarm.NET_EHOME_ALARM_MSG()
            struAlarmMsg.sSerialNumber = New Byte(HCEHomeAlarm.NET_EHOME_SERIAL_LEN - 1) {}
            struAlarmMsg.byRes = New Byte(19) {}
            struAlarmMsg = DirectCast(Marshal.PtrToStructure(pAlarmMsg, GetType(HCEHomeAlarm.NET_EHOME_ALARM_MSG)), HCEHomeAlarm.NET_EHOME_ALARM_MSG)

            If m_bUseAccessList AndAlso Not IsInAccessDeviceList(struAlarmMsg.sSerialNumber) Then
                Return False
            End If
            ProcessAlarmData(CInt(struAlarmMsg.dwAlarmType), struAlarmMsg.pAlarmInfo, CInt(struAlarmMsg.dwAlarmInfoLen), struAlarmMsg.pXmlBuf, CInt(struAlarmMsg.dwXmlBufLen), struAlarmMsg.pHttpUrl, CInt(struAlarmMsg.dwHttpUrlLen))
            SendMessage(m_logListHandle, EHomeForm.WM_LISTENED_ALARM, struAlarmMsg.pXmlBuf, pAlarmMsg)
        Catch ex As Exception

        End Try
        Return True
    End Function
#End Region
    'Newly Add Content
    Public Sub ProcessAlarmData(ByVal dwAlarmType As Integer, ByVal pStru As IntPtr, ByVal dwStruLen As Integer, ByRef pXml As IntPtr, ByVal dwXmlLen As Integer, ByVal pUrl As IntPtr, ByVal dwUrlLen As Integer)
        If pUrl <> IntPtr.Zero Then
            dwAlarmType = EHOME_ISAPI_ALARM
        End If
        Select Case dwAlarmType
            Case EHOME_ALARM_ACS
                ProcessEhomeAlarmAcs(pXml, dwXmlLen)
            Case EHOME_ALARM_FACETEMP
                ProcessFaceTempAlarm(pXml, dwXmlLen)
            Case Else
        End Select
    End Sub
    Public Sub ProcessFaceTempAlarm(ByRef pXml As IntPtr, ByVal dwXmlLen As Integer)
        Dim strOutJson As String = ""
        'Parse Temperature Info
        strOutJson = Marshal.PtrToStringAnsi(pXml, dwXmlLen)
        Dim szInfoBuf As String = String.Empty

        Dim outStr As String = String.Empty
        Dim InputBeforeStr As String = "<Command>"
        Dim InputAfterStr As String = "</Command>"
        If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
            szInfoBuf &= "Command: " & outStr & " "
        End If
        InputBeforeStr = "<Params><![CDATA["
        InputAfterStr = "]]></Params>"
        If strOutJson.Contains(InputBeforeStr) AndAlso strOutJson.Contains(InputAfterStr) Then
            Dim FirstIndex As Integer = strOutJson.LastIndexOf(InputBeforeStr)
            Dim SecondIndex As Integer = strOutJson.LastIndexOf(InputAfterStr)
            strOutJson = strOutJson.Substring(FirstIndex + InputBeforeStr.Length, SecondIndex - FirstIndex - InputAfterStr.Length - 5)
            Dim Facert As FaceAlarmInfo = JsonConvert.DeserializeObject(Of FaceAlarmInfo)(strOutJson)
            Dim FaceEvent As FaceTemperatureMeasurementEvent = Facert.FaceTemperatureMeasurementEvent

            szInfoBuf &= "  [IP]: " & Facert.ipAddress & " DeviceSerialNo: " & Facert.FaceTemperatureMeasurementEvent.serialNo & " Mask: " & Facert.FaceTemperatureMeasurementEvent.mask & "  isAbnormal: " & Facert.FaceTemperatureMeasurementEvent.isAbnomalTemperature & "  " & "CurrTemperature: " & Facert.FaceTemperatureMeasurementEvent.currTemperature.ToString() & "   PicDataUrl:" & Facert.FaceTemperatureMeasurementEvent.thermalURL

        Else
            InputBeforeStr = "<DeviceID>"
            InputAfterStr = "</DeviceID>"
            If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
                szInfoBuf &= "DeviceID: " & outStr & " "
            End If
            InputBeforeStr = "<Time>"
            InputAfterStr = "</Time>"
            If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
                szInfoBuf &= "Time: " & outStr & " "
            End If
            InputBeforeStr = "<MajorType>"
            InputAfterStr = "</MajorType>"
            If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
                szInfoBuf &= "MajorType: " & outStr & " "
            End If
            InputBeforeStr = "<MinorType>"
            InputAfterStr = "</MinorType>"
            If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
                szInfoBuf &= "MinorType: " & outStr & " "
            End If
        End If
        pXml = Marshal.StringToHGlobalAnsi(szInfoBuf)
    End Sub
    Public Sub ProcessEhomeAlarmAcs(ByRef pXml As IntPtr, ByVal dwXmlLen As Integer)

#Region "CommentOriginal"
        'Try
        '    If (pXml = IntPtr.Zero) OrElse (dwXmlLen = 0) Then
        '        Return
        '    End If
        '    Dim szInfoBuf As String = String.Empty
        '    Dim byOutput(1023) As Char
        '    Dim outStr As String = String.Empty
        '    Dim InputBeforeStr As String = "<Command>"
        '    Dim InputAfterStr As String = "</Command>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "Command: " & outStr & " "
        '    End If

        '    InputBeforeStr = "<DeviceID>"
        '    InputAfterStr = "</DeviceID>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "DeviceID: " & outStr & " "
        '    End If

        '    InputBeforeStr = "<MajorType>"
        '    InputAfterStr = "</MajorType>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "MajorType: " & outStr & " "
        '    End If

        '    InputBeforeStr = "<MinorType>"
        '    InputAfterStr = "</MinorType>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "MinorType: " & outStr & " "
        '    End If

        '    InputBeforeStr = "<IDNum>"
        '    InputAfterStr = "</IDNum>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "IDNum: " & outStr & " "
        '    End If

        '    InputBeforeStr = "<passportNo>"
        '    InputAfterStr = "</passportNo>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "passportNo: " & outStr & " "
        '    End If

        '    InputBeforeStr = "<byOCR>"
        '    InputAfterStr = "</byOCR>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "byOCR: " & outStr & " "
        '    End If
        '    InputBeforeStr = "<countryIssue>"
        '    InputAfterStr = "</countryIssue>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "countryIssue: " & outStr & " "
        '    End If

        '    InputBeforeStr = "<Time>"
        '    InputAfterStr = "</Time>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "Time: " & outStr & " "
        '    End If

        '    InputBeforeStr = "<addr>"
        '    InputAfterStr = "</addr>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "addr: " & outStr & " "
        '    End If

        '    InputBeforeStr = "<currTemperature>"
        '    InputAfterStr = "</currTemperature>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "CurrTemperature: " & outStr & " "
        '    End If

        '    InputBeforeStr = "<CardNo>"
        '    InputAfterStr = "</CardNo>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "CardNo: " & outStr & " "
        '    End If
        '    InputBeforeStr = "<CardType>"
        '    InputAfterStr = "</CardType>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "CardType: " & outStr & " "
        '    End If
        '    InputBeforeStr = "<CardReaderNo>"
        '    InputAfterStr = "</CardReaderNo>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "CardReaderNo: " & outStr & " "
        '    End If
        '    InputBeforeStr = "<DoorNo>"
        '    InputAfterStr = "</DoorNo>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "DoorNo: " & outStr & " "
        '    End If
        '    InputBeforeStr = "<mask>"
        '    InputAfterStr = "</mask>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "mask: " & outStr & " "
        '    End If
        '    InputBeforeStr = "<PicDataUrl>"
        '    InputAfterStr = "</PicDataUrl>"
        '    If XmlPrase(pXml, InputBeforeStr, InputAfterStr, outStr) Then
        '        szInfoBuf &= "PicDataUrl:" & outStr
        '    End If
        '    pXml = Marshal.StringToHGlobalAnsi(szInfoBuf)
        'Catch ex As Exception

        'End Try
#End Region
        Try
            Dim str As String
            Dim strTemp As String = Marshal.PtrToStringAnsi(pXml, CInt(dwXmlLen))
            TraceServiceTextEvents(strTemp)
            Dim TempStr As String = "PicDataUrl"
            Dim bArr As Byte() = Nothing 'nitin
            Dim CardNo As String = ""
            Dim PuchTime As DateTime = Convert.ToDateTime("1900-01-01 00:00:00")
            Dim PuchTimeStr As String = ""
            Dim DeviceID As String = ""
            Dim CardType As String = ""
            Dim verifymode As String = ""
            Dim name As String = ""
            Dim xml As New XmlDocument()
            Dim ImageTodelete As String = ""
            Dim ImgPunchPath As String = ""

            xml.LoadXml(strTemp) ' suppose that myXmlString contains "<Names>...</Names>"
            If (strTemp.Contains(TempStr) Or strTemp.Contains("employeeNoString") Or strTemp.Contains("currTemperature")) Then 'And Not strTemp.Contains("Failed-to")
                Try

                    Dim XserialNo As String = ""
                    Dim xnListserialNo As XmlNodeList = xml.SelectNodes("/PPVSPMessage/serialNo")
                    For Each xn As XmlNode In xnListserialNo
                        XserialNo = xn.InnerText
                    Next xn

                    Dim xnListT As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/Time")
                    For Each xn As XmlNode In xnListT
                        PuchTime = Convert.ToDateTime(xn.InnerText)
                    Next xn
                    Dim xnListD As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/DeviceID")
                    For Each xn As XmlNode In xnListD
                        DeviceID = xn.InnerText
                    Next xn
                    'Try
                    '    If Not EHomeForm.ActiveSerialNumber.Contains(DeviceID) Then
                    '        Return
                    '    End If
                    'Catch ex As Exception
                    '    Return
                    'End Try

                    'InsertEventLog(XserialNo, DeviceID, PuchTime)

                    'Dim PicIndex As Integer = FilterAlarmMsg.LastIndexOf("PicDataUrl")
                    Dim PicUrl As String = "" ' FilterAlarmMsg.Substring(PicIndex + TempStr.Length)
                    Dim xnListPicUrl As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/PicDataUrl")
                    For Each xn As XmlNode In xnListPicUrl
                        PicUrl = xn.InnerText
                    Next xn
                    'Thread.Sleep(100)
                    'If EHomeForm.AllowPhoto = "Y" Then
#Region "ImageCheck"
                    If PicUrl.Trim <> "" Then
                        If PicUrl.Contains("/pic?") Then
                            Try
                                PicUrl = PicUrl.Substring(PicUrl.LastIndexOf("/pic?") + 5, PicUrl.Length - PicUrl.LastIndexOf("/pic?") - 5)
                                Dim img As System.Drawing.Image
                                ImgPunchPath = SS_STORAGE_PATH & "\" & PicUrl
                                If System.IO.File.Exists(ImgPunchPath) Then
                                    img = System.Drawing.Image.FromFile(ImgPunchPath)
                                Else
                                    ImgPunchPath = ImgPunchPath & "202"
                                    img = System.Drawing.Image.FromFile(ImgPunchPath)
                                End If
                                bArr = imgToByteArray(img) 'nitin
                                img.Dispose()
                                ImageTodelete = ImgPunchPath
                            Catch ex As Exception

                            End Try
                        ElseIf PicUrl.Contains("/kms") Then
                            Try
                                PicUrl = PicUrl.Substring(PicUrl.LastIndexOf("id=") + 3, PicUrl.Length - PicUrl.LastIndexOf("id=") - 3)
                                Dim img As Image
                                ImgPunchPath = SS_STORAGE_PATH & "\" & PicUrl
                                If System.IO.File.Exists(ImgPunchPath) Then
                                    img = Image.FromFile(ImgPunchPath)
                                Else
                                    ImgPunchPath = ImgPunchPath & "202"
                                    img = Image.FromFile(ImgPunchPath)
                                End If
                                bArr = imgToByteArray(img) 'nitin
                                img.Dispose()
                                ImageTodelete = ImgPunchPath

                            Catch ex As Exception

                            End Try
                        End If
                    End If
#End Region
                    'End If

                    'MsgBox(strTemp)

                    Dim XCommandType As String = ""
                    Dim xnListCtype As XmlNodeList = xml.SelectNodes("/PPVSPMessage/CommandType")
                    For Each xn As XmlNode In xnListCtype
                        XCommandType = xn.InnerText
                    Next xn

                    Dim xnList As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/employeeNoString")
                    For Each xn As XmlNode In xnList
                        CardNo = xn.InnerText
                    Next xn
                    If CardNo = "" Then
                        CardNo = "Visitor"
                        If EHomeForm.AlowVisitor = "N" Then
                            Return
                        End If
                    End If
                    If CardNo = "Visitor" Or AllowSeconds = "Y" Then
                        PuchTimeStr = PuchTime.ToString("yyyy-MM-dd HH:mm:ss")
                    Else
                        PuchTimeStr = PuchTime.ToString("yyyy-MM-dd HH:mm:00")
                        'PuchTimeStr = PuchTime.ToString("yyyy-MM-dd HH:mm:ss")  'smart one of the customer
                    End If
                    'MsgBox("CardNo " & CardNo)
                    Try
                        Dim xnListName As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/name")
                        For Each xn As XmlNode In xnListName
                            name = xn.InnerText
                        Next xn
                    Catch ex As Exception
                        name = ""
                    End Try


                    Dim xnListCT As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/CardType")
                    For Each xn As XmlNode In xnListCT
                        CardType = xn.InnerText
                    Next xn
                    'MsgBox("CardType " & CardType)

                    Dim xnListVM As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/currentVerifyMode")
                    For Each xn As XmlNode In xnListVM
                        verifymode = xn.InnerText
                    Next xn
                    'MsgBox("verifymode " & verifymode)

                    Dim fCurrTemperature As Double
                    Dim thermometryUnit As String = ""
                    Dim Ftemp As String = ""
                    Dim Ctemp As String = ""
                    Dim MaskWear As String = ""
                    Dim TeperatureStatus As String = ""
                    Dim xnListTU As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/thermometryUnit")
                    For Each xn As XmlNode In xnListTU
                        thermometryUnit = xn.InnerText
                    Next xn
                    Dim xnListTempe As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/currTemperature")
                    For Each xn As XmlNode In xnListTempe
                        fCurrTemperature = xn.InnerText
                    Next xn

                    If thermometryUnit.Trim = "celsius" Then
                        Ctemp = fCurrTemperature
                        Ftemp = Math.Round((fCurrTemperature * 9 / 5) + 32, 2)
                    ElseIf thermometryUnit.Trim = "fahrenheit" Then
                        Ftemp = fCurrTemperature
                        Ctemp = Math.Round((fCurrTemperature - 32) * 5 / 9, 2)
                    Else
                        Ftemp = ""
                        Ctemp = ""
                    End If

                    Dim xnListmask As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/mask")
                    For Each xn As XmlNode In xnListmask
                        MaskWear = xn.InnerText
                    Next xn
                    Dim xnListAbNormal As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/isAbnomalTemperature")
                    For Each xn As XmlNode In xnListAbNormal
                        TeperatureStatus = xn.InnerText
                    Next xn

                    Dim io_mode As String = ""
                    Dim IsMasterDevice As String = "N"
                    If CardNo <> "" And CardType = "1" Then
                        Dim SaveImage As String = "Y"
                        Dim IsThermal As String = "N"
                        Dim con As New SqlConnection(EHomeForm.ConnectionString)

                        Dim dsM As DataSet = New DataSet
                        Dim sSqlM As String = "select IN_OUT, SaveImage, ID_NO, IsMasterDevice,IsThermal from tblmachine where SerialNumber='" & DeviceID & "'"
                        Dim adpM As SqlDataAdapter = New SqlDataAdapter(sSqlM, con)
                        adpM.Fill(dsM)

                        If dsM.Tables(0).Rows.Count > 0 Then
                            SaveImage = dsM.Tables(0).Rows(0)("SaveImage").ToString.Trim
                            IsThermal = dsM.Tables(0).Rows(0)("IsThermal").ToString.Trim
                            IsMasterDevice = dsM.Tables(0).Rows(0)("IsMasterDevice").ToString.Trim
                            If CardNo <> "Visitor" Then
                                If dsM.Tables(0).Rows(0)(0).ToString.Trim = "0" Then
                                    'io_mode = "I"
                                    io_mode = "0"
                                ElseIf dsM.Tables(0).Rows(0)(0).ToString.Trim = "1" Then
                                    'io_mode = "O"
                                    io_mode = "1"
                                ElseIf dsM.Tables(0).Rows(0)(0).ToString.Trim = "2" Or dsM.Tables(0).Rows(0)(0).ToString.Trim = "A" Then
                                    Try
                                        Dim xnListIN_OUT As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/attendanceStatus")
                                        For Each xn As XmlNode In xnListIN_OUT
                                            io_mode = xn.InnerText
                                        Next xn
                                        If io_mode.ToLower = "checkin" Then
                                            io_mode = "0"
                                        ElseIf io_mode.ToLower = "checkout" Then
                                            io_mode = "1"
                                        End If
                                    Catch ex As Exception
                                        io_mode = ""
                                    End Try
                                    ''LnT
                                    'If io_mode = "" Or io_mode.ToLower = "undefined" Or dsM.Tables(0).Rows(0)(0).ToString.Trim = "2" Then
                                    '    Dim ds2 As DataSet = New DataSet
                                    '    Dim sSql1 = "select count (UserID) from UserAttendance where UserID = '" & CardNo & "' and convert(varchar, AttDateTime, 23) = '" & PuchTime.ToString("yyyy-MM-dd") & "'"
                                    '    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql1, con)
                                    '    adap.Fill(ds2)
                                    '    If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                                    '        io_mode = "0"
                                    '    Else
                                    '        If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                                    '            io_mode = "0"
                                    '        Else
                                    '            io_mode = "1"
                                    '        End If
                                    '    End If
                                    'End If
                                    ''end LnT
                                End If
                            End If
                        End If

                        Dim ds As New DataSet()
                        Dim cmd As SqlCommand
                        Dim sSql As String = ""
                        Dim pIsVU As Boolean = IsValidUserAtt(CardNo, PuchTime, con, EHomeForm.DuplicateCheck, DeviceID)
                        If pIsVU Then    'Sapphire
                            'If CardNo <> "Visitor" Then    'LNT
                            Try
                                'Dim wonderValid As Boolean = True
                                'If (MaskWear.ToLower = "no" Or Ftemp.Trim = "" Or TeperatureStatus.ToLower = "true") And IsThermal = "Y" Then
                                '    wonderValid = False
                                'ElseIf MaskWear.ToLower = "no" And IsThermal = "N" Then
                                '    wonderValid = False
                                'End If

#Region "Smartinfocom"

                                Try
                                    sSql = "insert into DumpUserAttendance(DeviceID,UserID,VerifyMode,AttDateTime, CTemp, FTemp,[MaskStatus],[IsAbnomal], UpdatedOn,[io_mode],[AttState]) " &
                        "values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTimeStr & "','" & Ctemp & "', '" & Ftemp & "','" & MaskWear & "','" & TeperatureStatus & "',getdate(),'" & io_mode & "','" & io_mode & "')"
                                    If con.State <> ConnectionState.Open Then
                                        con.Open()
                                    End If
                                    cmd = New SqlCommand(sSql, con)
                                    cmd.ExecuteNonQuery()
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If
                                Catch ex As Exception
                                    If Not ex.Message.ToString.Contains("Violation of PRIMARY KEY constraint") Then
                                        'save failed logs text                               
                                        Dim failedTxt As String = DeviceID & "," & CardNo & "," & verifymode & "," & PuchTimeStr & "," & Ctemp & ", " & Ftemp & "," & MaskWear & "," & TeperatureStatus & "," & io_mode & "," & ex.Message.ToString & ",RealTime"
                                        TraceServiceFailedLogsDump(failedTxt)
                                    End If
                                End Try
#End Region

                                Try
                                    sSql = "select Userid from UserAttendance  with (nolock) where DeviceID='" & DeviceID & "' and UserID='" & CardNo & "' and AttDateTime='" & PuchTimeStr & "' " &
                            " IF @@ROWCOUNT=0 " &
                            "insert into UserAttendance(DeviceID,UserID,VerifyMode,AttDateTime, CTemp, FTemp,[MaskStatus],[IsAbnomal],[LImage], UpdateedOn,[io_mode],[AttState]) " &
                            "values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTimeStr & "','" & Ctemp & "', '" & Ftemp & "','" & MaskWear & "','" & TeperatureStatus & "',@LImage,Getdate(),'" & io_mode & "','" & io_mode & "')"


                                    ' 'for tempreture adjustment
                                    'sSql = "DECLARE @TEMP varchar(100)
                                    'select @TEMP=isnull(FTemp,'') from UserAttendance  with (nolock) where DeviceID='" & DeviceID & "' and UserID='" & CardNo & "' and AttDateTime='" & PuchTimeStr & "'
                                    'IF @TEMP='' 
                                    'BEGIN
                                    ' update UserAttendance set CTemp='" & Ctemp & "', FTemp='" & Ftemp & "' where DeviceID='" & DeviceID & "' and UserID='" & CardNo & "' and AttDateTime='" & PuchTimeStr & "'
                                    'END
                                    'ELSE
                                    'BEGIN
                                    'insert into UserAttendance(DeviceID,UserID,VerifyMode,AttDateTime, CTemp, FTemp,[MaskStatus],[IsAbnomal],[LImage], UpdateedOn,[io_mode],[AttState]) 
                                    'values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTimeStr & "','" & Ctemp & "', '" & Ftemp & "','" & MaskWear & "','" & TeperatureStatus & "',@LImage,Getdate(),'" & io_mode & "','" & io_mode & "')
                                    'END"



                                    'Main insert
                                    '        sSql = "insert into UserAttendance(DeviceID,UserID,VerifyMode,AttDateTime, CTemp, FTemp,[MaskStatus],[IsAbnomal],[LImage], UpdateedOn,[io_mode],[AttState]) " &
                                    '"values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTimeStr & "','" & Ctemp & "', '" & Ftemp & "','" & MaskWear & "','" & TeperatureStatus & "',@LImage,Getdate(),'" & io_mode & "','" & io_mode & "')"

                                    'TraceServiceText(sSql)
                                    'Query(sSql)

                                    If SaveImage <> "Y" Then
                                        'sSql = sSql.Replace(",[LImage]", "").Replace(",@LImage", "")
                                        bArr = Nothing
                                    End If

                                    If bArr Is Nothing Then
                                        sSql = sSql.Replace(",[LImage]", "").Replace(",@LImage", "")
                                    End If

                                    'If TImage Is Nothing Then
                                    '    sSql = sSql.Replace(",[TImage]", "").Replace(",@TImage", "")
                                    'End If
                                    If con.State <> ConnectionState.Open Then
                                        con.Open()
                                    End If

                                    ''only for wonder cement
                                    'If Not wonderValid Then
                                    '    sSql = sSql.Replace("UserAttendance", "UserAttendanceRaw")
                                    'End If

                                    cmd = New SqlCommand(sSql, con)
                                    If bArr IsNot Nothing Then
                                        'cmd.Parameters.AddWithValue("@LImage", LImage)
                                        Dim sqlParamLogImg As SqlParameter = New SqlParameter("@LImage", SqlDbType.VarBinary)
                                        sqlParamLogImg.Direction = ParameterDirection.Input
                                        sqlParamLogImg.Size = bArr.Length
                                        sqlParamLogImg.Value = bArr
                                        cmd.Parameters.Add(sqlParamLogImg)
                                    End If
                                    cmd.ExecuteNonQuery()
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If

#Region "Panipath"  'Kapoor Industries
                                    '        Try

                                    '            Dim ID_NO As String = "1"
                                    '            Try
                                    '                ID_NO = (dsM.Tables(0).Rows(0)("ID_NO").ToString.Trim) '.ToString("00")
                                    '            Catch ex As Exception
                                    '            End Try
                                    '            'TraceServiceTextPanipat(sSqlM & " " & ID_NO)
                                    '            Dim conTmp As New SqlConnection("Data Source=10.20.33.140;Initial Catalog=iDMS;User Id=sa;Password=kil@1234;MultipleActiveResultSets=true;")
                                    '            Dim conTmp1 As New SqlConnection("Data Source=10.20.33.24;Initial Catalog=Payroll;User Id=sa;Password=;MultipleActiveResultSets=true;")
                                    '            Dim Ezone As String = "COMP"
                                    '            If CardNo.Substring(0, 1) = "4" Then
                                    '                Ezone = "B"
                                    '            Else
                                    '                Ezone = "COMP"
                                    '            End If
                                    '            sSql = "insert into DAILY_ATTANDANCE_TEMP(Ezone,CardNo,Att_Date,PunchTime, MacId, Night) " &
                                    '"values('" & Ezone & "','" & CardNo & "','" & PuchTime.ToString("yyyy-MM-dd") & "','" & PuchTime.ToString("HH:mm") & "','" & ID_NO & "', '0')"

                                    '            'TraceServiceTextPanipat(sSql)
                                    '            Try
                                    '                If conTmp.State <> ConnectionState.Open Then
                                    '                    conTmp.Open()
                                    '                End If
                                    '                cmd = New SqlCommand(sSql, conTmp)
                                    '                cmd.ExecuteNonQuery()
                                    '                If conTmp.State <> ConnectionState.Closed Then
                                    '                    conTmp.Close()
                                    '                End If
                                    '            Catch ex As Exception
                                    '                If conTmp.State <> ConnectionState.Closed Then
                                    '                    conTmp.Close()
                                    '                End If
                                    '                TraceServiceTextPanipat("10.20.33.140 " & sSql & " " & ex.Message)
                                    '            End Try
                                    '            Try
                                    '                If conTmp1.State <> ConnectionState.Open Then
                                    '                    conTmp1.Open()
                                    '                End If
                                    '                cmd = New SqlCommand(sSql, conTmp1)
                                    '                cmd.ExecuteNonQuery()
                                    '                If conTmp1.State <> ConnectionState.Closed Then
                                    '                    conTmp1.Close()
                                    '                End If
                                    '            Catch ex As Exception
                                    '                If conTmp1.State <> ConnectionState.Closed Then
                                    '                    conTmp1.Close()
                                    '                End If
                                    '                TraceServiceTextPanipat("10.20.33.24 " & sSql & " " & ex.Message)
                                    '            End Try
                                    '        Catch ex As Exception
                                    '            TraceServiceTextPanipat(ex.Message)
                                    '        End Try
#End Region

                                    ' Image Delete From Folder Ajitesh
                                    Try
                                        If ImageTodelete.Trim <> "" Then
                                            My.Computer.FileSystem.DeleteFile(ImageTodelete)
                                        End If
                                    Catch ex1 As Exception
                                    End Try


                                Catch ex As Exception
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If

                                    If Not ex.Message.ToString.Contains("Violation of PRIMARY KEY constraint") Then
                                        'save failed logs text                               
                                        Dim failedTxt As String = DeviceID & "," & CardNo & "," & verifymode & "," & PuchTimeStr & "," & Ctemp & ", " & Ftemp & "," & MaskWear & "," & TeperatureStatus & "," & io_mode & "," & ex.Message.ToString & ",RealTime"
                                        TraceServiceFailedLogs(failedTxt)
                                    End If
                                    EHomeForm.TraceService(ex.Message, "DeviceLogList Line 631")
                                    'If con.State <> ConnectionState.Closed Then
                                    '    con.Close()
                                    'End If
                                End Try

#Region "check user in DB"

                                Try 'check user in DB
                                    If EHomeForm.AutoSync = "Y" And IsMasterDevice = "Y" Then
                                        If CardNo <> "Visitor" Then
                                            sSql = "select * from Userdetail where DeviceID ='" & DeviceID & "' and UserID='" & CardNo & "' " &
                                                    "If @@ROWCOUNT=0 " &
                                                    "Begin " &
                                                    "select Dev_Cmd_Id from DeviceCommands where SerialNumber='" & DeviceID & "' and  TransfertoDevice='" & DeviceID & "' and CommandContent='CHECK' and UserID='" & CardNo & "' and Executed ='0' " &
                                                    "IF @@ROWCOUNT=0 " &
                                                    "insert into Devicecommands (SerialNumber, CommandContent, TransferToDevice, UserId, Executed, IsOnline, CreatedOn) " &
                                                    "values('" & DeviceID & "','CHECK','" & DeviceID & "', '" & CardNo & "',0,1,getdate()) " &
                                                    "End"
                                            If con.State <> ConnectionState.Open Then
                                                con.Open()
                                            End If
                                            cmd = New SqlCommand(sSql, con)
                                            Dim count As Integer = cmd.ExecuteNonQuery()
                                            If con.State <> ConnectionState.Closed Then
                                                con.Close()
                                            End If
                                        End If
                                    End If
                                Catch ex As Exception
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If
                                    EHomeForm.TraceService(ex.Message, "DeviceLogList Line 654")
                                End Try
#End Region
                            Catch ex As Exception
                                EHomeForm.TraceService(ex.Message, "DeviceLogList Line 596")
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End Try
                            'End If   'End LNT
                        End If 'end Sapphire

                        CardNo = GetCardNumber(CardNo)
#Region "tblmachine"
                        Try
                            sSql = "Update tblMachine set UpdatedOn =getdate(), LastLog='" & PuchTime.ToString("yyyy-MM-dd HH:mm:ss") & "' where SerialNumber='" & DeviceID & "' "
                            If con.State <> ConnectionState.Open Then
                                con.Open()
                            End If
                            cmd = New SqlCommand(sSql, con)
                            Dim count As Integer = cmd.ExecuteNonQuery()
                            If con.State <> ConnectionState.Closed Then
                                con.Close()
                            End If
                        Catch ex As Exception
                            EHomeForm.TraceService(ex.Message, "DeviceLogList " & sSql & " Line 663")
                            If con.State <> ConnectionState.Closed Then
                                con.Close()
                            End If
                        End Try
#End Region
#Region "machinerawpunchandprocess"
                        If EHomeForm.AttProcess = "Y" Then
                            Try
                                If CardNo <> "Visitor" Then
                                    Dim adpA As New SqlDataAdapter
                                    ds = New DataSet
                                    'sSql = "select TM.id_No,TE.companycode,TE.PRESENTCARDNO,TE.SSN,TE.paycode,TE.DepartmentCode from tblMachine TM  with (nolock) inner join tblemployee TE on Tm.CompanyCode = TE.COMPANYCODE where TM.SerialNumber = '" & DeviceID & "' and TE.PresentCardNo = '" & CardNo & "'"
                                    sSql = "select TES.DUPLICATECHECKMIN, TM.id_No,TE.companycode,TE.PRESENTCARDNO,TE.SSN,TE.paycode,TE.DepartmentCode from tblMachine TM  with (nolock) inner join tblemployee TE on Tm.CompanyCode = TE.COMPANYCODE inner join tblemployeeshiftmaster TES on TES.paycode=TE.paycode where TM.SerialNumber = '" & DeviceID & "' and TE.PresentCardNo = '" & CardNo & "'"
                                    'sSql = "select TM.id_No,TE.companycode,TE.PRESENTCARDNO,TE.SSN,TE.paycode,TE.DepartmentCode from tblMachine TM  with (nolock) inner join tblemployee TE on Tm.CompanyCode = TE.COMPANYCODE inner join tblemployeeshiftmaster TES on TES.paycode=TE.paycode where TM.SerialNumber = '" & DeviceID & "' and TE.PresentCardNo = '" & CardNo & "'"
                                    adpA = New SqlDataAdapter(sSql, con)
                                    adpA.Fill(ds)
                                    If ds.Tables(0).Rows.Count > 0 Then
                                        Dim inout As String = ""
                                        'If (status = "0") Then
                                        '    inout = "I"
                                        'Else
                                        '    inout = "O"
                                        'End If
                                        Dim dupMin As Integer = 5
                                        Dim pIsV As Boolean = False
                                        If ds.Tables(0).Rows(0)("DUPLICATECHECKMIN").ToString().Trim() <> "" Then
                                            dupMin = ds.Tables(0).Rows(0)("DUPLICATECHECKMIN").ToString().Trim()
                                        End If
                                        ' dupMin = 5
                                        pIsV = IsValid(ds.Tables(0).Rows(0)("PayCode").ToString().Trim(), PuchTime, con, dupMin)
                                        If pIsV Then
                                            Try
                                                sSql = "insert into MachineRawPunch (CardNo,OfficePunch,P_Day,IsManual,MC_NO,INOUT,PAYCODE,SSN,CompanyCode)values('" & ds.Tables(0).Rows(0)("PresentCardNo").ToString().Trim() & "','" & PuchTime.ToString("yyyy-MM-dd HH:mm") & "','N','N'," & ds.Tables(0).Rows(0)("id_No").ToString().Trim() & ",'" & inout & "','" & ds.Tables(0).Rows(0)("PayCode").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("SSN").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "')"
                                                If con.State <> ConnectionState.Open Then
                                                    con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, con)
                                                cmd.ExecuteNonQuery()

                                                If con.State <> ConnectionState.Closed Then
                                                    con.Close()
                                                End If
                                            Catch ex As Exception
                                                EHomeForm.TraceService(ex.Message, "DeviceLogList Line 252")
                                                If con.State <> ConnectionState.Closed Then
                                                    con.Close()
                                                End If
                                            End Try

                                            'temp comment
                                            sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup where companycode='" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "') "
                                            Dim ds1 As DataSet = New DataSet
                                            Try
                                                adpA = New SqlDataAdapter(sSql, con)
                                                adpA.Fill(ds1)
                                            Catch ex As Exception
                                                EHomeForm.TraceService(ex.Message, "DeviceLogList Line 1563")
                                                If con.State <> ConnectionState.Closed Then
                                                    con.Close()
                                                End If
                                            End Try

                                            Dim SetupID As String = ""
                                            If ds1.Tables(0).Rows.Count > 0 Then
                                                SetupID = ds1.Tables(0).Rows(0)(0).ToString.Trim
                                                Try
                                                    If (con.State <> ConnectionState.Open) Then
                                                        con.Open()
                                                    End If
                                                    'Dim tmpdatetime As DateTime = Convert.ToDateTime(atttime.ToString.Trim)
                                                    cmd = New SqlCommand("ProcessBackDate", con)
                                                    cmd.CommandTimeout = 1800
                                                    cmd.CommandType = CommandType.StoredProcedure
                                                    cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                                                    cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                                                    cmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("CompanyCode").ToString.Trim
                                                    cmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("DepartmentCode").ToString.Trim
                                                    cmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("PayCode").ToString.Trim
                                                    cmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID
                                                    cmd.ExecuteNonQuery()
                                                    If (con.State <> ConnectionState.Closed) Then
                                                        con.Close()
                                                    End If
                                                Catch ex As Exception
                                                    EHomeForm.TraceService(ex.Message, "DeviceLogList Line 293")
                                                    If con.State <> ConnectionState.Closed Then
                                                        con.Close()
                                                    End If
                                                End Try
                                            End If
                                        End If
                                        'end temp comment
                                    End If
                                End If
                            Catch ex As Exception
                                EHomeForm.TraceService(ex.Message, "DeviceLogList Line 302")
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End Try
                        End If
#End Region
                    End If
                Catch e As IOException
                    str = String.Format("[ACS ALARM] save file failed![%s]", e.ToString())
                    EHomeForm.TraceService(e.Message, "DeviceLogList Line 322")
                End Try
            End If
            System.Windows.Forms.Application.DoEvents()
        Catch ex As Exception

        End Try

    End Sub
    Public Function XmlPrase(ByVal pXml As IntPtr, ByRef pInputBefore As String, ByRef pInputAfter As String, ByRef pOutput As String) As Boolean
        If (pXml = IntPtr.Zero) OrElse (pInputBefore = String.Empty) OrElse (pInputAfter = String.Empty) Then
            Return False
        End If
        Dim InputXml As String = Marshal.PtrToStringAnsi(pXml)
        Dim InputBeforeStr As String = pInputBefore
        Dim InputAfterStr As String = pInputAfter
        If (InputBeforeStr.Length = 0) OrElse (InputAfterStr.Length = 0) Then
            Return False
        End If
        Dim FirstIndex As Integer = InputXml.LastIndexOf(InputBeforeStr)
        Dim SecondIndex As Integer = InputXml.LastIndexOf(InputAfterStr)
        If (FirstIndex = -1) OrElse (SecondIndex = -1) OrElse (FirstIndex + InputBeforeStr.Length = SecondIndex) Then
            Return False
        End If
        pOutput = InputXml.Substring(FirstIndex + InputBeforeStr.Length, SecondIndex - FirstIndex - InputAfterStr.Length + 1)
        Return True
    End Function
    Private Sub EHomeDemo_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)
        Try
            StopPreviewListen()
            Dim struAmsAddr As New HCEHomePublic.NET_EHOME_AMS_ADDRESS()
            struAmsAddr.dwSize = Marshal.SizeOf(struAmsAddr)
            struAmsAddr.byEnable = 2
            Dim ptrAmsAddr As IntPtr = Marshal.AllocHGlobal(struAmsAddr.dwSize)
            Marshal.StructureToPtr(struAmsAddr, ptrAmsAddr, False)
            HCEHomeCMS.NET_ECMS_SetSDKLocalCfg(HCEHomePublic.NET_EHOME_LOCAL_CFG_TYPE.AMS_ADDRESS, ptrAmsAddr)
            Marshal.FreeHGlobal(ptrAmsAddr)
            HCEHomeAlarm.NET_EALARM_StopListen(m_lUdpAlarmHandle)
            HCEHomeAlarm.NET_EALARM_StopListen(m_lAlarmHandle)
            HCEHomeAlarm.NET_EALARM_StopListen(m_lEhome50AlarmHandle)
            HCEHomeAlarm.NET_EALARM_Fini()
            HCEHomeCMS.NET_ECMS_StopListen(0)
            HCEHomeCMS.NET_ECMS_Fini()
            HCEHomeStream.NET_ESTREAM_Fini()
            If g_bSS_Enable Then
                HCEHomeSS.NET_ESS_StopListen(m_lSSHandle)
                HCEHomeSS.NET_ESS_Fini()
            End If
        Catch ex As Exception

        End Try
    End Sub
    'Private Sub m_previewPanelOne_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles m_previewPanelOne.MouseDown

    '    'ControlPaint.DrawBorder(e., ClientRectangle, Color.Ivory, ButtonBorderStyle.Solid);
    '    m_iCurWndIndex = 0
    '    g_deviceTree.m_iCurWndIndex = 0
    'End Sub

    'Private Sub m_previewPanelTwo_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles m_previewPanelTwo.MouseDown

    '    m_iCurWndIndex = 1
    '    g_deviceTree.m_iCurWndIndex = 1
    'End Sub

    'Private Sub m_previewPanelThree_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles m_previewPanelThree.MouseDown

    '    m_iCurWndIndex = 2
    '    g_deviceTree.m_iCurWndIndex = 2
    'End Sub

    'Private Sub m_previewPanelFour_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles m_previewPanelFour.MouseDown

    '    m_iCurWndIndex = 3
    '    g_deviceTree.m_iCurWndIndex = 3
    'End Sub

    'Private Sub audioTalk_Click(ByVal sender As Object, ByVal e As EventArgs) Handles audioTalk_Conflict.Click
    '    Me.m_previewWnd.Visible = False
    '    Me.g_AudioTalk.Visible = True
    '    Me.g_TotalPanel.Controls.Clear()
    '    Me.g_TotalPanel.Controls.Add(g_AudioTalk)
    '    Me.g_TotalPanel.Controls.Add(m_panelDeviceLog)

    '    g_AudioTalk.CheckInitParam()

    'End Sub

    'Private Sub buttonPreview_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonPreview.Click
    '    If Not Me.buttonPreview.IsHandleCreated Then Return

    '    Me.g_AudioTalk.Visible = False
    '    Me.m_previewWnd.Visible = True
    '    Me.m_panelDeviceLog.Visible = True
    '    Me.g_TotalPanel.Controls.Clear()
    '    Me.g_TotalPanel.Controls.Add(m_previewWnd)
    '    Me.g_TotalPanel.Controls.Add(m_panelDeviceLog)
    'End Sub

    'Private Sub radioBtnLocalLog_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles m_rdoLocalLog.CheckedChanged
    '    If m_rdoLocalLog.Checked Then
    '        g_formList.showList(0)
    '    ElseIf m_rdoAlarmInformation.Checked Then
    '        g_formList.showList(1)
    '    End If
    'End Sub

    'Private Sub m_rdoAlarmInformation_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles m_rdoAlarmInformation.CheckedChanged
    '    If m_rdoLocalLog.Checked Then
    '        g_formList.showList(0)
    '    ElseIf m_rdoAlarmInformation.Checked Then
    '        g_formList.showList(1)
    '    End If
    'End Sub

    'Private Sub btnOthers_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnOthers.Click
    '    Me.m_MenuElse.Show(btnOthers, Me.btnOthers.PointToClient(Cursor.Position), ToolStripDropDownDirection.BelowRight)
    'End Sub

    'Private Sub GpsInfoSet_Click(ByVal sender As Object, ByVal e As EventArgs) Handles GpsInfoSet.Click
    '    Dim GPSInfoDlg As New GPSInfoSet()
    '    GPSInfoDlg.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    '    GPSInfoDlg.ShowDialog()
    'End Sub

    'Private Sub CommonConfigure_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim CommonConfigureDlg As New CommonConfigure()
    '    CommonConfigureDlg.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    '    CommonConfigureDlg.ShowDialog()
    'End Sub


    'Private Sub m_panelDeviceLog_MouseDoubleClick(ByVal sender As Object, ByVal e As MouseEventArgs)
    '    Me.m_previewWnd.Visible = False
    '    Dim x As Integer = Me.g_TotalPanel.Location.X
    '    Dim y As Integer = Me.g_TotalPanel.Location.Y
    '    Me.m_panelDeviceLog.Location = New System.Drawing.Point(x, y)
    '    Dim w As Integer = Me.g_TotalPanel.Width
    '    Dim h As Integer = Me.g_TotalPanel.Height
    '    Me.m_panelDeviceLog.Size = New System.Drawing.Size(w, h)

    'End Sub
    Private Sub EHomeForm_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            StopPreviewListen()
            Dim struAmsAddr As New HCEHomePublic.NET_EHOME_AMS_ADDRESS()
            struAmsAddr.dwSize = Marshal.SizeOf(struAmsAddr)
            struAmsAddr.byEnable = 2
            Dim ptrAmsAddr As IntPtr = Marshal.AllocHGlobal(struAmsAddr.dwSize)
            Marshal.StructureToPtr(struAmsAddr, ptrAmsAddr, False)
            HCEHomeCMS.NET_ECMS_SetSDKLocalCfg(HCEHomePublic.NET_EHOME_LOCAL_CFG_TYPE.AMS_ADDRESS, ptrAmsAddr)
            Marshal.FreeHGlobal(ptrAmsAddr)
            HCEHomeAlarm.NET_EALARM_StopListen(m_lUdpAlarmHandle)
            HCEHomeAlarm.NET_EALARM_StopListen(m_lAlarmHandle)
            HCEHomeAlarm.NET_EALARM_StopListen(m_lEhome50AlarmHandle)
            HCEHomeAlarm.NET_EALARM_Fini()
            HCEHomeCMS.NET_ECMS_StopListen(0)
            HCEHomeCMS.NET_ECMS_Fini()
            HCEHomeStream.NET_ESTREAM_Fini()
            If g_bSS_Enable Then
                HCEHomeSS.NET_ESS_StopListen(m_lSSHandle)
                HCEHomeSS.NET_ESS_Fini()
            End If
        Catch ex As Exception

        End Try
        Application.Exit()

    End Sub
#End Region
#Region "UserManagement"
    Public Class UserInfoCount
        Public Property userNumber As String
    End Class
    Public Class ResponseStatus
        Public Property requestURL As String
        Public Property statusCode As Integer
        Public Property statusString As String
        Public Property subStatusCode As String
        Public Property errorCode As Integer
        Public Property errorMsg As String
    End Class
    Public Class Valid
        Public Property enable As String
        Public Property beginTime As String
        Public Property endTime As String
        Public Property timeType As String
    End Class
    Public Class RightPlanItem
        Public Property doorNo As Integer
        Public Property planTemplateNo As String
    End Class
    Public Class UserInfoItem
        Public Property employeeNo As String
        Public Property name As String
        Public Property userType As String
        Public Property closeDelayEnabled As String
        Public Property Valid As Valid
        Public Property belongGroup As String
        Public Property password As String
        Public Property doorRight As String
        Public Property RightPlan As List(Of RightPlanItem)
        Public Property maxOpenDoorTime As Integer
        Public Property openDoorTime As Integer
        Public Property userVerifyMode As String
    End Class
    Public Class UserInfoSearch
        Public Property searchID As String
        Public Property responseStatusStrg As String
        Public Property numOfMatches As Integer
        Public Property totalMatches As Integer
        Public Property UserInfo As List(Of UserInfoItem)
    End Class
    Public Class UserInfoSearchRoot
        Public Property UserInfoSearch As UserInfoSearch
    End Class
    'Private m_iDeviceIndex As Integer
    'Private m_lUserID As Integer

    'Private g_deviceTree As DeviceTree = DeviceTree.Instance()
    'Private g_formList As DeviceLogList = DeviceLogList.Instance()
    Private struPTXML As HCEHomeCMS.NET_EHOME_PTXML_PARAM = New HCEHomeCMS.NET_EHOME_PTXML_PARAM()
    Private Method As String = String.Empty
    Private m_szInputBuffer As Byte() = New Byte(1499) {}
    Private m_szOutBuffer As Byte() = New Byte(10239) {}
    Private m_szUrl As Byte() = New Byte(1023) {}
    Private ptrCfg As IntPtr = IntPtr.Zero
    Private m_strInputXml As String = String.Empty
    Private strTemp As String = String.Empty
    Private Sub GetUserDataFromDevice(EmployeeNo As String, m_lUserID As Integer, DeviceId As String, Dev_cmd_Id As String, DeviceType As String)
        Dim QueryResult As String = "Y"
        Try
            'Dim EmployeeNo As String = textBoxEmployeeNo.Text
            'm_iDeviceIndex = g_deviceTree.m_iCurDeviceIndex
            'm_lUserID = DeviceTree.g_struDeviceInfo(m_iDeviceIndex).iLoginID

            Dim getUser As Boolean = True
            Dim userStartPos As Integer = 0
            Dim EmpNomList As New List(Of String)()

            Dim ds As DataSet = New DataSet
            Dim us As UserInfoSearchRoot
            'MsgBox("DeviceId " & DeviceId & vbCrLf & "DeviceId " & DeviceId & vbCrLf & "Dev_cmd_Id " & Dev_cmd_Id)
            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            While getUser
                Try
                    Dim strTemp As String
                    Dim m_strInputXml As String
                    Dim OutBufferLen As Integer = 0
                    If EmployeeNo.Length <> 0 And EmployeeNo.Trim <> "0" Then
                        strTemp = "/ISAPI/AccessControl/UserInfo/Search?format=json"
                        m_strInputXml = "{""UserInfoSearchCond"":{""searchID"":""1"",""searchResultPosition"":0,""maxResults"":30,""EmployeeNoList"":[{""employeeNo"":""" & EmployeeNo & """}]}}"
                        OutBufferLen = 5 * 1024
                    Else
                        strTemp = "/ISAPI/AccessControl/UserInfo/Search?format=json"
                        m_strInputXml = "{""UserInfoSearchCond"":{""searchID"":""1"",""searchResultPosition"":" & userStartPos & ",""maxResults"":30}}"
                        OutBufferLen = 30 * 1024
                    End If
                    ConfigMethodUser(strTemp, m_strInputXml, OutBufferLen)
                    If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                        'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_PostPTXMLConfig")
                        TraceService("GetUser " & HCEHomePublic.OPERATION_FAIL_T & " NET_ECMS_PostPTXMLConfig", "Line 1887")
                        Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                        Marshal.FreeHGlobal(struPTXML.pInBuffer)
                        Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                        Marshal.FreeHGlobal(ptrCfg)
                        QueryResult = "N"
                        GoTo updatecmd
                        Return
                    End If
                    'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_PostPTXMLConfig")
                    Dim strOutXML As String = ""
                    Try
                        strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
                    Catch e1 As ArgumentException
                        'MessageBox.Show("Memory Exception")
                        TraceService("GetUser Memory Exception", "Line 1900")
                        Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                        QueryResult = "N"
                        GoTo updatecmd
                        Return
                    End Try

                    If strOutXML <> String.Empty Then
                        us = JsonConvert.DeserializeObject(Of UserInfoSearchRoot)(strOutXML)
                        Query(strOutXML)
                        If 0 = us.UserInfoSearch.totalMatches Then
                            'labelMsg.Text = "Employee No isn't found!"
                            getUser = False
                        Else
                            If us.UserInfoSearch.totalMatches > 0 Then
                                'DeviceId = System.Text.Encoding.[Default].GetString(DeviceTree.g_struDeviceInfo(m_iDeviceIndex).sDeviceSerial).Trim(vbNullChar)
                                '     Dim s As String = "select * from tblmachine inner join ClientDeviceGroupMapping on tblMachine.SerialNumber=ClientDeviceGroupMapping.DeviceId " &
                                '"where tblmachine.GroupId in (select GroupId from ClientDeviceGroupMapping where ClientDeviceGroupMapping.DeviceId='" & DeviceId & "') " &
                                '"and ClientDeviceGroupMapping.DeviceID <> '" & DeviceId & "'"

                                Dim s As String = "select * from tblmachine inner join ClientDeviceGroupMapping on tblMachine.SerialNumber=ClientDeviceGroupMapping.DeviceId 
                                    where tblmachine.GroupId in (
                                    select ClientDeviceGroupMapping. GroupId from ClientDeviceGroupMapping inner join tblMachine on tblMachine.SerialNumber=ClientDeviceGroupMapping.DeviceId 
                                      where ClientDeviceGroupMapping.DeviceId='" & DeviceId & "' and tblmachine.IsMasterDevice='Y') 
                                    and ClientDeviceGroupMapping.DeviceID <> '" & DeviceId & "' "

                                Dim adpA As SqlDataAdapter = New SqlDataAdapter(s, con)
                                adpA.Fill(ds)
                                Dim i As Integer = 1
                                'listViewInfo.Items.Clear()
                                If us.UserInfoSearch.numOfMatches < 10 Then
                                    getUser = False
                                End If
                                'MsgBox("User Count " & us.UserInfoSearch.UserInfo.Count.ToString)
                                For Each userInfo As UserInfoItem In us.UserInfoSearch.UserInfo

                                    'Try
                                    '    s = "update CommandStatus set CommandContent='CHECK',ProcessStatus='Downloading User " & i & " of " & us.UserInfoSearch.totalMatches & "', UpdatedOn=GETDATE() where DeviceID='" & DeviceId & "'
                                    'IF @@ROWCOUNT=0
                                    'insert into CommandStatus (DeviceID ,CommandContent,ProcessStatus,UpdatedOn) values ('" & DeviceId & "','CHECK','Downloading Face " & i & " of " & us.UserInfoSearch.totalMatches & "',GETDATE()) "
                                    '    If (con.State <> ConnectionState.Open) Then
                                    '        con.Open()
                                    '    End If
                                    '    cmd = New SqlCommand(s, con)
                                    '    cmd.ExecuteNonQuery()
                                    '    If (con.State <> ConnectionState.Closed) Then
                                    '        con.Close()
                                    '    End If
                                    'Catch ex As Exception

                                    'End Try

                                    Dim PIN As String = userInfo.employeeNo.Trim(vbNullChar)
                                    EmpNomList.Add(PIN)
                                    Dim Name As String = userInfo.name.Trim(vbNullChar)
                                    Dim PRI As String
                                    If userInfo.userType.Trim(vbNullChar) = "normal" Then
                                        PRI = 0
                                    Else
                                        PRI = 1
                                    End If
                                    Dim Password As String = userInfo.password.Trim(vbNullChar)
                                    Dim AccesstimeFrom As String = ""
                                    Try
                                        AccesstimeFrom = Convert.ToDateTime(userInfo.Valid.beginTime).ToString("yyyy-MM-dd HH:mm:ss")
                                        If (Convert.ToDateTime(userInfo.Valid.beginTime).Year < 2000) Then
                                            AccesstimeFrom = "2000-01-01 00:00:00"
                                        End If
                                    Catch ex As Exception
                                        AccesstimeFrom = "2000-01-01 00:00:00"
                                    End Try
                                    Dim AccesstimeTo As String = ""
                                    Try
                                        AccesstimeTo = Convert.ToDateTime(userInfo.Valid.endTime).ToString("yyyy-MM-dd HH:mm:ss")
                                        If Convert.ToDateTime(userInfo.Valid.endTime).Year > 2037 Then
                                            AccesstimeTo = "2037-12-31 00:00:00"
                                        End If
                                    Catch ex As Exception
                                        AccesstimeTo = "2037-12-31 00:00:00"
                                    End Try
                                    Try
                                        s = "update UserDetail set Name='" & Name & "',Pri='" & PRI & "',Password='" & Password & "', AccesstimeFrom='" & AccesstimeFrom & "',AccesstimeTo='" & AccesstimeTo & "' where UserID='" & PIN & "' and DeviceID='" & DeviceId & "'" &
                                        " IF @@ROWCOUNT=0 " &
                                        " insert into UserDetail(DeviceID,UserID,Name,PRi,Password,AccesstimeFrom,AccesstimeTo,UpdateFlag,isdeleted, UpdatedOn) values('" & DeviceId & "','" & PIN & "','" & Name & "','" & PRI & "','" & Password & "','" & AccesstimeFrom & "','" & AccesstimeTo & "','N','N',getdate())"
                                        If (con.State <> ConnectionState.Open) Then
                                            con.Open()
                                        End If
                                        cmd = New SqlCommand(s, con)
                                        Dim RowsCount As Integer = cmd.ExecuteNonQuery
                                        If (con.State <> ConnectionState.Closed) Then
                                            con.Close()
                                        End If
                                    Catch ex As Exception
                                        TraceService(s & " " & ex.Message, "Line 1225")
                                    End Try
                                    i = i + 1
#Region "copy user to same Group"
                                    'Try 'copy user to same Group
                                    '    If ds.Tables(0).Rows.Count > 0 Then
                                    '        For x As Integer = 0 To ds.Tables(0).Rows.Count - 1
                                    '            Try
                                    '                s = "SELECT UserID from Userdetail where UserID = '" & PIN & "' and DeviceID='" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "' " &
                                    '            "IF @@ROWCOUNT=0  " &
                                    '            "select Dev_Cmd_Id from DeviceCommands where SerialNumber='" & DeviceId & "' and  TransfertoDevice='" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "' and CommandContent='flagupduserinfo' and UserID='" & PIN & "' and Executed ='0' " &
                                    '            "IF @@ROWCOUNT=0 " &
                                    '             " insert into DeviceCommands(SerialNumber,CommandContent,TransfertoDevice,UserID, Executed,CreatedOn)" &
                                    '            "values('" & DeviceId & "','flagupduserinfo','" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "','" & PIN & "','0',getdate())"

                                    '                If (con.State <> ConnectionState.Open) Then
                                    '                    con.Open()
                                    '                End If
                                    '                cmd = New SqlCommand(s, con)
                                    '                Dim RowsCount As Integer = cmd.ExecuteNonQuery
                                    '                If (con.State <> ConnectionState.Closed) Then
                                    '                    con.Close()
                                    '                End If
                                    '            Catch ex As Exception
                                    '                TraceService(s & " " & ex.Message, "Line 1735")
                                    '            End Try
                                    '        Next
                                    '    End If
                                    'Catch ex As Exception
                                    '    TraceService(ex.Message, "Line 1253")
                                    'End Try
#End Region
                                Next
                                userStartPos = userStartPos + us.UserInfoSearch.UserInfo.Count '30
                                'labelMsg.Text = "Search employee data succ!"
                            Else
                                getUser = False
                            End If
                        End If
                    End If

                    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                    Marshal.FreeHGlobal(struPTXML.pInBuffer)
                    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                    'Marshal.FreeHGlobal(ptrCfg1)
                Catch ex As Exception
                    TraceService("GetUser " & ex.Message, "Line 2016")
                    Try
                        Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                        Marshal.FreeHGlobal(struPTXML.pInBuffer)
                        Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                        'Marshal.FreeHGlobal(ptrCfg1)
                    Catch ex1 As Exception

                    End Try

                End Try
            End While
updatecmd:
            Dim EmpArr As String() = EmpNomList.Distinct.ToArray
            If m_lUserID >= 0 Then
                For i As Integer = 0 To EmpArr.Length - 1
                    'Try
                    '    Dim s As String = "update CommandStatus set CommandContent='CHECK',ProcessStatus='Downloading FP/FC/C " & i + 1 & " of " & EmpArr.Length & "', UpdatedOn=GETDATE() where DeviceID='" & DeviceId & "'
                    '    IF @@ROWCOUNT=0
                    '    insert into CommandStatus (DeviceID ,CommandContent,ProcessStatus,UpdatedOn) values ('" & DeviceId & "','CHECK','Downloading Face " & i & " of " & EmpArr.Length & "',GETDATE()) "
                    '    If (con.State <> ConnectionState.Open) Then
                    '        con.Open()
                    '    End If
                    '    cmd = New SqlCommand(s, con)
                    '    cmd.ExecuteNonQuery()
                    '    If (con.State <> ConnectionState.Closed) Then
                    '        con.Close()
                    '    End If
                    'Catch ex As Exception

                    'End Try

                    If DeviceType = "Face" Then
                        GetUserFace(EmpArr(i), m_lUserID, DeviceId, ds)
                    ElseIf DeviceType = "Finger" Then
                        GetUserFinger(EmpArr(i), m_lUserID, DeviceId, ds)
                    ElseIf DeviceType = "Both" Then
                        GetUserFace(EmpArr(i), m_lUserID, DeviceId, ds)
                        GetUserFinger(EmpArr(i), m_lUserID, DeviceId, ds)
                    End If

                    GetUserCard(EmpArr(i), m_lUserID, DeviceId)

                Next

                'Try
                '    Dim s As String = "Delete from CommandStatus where DeviceID='" & DeviceId & "'"
                '    If (con.State <> ConnectionState.Open) Then
                '        con.Open()
                '    End If
                '    cmd = New SqlCommand(s, con)
                '    cmd.ExecuteNonQuery()
                '    If (con.State <> ConnectionState.Closed) Then
                '        con.Close()
                '    End If
                'Catch ex As Exception

                'End Try


                ''For Each userInfo As UserInfoItem In us.UserInfoSearch.UserInfo
                ''    GetUserFinger(userInfo.employeeNo, m_lUserID, DeviceId, ds)
                ''    GetUserCard(userInfo.employeeNo, m_lUserID, DeviceId)
                ''Next
            End If

            'Dim s1 As String = "update DeviceCommands set Executed='1' where Dev_cmd_Id='" & Dev_cmd_Id & "'"
            Dim s1 As String = "update DeviceCommands Set Executed='1', QueryResult='" & QueryResult & "', ExecutedOn=GETDATE() where Dev_cmd_Id='" & Dev_cmd_Id & "'"
            If (con.State <> ConnectionState.Open) Then
                con.Open()
            End If
            cmd = New SqlCommand(s1, con)
            cmd.ExecuteNonQuery()
            If (con.State <> ConnectionState.Closed) Then
                con.Close()
            End If

            GetDeviceInfo(m_lUserID, DeviceId, "")
            Return
        Catch ex As Exception
            TraceService("GetUser " & ex.Message, "Line 2183")
        End Try
    End Sub
    Public Class FPInfo
        Public Property FingerPrintInfo As FingerPrintInfo
    End Class
    Public Class FingerPrintInfo
        Public Property searchID As String
        Public Property status As String
        Public Property FingerPrintList As List(Of FPList)
    End Class
    Public Class FPList
        Public Property cardReaderNo As Integer
        Public Property fingerPrintID As Integer
        Public Property fingerType As String
        Public Property fingerData As String
        Public Property leaderFP As List(Of String)
    End Class
    Private Sub GetUserFinger(empNo As String, m_lUserID As Integer, DeviceId As String, ds As DataSet)
        Try
            strTemp = "/ISAPI/AccessControl/UserInfo/Search?format=json"
            m_strInputXml = "{""UserInfoSearchCond"":{""searchID"":""1"",""searchResultPosition"":0,""maxResults"":30,""EmployeeNoList"":[{""employeeNo"":""" & empNo & """}]}}"
            ConfigMethod(strTemp, m_strInputXml)

            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                'MessageBox.Show("Employee Not Existed")
                EHomeForm.TraceService("GetFinger Employee Not Existed EmpNo:" & empNo, "Line 2051")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                Return
            End If

            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)
            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            Dim searchID As String = "1"
            strTemp = "/ISAPI/AccessControl/FingerPrintUpload?format=json"
            'm_strInputXml = "{""FingerPrintCond"":{""searchID"":""" & searchID & """,""employeeNo"":""" & empNo & """,""cardReaderNo"":1,""fingerPrintID"":" & textBoxFingerPrintNo.Text & "}}"
            For FID As Integer = 1 To 10
                m_strInputXml = "{""FingerPrintCond"":{""searchID"":""" & searchID & """,""employeeNo"":""" & empNo & """,""cardReaderNo"":1,""fingerPrintID"":" & FID & "}}"
                ConfigMethod(strTemp, m_strInputXml)
                If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                    'MessageBox.Show("Error: " & Marshal.GetLastWin32Error())
                    EHomeForm.TraceService("GetFinger  " & Marshal.GetLastWin32Error() & " EmpNo:" & empNo, "Line 2072")
                    'Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                    'Marshal.FreeHGlobal(struPTXML.pInBuffer)
                    'Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                    'Marshal.FreeHGlobal(ptrCfg)
                    Continue For 'Return
                End If

                Dim strOutXML As String = String.Empty
                Dim fingerData As String = String.Empty
                strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
                'strOutXML = "{
                '	""FingerPrintInfo"":	{
                '		""searchID"":	""1"",
                '		""status"":	""OK"",
                '		""FingerPrintList"":	[{
                '				""cardReaderNo"":	1,
                '				""fingerPrintID"":	1,
                '				""fingerType"":	""normalFP"",
                '				""fingerData"":	""MzAxPCPRFG+MdCMtFW9wBzLlFV+EhzqpFV9wgGFZFG+QXmj1FN9gamp1FH+M226xFH904iahFF+Y7Wu5FW94iGp5FY9sEi3tFG98A081FjmAi3rFFX+UjYPBFY+0kY99FH/AzozpFP+QUnNFFlW8loeRJa+Ih5ZxFS+NKKKRJJ+8Qp5hJV+BLLGFFZ9wpKs5Fc5sPMlBFE/APK9NJa5wN5cJJd+hSbqxFZ+wrSGVFj6Yj36NFkqoKyiBFj6MjFN1Fj+QHZG1FK+Y0Y2tFjiko7IVFl/AOcZ5Fii8OoEBFR+R7aO9FjjQM8aBJHmgMMhdJc+0ss7JFWiwRdFBJUapr9KxJZ7ATd1NJLh4MeO1Faiovel9FHhwJunRJbi4U+3FFGhwovB5FfiQyi3dE2ig3UXxE2u0W20FFI+U0wyVFkh4juIdFISQNPUtFHSYrPzRJHSomQACFZSgCAbSFFS4DgXyFJSol/VdFlSYPQAAAAAAXBMAbVcAUKETKM1mEiCzEofCVQCwuCTt6RAAsLkaSXsJAUGwEFIMXwFQwA6RTnEQQRAKEm1iEEG3DY1wTgEh2Bc84xQAQeEg0TQuEtCKCYhvMBAwehakemEgkI0H4twpEjGOFEL6TABhBieypRMAsOkqTVs/AVJQJjBABDJAFh0r2y4hYoMMiwAAAAAAarw="",
                '				""leaderFP"":	[]
                '			}]
                '	}
                '}      "
                Dim fpInfo As FPInfo = JsonConvert.DeserializeObject(Of FPInfo)(strOutXML)

                If strOutXML <> String.Empty Then
                    Query(strOutXML)
                    If fpInfo.FingerPrintInfo.status.Equals("OK") Then
                        'Dim FirstIndex As Integer = strOutXML.LastIndexOf("Data""")  'Data
                        'Dim FirstStr As String = "Data"":" & vbTab & """"   'Data
                        'Dim LastStr As String = """" & vbLf & vbTab & vbTab & vbTab & "}]"
                        'Dim LastIndex As Integer = strOutXML.LastIndexOf("""" & vbLf & vbTab & vbTab & vbTab & "}]")
                        'Dim SubLen As Integer = LastIndex - FirstIndex - LastStr.Length - 1
                        'fingerData = strOutXML.Substring(FirstIndex + FirstStr.Length, SubLen)
                        ''WriteFingerData(fingerData)

                        For fpcount As Integer = 0 To fpInfo.FingerPrintInfo.FingerPrintList.Count - 1 ' one id can have multiple fp
                            fingerData = fpInfo.FingerPrintInfo.FingerPrintList(fpcount).fingerData
                            Dim FIndex As Integer = fpInfo.FingerPrintInfo.FingerPrintList(fpcount).fingerPrintID
                            Dim PIN As String = empNo
                            Dim Size As String = fingerData.Length ' RowArr(2).Substring(RowArr(2).IndexOf("=", 0) + 1)
                            Dim Valid As String = 1 ' RowArr(3).Substring(RowArr(3).IndexOf("=", 0) + 1)
                            'Dim FingerTemplate As String '= RowArr(4).Substring(RowArr(4).IndexOf("=", 0) + 1)
                            Dim s As String = ""
                            Try
                                s = "update UserFinger SET FingerTemplate='" & fingerData & "', Size=@Size  where userid=@UserID and DeviceID=@DeviceID and FID=@FID " &
                                "IF @@ROWCOUNT=0 " &
                                " insert into UserFinger(DeviceID,UserID,FID,Size,Valid,FingerTemplate) values(@DeviceID,@UserID,@FID,@Size,@Valid,@FingerTemplate)"
                                If (con.State <> ConnectionState.Open) Then
                                    con.Open()
                                End If
                                cmd = New SqlCommand(s, con)
                                cmd.Parameters.AddWithValue("@DeviceID", DeviceId)
                                cmd.Parameters.AddWithValue("@UserID", PIN)
                                cmd.Parameters.AddWithValue("@FID", FID)
                                cmd.Parameters.AddWithValue("@Size", Size)
                                cmd.Parameters.AddWithValue("@Valid", Valid)
                                cmd.Parameters.AddWithValue("@FingerTemplate", fingerData)
                                Dim RowsCount As Integer = cmd.ExecuteNonQuery
                                If (con.State <> ConnectionState.Closed) Then
                                    con.Close()
                                End If
                            Catch ex As Exception
                                TraceService("GetFinger " & s & " " & ex.Message, "Line 1802")
                            End Try
                        Next


                    ElseIf fpInfo.FingerPrintInfo.status.Equals("NoFP") Then
                        'MessageBox.Show("This Employee doesn't input FingerPrint Info")
                        EHomeForm.TraceService("GetFinger This Employee doesn't input FingerPrint Info EmpNo:" & empNo, "Line 2153")
                    End If
                End If
            Next
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)
            Return
        Catch ex As Exception
            TraceService("GetUseFinger" & ex.Message, "Line 2318")
        End Try
    End Sub
    Public Class MatchListItem
        Public Property FPID() As String
        Public Property faceURL() As String
        Public Property name() As String
        Public Property modelData() As String
    End Class
    Public Class Root
        Public Property requestURL() As String
        Public Property statusCode() As Integer
        Public Property statusString() As String
        Public Property subStatusCode() As String
        Public Property responseStatusStrg() As String
        Public Property numOfMatches() As Integer
        Public Property totalMatches() As Integer
        Public Property MatchList() As List(Of MatchListItem)
    End Class
    Private Sub GetUserFace(empNo As String, m_lUserID As Integer, DeviceId As String, ds As DataSet)
        Try
            Thread.Sleep(50) '100
            ''TraceServiceText("empNo:" & empNo & " DeviceId:" & DeviceId)
            'Dim strTemp As String = "/ISAPI/AccessControl/UserInfo/Search?format=json"
            'Dim m_strInputXml As String = "{""UserInfoSearchCond"":{""searchID"":""1"",""searchResultPosition"":0,""maxResults"":30,""EmployeeNoList"":[{""employeeNo"":""" & empNo & """}]}}"
            'ConfigMethod(strTemp, m_strInputXml)
            'If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
            '    'MessageBox.Show("Error: " & Marshal.GetLastWin32Error())
            '    EHomeForm.TraceService("GetFace " & Marshal.GetLastWin32Error() & "Emp No " & empNo, "Line 2185")
            '    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            '    Marshal.FreeHGlobal(struPTXML.pInBuffer)
            '    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            '    Marshal.FreeHGlobal(ptrCfg)
            '    Return
            'End If
            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            Dim strOutXML As String = ""
            'strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            'If strOutXML <> String.Empty Then
            '    Dim us As UserInfoSearchRoot = JsonConvert.DeserializeObject(Of UserInfoSearchRoot)(strOutXML)
            '    If 0 = us.UserInfoSearch.totalMatches Then
            '        'MessageBox.Show("Employee No doesn't found!")
            '        EHomeForm.TraceService("GetFace Employee No doesn't found! Emp No " & empNo, "Line 2199")
            '    End If
            'End If
            'Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            'Marshal.FreeHGlobal(struPTXML.pInBuffer)
            'Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            'Marshal.FreeHGlobal(ptrCfg)

            strTemp = "/ISAPI/Intelligent/FDLib/FDSearch?format=json"
            m_strInputXml = "{""searchResultPosition"":0,""maxResults"":30,""faceLibType"":""blackFD"",""FDID"":""1"",""FPID"":""" & empNo & """}"
            'm_strInputXml = "{""searchResultPosition"":0,""maxResults"":300,""faceLibType"":""blackFD"",""FDID"":""1""}"
            'TraceServiceText(m_strInputXml)
            ConfigMethod(strTemp, m_strInputXml)

            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                'MessageBox.Show("Error: " & Marshal.GetLastWin32Error())
                EHomeForm.TraceService("GetFace " & Marshal.GetLastWin32Error() & " Emp No:" & empNo, "Line 2213")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                Return
            End If
            strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            'TraceServiceText(strOutXML)
            If strOutXML <> String.Empty Then
                Dim rt As Root = JsonConvert.DeserializeObject(Of Root)(strOutXML)
                If rt.statusCode = 1 Then
                    Dim bArr As Byte()
                    If rt.totalMatches = 1 Then
                        Dim picData As String = String.Empty
                        For Each item As MatchListItem In rt.MatchList
                            'If FacepictureBox.Image IsNot Nothing Then
                            '    FacepictureBox.Image.Dispose()
                            '    FacepictureBox.Image = Nothing
                            'End If
                            Dim FacePicUrl As String = item.faceURL
                            If FacePicUrl = String.Empty Then
                                Exit For
                            End If
                            Dim KMSPath As String = "/kms/services/rest/dataInfoService/downloadFile?id="
                            Dim CloudPath As String = "/pic"
                            If FacePicUrl.Contains(KMSPath) Then
                                Dim Length As Integer = KMSPath.Length
                                FacePicUrl = FacePicUrl.Substring(Length, FacePicUrl.Length - Length)
                            ElseIf FacePicUrl.Contains(CloudPath) Then
                                FacePicUrl = FacePicUrl.Substring(5, FacePicUrl.Length - 5)
                            End If
                            Dim BaseURL As String = System.Environment.CurrentDirectory & "\StorageServer\Storage\"
                            'Dim BaseURL As String = System.Environment.CurrentDirectory & "\t\"
                            BaseURL &= FacePicUrl
                            If File.Exists(BaseURL) Then
                                'FacepictureBox.Image = Image.FromFile(BaseURL)
                                'FaceText.Text = FacePicUrl
                                Dim img As Image = Image.FromFile(BaseURL)
                                bArr = imgToByteArray(img)
                                img.Dispose()
                            Else
                                TraceService("GetFace Pic Error " & empNo, "Line 1953")
                                'MessageBox.Show("Get Face Pic Error")
                            End If
                            Try
                                My.Computer.FileSystem.DeleteFile(BaseURL)
                            Catch ex As Exception
                            End Try
                        Next item

                        'save face
                        Dim PIN As String = empNo
                        Dim Size As String = bArr.Length ' RowArr(2).Substring(RowArr(2).IndexOf("=", 0) + 1)
                        Dim Valid As String = 1 ' RowArr(3).Substring(RowArr(3).IndexOf("=", 0) + 1)
                        'Dim FingerTemplate As String '= RowArr(4).Substring(RowArr(4).IndexOf("=", 0) + 1)
                        Dim s As String = ""
                        Try
                            s = "update Userface SET FaceByte=@FaceByte, Size=@Size,UpdatedOn=getdate()  where userid=@UserID and DeviceID=@DeviceID " &
                            "IF @@ROWCOUNT=0 " &
                            " insert into Userface(DeviceID,UserID,FID,Size,Valid,FaceByte,UpdatedOn) values(@DeviceID,@UserID,@FID,@Size,@Valid,@FaceByte,getdate())"
                            If (con.State <> ConnectionState.Open) Then
                                con.Open()
                            End If
                            cmd = New SqlCommand(s, con)
                            cmd.Parameters.AddWithValue("@DeviceID", DeviceId)
                            cmd.Parameters.AddWithValue("@UserID", PIN)
                            cmd.Parameters.AddWithValue("@FID", 0)
                            cmd.Parameters.AddWithValue("@Size", Size)
                            cmd.Parameters.AddWithValue("@Valid", Valid)
                            cmd.Parameters.AddWithValue("@FaceByte", bArr)
                            Dim RowsCount As Integer = cmd.ExecuteNonQuery
                            If (con.State <> ConnectionState.Closed) Then
                                con.Close()
                            End If
                        Catch ex As Exception
                            TraceService("GetFace " & s & " " & ex.Message, "Line 1987")
                        End Try

                        'for smart infocom
                        Try 'copy user in same group
                            If AutoSync = "Y" Then
                                If ds.Tables(0).Rows.Count > 0 Then
                                    For x As Integer = 0 To ds.Tables(0).Rows.Count - 1
                                        Try
                                            s = "SELECT UserID from Userdetail where UserID = '" & PIN & "' and DeviceID='" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "' " &
                                        "IF @@ROWCOUNT=0  " &
                                        "Begin " &
                                        "select Dev_Cmd_Id from DeviceCommands where SerialNumber='" & DeviceId & "' and  TransfertoDevice='" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "' and CommandContent='flagupduserinfo' and UserID='" & PIN & "' and Executed ='0' " &
                                        "IF @@ROWCOUNT=0 " &
                                        "Begin " & " insert into DeviceCommands(SerialNumber,CommandContent,TransfertoDevice,UserID, Executed,CreatedOn)" &
                                        "values('" & DeviceId & "','flagupduserinfo','" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "','" & PIN & "','0',getdate()) " &
                                        "End End"

                                            If (con.State <> ConnectionState.Open) Then
                                                con.Open()
                                            End If
                                            cmd = New SqlCommand(s, con)
                                            Dim RowsCount As Integer = cmd.ExecuteNonQuery
                                            If (con.State <> ConnectionState.Closed) Then
                                                con.Close()
                                            End If

                                            s = "SELECT UserID from Userdetail where UserID = '" & PIN & "' and DeviceID='" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "' " &
                                        "IF @@ROWCOUNT=0  " &
                                        "Begin " & "select Dev_Cmd_Id from DeviceCommands where SerialNumber='" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "' and  TransfertoDevice='" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "' and CommandContent='CHECK' and UserID='" & PIN & "' and Executed ='0' " &
                                        "IF @@ROWCOUNT=0 " &
                                        "insert into Devicecommands (SerialNumber, CommandContent, TransferToDevice, UserId, Executed, IsOnline, CreatedOn) " &
                                            "values('" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "','CHECK','" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "', '" & PIN & "',0,1,getdate()) " &
                                            "End"

                                            'TraceServiceText(s)
                                            If (con.State <> ConnectionState.Open) Then
                                                con.Open()
                                            End If
                                            cmd = New SqlCommand(s, con)
                                            RowsCount = cmd.ExecuteNonQuery
                                            If (con.State <> ConnectionState.Closed) Then
                                                con.Close()
                                            End If
                                        Catch ex As Exception
                                            TraceService(s & " " & ex.Message, "Line 1735")
                                        End Try
                                    Next
                                End If
                            End If
                        Catch ex As Exception
                            TraceService("GetFace " & ex.Message, "Line 1253")
                        End Try

                    Else
                        TraceService("Face Picture isn't found! " & empNo, "Line 1953")
                        'MessageBox.Show("Face Picture isn't found!")
                    End If
                End If
            End If
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)
            Return
        Catch ex As Exception
            TraceService("Face Picture isn't found! " & empNo, "Line 2479")
        End Try
    End Sub
    'Private Sub GetUserFace(empNo As String, m_lUserID As Integer, DeviceId As String, ds As DataSet)
    '    Thread.Sleep(1000)
    '    Dim strTemp As String = "/ISAPI/AccessControl/UserInfo/Search?format=json"
    '    Dim m_strInputXml As String = "{""UserInfoSearchCond"":{""searchID"":""1"",""searchResultPosition"":0,""maxResults"":30,""EmployeeNoList"":[{""employeeNo"":""" & empNo & """}]}}"
    '    Dim ptrCfg As IntPtr = IntPtr.Zero
    '    Dim OutBufferLen As Integer = 1024
    '    ConfigMethodGetFace(ptrCfg, strTemp, m_strInputXml, OutBufferLen)
    '    If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
    '        'MessageBox.Show("Error: " & Marshal.GetLastWin32Error())
    '        EHomeForm.TraceService("Error: " & Marshal.GetLastWin32Error() & "Emp No " & empNo, "Line 2185")
    '        Marshal.FreeHGlobal(struPTXML.pRequestUrl)
    '        Marshal.FreeHGlobal(struPTXML.pInBuffer)
    '        Marshal.FreeHGlobal(struPTXML.pOutBuffer)
    '        Marshal.FreeHGlobal(ptrCfg)
    '        Return
    '    End If
    '    Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
    '    Dim strOutXML As String = ""
    '    strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
    '    If strOutXML <> String.Empty Then
    '        Dim us As UserInfoSearchRoot = JsonConvert.DeserializeObject(Of UserInfoSearchRoot)(strOutXML)
    '        If 0 = us.UserInfoSearch.totalMatches Then
    '            'MessageBox.Show("Employee No doesn't found!")
    '            EHomeForm.TraceService("Employee No doesn't found! Emp No " & empNo, "Line 2199")
    '        End If
    '    End If
    '    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
    '    Marshal.FreeHGlobal(struPTXML.pInBuffer)
    '    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
    '    Marshal.FreeHGlobal(ptrCfg)

    '    strTemp = "/ISAPI/Intelligent/FDLib/FDSearch?format=json"
    '    m_strInputXml = "{""searchResultPosition"":0,""maxResults"":30,""faceLibType"":""blackFD"",""FDID"":""1"",""FPID"":""" & empNo & """}"
    '    OutBufferLen = 30 * 1024
    '    ConfigMethodGetFace(ptrCfg, strTemp, m_strInputXml, OutBufferLen)

    '    If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
    '        'MessageBox.Show("Error: " & Marshal.GetLastWin32Error())
    '        EHomeForm.TraceService("Error: " & Marshal.GetLastWin32Error() & " Emp No:" & empNo, "Line 2213")
    '        Marshal.FreeHGlobal(struPTXML.pRequestUrl)
    '        Marshal.FreeHGlobal(struPTXML.pInBuffer)
    '        Marshal.FreeHGlobal(struPTXML.pOutBuffer)
    '        Marshal.FreeHGlobal(ptrCfg)
    '        Return
    '    End If
    '    strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
    '    If strOutXML <> String.Empty Then
    '        Dim rt As Root = JsonConvert.DeserializeObject(Of Root)(strOutXML)
    '        If rt.statusCode = 1 Then
    '            Dim bArr As Byte()
    '            If rt.totalMatches = 1 Then
    '                Dim picData As String = String.Empty
    '                For Each item As MatchListItem In rt.MatchList
    '                    'If FacepictureBox.Image IsNot Nothing Then
    '                    '    FacepictureBox.Image.Dispose()
    '                    '    FacepictureBox.Image = Nothing
    '                    'End If
    '                    Dim FacePicUrl As String = item.faceURL
    '                    If FacePicUrl = String.Empty Then
    '                        Exit For
    '                    End If
    '                    Dim KMSPath As String = "/kms/services/rest/dataInfoService/downloadFile?id="
    '                    Dim CloudPath As String = "/pic"
    '                    If FacePicUrl.Contains(KMSPath) Then
    '                        Dim Length As Integer = KMSPath.Length
    '                        FacePicUrl = FacePicUrl.Substring(Length, FacePicUrl.Length - Length)
    '                    ElseIf FacePicUrl.Contains(CloudPath) Then
    '                        FacePicUrl = FacePicUrl.Substring(5, FacePicUrl.Length - 5)
    '                    End If
    '                    Dim BaseURL As String = System.Environment.CurrentDirectory & "\StorageServer\Storage\"
    '                    BaseURL &= FacePicUrl
    '                    If File.Exists(BaseURL) Then
    '                        'FacepictureBox.Image = Image.FromFile(BaseURL)
    '                        'FaceText.Text = FacePicUrl
    '                        Dim img As Image = Image.FromFile(BaseURL)
    '                        bArr = imgToByteArray(img)
    '                    Else
    '                        TraceService("Get Face Pic Error " & empNo, "Line 1953")
    '                        'MessageBox.Show("Get Face Pic Error")
    '                    End If
    '                Next item

    '                'save face
    '                Dim PIN As String = empNo
    '                Dim Size As String = bArr.Length ' RowArr(2).Substring(RowArr(2).IndexOf("=", 0) + 1)
    '                Dim Valid As String = 1 ' RowArr(3).Substring(RowArr(3).IndexOf("=", 0) + 1)
    '                'Dim FingerTemplate As String '= RowArr(4).Substring(RowArr(4).IndexOf("=", 0) + 1)
    '                Dim s As String = ""
    '                Try
    '                    s = "update Userface SET FaceByte=@FaceByte, Size=@Size,UpdatedOn=getdate()  where userid=@UserID and DeviceID=@DeviceID " &
    '                    "IF @@ROWCOUNT=0 " &
    '                    " insert into Userface(DeviceID,UserID,FID,Size,Valid,FaceByte,UpdatedOn) values(@DeviceID,@UserID,@FID,@Size,@Valid,@FaceByte,getdate())"
    '                    If (con.State <> ConnectionState.Open) Then
    '                        con.Open()
    '                    End If
    '                    cmd = New SqlCommand(s, con)
    '                    cmd.Parameters.AddWithValue("@DeviceID", DeviceId)
    '                    cmd.Parameters.AddWithValue("@UserID", PIN)
    '                    cmd.Parameters.AddWithValue("@FID", 0)
    '                    cmd.Parameters.AddWithValue("@Size", Size)
    '                    cmd.Parameters.AddWithValue("@Valid", Valid)
    '                    cmd.Parameters.AddWithValue("@FaceByte", bArr)
    '                    Dim RowsCount As Integer = cmd.ExecuteNonQuery
    '                    If (con.State <> ConnectionState.Closed) Then
    '                        con.Close()
    '                    End If
    '                Catch ex As Exception
    '                    TraceService(s & " " & ex.Message, "Line 1987")
    '                End Try
    '                'Try
    '                '    If ds.Tables(0).Rows.Count > 0 Then
    '                '        For x As Integer = 0 To ds.Tables(0).Rows.Count - 1
    '                '            Try
    '                '                s = "SELECT UserID from Userface where UserID = '" & PIN & "' and DeviceID='" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "' " &
    '                '               " IF @@ROWCOUNT=0 " &
    '                '               "select Dev_Cmd_Id from DeviceCommands where SerialNumber='" & DeviceId & "' and  TransfertoDevice='" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "' and CommandContent='flagsetface' and UserID='" & PIN & "' and Executed ='0' " &
    '                '                "IF @@ROWCOUNT=0 " &
    '                '               "insert into DeviceCommands(SerialNumber,CommandContent,TransfertoDevice,UserID, Executed,CreatedOn) " &
    '                '               "values('" & DeviceId & "','flagsetface','" & ds.Tables(0).Rows(x)("DeviceID").ToString.Trim & "','" & PIN & "','0',getdate())"

    '                '                If (con.State <> ConnectionState.Open) Then
    '                '                    con.Open()
    '                '                End If
    '                '                cmd = New SqlCommand(s, con)
    '                '                Dim RowsCount As Integer = cmd.ExecuteNonQuery
    '                '                If (con.State <> ConnectionState.Closed) Then
    '                '                    con.Close()
    '                '                End If
    '                '            Catch ex As Exception
    '                '                TraceService(s & " " & ex.Message, "Line 2000")
    '                '            End Try
    '                '        Next
    '                '    End If
    '                'Catch ex As Exception
    '                '    TraceService(ex.Message, "Line 1835")
    '                'End Try

    '            Else
    '                TraceService("Face Picture isn't found! " & empNo, "Line 1953")
    '                'MessageBox.Show("Face Picture isn't found!")
    '            End If
    '        End If
    '    End If
    '    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
    '    Marshal.FreeHGlobal(struPTXML.pInBuffer)
    '    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
    '    Marshal.FreeHGlobal(ptrCfg)
    '    Return
    'End Sub
    Public Function imgToByteArray(ByVal img As Image) As Byte()
        Using mStream As New MemoryStream()
            img.Save(mStream, img.RawFormat)
            Return mStream.ToArray()
        End Using
    End Function
    Public Class CardInfo
        Public Property employeeNo As String
        Public Property cardNo As String
        Public Property cardType As String
        Public Property leaderCard As String
    End Class
    Public Class CardInfoSearch
        Public Property searchID As String
        Public Property responseStatusStrg As String
        Public Property numOfMatches As String
        Public Property totalMatches As String
        Public Property CardInfo As List(Of CardInfo)
    End Class
    Public Class CardInfoSearchRoot
        Public Property CardInfoSearch As CardInfoSearch
    End Class
    Private Sub GetUserCard(empNo As String, m_lUserID As Integer, DeviceId As String)
        Try
            Dim strTemp As String = "/ISAPI/AccessControl/CardInfo/Search?format=json"
            Dim m_strInputXml As String = "{""CardInfoSearchCond"":{""searchID"":""1"",""searchResultPosition"":0,""maxResults"":30,""EmployeeNoList"":[{""employeeNo"":""" & empNo & """}]}}"
            m_szUrl = Encoding.UTF8.GetBytes(strTemp)
            struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
            Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
            struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
            strTemp = m_strInputXml

            If "" = strTemp Then
                struPTXML.pInBuffer = IntPtr.Zero
                struPTXML.dwInSize = 0
            Else
                m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
                struPTXML.pInBuffer = Marshal.AllocHGlobal(1024 * 10)
                Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
                struPTXML.dwInSize = CUInt(strTemp.Length)
            End If

            struPTXML.pOutBuffer = Marshal.AllocHGlobal(1024 * 10)

            For i As Integer = 0 To 1024 * 10 - 1
                Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
            Next

            struPTXML.dwOutSize = CUInt((1024 * 10))
            struPTXML.byRes = New Byte(31) {}
            Dim ptrCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
            Marshal.StructureToPtr(struPTXML, ptrCfg, False)

            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                'MessageBox.Show("Error: " & Marshal.GetLastWin32Error())
                EHomeForm.TraceService("GetCard " & Marshal.GetLastWin32Error() & "Emp No:" & empNo, "Line 2378")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                Return
            End If
            Dim strOutXML As String = ""
            strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            If strOutXML <> "" Then
                Dim cs As CardInfoSearchRoot = JsonConvert.DeserializeObject(Of CardInfoSearchRoot)(strOutXML)
                If cs.CardInfoSearch.responseStatusStrg.Equals("OK") Then
                    Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)

                    'MessageBox.Show("Search Card Succ!")
                    For Each info As CardInfo In cs.CardInfoSearch.CardInfo
                        Dim s As String
                        Try
                            s = "update Userdetail set Card='" & info.cardNo & "' where UserID='" & empNo & "' and DeviceID='" & DeviceId & "'"
                            If (con.State <> ConnectionState.Open) Then
                                con.Open()
                            End If
                            cmd = New SqlCommand(s, con)
                            Dim RowsCount As Integer = cmd.ExecuteNonQuery
                            If (con.State <> ConnectionState.Closed) Then
                                con.Close()
                            End If
                        Catch ex As Exception
                            TraceService(s & " " & ex.Message, "Line 1512")
                        End Try
                        Exit For
                    Next
                ElseIf cs.CardInfoSearch.responseStatusStrg.Equals("NO MATCH") Then
                    'MessageBox.Show("CardInfo not exist!")
                Else
                    'MessageBox.Show(cs.CardInfoSearch.responseStatusStrg)
                End If
            End If
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)
        Catch
        End Try

        Return
    End Sub
    Public Sub ConfigMethod(ByVal strTemp As String, ByVal m_strInputXml As String)
        Try
            m_szUrl = Encoding.UTF8.GetBytes(strTemp)
            struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
            Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
            struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
            strTemp = m_strInputXml

            If "" = strTemp Then
                struPTXML.pInBuffer = IntPtr.Zero
                struPTXML.dwInSize = 0
            Else
                m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
                struPTXML.pInBuffer = Marshal.AllocHGlobal(1024 * 10)
                Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
                struPTXML.dwInSize = CUInt(strTemp.Length)
            End If

            struPTXML.pOutBuffer = Marshal.AllocHGlobal(1024 * 10)

            For i As Integer = 0 To (1024 * 10) - 1
                Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
            Next i

            struPTXML.dwOutSize = CUInt(1024 * 10)
            struPTXML.byRes = New Byte(31) {}

            Try
                ptrCfg = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
                Marshal.StructureToPtr(struPTXML, ptrCfg, True)
            Catch e1 As ArgumentException
                'MessageBox.Show("非托管内存空间异常")
                EHomeForm.TraceService(e1.Message.ToString, "Line 2456")
                Throw
            End Try
        Catch
        End Try
    End Sub
    Public Sub ConfigMethodUserSet(ByVal strTemp As String, ByVal m_strInputXml As String, ByVal OutBufferLen As Integer)
        Try
            Dim m_szInputBuffer(strTemp.Length - 1) As Byte
            m_szUrl = Encoding.Default.GetBytes(strTemp)
            struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
            Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
            struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
            strTemp = m_strInputXml
            If "" = strTemp Then
                struPTXML.pInBuffer = IntPtr.Zero
                struPTXML.dwInSize = 0
            Else
                m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
                struPTXML.pInBuffer = Marshal.AllocHGlobal(strTemp.Length)
                Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
                struPTXML.dwInSize = CUInt(m_szInputBuffer.Length)
            End If
            struPTXML.pOutBuffer = Marshal.AllocHGlobal(OutBufferLen)
            For i As Integer = 0 To OutBufferLen - 1
                Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
            Next i
            struPTXML.dwOutSize = CUInt(OutBufferLen)
            struPTXML.byRes = New Byte(31) {}
            Try
                ptrCfg = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
                Marshal.StructureToPtr(struPTXML, ptrCfg, True)
            Catch e1 As ArgumentException
                'MessageBox.Show("Memory Exception")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                Return
            End Try
        Catch
        End Try
    End Sub
#Region "Upload User"
    'Private Sub SetUserDataToDevice(EmployeeNo As String, m_lUserID As Integer, FromDeviceId As String, Dev_cmd_Id As String)
    '    Try
    '        Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
    '        Dim ds As DataSet = New DataSet
    '        Dim adap As SqlDataAdapter
    '        Dim sSql As String = "Select * from Userdetail where UserID='" & EmployeeNo & "' and DeviceID='" & FromDeviceId & "'"
    '        adap = New SqlDataAdapter(sSql, con)
    '        adap.Fill(ds)
    '        If ds.Tables(0).Rows.Count = 0 Then
    '            Return
    '        End If
    '        Dim cardNo As String = ds.Tables(0).Rows(0)("Card").ToString.Trim
    '        strTemp = "/ISAPI/AccessControl/UserInfo/SetUp?format=json"
    '        m_strInputXml = "{""UserInfo"":{""employeeNo"":""" & EmployeeNo & """,""name"":""" & ds.Tables(0).Rows(0)("Name").ToString.Trim & """,""userType"":""normal"",""Valid"":{""enable"":true,""beginTime"":""" & Convert.ToDateTime(ds.Tables(0).Rows(0)("AccesstimeFrom").ToString.Trim).ToString("yyyy-MM-ddTHH:mm:ss") & """,""endTime"":""" & Convert.ToDateTime(ds.Tables(0).Rows(0)("AccesstimeTo").ToString.Trim).ToString("yyyy-MM-ddTHH:mm:ss") & """},""doorRight"": ""1"",""RightPlan"":[{""doorNo"":1,""planTemplateNo"":""1""}]}}"
    '        'normal
    '        Dim ptrCfg1 As IntPtr = IntPtr.Zero
    '        m_szUrl = Encoding.UTF8.GetBytes(strTemp)
    '        struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
    '        Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
    '        struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
    '        strTemp = m_strInputXml

    '        If "" = strTemp Then
    '            struPTXML.pInBuffer = IntPtr.Zero
    '            struPTXML.dwInSize = 0
    '        Else
    '            m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
    '            struPTXML.pInBuffer = Marshal.AllocHGlobal(1024 * 10)
    '            Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
    '            struPTXML.dwInSize = CUInt(strTemp.Length)
    '        End If

    '        struPTXML.pOutBuffer = Marshal.AllocHGlobal(1024 * 10)

    '        For i As Integer = 0 To 1024 * 10 - 1
    '            Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
    '        Next

    '        struPTXML.dwOutSize = CUInt((1024 * 10))
    '        struPTXML.byRes = New Byte(31) {}

    '        Try
    '            ptrCfg1 = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
    '            Marshal.StructureToPtr(struPTXML, ptrCfg1, True)
    '        Catch __unusedArgumentException1__ As ArgumentException
    '            TraceService(__unusedArgumentException1__.Message.ToString, "Line 2203")
    '            'MessageBox.Show("非托管内存异常")
    '            Throw
    '        End Try

    '        If Not HCEHomeCMS.NET_ECMS_PutPTXMLConfig(m_lUserID, ptrCfg1) Then
    '            'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_PutPTXMLConfig")
    '            TraceService("SetUser " & HCEHomePublic.OPERATION_FAIL_T & " " & EmployeeNo, "Line 2654")
    '            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
    '            Marshal.FreeHGlobal(struPTXML.pInBuffer)
    '            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
    '            Marshal.FreeHGlobal(ptrCfg1)

    '            Try
    '                Dim s As String = "update DeviceCommands set Executed='1' where Dev_cmd_Id='" & Dev_cmd_Id & "'"
    '                If (con.State <> ConnectionState.Open) Then
    '                    con.Open()
    '                End If
    '                cmd = New SqlCommand(s, con)
    '                cmd.ExecuteNonQuery()
    '                If (con.State <> ConnectionState.Closed) Then
    '                    con.Close()
    '                End If
    '            Catch ex As Exception
    '            End Try
    '            Return
    '        End If

    '        'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_PutPTXMLConfig")
    '        Dim strOutXML As String = String.Empty
    '        Try
    '            strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
    '        Catch __unusedArgumentException1__ As ArgumentException
    '            TraceService(__unusedArgumentException1__.Message.ToString, "Line 2222")
    '            'MessageBox.Show("非托管内存异常")
    '            Throw
    '        End Try
    '        If strOutXML <> String.Empty Then
    '            Dim TempItem As ListViewItem = New ListViewItem()
    '            Dim rs As ResponseStatus = JsonConvert.DeserializeObject(Of ResponseStatus)(strOutXML)
    '            If 1 = rs.statusCode Then
    '                Dim s As String = "update DeviceCommands set Executed='1' where Dev_cmd_Id='" & Dev_cmd_Id & "'"
    '                If (con.State <> ConnectionState.Open) Then
    '                    con.Open()
    '                End If
    '                cmd = New SqlCommand(s, con)
    '                cmd.ExecuteNonQuery()
    '                If (con.State <> ConnectionState.Closed) Then
    '                    con.Close()
    '                End If
    '            Else
    '                'labelMsg.Text = "Set Employee Failed"
    '                TraceService("Set Employee Failed", "Line 2685")
    '            End If

    '        End If
    '        Marshal.FreeHGlobal(struPTXML.pRequestUrl)
    '        Marshal.FreeHGlobal(struPTXML.pInBuffer)
    '        Marshal.FreeHGlobal(struPTXML.pOutBuffer)
    '        Marshal.FreeHGlobal(ptrCfg1)

    '        If Dev_cmd_Id.Trim <> "" Then
    '            'SetUserFinger(EmployeeNo, m_lUserID, FromDeviceId)
    '            SetUserFace(EmployeeNo, m_lUserID, FromDeviceId)
    '        End If
    '        If cardNo <> "" Then
    '            SetUserCard(EmployeeNo, m_lUserID, FromDeviceId, cardNo)
    '        End If
    '        Return
    '    Catch ex As Exception
    '        TraceService("SetUserDataToDevice" & ex.Message, "Line 2921")
    '    End Try
    'End Sub
    Private Sub SetUserDataToDevice(EmployeeNo As String, m_lUserID As Integer, FromDeviceId As String, Dev_cmd_Id As String, TransferToDevice As String, DeviceType As String)
        Dim QueryResult As String = "Y"
        Try
            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            Dim ds As DataSet = New DataSet
            Dim adap As SqlDataAdapter
            Dim sSql As String = "Select * from Userdetail where UserID='" & EmployeeNo & "' and DeviceID='" & FromDeviceId & "'"
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
            If ds.Tables(0).Rows.Count = 0 Then
                QueryResult = "N"
                GoTo cmdupdate
                Return
            End If


            '           Try
            '               Dim s As String = "update CommandStatus set CommandContent='flagupduserinfo',ProcessStatus='Uploading User " & EmployeeNo & "', UpdatedOn=GETDATE() where DeviceID='" & TransferToDevice & "'
            'IF @@ROWCOUNT=0
            'insert into CommandStatus (DeviceID ,CommandContent,ProcessStatus,UpdatedOn) values ('" & TransferToDevice & "','CHECK','Uploading User " & EmployeeNo & "',GETDATE()) "
            '               If (con.State <> ConnectionState.Open) Then
            '                   con.Open()
            '               End If
            '               cmd = New SqlCommand(s, con)
            '               cmd.ExecuteNonQuery()
            '               If (con.State <> ConnectionState.Closed) Then
            '                   con.Close()
            '               End If
            '           Catch ex As Exception

            '           End Try


            Dim cardNo As String = ds.Tables(0).Rows(0)("Card").ToString.Trim
            Dim AccesstimeFrom As String
            Dim AccesstimeTo As String
            Try
                AccesstimeFrom = Convert.ToDateTime(ds.Tables(0).Rows(0)("AccesstimeFrom").ToString.Trim).ToString("yyyy-MM-ddTHH:mm:ss")
            Catch ex As Exception
                AccesstimeFrom = "2000-01-01T00:00:00"
            End Try
            Try
                AccesstimeTo = Convert.ToDateTime(ds.Tables(0).Rows(0)("AccesstimeTo").ToString.Trim).ToString("yyyy-MM-ddTHH:mm:ss")
            Catch ex As Exception
                AccesstimeTo = "2029-12-31T23:59:59"
            End Try
            strTemp = "/ISAPI/AccessControl/UserInfo/SetUp?format=json"
            m_strInputXml = "{""UserInfo"":{""employeeNo"":""" & EmployeeNo & """,""name"":""" & ds.Tables(0).Rows(0)("Name").ToString.Trim & """,""userType"":""normal"",""Valid"":{""enable"":true,""beginTime"":""" & AccesstimeFrom & """,""endTime"":""" & AccesstimeTo & """},""doorRight"": ""1"",""RightPlan"":[{""doorNo"":1,""planTemplateNo"":""1""}]}}"

            'm_strInputXml = "{""UserInfo"":[{""employeeNo"":""123"",""name"":""Name"",""userType"":""normal"",""Valid"":{""enable"":true,""beginTime"":""2020-08-01 00:00:00"",""endTime"":""2029-08-01 00:00:00""},""doorRight"": ""1"",""RightPlan"":[{""doorNo"":1,""planTemplateNo"":""1""}]},{""employeeNo"":""456"",""name"":""Name"",""userType"":""normal"",""Valid"":{""enable"":true,""beginTime"":""2020-08-01 00:00:00"",""endTime"":""2029-08-01 00:00:00""},""doorRight"": ""1"",""RightPlan"":[{""doorNo"":1,""planTemplateNo"":""1""}]}]}"
            Dim OutBufferLen As Integer = 5 * 1024
            ConfigMethodUserSet(strTemp, m_strInputXml, OutBufferLen)

            If Not HCEHomeCMS.NET_ECMS_PutPTXMLConfig(m_lUserID, ptrCfg) Then
                'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_PutPTXMLConfig")
                TraceService("SetUser " & HCEHomePublic.OPERATION_FAIL_T & " " & EmployeeNo, "Line 3328")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                QueryResult = "N"
                GoTo cmdupdate
                Return
            End If
            'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_PutPTXMLConfig")
            Dim strOutXML As String = String.Empty
            Try
                strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            Catch e1 As ArgumentException
                'MessageBox.Show("Memory Exception")
                TraceService("SetUser Memory Exception " & EmployeeNo, "Line 3342")
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                QueryResult = "N"
                GoTo cmdupdate
                Return
            End Try
            If strOutXML <> String.Empty Then
                Dim TempItem As New ListViewItem()
                Dim rs As ResponseStatus = JsonConvert.DeserializeObject(Of ResponseStatus)(strOutXML)
                If 1 = rs.statusCode Then
                    'labelMsg.Text = "Modify Employee Success"
                Else
                    QueryResult = "N"
                    TraceService("SetUser Failed " & EmployeeNo, "Line 3353")
                    'labelMsg.Text = "Set Employee Failed"
                End If
            End If
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)


cmdupdate:  Try
                'Dim s As String = "update DeviceCommands set Executed='1' where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                Dim s As String = "update DeviceCommands Set Executed='1', QueryResult='" & QueryResult & "',ExecutedOn=GETDATE() where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                If (con.State <> ConnectionState.Open) Then
                    con.Open()
                End If
                cmd = New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
                s = "update userdetail set UpdateFlag='O' where userid='" & EmployeeNo & "' and DeviceId='" & FromDeviceId & "' "
                cmd = New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
                If (con.State <> ConnectionState.Closed) Then
                    con.Close()
                End If
            Catch ex As Exception
            End Try

            ''normal
            'Dim ptrCfg1 As IntPtr = IntPtr.Zero
            'm_szUrl = Encoding.UTF8.GetBytes(strTemp)
            'struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
            'Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
            'struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
            'strTemp = m_strInputXml

            'If "" = strTemp Then
            '    struPTXML.pInBuffer = IntPtr.Zero
            '    struPTXML.dwInSize = 0
            'Else
            '    m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
            '    struPTXML.pInBuffer = Marshal.AllocHGlobal(1024 * 10)
            '    Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
            '    struPTXML.dwInSize = CUInt(strTemp.Length)
            'End If

            'struPTXML.pOutBuffer = Marshal.AllocHGlobal(1024 * 10)

            'For i As Integer = 0 To 1024 * 10 - 1
            '    Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
            'Next

            'struPTXML.dwOutSize = CUInt((1024 * 10))
            'struPTXML.byRes = New Byte(31) {}

            'Try
            '    ptrCfg1 = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
            '    Marshal.StructureToPtr(struPTXML, ptrCfg1, True)
            'Catch __unusedArgumentException1__ As ArgumentException
            '    TraceService(__unusedArgumentException1__.Message.ToString, "Line 2203")
            '    'MessageBox.Show("非托管内存异常")
            '    Throw
            'End Try

            'If Not HCEHomeCMS.NET_ECMS_PutPTXMLConfig(m_lUserID, ptrCfg1) Then
            '    'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_PutPTXMLConfig")
            '    TraceService("SetUser " & HCEHomePublic.OPERATION_FAIL_T & " " & EmployeeNo, "Line 2654")
            '    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            '    Marshal.FreeHGlobal(struPTXML.pInBuffer)
            '    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            '    Marshal.FreeHGlobal(ptrCfg1)

            '    Try
            '        Dim s As String = "update DeviceCommands set Executed='1' where Dev_cmd_Id='" & Dev_cmd_Id & "'"
            '        If (con.State <> ConnectionState.Open) Then
            '            con.Open()
            '        End If
            '        cmd = New SqlCommand(s, con)
            '        cmd.ExecuteNonQuery()
            '        If (con.State <> ConnectionState.Closed) Then
            '            con.Close()
            '        End If
            '    Catch ex As Exception
            '    End Try
            '    Return
            'End If

            ''g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_PutPTXMLConfig")
            'Dim strOutXML As String = String.Empty
            'Try
            '    strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            'Catch __unusedArgumentException1__ As ArgumentException
            '    TraceService(__unusedArgumentException1__.Message.ToString, "Line 2222")
            '    'MessageBox.Show("非托管内存异常")
            '    Throw
            'End Try
            'If strOutXML <> String.Empty Then
            '    Dim TempItem As ListViewItem = New ListViewItem()
            '    Dim rs As ResponseStatus = JsonConvert.DeserializeObject(Of ResponseStatus)(strOutXML)
            '    If 1 = rs.statusCode Then
            '        Dim s As String = "update DeviceCommands set Executed='1' where Dev_cmd_Id='" & Dev_cmd_Id & "'"
            '        If (con.State <> ConnectionState.Open) Then
            '            con.Open()
            '        End If
            '        cmd = New SqlCommand(s, con)
            '        cmd.ExecuteNonQuery()
            '        If (con.State <> ConnectionState.Closed) Then
            '            con.Close()
            '        End If
            '    Else
            '        'labelMsg.Text = "Set Employee Failed"
            '        TraceService("Set Employee Failed", "Line 2685")
            '    End If

            'End If
            'Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            'Marshal.FreeHGlobal(struPTXML.pInBuffer)
            'Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            'Marshal.FreeHGlobal(ptrCfg1)

            If Dev_cmd_Id.Trim <> "" Then
                '               Try
                '                   Dim s As String = "update CommandStatus set CommandContent='flagupduserinfo',ProcessStatus='Uploading FP/FC " & EmployeeNo & "', UpdatedOn=GETDATE() where DeviceID='" & TransferToDevice & "'
                'IF @@ROWCOUNT=0
                'insert into CommandStatus (DeviceID ,CommandContent,ProcessStatus,UpdatedOn) values ('" & TransferToDevice & "','CHECK','Uploading User " & EmployeeNo & "',GETDATE()) "
                '                   If (con.State <> ConnectionState.Open) Then
                '                       con.Open()
                '                   End If
                '                   cmd = New SqlCommand(s, con)
                '                   cmd.ExecuteNonQuery()
                '                   If (con.State <> ConnectionState.Closed) Then
                '                       con.Close()
                '                   End If
                '               Catch ex As Exception

                '               End Try

                If DeviceType = "Face" Then
                    SetUserFace(EmployeeNo, m_lUserID, FromDeviceId)
                ElseIf DeviceType = "Finger" Then
                    SetUserFinger(EmployeeNo, m_lUserID, FromDeviceId)
                ElseIf DeviceType = "Both" Then
                    SetUserFace(EmployeeNo, m_lUserID, FromDeviceId)
                    SetUserFinger(EmployeeNo, m_lUserID, FromDeviceId)
                End If
            End If
            If cardNo <> "" Then
                '               Try
                '                   Dim s As String = "update CommandStatus set CommandContent='flagupduserinfo',ProcessStatus='Uploading Card for " & EmployeeNo & "', UpdatedOn=GETDATE() where DeviceID='" & TransferToDevice & "'
                'IF @@ROWCOUNT=0
                'insert into CommandStatus (DeviceID ,CommandContent,ProcessStatus,UpdatedOn) values ('" & TransferToDevice & "','CHECK','Uploading User " & EmployeeNo & "',GETDATE()) "
                '                   If (con.State <> ConnectionState.Open) Then
                '                       con.Open()
                '                   End If
                '                   cmd = New SqlCommand(s, con)
                '                   cmd.ExecuteNonQuery()
                '                   If (con.State <> ConnectionState.Closed) Then
                '                       con.Close()
                '                   End If
                '               Catch ex As Exception

                '               End Try
                SetUserCard(EmployeeNo, m_lUserID, FromDeviceId, cardNo)
            End If

            'Try
            '    Dim s As String = "Delete from CommandStatus where DeviceID='" & TransferToDevice & "'"
            '    If (con.State <> ConnectionState.Open) Then
            '        con.Open()
            '    End If
            '    cmd = New SqlCommand(s, con)
            '    cmd.ExecuteNonQuery()
            '    If (con.State <> ConnectionState.Closed) Then
            '        con.Close()
            '    End If
            'Catch ex As Exception

            'End Try


            Return
        Catch ex As Exception
            TraceService("SetUserDataToDevice" & ex.Message, "Line 2921")
        End Try
    End Sub
    Public Sub ConfigMethodUser(ByVal strTemp As String, ByVal m_strInputXml As String, ByVal OutBufferLen As Integer)
        Dim m_szInputBuffer(strTemp.Length - 1) As Byte
        m_szUrl = Encoding.Default.GetBytes(strTemp)
        struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
        Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
        struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
        strTemp = m_strInputXml
        If "" = strTemp Then
            struPTXML.pInBuffer = IntPtr.Zero
            struPTXML.dwInSize = 0
        Else
            m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
            struPTXML.pInBuffer = Marshal.AllocHGlobal(strTemp.Length)
            Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
            struPTXML.dwInSize = CUInt(m_szInputBuffer.Length)
        End If
        struPTXML.pOutBuffer = Marshal.AllocHGlobal(OutBufferLen)
        For i As Integer = 0 To OutBufferLen - 1
            Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
        Next i
        struPTXML.dwOutSize = CUInt(OutBufferLen)
        struPTXML.byRes = New Byte(31) {}
        Try
            ptrCfg = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
            Marshal.StructureToPtr(struPTXML, ptrCfg, True)
        Catch e1 As ArgumentException
            'MessageBox.Show("Memory Exception")
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)
            Return
        End Try
    End Sub
    Private Sub SetUserFinger(empNo As String, m_lUserID As Integer, FromDeviceId As String)
        Try
            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            Dim ds As DataSet = New DataSet
            Dim adap As SqlDataAdapter
            Dim sSql As String = "Select * from UserFinger where UserID='" & empNo & "' and DeviceID='" & FromDeviceId & "'"
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
            If ds.Tables(0).Rows.Count = 0 Then
                Return
            End If

            strTemp = "/ISAPI/AccessControl/UserInfo/Search?format=json"
            m_strInputXml = "{""UserInfoSearchCond"":{""searchID"":""1"",""searchResultPosition"":0,""maxResults"":30,""EmployeeNoList"":[{""employeeNo"":""" & empNo & """}]}}"
            ConfigMethod(strTemp, m_strInputXml)

            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                TraceService("Error: " & Marshal.GetLastWin32Error(), "Line 2281")
                'MessageBox.Show("Error: " & Marshal.GetLastWin32Error())
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                Return
            End If

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                strTemp = "/ISAPI/AccessControl/FingerPrintDownload?format=json"
                Dim FingerData As String = ds.Tables(0).Rows(i)("FingerTemplate").ToString.Trim 'String.Empty            
                Dim FingerID As String = ds.Tables(0).Rows(i)("FID").ToString.Trim
                m_strInputXml = "{""FingerPrintCfg"":{""employeeNo"":""" & empNo & """,""enableCardReader"":[1],""fingerPrintID"":" & Integer.Parse(FingerID) & ",""fingerType"":""normalFP"",""fingerData"":""" & FingerData & """}}"
                ConfigMethod(strTemp, m_strInputXml)
                If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                    Marshal.FreeHGlobal(struPTXML.pInBuffer)
                    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                    Marshal.FreeHGlobal(ptrCfg)
                    Continue For ' Return
                End If
                Dim strOutXML As String = String.Empty
                strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
                If strOutXML <> String.Empty Then
                    Dim rs As ResponseStatus = JsonConvert.DeserializeObject(Of ResponseStatus)(strOutXML)

                    If rs.statusCode = 1 Then
                        'MessageBox.Show("FingerPrint SetUP Success")
                    Else
                        TraceService("FingerPrint SetUp Error: " & rs.statusString & " " & empNo, "Line 2315")
                        'MessageBox.Show("FingerPrint SetUp Error: " & rs.statusString)
                    End If
                End If
            Next
            Try
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
            Catch ex As Exception
            End Try
            Return
        Catch ex As Exception
            TraceService("SetUserFinger" & ex.Message, "Line 2991")
        End Try
    End Sub
    Private Sub SetUserFace(empNo As String, m_lUserID As Integer, FromDeviceId As String)
        Try
            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            Dim ds As DataSet = New DataSet
            Dim adap As SqlDataAdapter
            Dim sSql As String = "Select * from Userface where UserID='" & empNo & "' and DeviceID='" & FromDeviceId & "'"
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
            If ds.Tables(0).Rows.Count = 0 Then
                Return
            End If

#Region "upload face to deviceserver"
            Dim bArr As Byte() = ds.Tables(0).Rows(0)("FaceByte")
            'Dim UserImage As Image = byteArrayToImage(bArr)
            'Dim FilePath As String = System.Environment.CurrentDirectory & "\t\" & FromDeviceId & "_" & empNo & ".jpg"
            Dim FilePath As String = System.Environment.CurrentDirectory & "\t\" & empNo & ".jpg"
            ''UserImage.Save(FilePath)
            ''File.WriteAllBytes(FilePath, bArr)

            Using mStream As New MemoryStream(bArr)
                Image.FromStream(mStream).Save(FilePath)
            End Using
            'TraceServiceText(FilePath)
            'Thread.Sleep(50) '100           
            Dim struClientParam As New HCEHomeSS.NET_EHOME_SS_CLIENT_PARAM()
            struClientParam.Init()
            struClientParam.enumType = HCEHomeSS.NET_EHOME_SS_CLIENT_TYPE.NET_EHOME_SS_CLIENT_TYPE_TOMCAT
            Dim m_csListenIP As String = m_csLocalIP
            m_csListenIP.CopyTo(0, struClientParam.struAddress.szIP, 0, m_csListenIP.Length)
            struClientParam.struAddress.wPort = PicServerPort '10003 'CShort(m_nPort) '8898 '
            struClientParam.byHttps = 0
            Dim lPssClientHandle As Integer = -1
            Dim szUrl(HCEHomePublic.MAX_URL_LEN_SS - 1) As Char
            Dim szPicUrl(HCEHomePublic.MAX_URL_LEN_SS - 1) As Char
            struClientParam.enumType = HCEHomeSS.NET_EHOME_SS_CLIENT_TYPE.NET_EHOME_SS_CLIENT_TYPE_KMS
            lPssClientHandle = HCEHomeSS.NET_ESS_CreateClient(struClientParam)
            If lPssClientHandle < 0 Then
                TraceService("SetFace " & HCEHomePublic.OPERATION_FAIL_T & " NET_ESS_CreateClient " & empNo, "Line 2361")
                'DeviceTree.g_formList.AddLog(0, HCEHomePublic.OPERATION_FAIL_T, 0, "NET_ESS_CreateClient")
                Return
            End If
            Dim strAK As String = "" ' textBoxAK.Text
            Dim strSK As String = "" 'textBoxSK.Text
            Dim SS_CLIENT_KMS_USER_NAME As String = "KMS-Username"
            Dim SS_CLIENT_KMS_PASSWIRD As String = "KMS-Password"
            Dim SS_CLIENT_FILE_PATH_PARAM_NAME As String = "File-Path"

            HCEHomeSS.NET_ESS_ClientSetTimeout(lPssClientHandle, 60 * 1000, 60 * 1000)
            HCEHomeSS.NET_ESS_ClientSetParam(lPssClientHandle, SS_CLIENT_FILE_PATH_PARAM_NAME, FilePath)

            'KMS
            HCEHomeSS.NET_ESS_ClientSetParam(lPssClientHandle, SS_CLIENT_KMS_USER_NAME, strAK)
            HCEHomeSS.NET_ESS_ClientSetParam(lPssClientHandle, SS_CLIENT_KMS_PASSWIRD, strSK)

            Dim ptrszUrl As IntPtr = Marshal.AllocHGlobal(HCEHomePublic.MAX_URL_LEN_SS)
            Dim Flag As Boolean = HCEHomeSS.NET_ESS_ClientDoUpload(lPssClientHandle, ptrszUrl, HCEHomePublic.MAX_URL_LEN_SS - 1)
            If Not Flag Then
                'MessageBox.Show("Upload Error: " & HCEHomeCMS.NET_ECMS_GetLastError())
                TraceService("SetFace " & HCEHomeCMS.NET_ECMS_GetLastError() & " " & empNo, "Line 2639")
                HCEHomeSS.NET_ESS_DestroyClient(lPssClientHandle)
                Return
            End If
            Dim strszUrl As String = Marshal.PtrToStringAnsi(ptrszUrl)


            Marshal.FreeHGlobal(ptrszUrl)
            'textBoxURLresult.Text = strszUrl ' res;
            HCEHomeSS.NET_ESS_DestroyClient(lPssClientHandle)
            Try
                My.Computer.FileSystem.DeleteFile(FilePath)
            Catch ex As Exception

            End Try
#End Region
#Region "Assign to employee"
            'Dim strTemp As String = "/ISAPI/AccessControl/UserInfo/Search?format=json"
            'Dim m_strInputXml As String = "{""UserInfoSearchCond"":{""searchID"":""1"",""searchResultPosition"":0,""maxResults"":30,""EmployeeNoList"":[{""employeeNo"":""" & empNo & """}]}}"
            'ConfigMethod(strTemp, m_strInputXml)

            'If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
            '    'MessageBox.Show("Error: " & Marshal.GetLastWin32Error())
            '    TraceService("Set Face Error: " & Marshal.GetLastWin32Error() & " " & empNo, "Line 2653")
            '    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            '    Marshal.FreeHGlobal(struPTXML.pInBuffer)
            '    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            '    Marshal.FreeHGlobal(ptrCfg)
            '    Return
            'End If
            'Dim strOutXML As String = ""
            'strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            'If strOutXML <> String.Empty Then
            '    Dim us As UserInfoSearchRoot = JsonConvert.DeserializeObject(Of UserInfoSearchRoot)(strOutXML)
            '    If 0 = us.UserInfoSearch.totalMatches Then
            '        'MessageBox.Show("Please Create Employee First")
            '        TraceService("Please Create Employee First " & empNo, "Line 2666")
            '    End If
            'End If
            'Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            'Marshal.FreeHGlobal(struPTXML.pInBuffer)
            'Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            'Marshal.FreeHGlobal(ptrCfg)

            'strTemp = "/ISAPI/Intelligent/FDLib/FDSearch?format=json"
            'm_strInputXml = "{""searchResultPosition"":0,""maxResults"":30,""faceLibType"":""blackFD"",""FDID"":""1"",""FPID"":""" & empNo & """}"
            'ConfigMethod(strTemp, m_strInputXml)

            'If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
            '    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            '    Marshal.FreeHGlobal(struPTXML.pInBuffer)
            '    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            '    Marshal.FreeHGlobal(ptrCfg)
            '    Return
            'End If
            'strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            'If strOutXML <> String.Empty Then
            '    Dim rt As Root = JsonConvert.DeserializeObject(Of Root)(strOutXML)
            '    If rt.statusCode = 1 Then
            '        If rt.totalMatches <> 0 Then
            '            'MessageBox.Show("This Employee has Record Face Info")
            '            TraceService("This Employee has Record Face Info " & empNo, "Line 2691")
            '            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            '            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            '            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            '            Marshal.FreeHGlobal(ptrCfg)
            '            Return
            '        End If
            '    End If
            '    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            '    Marshal.FreeHGlobal(struPTXML.pInBuffer)
            '    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            '    Marshal.FreeHGlobal(ptrCfg)
            'End If
            'strTemp = "/ISAPI/Intelligent/FDLib/FaceDataRecord?format=json"
            'Dim FPID As String = empNo
            'Dim bornTime As String = "2004-05-03"
            'Dim FaceUrl As String = strszUrl 'FaceText.Text
            'm_strInputXml = "{""faceURL"":""" & FaceUrl & """,""faceLibType"":""blackFD"",""FDID"":""1"",""name"":"""",""FPID"":""" & FPID & """,""bornTime"":""" & bornTime & """}"
            'ConfigMethod(strTemp, m_strInputXml)
            'If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
            '    'MessageBox.Show("Set Face Error: " & HCEHomeCMS.NET_ECMS_GetLastError())
            '    TraceService("Set Face Error: " & HCEHomeCMS.NET_ECMS_GetLastError() & " " & empNo, "Line 2712")
            '    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            '    Marshal.FreeHGlobal(struPTXML.pInBuffer)
            '    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            '    Marshal.FreeHGlobal(ptrCfg)
            '    Return
            'End If
            'strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            'If strOutXML <> String.Empty Then
            '    Dim rs As ResponseStatus = JsonConvert.DeserializeObject(Of ResponseStatus)(strOutXML)
            '    If rs.statusCode = "1" Then
            '        'MessageBox.Show("Face SetUp Success")
            '    Else
            '        'MessageBox.Show("Face SetUp Error: " & rs.statusString)
            '        TraceService("Set Face Error: " & rs.statusString & " " & empNo, "Line 2726")
            '    End If
            'End If
            'Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            'Marshal.FreeHGlobal(struPTXML.pInBuffer)
            'Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            'Marshal.FreeHGlobal(ptrCfg)
#End Region
#Region "Assign to emp New"
            ''tmp comment 
            'Dim strTemp As String = "/ISAPI/AccessControl/UserInfo/Search?format=json"
            'Dim m_strInputXml As String = "{""UserInfoSearchCond"":{""searchID"":""1"",""searchResultPosition"":0,""maxResults"":30,""EmployeeNoList"":[{""employeeNo"":""" & empNo & """}]}}"
            'ptrCfg = IntPtr.Zero
            'Dim OutBufferLen As Integer = 2 * 1024
            'ConfigMethodCapture(ptrCfg, strTemp, m_strInputXml, OutBufferLen)
            'If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
            '    'MessageBox.Show("Error: " & Marshal.GetLastWin32Error())
            '    TraceService("Setface: " & Marshal.GetLastWin32Error() & " " & empNo, "Line 2653")
            '    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            '    Marshal.FreeHGlobal(struPTXML.pInBuffer)
            '    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            '    Marshal.FreeHGlobal(ptrCfg)
            '    Return
            'End If
            Dim strOutXML As String = ""
            'strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            'If strOutXML <> String.Empty Then
            '    Dim us As UserInfoSearchRoot = JsonConvert.DeserializeObject(Of UserInfoSearchRoot)(strOutXML)
            '    If 0 = us.UserInfoSearch.totalMatches Then
            '        'MessageBox.Show("Please Create Employee First")
            '        TraceService("SetFace Emp not created " & empNo, "Line 2666")
            '    End If
            'End If
            'Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            'Marshal.FreeHGlobal(struPTXML.pInBuffer)
            'Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            'Marshal.FreeHGlobal(ptrCfg)

            'strTemp = "/ISAPI/Intelligent/FDLib/FDSearch?format=json"
            'm_strInputXml = "{""searchResultPosition"":0,""maxResults"":30,""faceLibType"":""blackFD"",""FDID"":""1"",""FPID"":""" & empNo & """}"
            'OutBufferLen = 5 * 1024
            'ConfigMethodCapture(ptrCfg, strTemp, m_strInputXml, OutBufferLen)

            'If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
            '    TraceService("SetFace Error " & empNo, "Line 2691")
            '    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            '    Marshal.FreeHGlobal(struPTXML.pInBuffer)
            '    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            '    Marshal.FreeHGlobal(ptrCfg)
            '    Return
            'End If
            'strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            'If strOutXML <> String.Empty Then
            '    Dim rt As Root = JsonConvert.DeserializeObject(Of Root)(strOutXML)
            '    If rt.statusCode = 1 Then
            '        If rt.totalMatches <> 0 Then
            '            'MessageBox.Show("This Employee has Record Face Info")
            '            TraceService("This Employee has Record Face Info " & empNo, "Line 2691")
            '            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            '            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            '            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            '            Marshal.FreeHGlobal(ptrCfg)
            '            Return
            '        End If
            '    End If
            '    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            '    Marshal.FreeHGlobal(struPTXML.pInBuffer)
            '    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            '    Marshal.FreeHGlobal(ptrCfg)
            'End If
            ''end tmp comment 




            Dim OutBufferLen As Integer = 5 * 1024  'tmp
            strTemp = "/ISAPI/Intelligent/FDLib/FaceDataRecord?format=json"
            Dim FPID As String = empNo
            Dim bornTime As String = "2004-05-03"
            Dim FaceUrl As String = strszUrl 'FaceText.Text
            m_strInputXml = "{""faceURL"":""" & FaceUrl & """,""faceLibType"":""blackFD"",""FDID"":""1"",""name"":"""",""FPID"":""" & FPID & """,""bornTime"":""" & bornTime & """}"
            ConfigMethodCapture(ptrCfg, strTemp, m_strInputXml, OutBufferLen)
            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                'MessageBox.Show("Set Face Error: " & HCEHomeCMS.NET_ECMS_GetLastError())
                TraceService("Set Face Error: " & HCEHomeCMS.NET_ECMS_GetLastError() & " " & empNo, "Line 2712")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                Return
            End If
            strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(Math.Truncate(struPTXML.dwOutSize)))
            If strOutXML <> String.Empty Then
                Dim rs As ResponseStatus = JsonConvert.DeserializeObject(Of ResponseStatus)(strOutXML)
                If rs.statusCode = 1 Then
                    'MessageBox.Show("Face SetUp Success")
                Else
                    'MessageBox.Show("Face SetUp Error: " & rs.statusString)
                    TraceService("Set Face Error: " & rs.statusString & " " & empNo, "Line 2726")
                    Try
                        sSql = "insert into FailedEntries (UserId, DeviceID, Content, UpdatedOn) values ('" & empNo & "','" & FromDeviceId & "','Setface',getdate())"
                        If con.State <> ConnectionState.Open Then
                            con.Open()
                        End If
                        Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    Catch ex As Exception
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                        TraceService("Set Face FailedEntries: " & ex.Message & " " & empNo, "Line 3793")
                    End Try


                End If
            End If
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)
#End Region
            Return
        Catch ex As Exception
            TraceService("SetUserFace" & ex.Message, "Line 3252")
        End Try
    End Sub
    Public Function byteArrayToImage(ByVal byteArrayIn As Byte()) As Image
        Using mStream As New MemoryStream(byteArrayIn)
            Return Image.FromStream(mStream)
        End Using
    End Function
    Private Sub SetUserCard(empNo As String, m_lUserID As Integer, FromDeviceId As String, cardNo As String)
        Try
            Dim strTemp As String = String.Empty
            Dim m_strInputXml As String = String.Empty
            strTemp = "/ISAPI/AccessControl/CardInfo/SetUp?format=json"
            m_strInputXml = "{""CardInfo"":{""employeeNo"":""" & empNo & """,""cardNo"":""" & cardNo & """,""cardType"":""normalCard""}}"
            m_szUrl = Encoding.UTF8.GetBytes(strTemp)
            struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
            Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
            struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
            strTemp = m_strInputXml

            If "" = strTemp Then
                struPTXML.pInBuffer = IntPtr.Zero
                struPTXML.dwInSize = 0
            Else
                m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
                struPTXML.pInBuffer = Marshal.AllocHGlobal(1024 * 10)
                Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
                struPTXML.dwInSize = CUInt(strTemp.Length)
            End If

            struPTXML.pOutBuffer = Marshal.AllocHGlobal(1024 * 10)

            For i As Integer = 0 To 1024 * 10 - 1
                Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
            Next

            struPTXML.dwOutSize = CUInt((1024 * 10))
            struPTXML.byRes = New Byte(31) {}
            Dim ptrCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
            Marshal.StructureToPtr(struPTXML, ptrCfg, False)

            If Not HCEHomeCMS.NET_ECMS_PutPTXMLConfig(m_lUserID, ptrCfg) Then
                'MessageBox.Show("Error: " & HCEHomeCMS.NET_ECMS_GetLastError())
                EHomeForm.TraceService("SetUserCard " & Marshal.GetLastWin32Error() & "Emp No:" & empNo, "Line 2826")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                Return
            End If

            Dim strOutXML As String = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))

            If strOutXML <> String.Empty Then
                Dim rs As ResponseStatus = JsonConvert.DeserializeObject(Of ResponseStatus)(strOutXML)
                If 1 = rs.statusCode Then
                    'MessageBox.Show("Set Card Succ!")
                Else
                    'MessageBox.Show("Please Check Employee is Existed or Not!")
                End If
            End If
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)
            Return
        Catch ex As Exception
            TraceService("SetUserCard" & ex.Message, "Line 3319")
        End Try
    End Sub
    Private Sub SetDeviceConfig(m_lUserID As Integer, SerialNumber As String, Dev_cmd_Id As String, DeviceCommand As String)

        Dim QueryResult As String = "Y"
        Try
            If DeviceCommand = "flagsetdatetime" Then
                m_strInputXml = "<?xml version='1.0' encoding='UTF-8'?>" & vbCrLf &
                                    "<PPVSPMessage>" & vbCrLf &
                                    "<Version>4.0</Version>" & vbCrLf &
                                    "<Sequence>1</Sequence>" & vbCrLf &
                                    "<CommandType>REQUEST</CommandType>" & vbCrLf &
                                    "<Method>CONTROL</Method>" & vbCrLf &
                                    "<Command>ADJUSTTIME</Command>" & vbCrLf &
                                    "<Params>" & vbCrLf &
                                    "<TimeZone>0</TimeZone>" & vbCrLf &
                                    "<Time>" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "</Time>" & vbCrLf &
                                    "</Params>" & vbCrLf &
                                    "</PPVSPMessage>"
            ElseIf DeviceCommand = "REBOOT" Then
                m_strInputXml = "<?xml version='1.0' encoding='UTF-8'?>" & vbCrLf &
                                "<PPVSPMessage>" & vbCrLf &
                                    "<Version>4.0</Version>" & vbCrLf &
                                    "<Sequence>1</Sequence>" & vbCrLf &
                                    "<CommandType>REQUEST</CommandType>" & vbCrLf &
                                    "<Method>CONTROL</Method>" & vbCrLf &
                                    "<Command>REBOOT</Command>" & vbCrLf &
                                "</PPVSPMessage>"
            End If

            Dim struXMLCfg As New HCEHomeCMS.NET_EHOME_XML_REMOTE_CTRL_PARAM()
            Dim strTemp As String = m_strInputXml
            Dim utf8 As New UTF8Encoding()
            Dim encodedBytes() As Byte = utf8.GetBytes(strTemp)
            Dim decodedString As String = utf8.GetString(encodedBytes)
            m_szInputBuffer = Encoding.UTF8.GetBytes(decodedString)
            struXMLCfg.lpInbuffer = Marshal.AllocHGlobal(1500)
            For i As Integer = 0 To 1499
                Marshal.WriteByte(struXMLCfg.lpInbuffer, i, 0)
            Next i
            Marshal.Copy(m_szInputBuffer, 0, struXMLCfg.lpInbuffer, m_szInputBuffer.Length)
            struXMLCfg.dwInBufferSize = 1500


            struXMLCfg.lpOutBuffer = Marshal.AllocHGlobal(1024 * 100)
            For i As Integer = 0 To (1024 * 100) - 1
                Marshal.WriteByte(struXMLCfg.lpOutBuffer, i, 0)
            Next i
            struXMLCfg.dwOutBufferSize = 102400

            struXMLCfg.lpStatusBuffer = Marshal.AllocHGlobal(1024 * 100)
            For i As Integer = 0 To (1024 * 100) - 1
                Marshal.WriteByte(struXMLCfg.lpStatusBuffer, i, 0)
            Next i
            struXMLCfg.dwStatusBufferSize = CUInt(1024 * 100)


            struXMLCfg.dwSize = CUInt(Marshal.SizeOf(struXMLCfg))
            struXMLCfg.dwRecvTimeOut = 30 * 1000


            Dim dwSize As Integer = 0
            Dim ptrCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struXMLCfg))
            Marshal.StructureToPtr(struXMLCfg, ptrCfg, False)
            dwSize = Marshal.SizeOf(GetType(HCEHomeCMS.NET_EHOME_XML_REMOTE_CTRL_PARAM))

            If Not HCEHomeCMS.NET_ECMS_XMLRemoteControl(m_lUserID, ptrCfg, dwSize) Then
                QueryResult = "N"
                'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_XMLRemoteControl")
            Else
                'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_XMLRemoteControl")
                strTemp = ""
                strTemp = Marshal.PtrToStringAnsi(struXMLCfg.lpStatusBuffer, CInt(struXMLCfg.dwStatusBufferSize))
                'richTextBoxOutput.Text = strTemp
                Marshal.FreeHGlobal(struXMLCfg.lpInbuffer)
                Marshal.FreeHGlobal(struXMLCfg.lpStatusBuffer)
                Marshal.FreeHGlobal(ptrCfg)
            End If
        Catch ex As Exception
            QueryResult = "N"
            GoTo cmd
        End Try
cmd:    Try
            If Dev_cmd_Id <> "" Then
                Dim s As String = "update DeviceCommands Set Executed='1', QueryResult='" & QueryResult & "',ExecutedOn=GETDATE() where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
                If (con.State <> ConnectionState.Open) Then
                    con.Open()
                End If
                cmd = New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
                If (con.State <> ConnectionState.Closed) Then
                    con.Close()
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SetReboot(m_lUserID As Integer, SerialNumber As String, Dev_cmd_Id As String, DeviceCommand As String)

        Dim QueryResult As String = "Y"
        Try
            Dim strTemp As String = "/ISAPI/System/reboot"
            m_strInputXml = ""
            m_szUrl = Encoding.UTF8.GetBytes(strTemp)
            struPTXML.pRequestUrl = Marshal.AllocHGlobal(1024)
            Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
            struPTXML.dwRequestUrlLen = CType(m_szUrl.Length, UInteger)
            strTemp = m_strInputXml
            If ("" = strTemp) Then
                struPTXML.pInBuffer = IntPtr.Zero
                struPTXML.dwInSize = 0
            Else
                m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
                struPTXML.pInBuffer = Marshal.AllocHGlobal((1024 * 10))
                Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
                struPTXML.dwInSize = CType(strTemp.Length, UInteger)
            End If

            struPTXML.pOutBuffer = Marshal.AllocHGlobal((1024 * 10))
            Dim i As Integer = 0
            Do While (i < (1024 * 10))
                Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
                i = (i + 1)
            Loop

            struPTXML.dwOutSize = CType((1024 * 10), UInteger)
            struPTXML.byRes = New Byte((32) - 1) {}
            Dim ptrCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
            Marshal.StructureToPtr(struPTXML, ptrCfg, False)

            If Not HCEHomeCMS.NET_ECMS_PutPTXMLConfig(m_lUserID, ptrCfg) Then
                'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_PutPTXMLConfig")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                QueryResult = "N"
                GoTo cmd
            End If

            'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_XXXPTXMLConfig")
            strTemp = ""
            strTemp = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CType(struPTXML.dwOutSize, Integer))
            'rtbOutputXml.Text = strTemp

            If Not strTemp.Contains("<statusString>OK</statusString>") Then
                QueryResult = "N"
            End If

            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)

        Catch ex As Exception
            QueryResult = "N"
            GoTo cmd
        End Try
cmd:    Try
            If Dev_cmd_Id <> "" Then
                Dim s As String = "update DeviceCommands Set Executed='1', QueryResult='" & QueryResult & "',ExecutedOn=GETDATE() where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
                If (con.State <> ConnectionState.Open) Then
                    con.Open()
                End If
                cmd = New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
                If (con.State <> ConnectionState.Closed) Then
                    con.Close()
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region
    Private Sub DeleteUserFromDevice(EmployeeNo As String, m_lUserID As Integer, DeviceID As String, Dev_cmd_Id As String)
        Dim QueryResult As String = "Y"
        Try
            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            strTemp = "/ISAPI/AccessControl/UserInfo/Delete?format=json"
            m_strInputXml = "{""UserInfoDelCond"":{""EmployeeNoList"":[{""employeeNo"":""" & EmployeeNo & """}]}}"
            Dim ptrCfg1 As IntPtr = IntPtr.Zero
            m_szUrl = Encoding.UTF8.GetBytes(strTemp)
            struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
            Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
            struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)

            If m_strInputXml = String.Empty Then
                struPTXML.pInBuffer = IntPtr.Zero
                struPTXML.dwInSize = 0
            Else
                m_szInputBuffer = Encoding.UTF8.GetBytes(m_strInputXml)
                struPTXML.pInBuffer = Marshal.AllocHGlobal(1024 * 10)
                Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
                struPTXML.dwInSize = CUInt(m_strInputXml.Length)
            End If

            struPTXML.pOutBuffer = Marshal.AllocHGlobal(1024 * 10)

            For i As Integer = 0 To 1024 * 10 - 1
                Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
            Next

            struPTXML.dwOutSize = CUInt((1024 * 10))
            struPTXML.byRes = New Byte(31) {}
            Try
                ptrCfg1 = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
                Marshal.StructureToPtr(struPTXML, ptrCfg1, True)
            Catch __unusedArgumentNullException1__ As ArgumentNullException
                'MessageBox.Show("非托管内存异常")
                EHomeForm.TraceService(__unusedArgumentNullException1__.Message, "Line 2884")
                GoTo cmd
                'Throw
            End Try
            Try
                If Not HCEHomeCMS.NET_ECMS_PutPTXMLConfig(m_lUserID, ptrCfg1) Then
                    'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_PutPTXMLConfig")
                    Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                    Marshal.FreeHGlobal(struPTXML.pInBuffer)
                    Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                    Marshal.FreeHGlobal(ptrCfg1)
                    GoTo cmd ' Return
                End If

            Catch __unusedArgumentNullException1__ As ArgumentNullException
                'MessageBox.Show("调用Put方法出现空指针异常！")
                EHomeForm.TraceService(__unusedArgumentNullException1__.Message, "Line 2899")
                GoTo cmd '  Return
            Catch __unusedArgumentOutOfRangeException2__ As ArgumentOutOfRangeException
                'MessageBox.Show("调用Put方法出现栈溢出的异常")
                EHomeForm.TraceService(__unusedArgumentOutOfRangeException2__.Message, "Line 2903")
                GoTo cmd ' Return
            Catch __unusedException3__ As Exception
                GoTo cmd ' Return
            End Try

            'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_PutPTXMLConfig")
            Dim strOutXML As String = String.Empty

            Try
                strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            Catch __unusedArgumentException1__ As ArgumentException
                'MessageBox.Show("非托管内存异常")
                EHomeForm.TraceService(__unusedArgumentException1__.Message, "Line 2916")
                GoTo cmd
                'Throw
            End Try

            If strOutXML <> String.Empty Then
                Dim rs As ResponseStatus = JsonConvert.DeserializeObject(Of ResponseStatus)(strOutXML)
                If 1 = rs.statusCode Then
cmd:                If Dev_cmd_Id <> "" Then
                        'Dim s As String = "update DeviceCommands set Executed='1' where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                        Try
                            Dim s As String = "update DeviceCommands Set Executed='1', QueryResult='" & QueryResult & "',ExecutedOn=GETDATE() where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                            If (con.State <> ConnectionState.Open) Then
                                con.Open()
                            End If
                            cmd = New SqlCommand(s, con)
                            cmd.ExecuteNonQuery()
                            If (con.State <> ConnectionState.Closed) Then
                                con.Close()
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                    'Dim TempItem As ListViewItem = New ListViewItem()
                    'Dim EmpNo As String = textBoxEmployeeNo.Text
                    'TempItem.SubItems.Add(textBoxEmployeeNo.Text)
                    'TempItem.SubItems.Add(textBoxName.Text)
                    'TempItem.SubItems.Add("normal")
                    'TempItem.SubItems.Add(textBoxRightPlan.Text)
                    'Dim Items As ListViewItem = New ListViewItem()
                    'Dim Count As Integer = listViewInfo.Items.Count

                    'If Count = 1 Then
                    '    listViewInfo.Items.RemoveAt(0)
                    'Else

                    '    For i As Integer = 0 To Count - 1

                    '        If listViewInfo.Items(i).SubItems(1).Text = EmpNo Then
                    '            listViewInfo.Items.RemoveAt(i)
                    '            Exit For
                    '        End If
                    '    Next
                    'End If
                Else
                    'labelMsg.Text = "Delete Employee Falied: " & rs.errorMsg
                End If
            End If
            Try
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg1)
            Catch ex As Exception

            End Try

            Return
        Catch ex As Exception
            TraceService("DeleteUserFromDevice" & ex.Message, "Line 3440")
        End Try
    End Sub
    Private Sub ClearDeviceData(m_lUserID As Integer, DeviceID As String, Dev_cmd_Id As String)
        Dim QueryResult As String = "Y"
        Try
            Dim strTemp As String
            Dim m_strInputXml As String
            strTemp = "/ISAPI/AccessControl/UserInfo/Search?format=json"
            m_strInputXml = "{""UserInfoSearchCond"":{""searchID"":""1"",""searchResultPosition"":0,""maxResults"":30}}"
            Dim ptrCfg1 As IntPtr = IntPtr.Zero
            m_szUrl = Encoding.UTF8.GetBytes(strTemp)
            struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
            Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
            struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
            strTemp = m_strInputXml
            If "" = strTemp Then
                struPTXML.pInBuffer = IntPtr.Zero
                struPTXML.dwInSize = 0
            Else
                m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
                struPTXML.pInBuffer = Marshal.AllocHGlobal(1024 * 10)
                Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
                struPTXML.dwInSize = CUInt(strTemp.Length)
            End If
            struPTXML.pOutBuffer = Marshal.AllocHGlobal(1024 * 10)
            For i As Integer = 0 To 1024 * 10 - 1
                Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
            Next
            struPTXML.dwOutSize = CUInt((1024 * 10))
            struPTXML.byRes = New Byte(31) {}
            Try
                ptrCfg1 = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
                Marshal.StructureToPtr(struPTXML, ptrCfg1, True)
            Catch __unusedArgumentException1__ As ArgumentException
                'MessageBox.Show("非托管内存异常")
                EHomeForm.TraceService(__unusedArgumentException1__.Message, "Line 2996")
                Throw
            End Try
            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg1) Then
                'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_PostPTXMLConfig")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg1)
                Return
            End If
            Dim strOutXML As String = ""
            Try
                strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            Catch __unusedArgumentException1__ As ArgumentException
                'MessageBox.Show("非托管内存异常")
                Throw
            End Try
            Dim us As UserInfoSearchRoot
            'Dim DeviceId As String
            Dim ds As DataSet = New DataSet
            'listViewInfo.GridLines = True
            If strOutXML <> String.Empty Then
                us = JsonConvert.DeserializeObject(Of UserInfoSearchRoot)(strOutXML)
                If 0 = us.UserInfoSearch.totalMatches Then
                    'labelMsg.Text = "Employee No isn't found!"
                Else
                    If us.UserInfoSearch.totalMatches > 0 Then
                        For Each userInfo As UserInfoItem In us.UserInfoSearch.UserInfo
                            Dim PIN As String = userInfo.employeeNo.Trim(vbNullChar)
                        Next
                        'labelMsg.Text = "Search employee data succ!"
                    Else
                    End If
                End If
                Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
                'Dim s As String = "update DeviceCommands set Executed='1' where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                Dim s As String = "update DeviceCommands Set Executed='1', QueryResult='" & QueryResult & "',ExecutedOn=GETDATE() where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                If (con.State <> ConnectionState.Open) Then
                    con.Open()
                End If
                cmd = New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
                If (con.State <> ConnectionState.Closed) Then
                    con.Close()
                End If
            End If

            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg1)

            If us.UserInfoSearch.totalMatches > 0 Then
                For Each userInfo As UserInfoItem In us.UserInfoSearch.UserInfo
                    DeleteUserFromDevice(userInfo.employeeNo, m_lUserID, DeviceID, "")
                Next
            End If
        Catch ex As Exception
            TraceService("ClearDeviceData" & ex.Message, "Line 3533")
        End Try
    End Sub
    Private Sub GetDeviceInfo(m_lUserID As Integer, DeviceID As String, Dev_cmd_Id As String)
        Dim QueryResult As String = "Y"
        Try
            strTemp = "/ISAPI/AccessControl/UserInfo/Count?format=json"
            m_strInputXml = String.Empty
            Dim ptrCfg1 As IntPtr = IntPtr.Zero
            m_szUrl = Encoding.UTF8.GetBytes(strTemp)
            struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
            Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
            struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
            strTemp = m_strInputXml

            If "" = strTemp Then
                struPTXML.pInBuffer = IntPtr.Zero
                struPTXML.dwInSize = 0
            Else
                m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
                struPTXML.pInBuffer = Marshal.AllocHGlobal(1024 * 10)
                Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
                struPTXML.dwInSize = CUInt(strTemp.Length)
            End If

            struPTXML.pOutBuffer = Marshal.AllocHGlobal(1024 * 10)

            For i As Integer = 0 To 1024 * 10 - 1
                Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
            Next

            struPTXML.dwOutSize = CUInt((1024 * 10))
            struPTXML.byRes = New Byte(31) {}

            Try
                ptrCfg1 = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
                Marshal.StructureToPtr(struPTXML, ptrCfg1, True)
            Catch __unusedArgumentException1__ As ArgumentException
                EHomeForm.TraceService(__unusedArgumentException1__.Message & " " & DeviceID, "Line 3340")
                QueryResult = "N"
                GoTo Update
                'MessageBox.Show("非托管内存异常")
                Throw
            End Try

            If Not HCEHomeCMS.NET_ECMS_GetPTXMLConfig(m_lUserID, ptrCfg1) Then
                'MessageBox.Show("Error: " & HCEHomeCMS.NET_ECMS_GetLastError())
                EHomeForm.TraceService("Get Device Info Error: " & HCEHomeCMS.NET_ECMS_GetLastError() & " " & DeviceID, "Line 3102")
                ''g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_GetPTXMLConfig")
                'Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                'Marshal.FreeHGlobal(struPTXML.pInBuffer)
                'Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                'Marshal.FreeHGlobal(ptrCfg1)
                QueryResult = "N"
                GoTo Update
                'Return
            End If
            'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_GetPTXMLConfig")
            Dim strOutXML As String = String.Empty
            Try
                strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))
            Catch __unusedArgumentException1__ As ArgumentException
                'MessageBox.Show("非托管内存异常")
                EHomeForm.TraceService(__unusedArgumentException1__.Message & " " & DeviceID, "Line 3116")
                QueryResult = "N"
                GoTo Update
                Throw
            End Try

            Dim Res As String = ""
            Dim index As Integer = 0
            Dim flag As Boolean = False

            If strOutXML <> String.Empty Then
                index = strOutXML.LastIndexOf(":")
                For i As Integer = index To strOutXML.Length - 1
                    'If strOutXML(i) >= "0" AndAlso strOutXML(i) <= "9" Then
                    If strOutXML(i) >= "0" Then
                        For j As Integer = i To strOutXML.Length - 1
                            If strOutXML(j) = vbLf Then
                                Res = strOutXML.Substring(i, j - i)
                                flag = True
                                Exit For
                            End If
                        Next
                    End If

                    Res = Res.TrimStart(":").Trim

                    If flag Then
                        'labelMsg.Text = "Total Num of Employee: " & Res
                        Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
                        Dim s As String
                        If Dev_cmd_Id <> "" Then
                            s = "update tblmachine set UserCount='" & Res & "' where SerialNumber='" & DeviceID & "' update DeviceCommands set Executed='1' , QueryResult='" & QueryResult & "', ExecutedOn=GETDATE()  where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                        Else
                            s = "update tblmachine set UserCount='" & Res & "' where SerialNumber='" & DeviceID & "'"
                        End If
                        If (con.State <> ConnectionState.Open) Then
                            con.Open()
                        End If
                        cmd = New SqlCommand(s, con)
                        cmd.ExecuteNonQuery()
                        If (con.State <> ConnectionState.Closed) Then
                            con.Close()
                        End If
                        Exit For
                        GoTo memclear
                    End If
                Next
            End If

Update:     Dim con1 As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            Dim s1 As String
            If Dev_cmd_Id <> "" Then
                s1 = "update DeviceCommands set Executed='1' , QueryResult='" & QueryResult & "', ExecutedOn=GETDATE()  where Dev_cmd_Id='" & Dev_cmd_Id & "'"
            End If
            If (con1.State <> ConnectionState.Open) Then
                con1.Open()
            End If
            cmd = New SqlCommand(s1, con1)
            cmd.ExecuteNonQuery()
            If (con1.State <> ConnectionState.Closed) Then
                con1.Close()
            End If


memclear:   Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg1)
            Return
        Catch ex As Exception
            TraceService("GetDeviceInfo " & ex.Message & " " & DeviceID, "Line 3641")
        End Try
    End Sub
    Private Sub RempteEnroll(EmployeeNo As String, m_lUserID As Integer, DeviceId As String, Dev_cmd_Id As String, TransferToDevice As String, DeviceType As String)
        Dim QueryResult As String = "Y"
        Try
            SetUserDataToDevice(EmployeeNo, m_lUserID, DeviceId, "", TransferToDevice, DeviceType)

#Region "Capture in machine"
            Dim strTemp As String = "/ISAPI/AccessControl/CaptureFaceData"
            Dim m_strInputXml As String = "<CaptureFaceDataCond version=""2.0"" xmlns=""http://www.isapi.org/ver20/XMLSchema""><captureInfrared>false</captureInfrared><dataType>url</dataType></CaptureFaceDataCond>"
            Dim ptrCfg As IntPtr = IntPtr.Zero
            Dim OutBufferLen As Integer = 5 * 1024
            ConfigMethodCapture(ptrCfg, strTemp, m_strInputXml, OutBufferLen)
            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                'MessageBox.Show("Capture Face Failed: " & HCEHomeCMS.NET_ECMS_GetLastError())
                QueryResult = "N"
                EHomeForm.TraceService("Capture Face Failed: " & HCEHomeCMS.NET_ECMS_GetLastError(), "Line 3181")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                GoTo Dev_cmd_Id
                'Return
            End If
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)
            'MessageBox.Show("Capture Accomplished!")
            'Return
#End Region
            Thread.Sleep(3000) '5000
#Region "get KMS link"
            Dim checkCount As Integer = 1
kmslink:    strTemp = "/ISAPI/AccessControl/CaptureFaceData/Progress"
            m_strInputXml = ""
            ptrCfg = IntPtr.Zero
            OutBufferLen = 1024
            ConfigMethodCapture(ptrCfg, strTemp, m_strInputXml, OutBufferLen)
            If Not HCEHomeCMS.NET_ECMS_GetPTXMLConfig(m_lUserID, ptrCfg) Then
                'MessageBox.Show("Error: " & Marshal.GetLastWin32Error())
                QueryResult = "N"
                EHomeForm.TraceService("Error: " & HCEHomeCMS.NET_ECMS_GetLastError(), "Line 3202")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                GoTo Dev_cmd_Id
                'Return
            End If
            Thread.Sleep(1000)
            Dim strOutXML As String = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(Math.Truncate(struPTXML.dwOutSize)))
            Dim FirstStr As String = "<faceDataUrl>"
            Dim LastStr As String = "</faceDataUrl"
            Dim FirstIndex As Integer = strOutXML.IndexOf("<faceDataUrl>")
            Dim LastIndex As Integer = strOutXML.IndexOf("</faceDataUrl>")
            Dim SubLen As Integer = LastIndex - FirstIndex - LastStr.Length
            If SubLen < 0 Then
                checkCount = checkCount + 1
                'MessageBox.Show("Capture Invaild Pic")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)

                If checkCount > 10 Then
                    QueryResult = "N"
                    EHomeForm.TraceService(" Remote Enroll checkCount > 10 EmployeeNo:" & EmployeeNo, "Line 3226")
                    GoTo Dev_cmd_Id
                    'Return
                Else
                    GoTo kmslink
                End If

            End If

            Dim FaceUrl As String = strOutXML.Substring(FirstIndex + FirstStr.Length, SubLen)
            Dim finalPicURL As String = FaceUrl


            Dim DirPath As String = System.Environment.CurrentDirectory & "\StorageServer\Storage\"
            Dim KMSPath As String = "/kms/services/rest/dataInfoService/downloadFile?id="
            Dim CloudPath As String = "/pic"
            If FaceUrl.Contains(KMSPath) Then
                Dim Length As Integer = KMSPath.Length
                FaceUrl = FaceUrl.Substring(Length, FaceUrl.Length - Length)
            ElseIf FaceUrl.Contains(CloudPath) Then
                FaceUrl = FaceUrl.Substring(5, FaceUrl.Length - 5)
            End If
            DirPath &= FaceUrl
            If File.Exists(DirPath) Then
                'FacepictureBox.Image = Image.FromFile(DirPath)
            Else

                EHomeForm.TraceService(" Remote Enroll PIC Don't Existed", "Line 3234")
                'MessageBox.Show("PIC Don't Existed")
            End If
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)
            'Return

#End Region
            Thread.Sleep(1000)
#Region "Assign to employee"
            strTemp = "/ISAPI/AccessControl/UserInfo/Search?format=json"
            m_strInputXml = "{""UserInfoSearchCond"":{""searchID"":""1"",""searchResultPosition"":0,""maxResults"":30,""EmployeeNoList"":[{""employeeNo"":""" & EmployeeNo & """}]}}"
            ptrCfg = IntPtr.Zero
            OutBufferLen = 2 * 1024
            ConfigMethodCapture(ptrCfg, strTemp, m_strInputXml, OutBufferLen)
            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                QueryResult = "N"
                'MessageBox.Show("Error: " & Marshal.GetLastWin32Error())
                EHomeForm.TraceService(" Remote Enroll " & Marshal.GetLastWin32Error() & " EmployeeNo:" & EmployeeNo, "Line 3262")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                GoTo Dev_cmd_Id
                'Return
            End If
            strOutXML = ""
            strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(Math.Truncate(struPTXML.dwOutSize)))
            If strOutXML <> String.Empty Then
                Dim us As UserInfoSearchRoot = JsonConvert.DeserializeObject(Of UserInfoSearchRoot)(strOutXML)
                If 0 = us.UserInfoSearch.totalMatches Then
                    'MessageBox.Show("Please Create Employee First")
                    'QueryResult = "N"
                    EHomeForm.TraceService(" Remote Enroll Please Create Employee First " & EmployeeNo, "Line 3275")
                End If
            End If
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)

            strTemp = "/ISAPI/Intelligent/FDLib/FDSearch?format=json"
            m_strInputXml = "{""searchResultPosition"":0,""maxResults"":30,""faceLibType"":""blackFD"",""FDID"":""1"",""FPID"":""" & EmployeeNo & """}"

            OutBufferLen = 5 * 1024
            ConfigMethodCapture(ptrCfg, strTemp, m_strInputXml, OutBufferLen)

            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                QueryResult = "N"
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                GoTo Dev_cmd_Id
                'Return
            End If
            strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(Math.Truncate(struPTXML.dwOutSize)))
            If strOutXML <> String.Empty Then
                Dim rt As Root = JsonConvert.DeserializeObject(Of Root)(strOutXML)
                If rt.statusCode = 1 Then
                    If rt.totalMatches <> 0 Then
                        'MessageBox.Show("This Employee has Record Face Info")
                        QueryResult = "N"
                        EHomeForm.TraceService(" Remote Enroll This Employee has Record Face Info " & EmployeeNo, "Line 3302")
                        Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                        Marshal.FreeHGlobal(struPTXML.pInBuffer)
                        Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                        Marshal.FreeHGlobal(ptrCfg)
                        GoTo Dev_cmd_Id
                        'Return
                    End If
                End If
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
            End If
            strTemp = "/ISAPI/Intelligent/FDLib/FaceDataRecord?format=json"
            Dim FPID As String = EmployeeNo
            Dim bornTime As String = "2004-05-03"

            m_strInputXml = "{""faceURL"":""" & finalPicURL & """,""faceLibType"":""blackFD"",""FDID"":""1"",""name"":"""",""FPID"":""" & FPID & """,""bornTime"":""" & bornTime & """}"
            ConfigMethodCapture(ptrCfg, strTemp, m_strInputXml, OutBufferLen)
            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                QueryResult = "N"
                'MessageBox.Show("Set Face Error: " & HCEHomeCMS.NET_ECMS_GetLastError())
                EHomeForm.TraceService(" Remote Enroll Set Face Error: " & HCEHomeCMS.NET_ECMS_GetLastError() & " EmployeeNo:" & EmployeeNo, "Line 3327")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                GoTo Dev_cmd_Id
                'Return
            End If
            strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(Math.Truncate(struPTXML.dwOutSize)))
            If strOutXML <> String.Empty Then
                Dim rs As ResponseStatus = JsonConvert.DeserializeObject(Of ResponseStatus)(strOutXML)
                If rs.statusCode = 1 Then
                    'MessageBox.Show("Face SetUp Success")
                Else
                    QueryResult = "N"
                    'MessageBox.Show("Face SetUp Error: " & rs.statusString)
                    EHomeForm.TraceService("Face SetUp Error: " & rs.statusString & " EmployeeNo:" & EmployeeNo, "Line 3343")
                End If
            End If
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)

            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)

            Try
                Dim s As String = "insert into DeviceCommands (SerialNumber, CommandContent, TransferToDevice,Executed,IsOnline,CreatedOn, UserId) values('" & DeviceId & "','CHECK','" & DeviceId & "','0','1',getdate(),'" & EmployeeNo & "')"
                If (con.State <> ConnectionState.Open) Then
                    con.Open()
                End If
                cmd = New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
                If (con.State <> ConnectionState.Closed) Then
                    con.Close()
                End If
            Catch ex As Exception
                EHomeForm.TraceService(ex.Message.ToString, "Line 3375")
            End Try

Dev_cmd_Id: Try
                con = New SqlConnection(EHomeForm.ConnectionString)
                Dim s As String = "update DeviceCommands Set Executed='1', QueryResult='" & QueryResult & "',ExecutedOn=GETDATE() where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                If (con.State <> ConnectionState.Open) Then
                    con.Open()
                End If
                cmd = New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
                If (con.State <> ConnectionState.Closed) Then
                    con.Close()
                End If
            Catch ex As Exception
                EHomeForm.TraceService(ex.Message.ToString, "Line 3390")
            End Try
#End Region
        Catch ex As Exception
            TraceService("RemoteEnroll" & ex.Message, "Line 3868")
        End Try
    End Sub
    Private Sub RempteEnrollFP(EmployeeNo As String, m_lUserID As Integer, DeviceId As String, Dev_cmd_Id As String, TransferToDevice As String, DeviceType As String)
        Dim QueryResult As String = "Y"
        Try
            SetUserDataToDevice(EmployeeNo, m_lUserID, DeviceId, "", TransferToDevice, DeviceType)

#Region "Capture FP"

            strTemp = "/ISAPI/AccessControl/CaptureFingerPrint"
            m_strInputXml = ("<CaptureFingerPrintCond xmlns=""http://www.isapi.org/ver20/XMLSchema"" version=""2.0"" ><fingerNo>1</fingerNo></CaptureFingerPrintCond>")
            Dim ptrCfg As IntPtr = IntPtr.Zero
            Dim OutBufferLen As Integer = (10 * 1024)
            ConfigMethodCapture(ptrCfg, strTemp, m_strInputXml, OutBufferLen)
            Dim szFingerData As String = String.Empty
            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                QueryResult = "N"
                EHomeForm.TraceService("RemoteEnrollFp failed " & EmployeeNo, "Line 4663")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                GoTo Dev_cmd_Id
            End If

            Dim strOutXML As String = String.Empty
            strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CType(struPTXML.dwOutSize, Integer))
            Dim FirstStr As String = "<fingerData>"
            Dim LastStr As String = "</fingerData"
            Dim FirstIndex As Integer = strOutXML.IndexOf("<fingerData>")
            Dim LastIndex As Integer = strOutXML.IndexOf("</fingerData>")
            Dim SubLen As Integer = (LastIndex _
                    - (FirstIndex - LastStr.Length))
            If (SubLen < 0) Then
                EHomeForm.TraceService("RemoteEnrollFp Capture Invalid FingerPrint " & EmployeeNo, "Line 4680")
                'MessageBox.Show("Capture Invalid FingerPrint")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                QueryResult = "N"
                GoTo Dev_cmd_Id
            End If

            Dim fingerData As String = strOutXML.Substring((FirstIndex + FirstStr.Length), SubLen)
            If (fingerData <> String.Empty) Then
                'CapFingerData(fingerData)
            End If

            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)


#End Region
#Region "Assigen to Employee"
            strTemp = "/ISAPI/AccessControl/FingerPrintDownload?format=json"
            Dim FingerID As String = "1"
            m_strInputXml = "{""FingerPrintCfg"":{""employeeNo"":""" + EmployeeNo +
          """,""enableCardReader"":[1],""fingerPrintID"":1,""fingerType"":""normalFP"",""fingerData"":""" + fingerData + """}}"
            OutBufferLen = 1024
            ConfigMethodCapture(ptrCfg, strTemp, m_strInputXml, OutBufferLen)
            If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                EHomeForm.TraceService("RemoteEnrollFp Assign to Emp " & EmployeeNo, "Line 4721")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                QueryResult = "N"
                GoTo Dev_cmd_Id
            End If

            strOutXML = String.Empty
            strOutXML = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CType(struPTXML.dwOutSize, Integer))
            If (strOutXML <> String.Empty) Then
                Dim rs As ResponseStatus = JsonConvert.DeserializeObject(Of ResponseStatus)(strOutXML)
                If (rs.statusCode = 1) Then
                    'MessageBox.Show("FingerPrint SetUP Success")
                Else
                    'MessageBox.Show(("FingerPrint SetUp Error: " + rs.statusString))
                    QueryResult = "N"
                    EHomeForm.TraceService("RemoteEnrollFp Assign to Emp " & rs.statusString & " " & EmployeeNo, "Line 4737")
                End If

            End If
            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
            Marshal.FreeHGlobal(struPTXML.pInBuffer)
            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
            Marshal.FreeHGlobal(ptrCfg)
#End Region

            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            Try
                Dim s As String = "insert into DeviceCommands (SerialNumber, CommandContent, TransferToDevice,Executed,IsOnline,CreatedOn, UserId) values('" & DeviceId & "','CHECK','" & DeviceId & "','0','1',getdate(),'" & EmployeeNo & "')"
                If (con.State <> ConnectionState.Open) Then
                    con.Open()
                End If
                cmd = New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
                If (con.State <> ConnectionState.Closed) Then
                    con.Close()
                End If
            Catch ex As Exception
                EHomeForm.TraceService(ex.Message.ToString, "Line 4758")
            End Try
Dev_cmd_Id: Try
                con = New SqlConnection(EHomeForm.ConnectionString)
                Dim s As String = "update DeviceCommands Set Executed='1', QueryResult='" & QueryResult & "',ExecutedOn=GETDATE() where Dev_cmd_Id='" & Dev_cmd_Id & "'" ' "update DeviceCommands set Executed='1' where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                If (con.State <> ConnectionState.Open) Then
                    con.Open()
                End If
                cmd = New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
                If (con.State <> ConnectionState.Closed) Then
                    con.Close()
                End If
            Catch ex As Exception
                EHomeForm.TraceService(ex.Message.ToString, "Line 4773")
            End Try

        Catch ex As Exception
            TraceService("RemoteEnroll" & ex.Message, "Line 4776")
        End Try
    End Sub
    Private Sub CapFingerData(ByVal fingerData As String)
        Dim szPath As String = Nothing
        szPath = fingerData ' textBoxFingerData.Text
        Dim byfingerData() As Byte = System.Text.Encoding.Default.GetBytes(fingerData)
        Try
            Dim fs As FileStream = New FileStream(szPath, FileMode.Truncate, FileAccess.ReadWrite)
            fs.Write(byfingerData, 0, byfingerData.Length)
            fs.Close()
            MessageBox.Show("FingerPrint Cap SUCCEED", "SUCCEED", MessageBoxButtons.OK)
        Catch ex As System.Exception
            MessageBox.Show("CapFingerprint process failed")
        End Try

    End Sub
    Public Sub ConfigMethodCapture(ByRef ptrCfg As IntPtr, ByVal strTemp As String, ByVal m_strInputXml As String, ByVal OutBufferLen As Integer)
        Try
            Dim m_szInputBuffer(strTemp.Length - 1) As Byte
            m_szUrl = Encoding.Default.GetBytes(strTemp)
            struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
            Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
            struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
            strTemp = m_strInputXml
            If "" = strTemp Then
                struPTXML.pInBuffer = IntPtr.Zero
                struPTXML.dwInSize = 0
            Else
                m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
                struPTXML.pInBuffer = Marshal.AllocHGlobal(strTemp.Length)
                Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
                struPTXML.dwInSize = CUInt(m_szInputBuffer.Length)
            End If
            struPTXML.pOutBuffer = Marshal.AllocHGlobal(OutBufferLen)
            For i As Integer = 0 To OutBufferLen - 1
                Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
            Next i
            struPTXML.dwOutSize = CUInt(OutBufferLen)
            struPTXML.byRes = New Byte(31) {}
            Try
                ptrCfg = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
                Marshal.StructureToPtr(struPTXML, ptrCfg, False)
            Catch e1 As ArgumentException
                'MessageBox.Show("Memory Exception")
                EHomeForm.TraceService("Memory Exception: " & e1.Message, "Line 3419")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                Return
            End Try
        Catch
        End Try
    End Sub
    Public Sub ConfigMethodGetFace(ByRef ptrCfg As IntPtr, ByVal strTemp As String, ByVal m_strInputXml As String, ByVal OutBufferLen As Integer)
        Try
            Dim m_szInputBuffer(strTemp.Length - 1) As Byte
            m_szUrl = Encoding.Default.GetBytes(strTemp)
            struPTXML.pRequestUrl = Marshal.AllocHGlobal(300)
            Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
            struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
            strTemp = m_strInputXml
            If "" = strTemp Then
                struPTXML.pInBuffer = IntPtr.Zero
                struPTXML.dwInSize = 0
            Else
                m_szInputBuffer = Encoding.UTF8.GetBytes(strTemp)
                struPTXML.pInBuffer = Marshal.AllocHGlobal(strTemp.Length)
                Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
                struPTXML.dwInSize = CUInt(m_szInputBuffer.Length)
            End If
            struPTXML.pOutBuffer = Marshal.AllocHGlobal(OutBufferLen)
            For i As Integer = 0 To OutBufferLen - 1
                Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
            Next i
            struPTXML.dwOutSize = CUInt(OutBufferLen)
            struPTXML.byRes = New Byte(31) {}
            Try
                ptrCfg = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
                Marshal.StructureToPtr(struPTXML, ptrCfg, False)
            Catch e1 As ArgumentException
                MessageBox.Show("Memory Exception")
                Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                Marshal.FreeHGlobal(struPTXML.pInBuffer)
                Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                Marshal.FreeHGlobal(ptrCfg)
                Return
            End Try
        Catch
        End Try
    End Sub
    Private m_csLock As New Object()
    Private Sub GetAttLogs(m_lUserID As Integer, DeviceID As String, Dev_cmd_Id As String, strt As DateTime, endd As DateTime)
        Dim QueryResult As String = "Y"
        Dim szResponse As String = String.Empty
        Try
            SyncLock m_csLock
                'listViewEvent.Items.Clear()
                Dim m_lLogNum As Integer = 0

                Dim szUrl As String = "/ISAPI/AccessControl/AcsEvent?format=json"

                Dim MajorType As String = "All" 'comboBoxMainType.SelectedItem.ToString()
                Dim dwMajor As UInteger = GetAcsEventType.ReturnMajorTypeValue(MajorType)

                Dim MinorType As String = "All" 'comboBoxSecondType.SelectedItem.ToString()
                Dim dwMinor As UInteger = GetAcsEventType.ReturnMinorTypeValue(MinorType)

                Dim startYear As Integer = strt.Year
                Dim startMonth As Integer = strt.Month
                Dim startDay As Integer = strt.Day
                Dim startHour As Integer = strt.Hour
                Dim startMinute As Integer = strt.Minute
                Dim startSecond As Integer = strt.Second
                Dim startTime As String = GetStdTime(startYear, startMonth, startDay, startHour, startMinute, startSecond) & "+08:00" 'UTC时间格式
                Dim endYear As Integer = endd.Year
                Dim endMonth As Integer = endd.Month
                Dim endDay As Integer = endd.Day
                Dim endHour As Integer = endd.Hour
                Dim endMinute As Integer = endd.Minute
                Dim endSecond As Integer = endd.Second
                Dim endTime As String = GetStdTime(endYear, endMonth, endDay, endHour, endMinute, endSecond) & "+08:00"
                Dim szName As String = "" ' textBoxName.Text
                Dim szEmployeeNo As String = "" 'textBoxEmployeeNo.Text
                Dim szCardNo As String = "" 'textBoxCardNo.Text
                Dim pos As String = "0"
                Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
                Do
                    Try
                        'Dim struPTXML As HCEHomeCMS.NET_EHOME_PTXML_PARAM = New HCEHomeCMS.NET_EHOME_PTXML_PARAM()
                        'Dim szResponse As String = String.Empty
                        Dim szRequest As String = "{""AcsEventCond"":{""searchID"":""1"",""searchResultPosition"":" & pos & ",""maxResults"":30,""major"":" & dwMajor & ",""minor"":" & dwMinor & ",""startTime"":""" & startTime & """,""endTime"":""" & endTime
                        'Dim szRequest As String = "{""AcsEventCond"":{""searchID"":""1"",""searchResultPosition"":" & pos & ",""maxResults"":20,""major"":" & dwMajor & ",""minor"":" & dwMinor & ",""startTime"":""" & startTime & """,""endTime"":""" & endTime
                        If szCardNo <> "" Then
                            szRequest &= """,""cardNo"":""" & szCardNo
                        End If
                        If szName <> "" Then
                            szRequest &= """,""name"":""" & szName
                        End If
                        If szEmployeeNo <> "" Then
                            szRequest &= """,""employeeNoString"":""" & szEmployeeNo
                        End If
                        szRequest &= """}}"
                        Query("szRequest:" & szRequest)
                        m_szUrl = Encoding.UTF8.GetBytes(szUrl)
                        struPTXML.pRequestUrl = Marshal.AllocHGlobal(256)
                        Marshal.Copy(m_szUrl, 0, struPTXML.pRequestUrl, m_szUrl.Length)
                        struPTXML.dwRequestUrlLen = CUInt(m_szUrl.Length)
                        m_szInputBuffer = Encoding.UTF8.GetBytes(szRequest)
                        struPTXML.pInBuffer = Marshal.AllocHGlobal(1024 * 50)
                        Marshal.Copy(m_szInputBuffer, 0, struPTXML.pInBuffer, m_szInputBuffer.Length)
                        struPTXML.dwInSize = CUInt(szRequest.Length)
                        struPTXML.pOutBuffer = Marshal.AllocHGlobal(1024 * 50)
                        For i As Integer = 0 To (1024 * 50) - 1
                            Marshal.WriteByte(struPTXML.pOutBuffer, i, 0)
                        Next i
                        struPTXML.dwOutSize = CUInt(1024 * 50)
                        struPTXML.byRes = New Byte(31) {}
                        Dim ptrCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struPTXML))
                        Marshal.StructureToPtr(struPTXML, ptrCfg, False)
                        If Not HCEHomeCMS.NET_ECMS_PostPTXMLConfig(m_lUserID, ptrCfg) Then
                            'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_FAIL_T, 1, "NET_ECMS_PostPTXMLConfig"
                            TraceService("GetAttLogs NET_ECMS_PostPTXMLConfig " & DeviceID, "Line 4016")
                            Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                            Marshal.FreeHGlobal(struPTXML.pInBuffer)
                            Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                            Marshal.FreeHGlobal(ptrCfg)
                            QueryResult = "N"
                            GoTo Dev_cmd_Id
                            'Return
                        End If


                        'For j As Integer = 0 To DeviceTree.g_struDeviceInfo.Count - 1
                        '    If DeviceTree.g_struDeviceInfo(j).iLoginID = m_lUserID Then
                        '        DeviceID = System.Text.Encoding.[Default].GetString(DeviceTree.g_struDeviceInfo(j).sDeviceSerial).Trim(vbNullChar)
                        '        Exit For
                        '    End If
                        'Next

                        'TraceServiceLogText("GetAttlog(Line 5092) Login Id For Device " & DeviceID _
                        '                    & " :" & m_lUserID)


                        'g_formList.AddLog(m_iDeviceIndex, HCEHomePublic.OPERATION_SUCC_T, 1, "NET_ECMS_XXXPTXMLConfig")
                        szResponse = ""
                        szResponse = Marshal.PtrToStringAnsi(struPTXML.pOutBuffer, CInt(struPTXML.dwOutSize))

                        'TraceServiceText(szResponse)
                        Marshal.FreeHGlobal(struPTXML.pRequestUrl)
                        Marshal.FreeHGlobal(struPTXML.pInBuffer)
                        Marshal.FreeHGlobal(struPTXML.pOutBuffer)
                        Marshal.FreeHGlobal(ptrCfg)

                        'Dim logString = JsonConvert.SerializeObject(szResponse)

                        If szResponse <> String.Empty Then
                            'TraceServiceTextQuery(szResponse)
                            Dim tmpStr As String = "1"
                            Try
                                Dim rb As AcsEventObject = JsonConvert.DeserializeObject(Of AcsEventObject)(szResponse)
                                tmpStr = "2"
                                If rb.AcsEvent Is Nothing Then
                                    'TraceServiceTextQuery("AcsEvent is NULL!")
                                    Exit Do
                                End If
                                Dim responseTemp As String = rb.AcsEvent.responseStatusStrg
                                tmpStr = "responseTemp : " & responseTemp
                                If responseTemp.Equals("NO MATCH") Then
                                    TraceServiceTextQuery("NO MATCH")
                                    Exit Do
                                End If
                                tmpStr = "21"
                                tmpStr = szResponse
                                For Each info As InfoListItem In rb.AcsEvent.InfoList
                                    tmpStr = "3"
                                    If info.minor = "75" Or info.minor = "1" Or info.minor = "38" Then
                                        tmpStr = "4"
                                        'TraceServiceText(szResponse)
                                        Try
                                            Dim CardNo As String = info.employeeNoString.Trim
                                            If CardNo = "0" Then
                                                CardNo = "Visitor"
                                            End If

                                            Dim PuchTime As DateTime = Convert.ToDateTime(info.time.Substring(0, 19)) '& "-08:00"
                                            Dim CardType As Integer = info.cardType
                                            Dim verifymode As String = info.verifyNo
                                            Dim fCurrTemperature As Double = info.currTemperature
                                            Dim thermometryUnit As String = info.thermometryUnit
                                            Dim Ftemp As String = ""
                                            Dim Ctemp As String = ""
                                            Dim MaskWear As String = ""
                                            Dim TeperatureStatus As String = ""
                                            If Not IsNumeric(info.deviceNo) Then
                                                DeviceID = info.deviceNo
                                            End If
                                            'TraceServiceText("CardNo:" & CardNo & "," & "PuchTime:" & PuchTime.ToString("yyyy-MM-dd HH:mm:ss"))
                                            Dim PuchTimeStr As String
                                            If CardNo = "Visitor" Or AllowSeconds = "Y" Then
                                                PuchTimeStr = PuchTime.ToString("yyyy-MM-dd HH:mm:ss")
                                            Else
                                                PuchTimeStr = PuchTime.ToString("yyyy-MM-dd HH:mm:00")
                                                'PuchTimeStr = PuchTime.ToString("yyyy-MM-dd HH:mm:ss")  'smart one of the customer
                                            End If
                                            Try
                                                If thermometryUnit.Trim = "celsius" Then
                                                    Ctemp = fCurrTemperature
                                                    Ftemp = Math.Round((fCurrTemperature * 9 / 5) + 32, 2)
                                                ElseIf thermometryUnit.Trim = "fahrenheit" Then
                                                    Ftemp = fCurrTemperature
                                                    Ctemp = Math.Round((fCurrTemperature - 32) * 5 / 9, 2)
                                                Else
                                                    Ftemp = ""
                                                    Ctemp = ""
                                                End If
                                                MaskWear = info.mask
                                                TeperatureStatus = info.isAbnomalTemperature.ToLower
                                            Catch ex As Exception

                                            End Try

                                            Dim io_mode As String = ""
                                            If CardNo <> "" And CardType = "1" Then
                                                'Dim con As New SqlConnection(EHomeForm.ConnectionString)
                                                Dim dsM As DataSet = New DataSet
                                                Dim sSqlM As String = "select IN_OUT, ID_NO from tblmachine where SerialNumber='" & DeviceID & "'"
                                                Dim adpM As SqlDataAdapter = New SqlDataAdapter(sSqlM, con)
                                                adpM.Fill(dsM)
                                                If dsM.Tables(0).Rows.Count > 0 Then
                                                    If dsM.Tables(0).Rows(0)(0).ToString.Trim = "0" Then
                                                        'io_mode = "I"
                                                        io_mode = "0"
                                                    ElseIf dsM.Tables(0).Rows(0)(0).ToString.Trim = "1" Then
                                                        'io_mode = "O"
                                                        io_mode = "1"
                                                    ElseIf dsM.Tables(0).Rows(0)(0).ToString.Trim = "2" Or dsM.Tables(0).Rows(0)(0).ToString.Trim = "A" Then
                                                        'Try
                                                        '    Dim xnListIN_OUT As XmlNodeList = Xml.SelectNodes("/PPVSPMessage/Params/attendanceStatus")
                                                        '    For Each xn As XmlNode In xnListIN_OUT
                                                        '        io_mode = xn.InnerText
                                                        '    Next xn
                                                        '    If io_mode.ToLower = "checkin" Then
                                                        '        io_mode = "0"
                                                        '    ElseIf io_mode.ToLower = "checkout" Then
                                                        '        io_mode = "1"
                                                        '    End If
                                                        'Catch ex As Exception
                                                        '    io_mode = ""
                                                        'End Try

                                                        ''LnT
                                                        'If io_mode = "" Or io_mode.ToLower = "undefined" Or dsM.Tables(0).Rows(0)(0).ToString.Trim = "2" Then
                                                        '    Dim ds2 As DataSet = New DataSet
                                                        '    Dim sSql1 = "select count (UserID) from UserAttendance where UserID = '" & CardNo & "' and convert(varchar, AttDateTime, 23) = '" & PuchTime.ToString("yyyy-MM-dd") & "'"
                                                        '    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql1, con)
                                                        '    adap.Fill(ds2)
                                                        '    If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                                                        '        io_mode = "0"
                                                        '    Else
                                                        '        If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                                                        '            io_mode = "0"
                                                        '        Else
                                                        '            io_mode = "1"
                                                        '        End If
                                                        '    End If
                                                        'End If
                                                        ''end LnT

                                                    End If
                                                End If
                                                Dim ds As DataSet = New DataSet()
                                                Dim cmd As SqlCommand
                                                Dim sSql As String = ""
                                                Try
                                                    Dim pIsVU As Boolean = IsValidUserAtt(CardNo, PuchTime, con, EHomeForm.DuplicateCheck, DeviceID)
                                                    'TraceServiceText("pIsVU:" & pIsVU.ToString)
                                                    If pIsVU Then    'Sapphire


#Region "Smartinfocom"
                                                        Try
                                                            sSql = "insert into DumpUserAttendance(DeviceID,UserID,VerifyMode,AttDateTime, CTemp, FTemp,[MaskStatus],[IsAbnomal], UpdatedOn,[io_mode],[AttState]) " &
                                "values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTimeStr & "','" & Ctemp & "', '" & Ftemp & "','" & MaskWear & "','" & TeperatureStatus & "',getdate(),'" & io_mode & "','" & io_mode & "')"
                                                            If con.State <> ConnectionState.Open Then
                                                                con.Open()
                                                            End If
                                                            cmd = New SqlCommand(sSql, con)
                                                            cmd.ExecuteNonQuery()
                                                            If con.State <> ConnectionState.Closed Then
                                                                con.Close()
                                                            End If
                                                        Catch ex As Exception
                                                            If Not ex.Message.ToString.Contains("Violation of PRIMARY KEY constraint") Then
                                                                'save failed logs text                               
                                                                Dim failedTxt As String = DeviceID & "," & CardNo & "," & verifymode & "," & PuchTime.ToString("yyyy-MM-dd HH:mm:00") & "," & Ctemp & ", " & Ftemp & "," & MaskWear & "," & TeperatureStatus & "," & io_mode & "," & ex.Message.ToString & ",RealTime"
                                                                TraceServiceFailedLogsDump(failedTxt)
                                                            End If
                                                        End Try
#End Region

                                                        Try
                                                            sSql = "select Userid from UserAttendance  with (nolock) where DeviceID='" & DeviceID & "' and UserID='" & CardNo & "' and AttDateTime='" & PuchTimeStr & "' " &
                                                        " IF @@ROWCOUNT=0 " &
                                                        "insert into UserAttendance(DeviceID,UserID,VerifyMode,AttDateTime, CTemp, FTemp,[MaskStatus],[IsAbnomal],[io_mode],[UpdateedOn],[AttState]) " &
                                                        "values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTimeStr & "','" & Ctemp & "', '" & Ftemp & "','" & MaskWear & "','" & TeperatureStatus & "','" & io_mode & "',GETDATE(),'" & io_mode & "')"
                                                            '    sSql = "insert into UserAttendance(DeviceID,UserID,VerifyMode,AttDateTime, CTemp, FTemp,[MaskStatus],[IsAbnomal],[io_mode],[UpdateedOn]) " &
                                                            '"values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTimeStr & "','" & Ctemp & "', '" & Ftemp & "','" & MaskWear & "','" & TeperatureStatus & "','" & io_mode & "',GETDATE())"
                                                            'Query(sSql)

                                                            'TraceServiceLogText("GetAttlog(Line 5239) Device Id " & DeviceID & " Query: " & sSql)

                                                            ''only for wondercement
                                                            'If MaskWear.ToLower = "no" Or Ftemp.Trim = "" Or TeperatureStatus.ToLower = "true" Then
                                                            '    sSql = sSql.Replace("UserAttendance", "UserAttendanceRaw")
                                                            'End If

                                                            If con.State <> ConnectionState.Open Then
                                                                con.Open()
                                                            End If
                                                            cmd = New SqlCommand(sSql, con)
                                                            cmd.ExecuteNonQuery()
                                                            If con.State <> ConnectionState.Closed Then
                                                                con.Close()
                                                            End If
                                                        Catch ex As Exception
                                                            If con.State <> ConnectionState.Closed Then
                                                                con.Close()
                                                            End If

                                                            If Not ex.Message.ToString.Contains("Violation of PRIMARY KEY constraint") Then
                                                                'save failed logs text                               
                                                                Dim failedTxt As String = DeviceID & "," & CardNo & "," & verifymode & "," & PuchTime.ToString("yyyy-MM-dd HH:mm:00") & "," & Ctemp & ", " & Ftemp & "," & MaskWear & "," & TeperatureStatus & "," & io_mode & "," & ex.Message.ToString & ",Manual"
                                                                TraceServiceFailedLogs(failedTxt)
                                                            End If

                                                            EHomeForm.TraceService(ex.Message, "GetAttlogs Line 588")
                                                            'If con.State <> ConnectionState.Closed Then
                                                            '    con.Close()
                                                            'End If
                                                        End Try
                                                    End If 'end Sapphire


#Region "Panipath"  'Kapoor Industries
                                                    '                            Try
                                                    '                                Dim ID_NO As String = "1"
                                                    '                                Try
                                                    '                                    ID_NO = (dsM.Tables(0).Rows(0)("ID_NO").ToString.Trim) '.ToString("00")
                                                    '                                Catch ex As Exception
                                                    '                                End Try
                                                    '                                'TraceServiceTextPanipat(sSqlM & " " & ID_NO)
                                                    '                                Dim conTmp As New SqlConnection("Data Source=10.20.33.140;Initial Catalog=iDMS;User Id=sa;Password=kil@1234;MultipleActiveResultSets=true;")
                                                    '                                Dim conTmp1 As New SqlConnection("Data Source=10.20.33.24;Initial Catalog=Payroll;User Id=sa;Password=;MultipleActiveResultSets=true;")
                                                    '                                Dim Ezone As String = "COMP"
                                                    '                                If CardNo.Substring(0, 1) = "4" Then
                                                    '                                    Ezone = "B"
                                                    '                                Else
                                                    '                                    Ezone = "COMP"
                                                    '                                End If
                                                    '                                sSql = "insert into DAILY_ATTANDANCE_TEMP(Ezone,CardNo,Att_Date,PunchTime, MacId, Night) " &
                                                    '"values('" & Ezone & "','" & CardNo & "','" & PuchTime.ToString("yyyy-MM-dd") & "','" & PuchTime.ToString("HH:mm") & "','" & ID_NO & "', '0')"

                                                    '                                'TraceServiceTextPanipat(sSql)

                                                    '                                Try
                                                    '                                    If conTmp.State <> ConnectionState.Open Then
                                                    '                                        conTmp.Open()
                                                    '                                    End If
                                                    '                                    cmd = New SqlCommand(sSql, conTmp)
                                                    '                                    cmd.ExecuteNonQuery()
                                                    '                                    If conTmp.State <> ConnectionState.Closed Then
                                                    '                                        conTmp.Close()
                                                    '                                    End If
                                                    '                                Catch ex As Exception
                                                    '                                    If conTmp.State <> ConnectionState.Closed Then
                                                    '                                        conTmp.Close()
                                                    '                                    End If
                                                    '                                    TraceServiceTextPanipat("10.20.33.140 " & sSql & " " & ex.Message)
                                                    '                                End Try
                                                    '                                Try
                                                    '                                    If conTmp1.State <> ConnectionState.Open Then
                                                    '                                        conTmp1.Open()
                                                    '                                    End If
                                                    '                                    cmd = New SqlCommand(sSql, conTmp1)
                                                    '                                    cmd.ExecuteNonQuery()
                                                    '                                    If conTmp1.State <> ConnectionState.Closed Then
                                                    '                                        conTmp1.Close()
                                                    '                                    End If
                                                    '                                Catch ex As Exception
                                                    '                                    If conTmp1.State <> ConnectionState.Closed Then
                                                    '                                        conTmp1.Close()
                                                    '                                    End If
                                                    '                                    TraceServiceTextPanipat("10.20.33.24 " & sSql & " " & ex.Message)
                                                    '                                End Try
                                                    '                            Catch ex As Exception
                                                    '                                TraceServiceTextPanipat(ex.Message)
                                                    '                            End Try
#End Region

                                                Catch ex As Exception
                                                    EHomeForm.TraceService(ex.Message, "GetAttlog Line 4097")
                                                    If con.State <> ConnectionState.Closed Then
                                                        con.Close()
                                                    End If
                                                End Try
                                                CardNo = GetCardNumber(CardNo)
#Region "tblmachine"
                                                Try
                                                    sSql = "Update tblMachine set UpdatedOn =getdate() where SerialNumber='" & DeviceID & "' "
                                                    If con.State <> ConnectionState.Open Then
                                                        con.Open()
                                                    End If
                                                    cmd = New SqlCommand(sSql, con)
                                                    Dim count As Integer = cmd.ExecuteNonQuery()
                                                    If con.State <> ConnectionState.Closed Then
                                                        con.Close()
                                                    End If
                                                Catch ex As Exception
                                                    EHomeForm.TraceService(ex.Message, "GetAttlog " & sSql & " Line 4115")
                                                    If con.State <> ConnectionState.Closed Then
                                                        con.Close()
                                                    End If
                                                End Try
#End Region
#Region "machinerawpunchandprocess"
                                                If EHomeForm.AttProcess = "Y" Then
                                                    Try
                                                        Dim adpA As New SqlDataAdapter
                                                        ds = New DataSet
                                                        'sSql = "select TM.id_No,TE.companycode,TE.PRESENTCARDNO,TE.SSN,TE.paycode,TE.DepartmentCode from tblMachine TM  with (nolock) inner join tblemployee TE on Tm.CompanyCode = TE.COMPANYCODE where TM.SerialNumber = '" & DeviceID & "' and TE.PresentCardNo = '" & CardNo & "'"
                                                        sSql = "select TES.DUPLICATECHECKMIN, TM.id_No,TE.companycode,TE.PRESENTCARDNO,TE.SSN,TE.paycode,TE.DepartmentCode from tblMachine TM  with (nolock) inner join tblemployee TE on Tm.CompanyCode = TE.COMPANYCODE inner join tblemployeeshiftmaster TES on TES.paycode=TE.paycode where TM.SerialNumber = '" & DeviceID & "' and TE.PresentCardNo = '" & CardNo & "'"
                                                        adpA = New SqlDataAdapter(sSql, con)
                                                        adpA.Fill(ds)
                                                        If ds.Tables(0).Rows.Count > 0 Then
                                                            Dim inout As String = ""
                                                            'If (status = "0") Then
                                                            '    inout = "I"
                                                            'Else
                                                            '    inout = "O"
                                                            'End If
                                                            Dim dupMin As Integer = 5
                                                            Dim pIsV As Boolean = False
                                                            If ds.Tables(0).Rows(0)("DUPLICATECHECKMIN").ToString().Trim() <> "" Then
                                                                dupMin = ds.Tables(0).Rows(0)("DUPLICATECHECKMIN").ToString().Trim()
                                                            End If
                                                            pIsV = IsValid(ds.Tables(0).Rows(0)("PayCode").ToString().Trim(), PuchTime, con, dupMin)
                                                            If pIsV Then
                                                                Try
                                                                    sSql = "insert into MachineRawPunch (CardNo,OfficePunch,P_Day,IsManual,MC_NO,INOUT,PAYCODE,SSN,CompanyCode)values('" & ds.Tables(0).Rows(0)("PresentCardNo").ToString().Trim() & "','" & PuchTime.ToString("yyyy-MM-dd HH:mm") & "','N','N'," & ds.Tables(0).Rows(0)("id_No").ToString().Trim() & ",'" & inout & "','" & ds.Tables(0).Rows(0)("PayCode").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("SSN").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "')"
                                                                    If con.State <> ConnectionState.Open Then
                                                                        con.Open()
                                                                    End If
                                                                    cmd = New SqlCommand(sSql, con)
                                                                    cmd.ExecuteNonQuery()

                                                                    If con.State <> ConnectionState.Closed Then
                                                                        con.Close()
                                                                    End If
                                                                Catch ex As Exception
                                                                    EHomeForm.TraceService(ex.Message, "GetAttlog Line 4155")
                                                                    If con.State <> ConnectionState.Closed Then
                                                                        con.Close()
                                                                    End If
                                                                End Try

                                                                'temp comment
                                                                sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup where companycode='" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "') "
                                                                Dim ds1 As DataSet = New DataSet
                                                                Try
                                                                    adpA = New SqlDataAdapter(sSql, con)
                                                                    adpA.Fill(ds1)
                                                                Catch ex As Exception
                                                                    EHomeForm.TraceService(ex.Message, "GetAttlog Line 4168")
                                                                    If con.State <> ConnectionState.Closed Then
                                                                        con.Close()
                                                                    End If
                                                                End Try

                                                                Dim SetupID As String = ""
                                                                If ds1.Tables(0).Rows.Count > 0 Then
                                                                    SetupID = ds1.Tables(0).Rows(0)(0).ToString.Trim
                                                                    Try
                                                                        If (con.State <> ConnectionState.Open) Then
                                                                            con.Open()
                                                                        End If
                                                                        'Dim tmpdatetime As DateTime = Convert.ToDateTime(atttime.ToString.Trim)
                                                                        cmd = New SqlCommand("ProcessBackDate", con)
                                                                        cmd.CommandTimeout = 1800
                                                                        cmd.CommandType = CommandType.StoredProcedure
                                                                        cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                                                                        cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                                                                        cmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("CompanyCode").ToString.Trim
                                                                        cmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("DepartmentCode").ToString.Trim
                                                                        cmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("PayCode").ToString.Trim
                                                                        cmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID
                                                                        cmd.ExecuteNonQuery()
                                                                        If (con.State <> ConnectionState.Closed) Then
                                                                            con.Close()
                                                                        End If
                                                                    Catch ex As Exception
                                                                        EHomeForm.TraceService(ex.Message, "GetAttlog Line 4196")
                                                                        If con.State <> ConnectionState.Closed Then
                                                                            con.Close()
                                                                        End If
                                                                    End Try
                                                                End If
                                                            End If
                                                            'end temp comment
                                                        End If
                                                    Catch ex As Exception
                                                        EHomeForm.TraceService(ex.Message, "GetAttlog Line 4206")
                                                        If con.State <> ConnectionState.Closed Then
                                                            con.Close()
                                                        End If
                                                    End Try
                                                End If
#End Region
                                            End If
                                        Catch e As IOException
                                            'Str = String.Format("[ACS ALARM] save file failed![%s]", e.ToString())
                                            TraceService(e.Message, "GetAttlog Line 4215")
                                        End Try

                                    End If
                                    tmpStr = "5"
                                    'listViewEvent.BeginUpdate()
                                    Dim item As New ListViewItem()
                                    m_lLogNum += 1
                                    tmpStr = "m_lLogNum " & m_lLogNum
                                Next info
                                Dim posTmp As Integer = 0
                                Integer.TryParse(pos, posTmp)
                                tmpStr = "pos " & pos
                                Dim numOfMatches As Integer = rb.AcsEvent.numOfMatches
                                tmpStr = "numOfMatches " & numOfMatches
                                posTmp += numOfMatches
                                pos = posTmp.ToString()

                                If rb.AcsEvent.responseStatusStrg.Equals("OK") Then
                                    Exit Do
                                End If
                            Catch ex As Exception
                                Query(tmpStr)
                                Query(szResponse)
                                TraceService("GetAttLogs" & ex.Message, "Line 5301")
                                Exit Do
                            End Try
                        End If
                    Catch ex As Exception
                        TraceService("GetAttLogs" & ex.Message, "Line 5305")
                        Exit Do
                    End Try
                Loop
Dev_cmd_Id:     If Dev_cmd_Id <> "" Then
                    'Dim s As String = "update DeviceCommands set Executed='1' where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                    Dim s As String = "update DeviceCommands Set Executed='1', QueryResult='" & QueryResult & "', ExecutedOn=GETDATE() where Dev_cmd_Id='" & Dev_cmd_Id & "'"
                    If (con.State <> ConnectionState.Open) Then
                        con.Open()
                    End If
                    cmd = New SqlCommand(s, con)
                    cmd.ExecuteNonQuery()
                    If (con.State <> ConnectionState.Closed) Then
                        con.Close()
                    End If
                End If
            End SyncLock
        Catch ex As Exception
            TraceServiceText(szResponse)
            TraceService("GetAttLogs" & ex.Message, "Line 4285")
        End Try
    End Sub
    Private Function GetStdTime(ByVal year As Integer, ByVal month As Integer, ByVal day As Integer, ByVal hour As Integer, ByVal min As Integer, ByVal sec As Integer) As String
        Dim dt As New DateTime(year, month, day, hour, min, sec)
        Return String.Format("{0:yyyy-MM-ddTHH:mm:ss}", dt)
        '2017-04-01T13:16:32.108
    End Function
#End Region

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        'GetUserDataFromDevice("")
    End Sub
    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        'SetUserDataToDevice("2", "C95174593")
    End Sub
    'Private Sub TimerMain_Tick(sender As System.Object, e As System.EventArgs) Handles TimerMain.Tick
    Sub TimerDeviceOL(ByVal sender As Object, ByVal e As ElapsedEventArgs)
        Try
            'If EnableMaintimer Then
            'Dim nodeCount = DeviceTree.treeView1.Nodes.Item(0).Nodes.Item(0).Text
            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            'timerOL.Enabled = False
            Dim ls As New List(Of String)()
            For x As Integer = 0 To DeviceTree.g_struDeviceInfo.Count - 1 'DeviceTree.treeView1.Nodes.Item(0).Nodes.Count - 1  'get device list from tree 
                'ls.Add(DeviceTree.treeView1.Nodes.Item(0).Nodes.Item(x).Text.Trim(vbNullChar)) 'add to list    
                If DeviceTree.g_struDeviceInfo(x).iDeviceIndex <> -1 Then
                    ls.Add(System.Text.Encoding.Default.GetString(DeviceTree.g_struDeviceInfo(x).byDeviceID).Trim(vbNullChar))
                End If
            Next

            'update updatedon for each device in list
            'Dim s As String = "update tblmachine set isfp ='Y', UpdatedOn=getdate() where SerialNumber in ('" & String.Join("', '", ls.ToArray()) & "') or DeviceName in ('" & String.Join("', '", ls.ToArray()) & "') update tblmachine set isfp ='N' where SerialNumber not in ('" & String.Join("', '", ls.ToArray()) & "') "
            Dim s As String = "update tblmachine set isfp ='Y', UpdatedOn=getdate() where SerialNumber in ('" & String.Join("', '", ls.Distinct.ToArray()) & "')"
            'TraceServiceText(s)
            If (con.State <> ConnectionState.Open) Then
                con.Open()
            End If
            cmd = New SqlCommand(s, con)
            Dim RowsCount As Integer = cmd.ExecuteNonQuery
            If (con.State <> ConnectionState.Closed) Then
                con.Close()
            End If

            'timerOL.Enabled = True
            'End If
        Catch ex As Exception
            TraceService(ex.Message & " OL Timer ", "Line 4327")
        End Try
    End Sub
    Sub TimerDeviceCommand1(ByVal sender As Object, ByVal e As ElapsedEventArgs)
        Try
            'TraceServiceText("EnableMaintimer " & EnableMaintimer.ToString)
            'TimerDeviceCommand.Enabled = False
            timerCommand.Enabled = False
            If ActiveSerialNumber Is Nothing Then
                timerCommand.Enabled = True
                Return
            End If
            If ActiveSerialNumber.Count = 0 Then
                timerCommand.Enabled = True
                Return
            End If
            'If EnableMaintimer Then        
            'Thread.Sleep(10)
            Dim ls As New List(Of String)()
            For x As Integer = 0 To DeviceTree.g_struDeviceInfo.Count - 1 'DeviceTree.treeView1.Nodes.Item(0).Nodes.Count - 1
                'ls.Add(DeviceTree.treeView1.Nodes.Item(0).FirstNode.Text.Trim(vbNullChar))
                'ls.Add(DeviceTree.treeView1.Nodes.Item(0).Nodes.Item(x).Text.Trim(vbNullChar))
                If DeviceTree.g_struDeviceInfo(x).iDeviceIndex <> -1 Then
                    ls.Add(System.Text.Encoding.Default.GetString(DeviceTree.g_struDeviceInfo(x).byDeviceID).Trim(vbNullChar))
                End If
            Next
            Dim activeDevice As String() = ls.Distinct.ToArray
            Dim adap As SqlDataAdapter
            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            Dim ds As DataSet = New DataSet
            'Dim sSql As String = "select top 1 * from DeviceCommands where TransferToDevice in ('" & String.Join("', '", activeDevice) & "') and TransferToDevice in ('" & String.Join("', '", ActiveSerialNumber) & "') and Executed='0' order by Priority, TransferToDevice, CommandContent"

            Dim sSql As String = "select top 1 * from DeviceCommands, tblmachine where DeviceCommands.TransferToDevice = tblmachine.serialnumber and TransferToDevice in ('" & String.Join("', '", activeDevice) & "') and TransferToDevice in ('" & String.Join("', '", ActiveSerialNumber) & "') and Executed='0' order by Priority--, TransferToDevice, CommandContent"
            'Dim sSql As String = "select top 1 * from DeviceCommands, tblmachine where DeviceCommands.TransferToDevice = tblmachine.serialnumber and TransferToDevice in ('" & String.Join("', '", activeDevice) & "') and  Executed='0' order by Priority"  --, TransferToDevice, CommandContent


            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
            'Query("Count " & ds.Tables(0).Rows.Count)
            'Query(sSql)
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                'Dim DeviceIdBy As Byte() = System.Text.Encoding.[Default].GetBytes(activeDevice(i))
                Dim index As Integer '= DeviceTree.g_struDeviceInfo(m_iDeviceIndex).sDeviceSerial
                Dim m_lUserID As Integer
                For j As Integer = 0 To DeviceTree.g_struDeviceInfo.Count - 1
                    If System.Text.Encoding.[Default].GetString(DeviceTree.g_struDeviceInfo(j).sDeviceSerial).Trim(vbNullChar) = ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim Then
                        index = j
                        m_lUserID = DeviceTree.g_struDeviceInfo(index).iLoginID
                        'TraceServiceLogText("Command Login Id For Device " & System.Text.Encoding.[Default].GetString(DeviceTree.g_struDeviceInfo(j).sDeviceSerial).Trim(vbNullChar) _
                        '                    & " :" & m_lUserID)
                        Exit For
                    End If
                Next
                'TraceServiceText("CommandContent:" & ds.Tables(0).Rows(i)("CommandContent").ToString.Trim & " TransferToDevice:" & ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim)
                If ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagsetdatetime" Then
                    SetDeviceConfig(m_lUserID, ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim, "flagsetdatetime")
                ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagreboot" Then
                    SetReboot(m_lUserID, ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim, "REBOOT")
                ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagremoteenrollFace" Then
                    RempteEnroll(ds.Tables(0).Rows(i)("UserID").ToString.Trim, m_lUserID, ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("DeviceType").ToString.Trim)
                ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagremoteenroll" Then
                    RempteEnrollFP(ds.Tables(0).Rows(i)("UserID").ToString.Trim, m_lUserID, ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("DeviceType").ToString.Trim)
                ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flaginfo" Then
                    GetDeviceInfo(m_lUserID, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim)
                ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "CHECK" Then
                    'GetUserDataFromDevice("", m_lUserID, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim)
                    GetUserDataFromDevice(ds.Tables(0).Rows(i)("UserID").ToString.Trim, m_lUserID, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim, ds.Tables(0).Rows(i)("DeviceType").ToString.Trim)
                ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagupduserinfo" Then 'Or ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagsetfp" 
                    SetUserDataToDevice(ds.Tables(0).Rows(i)("UserID").ToString.Trim, m_lUserID, ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("DeviceType").ToString.Trim)
                ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagdeluserinfo" Then
                    DeleteUserFromDevice(ds.Tables(0).Rows(i)("UserID").ToString.Trim, m_lUserID, ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim)
                ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagclearData" Then
                    ClearDeviceData(m_lUserID, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim)
                ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "ATTLOG" Then 'ATTLOG
                    Dim strt As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(i)("StartDateATTLOG").ToString.Trim)
                    Dim endd As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(i)("EndDateATTLOG").ToString.Trim)
                    GetAttLogs(m_lUserID, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim, strt, endd)
                End If
            Next
            '
            'End If
            timerCommand.Enabled = True
            'TimerDeviceCommand.Enabled = True
        Catch ex As Exception
            timerCommand.Enabled = True
            TraceService(ex.Message & " Command Timer ", "Line 3988")
        End Try
    End Sub
    'Sub TimerDeviceCommand1(ByVal sender As Object, ByVal e As ElapsedEventArgs)
    '    Try
    '        'TraceServiceText("EnableMaintimer " & EnableMaintimer.ToString)
    '        'TimerDeviceCommand.Enabled = False
    '        timerCommand.Enabled = False
    '        'If EnableMaintimer Then        
    '        'Thread.Sleep(10)
    '        Dim ls As New List(Of String)()
    '        For x As Integer = 0 To DeviceTree.g_struDeviceInfo.Count - 1 'DeviceTree.treeView1.Nodes.Item(0).Nodes.Count - 1
    '            'ls.Add(DeviceTree.treeView1.Nodes.Item(0).FirstNode.Text.Trim(vbNullChar))
    '            'ls.Add(DeviceTree.treeView1.Nodes.Item(0).Nodes.Item(x).Text.Trim(vbNullChar))
    '            ls.Add(System.Text.Encoding.Default.GetString(DeviceTree.g_struDeviceInfo(x).byDeviceID).Trim(vbNullChar))
    '        Next
    '        Dim activeDevice As String() = ls.Distinct.ToArray
    '        Dim adap As SqlDataAdapter
    '        Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
    '        Dim ds As DataSet = New DataSet
    '        Dim sSql As String = "select top 1 * from DeviceCommands where TransferToDevice in ('" & String.Join("', '", activeDevice) & "') and Executed='0' order by Dev_Cmd_ID, Priority"
    '        adap = New SqlDataAdapter(sSql, con)
    '        adap.Fill(ds)
    '        'MsgBox("Count " & ds.Tables(0).Rows.Count)
    '        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
    '            'Dim DeviceIdBy As Byte() = System.Text.Encoding.[Default].GetBytes(activeDevice(i))
    '            Dim index As Integer '= DeviceTree.g_struDeviceInfo(m_iDeviceIndex).sDeviceSerial
    '            Dim m_lUserID As Integer
    '            For j As Integer = 0 To DeviceTree.g_struDeviceInfo.Count - 1
    '                If System.Text.Encoding.[Default].GetString(DeviceTree.g_struDeviceInfo(j).sDeviceSerial).Trim(vbNullChar) = ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim Then
    '                    index = j
    '                End If
    '                m_lUserID = DeviceTree.g_struDeviceInfo(index).iLoginID
    '            Next
    '            'TraceServiceText("CommandContent:" & ds.Tables(0).Rows(i)("CommandContent").ToString.Trim & " TransferToDevice:" & ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim)
    '            If ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagremoteenrollFace" Then
    '                RempteEnroll(ds.Tables(0).Rows(i)("UserID").ToString.Trim, m_lUserID, ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim)
    '            ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flaginfo" Then
    '                GetDeviceInfo(m_lUserID, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim)
    '            ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "CHECK" Then
    '                'GetUserDataFromDevice("", m_lUserID, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim)
    '                GetUserDataFromDevice(ds.Tables(0).Rows(i)("UserID").ToString.Trim, m_lUserID, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim)
    '            ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagupduserinfo" Then 'Or ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagsetfp" 
    '                SetUserDataToDevice(ds.Tables(0).Rows(i)("UserID").ToString.Trim, m_lUserID, ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim)
    '            ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagdeluserinfo" Then
    '                DeleteUserFromDevice(ds.Tables(0).Rows(i)("UserID").ToString.Trim, m_lUserID, ds.Tables(0).Rows(i)("SerialNumber").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim)
    '            ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "flagclearData" Then
    '                ClearDeviceData(m_lUserID, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim)
    '            ElseIf ds.Tables(0).Rows(i)("CommandContent").ToString.Trim = "ATTLOG" Then 'ATTLOG
    '                Dim strt As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(i)("StartDateATTLOG").ToString.Trim)
    '                Dim endd As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(i)("EndDateATTLOG").ToString.Trim)
    '                GetAttLogs(m_lUserID, ds.Tables(0).Rows(i)("TransferToDevice").ToString.Trim, ds.Tables(0).Rows(i)("Dev_cmd_Id").ToString.Trim, strt, endd)
    '            End If
    '        Next
    '        '
    '        'End If
    '        timerCommand.Enabled = True
    '        'TimerDeviceCommand.Enabled = True
    '    Catch ex As Exception
    '        TraceService(ex.Message & " Command Timer ", "Line 3988")
    '    End Try
    'End Sub
    Sub timerDeleteFilesFun()
        timerDeleteFiles.Enabled = False
        If System.DateTime.Now.ToString("HH:mm") = AutoDownloadLogsTime Then
            Dim directoryName As String = SS_STORAGE_PATH
            For Each deleteFile In Directory.GetFiles(directoryName, "*.*", SearchOption.TopDirectoryOnly)
                Try
                    File.Delete(deleteFile)
                Catch ex As Exception
                End Try
            Next


            Try
                If AutoDownloadLogs = "Y" Then
                    Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
                    Dim sSql As String = "select LastLog, SerialNumber from tblmachine "
                    Dim ds As DataSet = New DataSet
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                    adap.Fill(ds)
                    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                        If ds.Tables(0).Rows(i)(0).ToString.Trim <> "" Then


                            'sSql = "insert into DeviceCommands (SerialNumber,CommandContent,TransferToDevice,Executed,IsOnline,CreatedOn,StartDateATTLOG,EndDateATTLOG) values " &
                            '"('" & ds.Tables(0).Rows(i)(1).ToString.Trim & "','ATTLOG','" & ds.Tables(0).Rows(i)(1).ToString.Trim & "','0','1',getdate(),'" & Now.AddDays(-1).ToString("yyyy-MM-dd 00:00:00") & "','" & Now.ToString("yyyy-MM-dd 23:59:59") & "') "
                            'If con.State <> ConnectionState.Open Then
                            '    con.Open()
                            'End If
                            'cmd = New SqlCommand(sSql, con)
                            'cmd.ExecuteNonQuery()
                            'If con.State <> ConnectionState.Closed Then
                            '    con.Close()
                            'End If


                            Dim startdate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(i)(0).ToString.Trim)
                            If Now.Subtract(startdate).Minutes > 15 Then
                                sSql = "select TransferToDevice from DeviceCommands where TransferToDevice='" & ds.Tables(0).Rows(i)(1).ToString.Trim & "' and CommandContent='ATTLOG' and StartDateATTLOG='" & startdate.ToString("yyyy-MM-dd HH:mm:00") & "'
                                    IF @@ROWCOUNT=0" &
                                    "insert into DeviceCommands (SerialNumber,CommandContent,TransferToDevice,Executed,IsOnline,CreatedOn,StartDateATTLOG,EndDateATTLOG,Priority) values " &
                            "('" & ds.Tables(0).Rows(i)(1).ToString.Trim & "','ATTLOG','" & ds.Tables(0).Rows(i)(1).ToString.Trim & "','0','1',getdate(),'" & startdate.ToString("yyyy-MM-dd HH:mm:00") & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "',3) "
                                If con.State <> ConnectionState.Open Then
                                    con.Open()
                                End If
                                cmd = New SqlCommand(sSql, con)
                                cmd.ExecuteNonQuery()
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception

            End Try

        End If
        timerDeleteFiles.Enabled = True
    End Sub
    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Dim adap As SqlDataAdapter
        Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
        Dim ds As DataSet = New DataSet
        Dim ssql As String = "select SerialNumber from tblMachine where DeviceName like'%JBR%'"
        adap = New SqlDataAdapter(ssql, con)
        adap.Fill(ds)
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim dsE As DataSet = New DataSet
            ssql = "select EmpId from EmpLocation where location='Kosamba'"
            adap = New SqlDataAdapter(ssql, con)
            adap.Fill(dsE)
            For j As Integer = 0 To dsE.Tables(0).Rows.Count - 1
                ssql = "insert into Devicecommands (SerialNumber, CommandContent, TransferToDevice, UserId, Executed, IsOnline, CreatedOn) " &
"values('" & ds.Tables(0).Rows(i)(0).ToString.Trim & "','flagdeluserinfo','" & ds.Tables(0).Rows(i)(0).ToString.Trim & "', '" & dsE.Tables(0).Rows(j)(0).ToString.Trim & "',0,1,getdate())"
                If con.State <> ConnectionState.Open Then
                    con.Open()
                End If
                cmd = New SqlCommand(ssql, con)
                Dim count As Integer = cmd.ExecuteNonQuery()
                If con.State <> ConnectionState.Closed Then
                    con.Close()
                End If
            Next
        Next
        MsgBox("Done")
    End Sub
    Public Sub CleanUpBytearray(ByRef source As Byte(), ByVal len As Integer)
        For i As Integer = 0 To len - 1
            source(i) = 0
        Next
    End Sub
    Private Sub btnConnect_Click(sender As Object, e As EventArgs) Handles btnConnect.Click
        Timer1.Enabled = False
        ConnectButtonClick()
        TimerPortCheck.Enabled = True
    End Sub
    Private Sub ConnectButtonClick()
        m_csLocalIP = m_cmbLocalIP.Text.Trim
        'Update UltraiDMSValues.xml
        Dim serverIp As String = textServerIP.Text.Trim

        Dim ip As IPAddress
        Dim is_valid As Boolean = IPAddress.TryParse(serverIp, ip)
        If is_valid = False Then
            MsgBox("Invalid IP Address")
            textServerIP.Select()
            Exit Sub
        End If

        Dim doc As XmlDocument = New XmlDocument
        doc.Load("UltraiDMSValues.xml")
        Dim root As XmlNode = doc.DocumentElement.FirstChild
        root.SelectSingleNode("//AlarmServerIP").InnerText = serverIp
        root.SelectSingleNode("//NTPServerIP").InnerText = serverIp
        root.SelectSingleNode("//PictureServerIP").InnerText = serverIp
        root.SelectSingleNode("//AddressMap//StreamServerIP").InnerText = serverIp
        root.SelectSingleNode("//AddressMap//AudioServerIP").InnerText = serverIp
        root.SelectSingleNode("//AddressMap//AlarmServerIP").InnerText = serverIp
        root.SelectSingleNode("//AddressMap//PictureServerIP").InnerText = serverIp
        root.SelectSingleNode("//DASIP").InnerText = serverIp
        If serverIp <> m_csLocalIP Then
            root.SelectSingleNode("//Enable").InnerText = "1"
        Else
            root.SelectSingleNode("//Enable").InnerText = "0"
        End If
        doc.Save("UltraiDMSValues.xml")


        Dim sSsql As String = ""
        Dim con As SqlConnection = New SqlConnection(ConnectionString)
        Try
            Dim sSql As String = "update iDMSSetting set ServerIP='" & serverIp & "'" &
                " IF @@ROWCOUNT=0 " &
                " Insert into iDMSSetting (ServerIP) values ('" & serverIp & "')"

            If con.State <> ConnectionState.Open Then
                con.Open()
            End If
            Dim cmd As SqlCommand
            cmd = New SqlCommand(sSql, con)
            cmd.ExecuteNonQuery()
            If con.State <> ConnectionState.Closed Then
                con.Close()
            End If
        Catch ex As Exception
            If con.State <> ConnectionState.Closed Then
                con.Close()
            End If
        End Try


        'g_formList = DeviceLogList.Instance()
        m_iChanelType = HCEHomeCMS.DEMO_CHANNEL_TYPE_ZERO
        m_hWnd = Me.Handle
        'm_hPreviewWnd = m_previewWnd.Handle
        m_treeHandle = g_deviceTree.treeView1.Handle
        'm_rdoLocalLog.[Select]()

        For i As Integer = 0 To GlobalDefinition.MAX_DEVICES - 1
            DeviceTree.g_struDeviceInfo(i).Init()

            For j As Integer = 0 To GlobalDefinition.MAX_CHAN_NUM_DEMO - 1
                DeviceTree.g_struDeviceInfo(i).struChanInfo(j).Init()
            Next
        Next

        'Me.m_panelDeviceLog.Controls.Add(g_formList)
        'Me.m_panelDeviceTree.Controls.Add(g_deviceTree)
        'g_formList.Dock = DockStyle.Fill
        'g_deviceTree.Dock = DockStyle.Fill

        'm_logListHandle = g_formList.Handle

        'Me.m_previewPanelOne.BackColor = Color.DarkGray
        'Me.m_previewPanelTwo.BackColor = Color.DarkGray
        'Me.m_previewPanelThree.BackColor = Color.DarkGray
        'Me.m_previewPanelFour.BackColor = Color.DarkGray
        'g_deviceTree.m_CurPreviewPanel(0) = Me.m_previewPanelOne
        'g_deviceTree.m_CurPreviewPanel(1) = Me.m_previewPanelTwo
        'g_deviceTree.m_CurPreviewPanel(2) = Me.m_previewPanelThree
        'g_deviceTree.m_CurPreviewPanel(3) = Me.m_previewPanelFour       
        DeviceTree.m_cslocalIP = m_csLocalIP
        DeviceTree.m_lport = m_nPort
        'AudioTalk.m_csIPAddr = m_csLocalIP
        'AudioTalk.m_lPort = m_nPort
        m_struServInfo.Init()
        InitParamFromXML()
        'InitChildWindow()
        InitLib()
        'InitPreviewListenParam()

        If False = System.IO.Directory.Exists(SS_STORAGE_PATH & "\tempImages") Then
            System.IO.Directory.CreateDirectory(SS_STORAGE_PATH & "\tempImages")
        End If

        If False = System.IO.Directory.Exists(System.Environment.CurrentDirectory & "\t") Then
            System.IO.Directory.CreateDirectory(System.Environment.CurrentDirectory & "\t")
        End If

        If False = System.IO.Directory.Exists(System.Environment.CurrentDirectory & "\Events") Then
            System.IO.Directory.CreateDirectory(System.Environment.CurrentDirectory & "\Events")
        End If

        If False = System.IO.Directory.Exists(System.Environment.CurrentDirectory & "\FailedEvents") Then
            System.IO.Directory.CreateDirectory(System.Environment.CurrentDirectory & "\FailedEvents")
        End If

        LabelStatus.Text = "Connected"
        Application.DoEvents()
        timerOL = New Timers.Timer(5000)  '5000   '30000 for wonder
        AddHandler timerOL.Elapsed, New ElapsedEventHandler(AddressOf TimerDeviceOL)
        timerOL.Start()

        timerCommand = New Timers.Timer(1000) '1000 '5000
        AddHandler timerCommand.Elapsed, New ElapsedEventHandler(AddressOf TimerDeviceCommand1)
        timerCommand.Start()


        timerDeleteFiles = New Timers.Timer(1000)  '36 00 000
        AddHandler timerDeleteFiles.Elapsed, New ElapsedEventHandler(AddressOf timerDeleteFilesFun)
        timerDeleteFiles.Start()


        'timerSmartInfo = New Timers.Timer(2400000)  '36 00 000  3600000
        'AddHandler timerSmartInfo.Elapsed, New ElapsedEventHandler(AddressOf timerSmartInfoFun)
        'timerSmartInfo.Start()


        'timerRestart = New Timers.Timer(600000)  '600000
        'AddHandler timerRestart.Elapsed, New ElapsedEventHandler(AddressOf timerRestartFun)
        'timerRestart.Start()

        Me.Hide()
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Enabled = False
        If textServerIP.Text.Trim <> "" Then
            ConnectButtonClick()
            'TimerPortCheck.Enabled = True
        End If
    End Sub
    Public Sub TraceServiceText(ByVal errmessage As String)
        Try
            Dim fs As FileStream = New FileStream("String.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.[End])
            sw.WriteLine(vbLf & errmessage.Trim(vbNullChar) & " " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
            sw.Flush()
            sw.Close()
        Catch
        End Try
    End Sub
    Public Function GetCardNumber(ByVal USerID As String) As String
        If IsNumeric(USerID) Then
            Dim cardno As String = "", zeros As String = ""
            Try
                Dim UIDLenth As Integer = USerID.Length
                Dim toloop As Integer = 8 - UIDLenth
                For i As Integer = 1 To toloop
                    zeros = zeros & "0"
                Next i
                cardno = zeros & USerID
                'TraceService(cardno, "DeviceLogList Line 2376");
                Return cardno
            Catch ex As Exception
                'TraceService(ex.Message, "DeviceLogList Line 2382");
                Return cardno
            End Try
        Else
            Return USerID
        End If
    End Function
    Public Function IsValid(ByVal UID As String, PTime As DateTime, conS As SqlConnection, dupMin As Integer) As Boolean

        Dim Valid As Boolean = False
        Try
            Dim sSql As String = ""
            Dim RsVald As DataSet = New DataSet()
            sSql = "select max(officepunch) as LastPunch from machinerawpunch where paycode ='" & UID.Trim & "' "
            'TraceServiceText(sSql)

            'TraceServiceText(PTime)
            Dim SDV As SqlDataAdapter = New SqlDataAdapter(sSql, conS)
            SDV.Fill(RsVald)
            'TraceServiceText(Convert.ToDateTime(RsVald.Tables(0).Rows(0)("LastPunch").ToString.Trim).AddMinutes(dupMin))
            If RsVald.Tables(0).Rows.Count = 0 Or RsVald.Tables(0).Rows(0)(0).ToString().Trim() = "" Or String.IsNullOrEmpty(RsVald.Tables(0).Rows(0)(0).ToString().Trim()) Then
                Valid = True
            ElseIf Convert.ToDateTime(RsVald.Tables(0).Rows(0)("LastPunch").ToString.Trim).AddMinutes(dupMin) < PTime Then
                Valid = True
            Else
                Valid = False
            End If

        Catch ex As Exception

            Return False
        End Try
        Return Valid
    End Function
    Public Function IsValidUserAtt(ByVal UID As String, PTime As DateTime, conS As SqlConnection, dupMin As Integer, DeviceID As String) As Boolean
        Dim Valid As Boolean = False
        If UID = "Visitor" Then
            Return True
        End If
        Dim sSql As String = ""
        Try
            If dupMin = 0 Then
                Return True
            End If

            Dim RsVald As DataSet = New DataSet()
            'sSql = "select max(AttDateTime) as LastPunch from UserAttendance where Userid ='" & UID.Trim & "' and DeviceID='" & DeviceID & "' "
            sSql = "select AttDateTime from UserAttendance where Userid ='" & UID.Trim & "' and DeviceID='" & DeviceID & "' and AttDateTime between '" & PTime.AddMinutes(-dupMin).ToString("yyyy-MM-dd HH:mm:00") & "' and '" & PTime.ToString("yyyy-MM-dd HH:mm:00") & "'"
            'TraceServiceText(sSql)

            Dim SDV As SqlDataAdapter = New SqlDataAdapter(sSql, conS)
            SDV.Fill(RsVald)
            'TraceServiceText(Convert.ToDateTime(RsVald.Tables(0).Rows(0)("LastPunch").ToString.Trim).AddMinutes(dupMin))

            If RsVald.Tables(0).Rows.Count > 0 Then
                Valid = False
            Else
                Valid = True
            End If

            'If RsVald.Tables(0).Rows(0)("LastPunch").ToString.Trim = "" Then
            '    Valid = True
            'ElseIf Convert.ToDateTime(RsVald.Tables(0).Rows(0)("LastPunch").ToString.Trim).AddMinutes(dupMin) > PTime Then
            '    Valid = False
            'Else
            '    Valid = True
            'End If

        Catch ex As Exception
            TraceServiceTextQuery(ex.Message & " " & sSql)
            Return False
        End Try

        Return Valid
    End Function
    Public Sub TraceServiceTextPanipat(ByVal errmessage As String)
        Try
            Dim fs As FileStream = New FileStream("Panipat.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.[End])
            sw.WriteLine(vbLf & errmessage & " " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
            sw.Flush()
            sw.Close()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub TimerLoadActiveDevices_Tick(sender As Object, e As EventArgs) Handles TimerLoadActiveDevices.Tick
        TimerLoadActiveDevices.Enabled = False
        'LoadActiveSerialNumber()
        TimerLoadActiveDevices.Enabled = True
    End Sub
    Public Sub TraceServiceTextQuery(ByVal errmessage As String)
        Try
            Dim fs As FileStream = New FileStream("Query.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.[End])
            sw.WriteLine(vbLf & errmessage)
            sw.Flush()
            sw.Close()
        Catch ex As Exception

        End Try

    End Sub
    Public Sub Query(ByVal errmessage As String)
        Try
            Dim fs As FileStream = New FileStream("Query.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.[End])
            sw.WriteLine(vbLf & errmessage)
            sw.Flush()
            sw.Close()
        Catch ex As Exception

        End Try

    End Sub
    Public Sub TraceServiceFailedLogs(ByVal errmessage As String)
        Try
            Dim fs As FileStream = New FileStream("FailedEvents\Failed.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.[End])
            sw.WriteLine(vbLf & errmessage)
            sw.Flush()
            sw.Close()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub TraceServiceFailedLogsDump(ByVal errmessage As String)
        Try
            Dim fs As FileStream = New FileStream("FailedEvents\FailedDump.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.[End])
            sw.WriteLine(vbLf & errmessage)
            sw.Flush()
            sw.Close()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub TraceServiceLogText(ByVal errmessage As String)
        Try
            Dim fs As FileStream = New FileStream("AttendanceLog.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.[End])
            sw.WriteLine(vbLf & errmessage.Trim(vbNullChar) & " " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
            sw.Flush()
            sw.Close()
        Catch
        End Try
    End Sub
    Public Sub TraceServiceTextEvents(ByVal errmessage As String)
        Try
            Dim fs As FileStream = New FileStream("Events\String" & Now.ToString("yyyyMMddHH") & ".txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.[End])
            sw.WriteLine(vbLf & errmessage & " " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
            sw.Flush()
            sw.Close()
        Catch ex As Exception

        End Try

    End Sub
    Sub timerSmartInfoFun()
        timerSmartInfo.Enabled = False
        Try
            'If AutoDownloadLogs = "Y" Then
            Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
            Dim sSql As String = "select LastLog, SerialNumber from tblmachine "
            Dim ds As DataSet = New DataSet
            Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                If ds.Tables(0).Rows(i)(0).ToString.Trim <> "" Then
                    Dim startdate As DateTime = Now.ToString("yyyy-MM-dd 00:00:00")
                    sSql = "select TransferToDevice from DeviceCommands where TransferToDevice='" & ds.Tables(0).Rows(i)(1).ToString.Trim & "' and CommandContent='ATTLOG' and Executed=0
                                    IF @@ROWCOUNT=0" &
                                    "insert into DeviceCommands (SerialNumber,CommandContent,TransferToDevice,Executed,IsOnline,CreatedOn,StartDateATTLOG,EndDateATTLOG,Priority) values " &
                            "('" & ds.Tables(0).Rows(i)(1).ToString.Trim & "','ATTLOG','" & ds.Tables(0).Rows(i)(1).ToString.Trim & "','0','1',getdate(),'" & Now.ToString("yyyy-MM-dd 00:00:00") & "','" & Now.ToString("yyyy-MM-dd 23:59:59") & "',3) "
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End If
            Next
            'End If
        Catch ex As Exception

        End Try
        timerSmartInfo.Enabled = True
    End Sub

    Private Function IsPortOpen(ByVal Host As String, ByVal PortNumber As Integer) As Boolean
        Dim Client As TcpClient = Nothing
        Try
            Client = New TcpClient(Host, PortNumber)
            Return True
        Catch ex As SocketException
            Return False
        Finally
            If Not Client Is Nothing Then
                Client.Close()
            End If
        End Try
    End Function

    Private Sub timerRestartFun(sender As Object, e As EventArgs) ' Handles TimerPortCheck.Tick
        Try
            'Dim result As Boolean = IsPortOpen("125.18.139.19", "7660")
            'If result = False Then
            '    Application.Exit()
            '    Thread.Sleep(2000)
            '    Process.Start(Application.ExecutablePath)
            '    Return
            'End If
            'result = IsPortOpen("125.18.139.19", "8899")
            'If result = False Then
            '    Application.Exit()
            '    Thread.Sleep(2000)
            '    Process.Start(Application.ExecutablePath)
            '    Return
            'End If
            'result = IsPortOpen("125.18.139.19", "8898")
            'If result = False Then
            '    Application.Exit()
            '    Thread.Sleep(2000)
            '    Process.Start(Application.ExecutablePath)
            '    Return
            'End If

            Dim result As Boolean
            result = PortInUse(7660) '7660
            If result = False Then
                Dim fs As FileStream = New FileStream("PortCheck.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & "7660 Closed " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
                sw.Flush()
                sw.Close()


                Application.Restart()
                'Application.Exit()
                'Thread.Sleep(2000)
                'Process.Start(Application.ExecutablePath)
                Return
            End If

            result = PortInUse(8899)
            If result = False Then
                Dim fs As FileStream = New FileStream("PortCheck.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & "8899 Closed " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
                sw.Flush()
                sw.Close()


                Application.Restart()
                'Application.Exit()
                'Thread.Sleep(2000)
                'Process.Start(Application.ExecutablePath)
                Return
            End If


            result = PortInUse(8898)
            If result = False Then
                Dim fs As FileStream = New FileStream("PortCheck.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & "8898 Closed " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
                sw.Flush()
                sw.Close()


                Application.Restart()
                'Application.Exit()
                'Thread.Sleep(2000)
                'Process.Start(Application.ExecutablePath)
                Return
            End If

            Try
                Dim con As SqlConnection = New SqlConnection(EHomeForm.ConnectionString)
                Dim sSql As String = "select Top 1 AttDateTime from UserAttendance where deviceid like 'E%' order by AttDateTime desc "
                Dim ds As DataSet = New DataSet
                Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, con)
                adap.Fill(ds)
                Dim logtime As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0)(0).ToString.Trim)
                If logtime < Now.AddMinutes(-20) Then
                    Dim fs As FileStream = New FileStream("PortCheck.txt", FileMode.OpenOrCreate, FileAccess.Write)
                    Dim sw As StreamWriter = New StreamWriter(fs)
                    sw.BaseStream.Seek(0, SeekOrigin.[End])
                    sw.WriteLine(vbLf & "Log time " & logtime.ToString("yyyy-MM-dd HH:mm:ss") & " 20 min gap " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
                    sw.Flush()
                    sw.Close()

                    Application.Restart()
                    'Application.Exit()
                    'Thread.Sleep(2000)
                    'Process.Start(Application.ExecutablePath)
                End If
            Catch ex As Exception
                Dim fs As FileStream = New FileStream("PortCheck.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & ex.Message.ToString & " " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
                sw.Flush()
                sw.Close()
            End Try


        Catch ex As Exception
            Dim fs As FileStream = New FileStream("PortCheck.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.[End])
            sw.WriteLine(vbLf & ex.Message.ToString & " " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
            sw.Flush()
            sw.Close()
        End Try

    End Sub

    Public Shared Function PortInUse(ByVal port As Integer) As Boolean
        Dim inUse As Boolean = False
        Dim ipProperties As IPGlobalProperties = IPGlobalProperties.GetIPGlobalProperties
        Dim ipEndPoints() As IPEndPoint = ipProperties.GetActiveTcpListeners
        For Each endPoint As IPEndPoint In ipEndPoints
            If (endPoint.Port = port) Then
                inUse = True
                Exit For
            End If

        Next
        Return inUse
    End Function

End Class

