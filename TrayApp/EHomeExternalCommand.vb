﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks

'Namespace EHomeDemo
Class EHomeExternalCommand
    Public Const NET_EHOME_GET_DEVICE_INFO As UInteger = 1
    Public Const NET_EHOME_GET_VERSION_INFO As Integer = 2
    Public Const NET_EHOME_GET_DEVICE_CFG As Integer = 3
    Public Const NET_EHOME_SET_DEVICE_CFG As Integer = 4
    Public Const NET_EHOME_GET_NETWORK_CFG As Integer = 5
    Public Const NET_EHOME_SET_NETWORK_CFG As Integer = 6
End Class
'End Namespace