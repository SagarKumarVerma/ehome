﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DeviceTree_Test
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.treeView1 = New System.Windows.Forms.TreeView()
        Me.treeImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.treeEhomeKeyMenuStrip = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.setEhomeKeyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.treeEhomeKeyMenuStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'treeView1
        '
        Me.treeView1.ImageIndex = 0
        Me.treeView1.ImageList = Me.treeImageList
        Me.treeView1.Location = New System.Drawing.Point(12, 12)
        Me.treeView1.Name = "treeView1"
        Me.treeView1.SelectedImageIndex = 0
        Me.treeView1.Size = New System.Drawing.Size(138, 639)
        Me.treeView1.TabIndex = 1
        '
        'treeImageList
        '
        Me.treeImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.treeImageList.ImageSize = New System.Drawing.Size(16, 16)
        Me.treeImageList.TransparentColor = System.Drawing.Color.Transparent
        '
        'treeEhomeKeyMenuStrip
        '
        Me.treeEhomeKeyMenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.setEhomeKeyToolStripMenuItem})
        Me.treeEhomeKeyMenuStrip.Name = "treeEhomeKeyMenuStrip"
        Me.treeEhomeKeyMenuStrip.Size = New System.Drawing.Size(153, 26)
        '
        'setEhomeKeyToolStripMenuItem
        '
        Me.setEhomeKeyToolStripMenuItem.Name = "setEhomeKeyToolStripMenuItem"
        Me.setEhomeKeyToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.setEhomeKeyToolStripMenuItem.Text = "Set Ehome Key"
        '
        'DeviceTree_Test
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(578, 485)
        Me.Controls.Add(Me.treeView1)
        Me.Name = "DeviceTree_Test"
        Me.Text = "DeviceTree"
        Me.treeEhomeKeyMenuStrip.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents treeView1 As TreeView
    Private WithEvents treeImageList As ImageList
    Private WithEvents treeEhomeKeyMenuStrip As ContextMenuStrip
    Private WithEvents setEhomeKeyToolStripMenuItem As ToolStripMenuItem
End Class
