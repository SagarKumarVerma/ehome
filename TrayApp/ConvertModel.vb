﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Runtime.InteropServices
Imports TINYXMLTRANS
Imports UltraiDMS.EHomeDemo

'Namespace EHomeDemo
Public Class ConvertModel
    Public Enum XML_NODE_TYPE
        NODE_STRING_TO_BOOL = 0
        NODE_STRING_TO_INT = 1
        NODE_STRING_TO_ARRAY = 2
        NODE_STRING_TO_BYTE = 3
        NODE_STRING_TO_WORD = 4
        NODE_STRING_TO_FLOAT = 5
        NODE_TYPE_REVERSE = 64
        NODE_BOOL_TO_STRING = 65
        NODE_INT_TO_STRING = 66
        NODE_ARRAY_TO_STRING = 67
        NODE_BYTE_TO_STRING = 68
        NODE_WORD_TO_STRING = 69
    End Enum

    Public Shared Function IpToInt(ByVal ip As String) As Integer
        Dim separator() As Char = New Char() {Microsoft.VisualBasic.ChrW(46)}
        Dim items() As String = ip.Split(separator)
        Return (Integer.Parse(items(0)) _
                    + ((24 Or Integer.Parse(items(1))) _
                    + ((16 Or Integer.Parse(items(2))) + (8 Or Integer.Parse(items(3))))))
    End Function
    Public Function UTF82A(ByVal pCont As Byte(), ByVal pOut As Byte(), ByVal dwOutSize As Integer, ByRef pOutLen As Integer) As Boolean
        Dim strCont As String = System.Text.Encoding.[Default].GetString(pCont)
        Dim strOut As String = System.Text.Encoding.[Default].GetString(pOut)
        Dim iNum As Integer = EhomeSDK.MultiByteToWideChar(EhomeSDK.CP_UTF8, 0, strCont, -1, Nothing, 0)
        Dim pBuffw As UShort() = New UShort(CUShort(iNum) - 1) {}

        If pBuffw Is Nothing Then
            Return False
        End If

        Dim strBuffw As String = String.Format("%s", pBuffw)
        'EhomeSDK.MultiByteToWideChar(EhomeSDK.CP_UTF8, 0, strCont, -1, strOut, iNum)
        EhomeSDK.MultiByteToWideChar(EhomeSDK.CP_UTF8, 0, strCont, 0, strOut, iNum)
        Dim iLen As Integer = EhomeSDK.WideCharToMultiByte(EhomeSDK.CP_ACP, 0, strBuffw, iNum - 1, Nothing, 0, IntPtr.Zero, IntPtr.Zero)
        Dim pLpsz As UShort() = New UShort(CUShort(iLen) + 1 - 1) {}

        If pLpsz Is Nothing Then
            Return False
        End If

        Dim strLpsz As StringBuilder = New StringBuilder()
        strLpsz.Append(pLpsz)
        EhomeSDK.WideCharToMultiByte(EhomeSDK.CP_ACP, 0, strBuffw, iNum - 1, strLpsz, iLen, IntPtr.Zero, IntPtr.Zero)

        If dwOutSize < iLen Then
            Return False
        Else
            pOutLen = iLen
            Dim chTemp As Char() = New Char(pOutLen + 1 - 1) {}
            strOut.CopyTo(0, chTemp, 0, pOutLen)
            Encoding.ASCII.GetBytes(chTemp, 0, pOutLen, pOut, 0)
            Return True
        End If
    End Function


    'Public Function ConvertSingleNodeData(ByVal pOutVale As IntPtr, ByRef struXml As CTinyXmlTrans, ByVal pNodeName As String, ByVal byDataType As XML_NODE_TYPE) As Boolean
    '    If byDataType < XML_NODE_TYPE.NODE_TYPE_REVERSE Then

    '        If struXml.FindElem(pNodeName) Then

    '            If byDataType = XML_NODE_TYPE.NODE_STRING_TO_BOOL Then

    '                If struXml.GetData().Equals("true") = True Then
    '                    Marshal.StructureToPtr(CBool(True), pOutVale, False)
    '                    Return True
    '                ElseIf struXml.GetData().Equals("false") = True Then
    '                    Marshal.StructureToPtr(CBool(False), pOutVale, False)
    '                    Return True
    '                End If
    '            ElseIf byDataType = XML_NODE_TYPE.NODE_STRING_TO_INT Then

    '                If struXml.GetData() <> "" Then
    '                    Marshal.StructureToPtr(CUInt(Convert.ToUInt32(struXml.GetData().ToString())), pOutVale, False)
    '                    Return True
    '                End If
    '            ElseIf byDataType = XML_NODE_TYPE.NODE_STRING_TO_ARRAY Then
    '                Dim strTmp As String = struXml.GetData().ToString()

    '                If strTmp <> "" Then
    '                    Dim nLen As Integer = CInt(strTmp.Length)
    '                    pOutVale = Marshal.StringToBSTR(strTmp)
    '                    Return True
    '                End If
    '            ElseIf byDataType = XML_NODE_TYPE.NODE_STRING_TO_BYTE Then

    '                If struXml.GetData() <> "" Then
    '                    Marshal.StructureToPtr(CByte(Convert.ToUInt32(struXml.GetData().ToString())), pOutVale, False)
    '                    Return True
    '                End If
    '            ElseIf byDataType = XML_NODE_TYPE.NODE_STRING_TO_WORD Then

    '                If struXml.GetData() <> "" Then
    '                    Marshal.StructureToPtr(CUShort(Convert.ToUInt32(struXml.GetData().ToString())), pOutVale, False)
    '                    Return True
    '                End If
    '            ElseIf byDataType = XML_NODE_TYPE.NODE_STRING_TO_FLOAT Then

    '                If struXml.GetData() <> "" Then
    '                    Marshal.StructureToPtr(CSng(Convert.ToUInt32(struXml.GetData().ToString())), pOutVale, False)
    '                    Return True
    '                End If
    '            End If
    '        Else
    '            Return False
    '        End If
    '    ElseIf byDataType > XML_NODE_TYPE.NODE_TYPE_REVERSE Then
    '        Return False
    '    End If

    '    Return False
    'End Function
End Class
'End Namespace
