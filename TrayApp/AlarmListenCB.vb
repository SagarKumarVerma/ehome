﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports UltraiDMS.EHomeDemo

'Namespace EHomeDemo
Public Class AlarmListenCB
    'Private g_formList As DeviceLogList = DeviceLogList.Instance()

    'Test

    Public Sub ProcessAlarmData(ByVal dwAlarmType As UInteger, ByVal pStru As IntPtr, ByVal dwStruLen As UInteger, ByVal pXml As IntPtr, ByVal dwXmlLen As UInteger)
        Select Case dwAlarmType
            Case HCEHomeAlarm.EHOME_ALARM
                ProcessEhomeAlarm(pStru, dwStruLen, pXml, dwXmlLen)
            Case HCEHomeAlarm.EHOME_ALARM_HEATMAP_REPORT
            Case HCEHomeAlarm.EHOME_ALARM_FACESNAP_REPORT
            Case HCEHomeAlarm.EHOME_ALARM_GPS
                ProcessEhomeGps(pStru, dwStruLen, pXml, dwXmlLen)
            Case HCEHomeAlarm.EHOME_ALARM_CID_REPORT
                ProcessEhomeCid(pStru, dwStruLen, pXml, dwXmlLen)
            Case HCEHomeAlarm.EHOME_ALARM_NOTICE_PICURL
            Case HCEHomeAlarm.EHOME_ALARM_NOTIFY_FAIL
            Case Else
                'g_formList.AddLog(-1, HCEHomePublic.ALARM_INFO_T, 0, "[Unknown_Alarm]")
        End Select
    End Sub

    Private Sub ProcessEhomeCid(ByVal pStru As IntPtr, ByVal dwStruLen As UInteger, ByVal pXml As IntPtr, ByVal dwXmlLen As UInteger)
        Dim struCIDInfo As HCEHomeAlarm.NET_EHOME_CID_INFO = New HCEHomeAlarm.NET_EHOME_CID_INFO
        struCIDInfo = CType(Marshal.PtrToStructure(pStru, GetType(HCEHomeAlarm.NET_EHOME_CID_INFO)), HCEHomeAlarm.NET_EHOME_CID_INFO)
        Dim str As String
        str = String.Format("[CID]DeviceID:{0},Code:{1},Type:{2},SubSysNo:{3},Describe:{4},byTriggerTime:{5}," & vbLf & "            UploadTi" & _
            "me:{6},UserType:{7},ZoneNo:{8},KeyboardNo:{9},VideoChanNo:{10},DiskNo:{11}," & vbLf & "            ModuleAddr:{" & _
            "12},UserName:{13}", struCIDInfo.byDeviceID, struCIDInfo.dwCIDCode, struCIDInfo.dwCIDType, struCIDInfo.dwSubSysNo, struCIDInfo.byCIDDescribe, struCIDInfo.byTriggerTime, struCIDInfo.byUploadTime, struCIDInfo.struCIDParam.dwUserType, struCIDInfo.struCIDParam.lZoneNo, struCIDInfo.struCIDParam.lKeyboardNo, struCIDInfo.struCIDParam.lVideoChanNo, struCIDInfo.struCIDParam.lDiskNo, struCIDInfo.struCIDParam.lModuleAddr, struCIDInfo.struCIDParam.byUserName)
        'g_formList.AddLog(-1, HCEHomePublic.ALARM_INFO_T, 0, str)
    End Sub

    Public Sub ProcessEhomeAlarm(ByVal pStru As IntPtr, ByVal dwStruLen As UInteger, ByVal pXml As IntPtr, ByVal dwXmlLen As UInteger)
        Dim struAlarmInfo As HCEHomeAlarm.NET_EHOME_ALARM_INFO = New HCEHomeAlarm.NET_EHOME_ALARM_INFO()
        Dim str As String
        str = String.Format("[ALARM]DeviceID:{0},Time:{1},Type:{2},Action:{3},Channel:{4},AlarmIn:{5},DiskNo:{6}", struAlarmInfo.szDeviceID, struAlarmInfo.szAlarmTime, struAlarmInfo.dwAlarmType, struAlarmInfo.dwAlarmAction, struAlarmInfo.dwVideoChannel, struAlarmInfo.dwAlarmInChannel, struAlarmInfo.dwDiskNumber)
        'g_formList.AddLog(-1, HCEHomePublic.ALARM_INFO_T, 0, str)
    End Sub

    Public Sub ProcessEhomeGps(ByVal pStru As IntPtr, ByVal dwStruLen As UInteger, ByVal pXml As IntPtr, ByVal dwXmlLen As UInteger)
        Dim struGps As HCEHomeAlarm.NET_EHOME_GPS_INFO
        struGps = CType(Marshal.PtrToStructure(pStru, GetType(HCEHomeAlarm.NET_EHOME_GPS_INFO)), HCEHomeAlarm.NET_EHOME_GPS_INFO)
        Dim str As String
        str = String.Format("[GPS]DeviceID:{0},SampleTime:{1},Division:[{2} {3}],Satelites:{4},Precision:{5},Longitude:{6},Latitude:{7},Direction:{8},Speed:{9},Height:{10}", struGps.byDeviceID, struGps.bySampleTime, struGps.byDivision(0), struGps.byDivision(1), struGps.bySatelites, struGps.byPrecision, struGps.dwLongitude, struGps.dwLatitude, struGps.dwDirection, struGps.dwSpeed, struGps.dwHeight)
        'g_formList.AddLog(-1, HCEHomePublic.ALARM_INFO_T, 0, str)
    End Sub
End Class
'End Namespace
