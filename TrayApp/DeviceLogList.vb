﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Windows.Forms
Imports System.Threading.Tasks
Imports System.Runtime.InteropServices
'Imports EHomeDemo.Language
Imports System.IO
Imports System.Xml
Imports System.Data.SqlClient
'Imports Oracle.DataAccess.Client

Namespace EHomeDemo
    Partial Public Class DeviceLogList
        Inherits UserControl

        Private locker As Object = New Object()
        Private m_struLogInfo As HCEHomePublic.LOCAL_LOG_INFO = New HCEHomePublic.LOCAL_LOG_INFO()
        Private Shared g_LogPanel As DeviceLogList = New DeviceLogList()
        Private m_bEnlarged As Boolean = False
        Private m_logPanelPos As Point = New Point()
        Private m_logPanelSize As Size = New Size()
        Private m_iIndex As Integer = 1
        Public AlarmPic As New List(Of String)()
        Public SS_PATH As String = System.Environment.CurrentDirectory & "\StorageServer\Storage\" '"C:\StorageServer\Storage\"
        Public Sub New()
            'InitializeComponent()
            'listViewAllLog.Visible = True
            'listViewAlarmInfo.Visible = False
        End Sub

        Public Shared Function Instance() As DeviceLogList
            Return g_LogPanel
        End Function

        Protected Overrides Sub DefWndProc(ByRef m As Message)
            'if (m.Msg == EHomeDemo.WM_LISTENED_ALARM && (m.LParam == m.WParam))
            '{
            '    ProcessAlarmData(m.LParam);
            '}
            'Newly Modify Content
            If m.Msg = EHomeForm.WM_LISTENED_ALARM Then
                ProcessAlarmData(m.LParam, m.WParam)
            End If
            MyBase.DefWndProc(m)
        End Sub
        Public Structure AlarmParam
            Public dwAlarmType As UInteger
            Public pStru As IntPtr
            Public dwStruLen As UInteger
            Public pXml As IntPtr
            Public dwXmlLen As UInteger
        End Structure
        Public Sub ProcessAlarmData(ByVal ptrAlarmParam As IntPtr, ByVal ptrFilterAlarmParam As IntPtr)
            Try


                Dim struAlarmParam As New HCEHomeAlarm.NET_EHOME_ALARM_MSG()
                struAlarmParam.Init()
                struAlarmParam = DirectCast(Marshal.PtrToStructure(ptrAlarmParam, GetType(HCEHomeAlarm.NET_EHOME_ALARM_MSG)), HCEHomeAlarm.NET_EHOME_ALARM_MSG)
                Dim FilterAlarmMsg As String = Marshal.PtrToStringAnsi(ptrFilterAlarmParam)

                Select Case struAlarmParam.dwAlarmType
                    Case HCEHomeAlarm.EHOME_ALARM
                    'ProcessEhomeAlarm(struAlarmParam.pAlarmInfo, struAlarmParam.dwAlarmInfoLen, struAlarmParam.pXmlBuf, struAlarmParam.dwXmlBufLen)
                    Case HCEHomeAlarm.EHOME_ALARM_HEATMAP_REPORT
                    Case HCEHomeAlarm.EHOME_ALARM_FACESNAP_REPORT
                        Exit Select
                    Case HCEHomeAlarm.EHOME_ALARM_GPS
                    'ProcessEhomeGps(struAlarmParam.pAlarmInfo, struAlarmParam.dwAlarmInfoLen, struAlarmParam.pXmlBuf, struAlarmParam.dwXmlBufLen)
                    Case HCEHomeAlarm.EHOME_ALARM_CID_REPORT
                    'ProcessEhomeCid(struAlarmParam.pAlarmInfo, struAlarmParam.dwAlarmInfoLen, struAlarmParam.pXmlBuf, struAlarmParam.dwXmlBufLen)
                    Case HCEHomeAlarm.EHOME_ALARM_NOTICE_PICURL
                    Case HCEHomeAlarm.EHOME_ALARM_NOTIFY_FAIL
                    Case HCEHomeAlarm.EHOME_ALARM_ACS
                        ShowEhomeAlarmACS(struAlarmParam.pXmlBuf, struAlarmParam.dwXmlBufLen, FilterAlarmMsg)
                    Case HCEHomeAlarm.EHOME_ALARM_FACETEMP
                        ShowFaceTempAlarm(struAlarmParam.pXmlBuf, struAlarmParam.dwXmlBufLen, FilterAlarmMsg)
                    Case Else
                        'AddLog(-1, HCEHomePublic.ALARM_INFO_T, 0, "[Unknown_Alarm]")
                End Select
            Catch ex As Exception

            End Try
        End Sub
        Public Sub ShowFaceTempAlarm(ByVal pXml As IntPtr, ByVal dwXmlLen As UInteger, ByRef FilterAlarmMsg As String)
            Try


                'MsgBox("ShowFaceTempAlarm")
                Dim str As String
                Dim strTemp As String = Marshal.PtrToStringAnsi(pXml, CInt(dwXmlLen))
                'TraceServiceText(strTemp)

                CMSAlarm.ShowISAPIAlarmInfo(FilterAlarmMsg)
                str = String.Format("[ISAPI ALARM] save to[c:\ISAPI\]")
                Try
                    Dim directoryPath As String = "C:\ISAPI"
                    Dim filePath As String = m_iIndex.ToString() & ".json"
                    If Not Directory.Exists(directoryPath) Then
                        Directory.CreateDirectory(directoryPath)
                    End If
                    Dim sw As New StreamWriter(Path.Combine(directoryPath, filePath))
                    sw.WriteLine(strTemp)
                    sw.Flush()
                    sw.Close()
                    m_iIndex += 1
                Catch e As IOException
                    str = String.Format("[ISAPI ALARM] save file failed![%s]", e.ToString())
                End Try
                Dim TempStr As String = "PicDataUrl:"
                Dim bArr As Byte() 'nitin
                'If FilterAlarmMsg.Contains(TempStr) AndAlso Not FilterAlarmMsg.Contains("Failed-to") Then
                If (strTemp.Contains(TempStr) Or strTemp.Contains("employeeNoString") Or strTemp.Contains("currTemperature")) And Not strTemp.Contains("Failed-to") Then
                    Dim PicIndex As Integer = FilterAlarmMsg.LastIndexOf("PicDataUrl:")
                    Dim PicUrl As String = FilterAlarmMsg.Substring(PicIndex + TempStr.Length)

                    If PicUrl.Contains("/pic?") Then
                        PicUrl = PicUrl.Substring(PicUrl.LastIndexOf("/pic?") + 5, PicUrl.Length - PicUrl.LastIndexOf("/pic?") - 5)
                        AlarmPic.Add(SS_PATH & PicUrl)
                        CMSAlarm.ReadThemalImageFromLocal(AlarmPic(0))
                        AlarmPic.RemoveRange(0, 1)

                        Dim img As Image = Image.FromFile(AlarmPic(0)) 'nitin
                        bArr = imgToByteArray(img) 'nitin

                    ElseIf PicUrl.Contains("/kms") Then
                        PicUrl = PicUrl.Substring(PicUrl.LastIndexOf("id=") + 3, PicUrl.Length - PicUrl.LastIndexOf("id=") - 3)
                        'AlarmPic.Add(SS_PATH + PicUrl);
                        AlarmPic.Add(SS_PATH & PicUrl)
                        CMSAlarm.ReadThemalImageFromLocal(AlarmPic(0))
                        AlarmPic.RemoveRange(0, 1)


                        Dim img As Image = Image.FromFile(AlarmPic(0)) 'nitin
                        bArr = imgToByteArray(img) 'nitin
                    End If

                    Try
                        'MsgBox(strTemp)
                        Dim CardNo As String = ""
                        Dim PuchTime As DateTime = Convert.ToDateTime("1900-01-01 00:00:00")
                        Dim DeviceID As String = ""
                        Dim CardType As String = ""
                        Dim verifymode As String = ""
                        Dim xml As New XmlDocument()
                        xml.LoadXml(strTemp) ' suppose that myXmlString contains "<Names>...</Names>"

                        Dim XCommandType As String = ""
                        Dim xnListCtype As XmlNodeList = xml.SelectNodes("/PPVSPMessage/CommandType")
                        For Each xn As XmlNode In xnListCtype
                            XCommandType = xn.InnerText
                        Next xn
                        Dim xnListD As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/DeviceID")
                        For Each xn As XmlNode In xnListD
                            DeviceID = xn.InnerText
                        Next xn

                        'for online registration request
                        If XCommandType = "REQUEST" Then



                        End If
                        'end online registration request

                        Dim xnList As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/employeeNoString")
                        For Each xn As XmlNode In xnList
                            CardNo = xn.InnerText
                        Next xn

                        Dim xnListT As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/Time")
                        For Each xn As XmlNode In xnListT
                            PuchTime = Convert.ToDateTime(xn.InnerText)
                        Next xn

                        Dim xnListCT As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/CardType")
                        For Each xn As XmlNode In xnListCT
                            CardType = xn.InnerText
                        Next xn
                        Dim xnListVM As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/currentVerifyMode")
                        For Each xn As XmlNode In xnListCT
                            verifymode = xn.InnerText
                        Next xn

                        Dim fCurrTemperature As Double
                        Dim thermometryUnit As String = ""
                        Dim Ftemp As String = ""
                        Dim Ctemp As String = ""
                        Dim MaskWear As String = ""
                        Dim TeperatureStatus As String = ""
                        Dim xnListTU As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/thermometryUnit")
                        For Each xn As XmlNode In xnListCT
                            thermometryUnit = xn.InnerText
                        Next xn
                        Dim xnListTempe As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/currTemperature")
                        For Each xn As XmlNode In xnListCT
                            fCurrTemperature = xn.InnerText
                        Next xn

                        If thermometryUnit.Trim = "celsius" Then
                            Ctemp = fCurrTemperature
                            Ftemp = Math.Round((fCurrTemperature * 9 / 5) + 32, 2)
                        Else
                            Ftemp = fCurrTemperature
                            Ctemp = Math.Round((fCurrTemperature - 32) * 5 / 9, 2)
                        End If

                        Dim xnListmask As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/mask")
                        For Each xn As XmlNode In xnListCT
                            MaskWear = xn.InnerText
                        Next xn
                        Dim xnListAbNormal As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/isAbnomalTemperature")
                        For Each xn As XmlNode In xnListCT
                            TeperatureStatus = xn.InnerText
                        Next xn


                        If CardNo <> "" And CardType = "1" Then
                            Dim con As New SqlConnection(EHomeForm.ConnectionString)
                            Dim ds As New DataSet()
                            CardNo = GetCardNumber(CardNo)

                            Dim cmd As SqlCommand
                            Dim sSql As String = ""
                            Try
                                'sSql = "select Userid from UserAttendance  with (nolock) where DeviceID='" & DeviceID & "' and UserID='" & CardNo & "' and AttDateTime='" & PuchTime.ToString("yyyy-MM-dd HH:mm:ss") & "' " &
                                '" IF @@ROWCOUNT=0 " &
                                '"insert into UserAttendance(DeviceID,UserID,VerifyMode,AttDateTime) " &
                                '"values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTime.ToString("yyyy-MM-dd HH:mm:ss") & "')"

                                Try
                                    sSql = "select Userid from UserAttendance  with (nolock) where DeviceID='" & DeviceID & "' and UserID='" & CardNo & "' and AttDateTime='" & PuchTime.ToString("yyyy-MM-dd HH:mm:ss") & "' " &
                        " IF @@ROWCOUNT=0 " &
                        "insert into UserAttendance(DeviceID,UserID,VerifyMode,AttDateTime, CTemp, FTemp,[MaskStatus],[IsAbnomal],[LImage]) " &
                        "values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTime.ToString("yyyy-MM-dd HH:mm:ss") & "','" & Ctemp & "', '" & Ftemp & "','" & MaskWear & "','" & TeperatureStatus & "',@LImage)"

                                    If bArr Is Nothing Then
                                        sSql = sSql.Replace(",[LImage]", "").Replace(",@LImage", "")
                                    End If
                                    'If TImage Is Nothing Then
                                    '    sSql = sSql.Replace(",[TImage]", "").Replace(",@TImage", "")
                                    'End If
                                    If con.State <> ConnectionState.Open Then
                                        con.Open()
                                    End If
                                    cmd = New SqlCommand(sSql, con)
                                    If bArr IsNot Nothing Then
                                        'cmd.Parameters.AddWithValue("@LImage", LImage)
                                        Dim sqlParamLogImg As SqlParameter = New SqlParameter("@LImage", SqlDbType.VarBinary)
                                        sqlParamLogImg.Direction = ParameterDirection.Input
                                        sqlParamLogImg.Size = bArr.Length
                                        sqlParamLogImg.Value = bArr
                                        cmd.Parameters.Add(sqlParamLogImg)
                                    End If

                                    'If TImage IsNot Nothing Then
                                    '    'cmd.Parameters.AddWithValue("@TImage", TImage)
                                    '    Dim sqlParamLogImg As SqlParameter = New SqlParameter("@TImage", SqlDbType.VarBinary)
                                    '    sqlParamLogImg.Direction = ParameterDirection.Input
                                    '    sqlParamLogImg.Size = TImage.Length
                                    '    sqlParamLogImg.Value = TImage
                                    '    cmd.Parameters.Add(sqlParamLogImg)
                                    'End If
                                    cmd.ExecuteNonQuery()
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If
                                Catch ex As Exception
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If

                                    'EHomeForm.TraceService(ex.Message, "DeviceLogList Line 221")
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If
                                End Try


                            Catch ex As Exception
                                EHomeForm.TraceService(ex.Message, "DeviceLogList Line 221")
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End Try
#Region "machinerawpunchandprocess"
                            Try
                                Dim adpA As New SqlDataAdapter
                                ds = New DataSet
                                sSql = "select TM.id_No,TE.companycode,TE.PRESENTCARDNO,TE.SSN,TE.paycode,TE.DepartmentCode from tblMachine TM  with (nolock) inner join tblemployee TE on Tm.CompanyCode = TE.COMPANYCODE where TM.SerialNumber = '" & DeviceID & "' and TE.PresentCardNo = '" & CardNo & "'"
                                adpA = New SqlDataAdapter(sSql, con)
                                adpA.Fill(ds)
                                If ds.Tables(0).Rows.Count > 0 Then
                                    Dim inout As String = ""
                                    'If (status = "0") Then
                                    '    inout = "I"
                                    'Else
                                    '    inout = "O"
                                    'End If
                                    Try
                                        sSql = "insert into MachineRawPunch (CardNo,OfficePunch,P_Day,IsManual,MC_NO,INOUT,PAYCODE,SSN,CompanyCode)values('" & ds.Tables(0).Rows(0)("PresentCardNo").ToString().Trim() & "','" & PuchTime.ToString("yyyy-MM-dd HH:mm:ss") & "','N','N'," & ds.Tables(0).Rows(0)("id_No").ToString().Trim() & ",'" & inout & "','" & ds.Tables(0).Rows(0)("PayCode").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("SSN").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "')"
                                        If con.State <> ConnectionState.Open Then
                                            con.Open()
                                        End If
                                        cmd = New SqlCommand(sSql, con)
                                        cmd.ExecuteNonQuery()

                                        If con.State <> ConnectionState.Closed Then
                                            con.Close()
                                        End If
                                    Catch ex As Exception
                                        EHomeForm.TraceService(ex.Message, "DeviceLogList Line 252")
                                        If con.State <> ConnectionState.Closed Then
                                            con.Close()
                                        End If
                                    End Try

                                    'temp comment
                                    sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup where companycode='" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "') "
                                    Dim ds1 As DataSet = New DataSet
                                    Try
                                        adpA = New SqlDataAdapter(sSql, con)
                                        adpA.Fill(ds1)
                                    Catch ex As Exception
                                        EHomeForm.TraceService(ex.Message, "DeviceLogList Line 1563")
                                        If con.State <> ConnectionState.Closed Then
                                            con.Close()
                                        End If
                                    End Try

                                    Dim SetupID As String = ""
                                    If ds1.Tables(0).Rows.Count > 0 Then
                                        SetupID = ds1.Tables(0).Rows(0)(0).ToString.Trim
                                        Try
                                            If (con.State <> ConnectionState.Open) Then
                                                con.Open()
                                            End If
                                            'Dim tmpdatetime As DateTime = Convert.ToDateTime(atttime.ToString.Trim)
                                            cmd = New SqlCommand("ProcessBackDate", con)
                                            cmd.CommandTimeout = 1800
                                            cmd.CommandType = CommandType.StoredProcedure
                                            cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                                            cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                                            cmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("CompanyCode").ToString.Trim
                                            cmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("DepartmentCode").ToString.Trim
                                            cmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("PayCode").ToString.Trim
                                            cmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID
                                            cmd.ExecuteNonQuery()
                                            If (con.State <> ConnectionState.Closed) Then
                                                con.Close()
                                            End If
                                        Catch ex As Exception
                                            EHomeForm.TraceService(ex.Message, "DeviceLogList Line 293")
                                            If con.State <> ConnectionState.Closed Then
                                                con.Close()
                                            End If
                                        End Try
                                    End If
                                    'end temp comment
                                End If
                            Catch ex As Exception
                                EHomeForm.TraceService(ex.Message, "DeviceLogList Line 302")
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End Try
#End Region
                        End If
                    Catch e As IOException
                        str = String.Format("[ACS ALARM] save file failed![%s]", e.ToString())
                        EHomeForm.TraceService(e.Message, "DeviceLogList Line 322")
                    End Try

                End If
                'AddLog(0, HCEHomePublic.ALARM_INFO_T, 0, str)
                'AddLog(0, HCEHomePublic.ALARM_INFO_T, 0, FilterAlarmMsg)
            Catch ex As Exception

            End Try
        End Sub
        Public Sub ShowEhomeAlarmACS(ByVal pXml As IntPtr, ByVal dwXmlLen As UInteger, ByRef FilterAlarmMsg As String)


            Try
                Dim str As String
                Dim strTemp As String = Marshal.PtrToStringAnsi(pXml, CInt(dwXmlLen))
                If FilterAlarmMsg.Contains("currTemperature") Then
                    CMSAlarm.ShowISAPIAlarmInfo(FilterAlarmMsg)
                Else
                    CMSAlarm.ShowCmsAlarmInfo(FilterAlarmMsg)
                End If
                TraceServiceTextEvents(strTemp)
                'str = String.Format("[ACS ALARM] save to[c:\ACS\]")
                'Try
                '    Dim directoryPath As String = "C:\ACS"
                '    Dim filePath As String = m_iIndex.ToString() & ".xml"
                '    If Not Directory.Exists(directoryPath) Then
                '        Directory.CreateDirectory(directoryPath)
                '    End If
                '    Dim sw As New StreamWriter(Path.Combine(directoryPath, filePath))
                '    sw.WriteLine(strTemp)
                '    sw.Flush()
                '    sw.Close()
                '    m_iIndex += 1
                'Catch e As IOException
                '    str = String.Format("[ACS ALARM] save file failed![%s]", e.ToString())
                'End Try
                Dim TempStr As String = "PicDataUrl"
                Dim bArr As Byte() = Nothing 'nitin
                Dim CardNo As String = ""
                Dim PuchTime As DateTime = Convert.ToDateTime("1900-01-01 00:00:00")
                Dim PuchTimeStr As String = ""
                Dim DeviceID As String = ""
                Dim CardType As String = ""
                Dim verifymode As String = ""
                Dim name As String = ""
                Dim xml As New XmlDocument()
                Dim ImageTodelete As String = ""
                Dim ImgPunchPath As String = ""

                xml.LoadXml(strTemp) ' suppose that myXmlString contains "<Names>...</Names>"
                If (strTemp.Contains(TempStr) Or strTemp.Contains("employeeNoString") Or strTemp.Contains("currTemperature")) Then 'And Not strTemp.Contains("Failed-to")
                    Try

                        Dim XserialNo As String = ""
                        Dim xnListserialNo As XmlNodeList = xml.SelectNodes("/PPVSPMessage/serialNo")
                        For Each xn As XmlNode In xnListserialNo
                            XserialNo = xn.InnerText
                        Next xn

                        Dim xnListT As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/Time")
                        For Each xn As XmlNode In xnListT
                            PuchTime = Convert.ToDateTime(xn.InnerText)
                        Next xn
                        Dim xnListD As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/DeviceID")
                        For Each xn As XmlNode In xnListD
                            DeviceID = xn.InnerText
                        Next xn
                        Try
                            If Not EHomeForm.ActiveSerialNumber.Contains(DeviceID) Then
                                Return
                            End If
                        Catch ex As Exception
                            Return
                        End Try
                        'InsertEventLog(XserialNo, DeviceID, PuchTime)



                        'Dim PicIndex As Integer = FilterAlarmMsg.LastIndexOf("PicDataUrl")
                        Dim PicUrl As String = "" ' FilterAlarmMsg.Substring(PicIndex + TempStr.Length)
                        Dim xnListPicUrl As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/PicDataUrl")
                        For Each xn As XmlNode In xnListPicUrl
                            PicUrl = xn.InnerText
                        Next xn
                        'Thread.Sleep(100)
                        'If EHomeForm.AllowPhoto = "Y" Then
#Region "ImageCheck"
                        If PicUrl.Trim <> "" Then
                            If PicUrl.Contains("/pic?") Then
                                Try
                                    PicUrl = PicUrl.Substring(PicUrl.LastIndexOf("/pic?") + 5, PicUrl.Length - PicUrl.LastIndexOf("/pic?") - 5)
                                    Dim img As System.Drawing.Image
                                    ImgPunchPath = SS_PATH & PicUrl
                                    If System.IO.File.Exists(ImgPunchPath) Then
                                        img = System.Drawing.Image.FromFile(ImgPunchPath)
                                    Else
                                        ImgPunchPath = ImgPunchPath & "202"
                                        img = System.Drawing.Image.FromFile(ImgPunchPath)
                                    End If
                                    bArr = imgToByteArray(img) 'nitin
                                    img.Dispose()
                                    ImageTodelete = ImgPunchPath
                                Catch ex As Exception

                                End Try
                            ElseIf PicUrl.Contains("/kms") Then
                                Try
                                    PicUrl = PicUrl.Substring(PicUrl.LastIndexOf("id=") + 3, PicUrl.Length - PicUrl.LastIndexOf("id=") - 3)
                                    Dim img As Image
                                    ImgPunchPath = SS_PATH & PicUrl
                                    If System.IO.File.Exists(ImgPunchPath) Then
                                        img = Image.FromFile(ImgPunchPath)
                                    Else
                                        ImgPunchPath = ImgPunchPath & "202"
                                        img = Image.FromFile(ImgPunchPath)
                                    End If
                                    bArr = imgToByteArray(img) 'nitin
                                    img.Dispose()
                                    ImageTodelete = ImgPunchPath

                                Catch ex As Exception

                                End Try
                            End If
                        End If
#End Region
                        'End If

                        'MsgBox(strTemp)

                        Dim XCommandType As String = ""
                        Dim xnListCtype As XmlNodeList = xml.SelectNodes("/PPVSPMessage/CommandType")
                        For Each xn As XmlNode In xnListCtype
                            XCommandType = xn.InnerText
                        Next xn

                        Dim xnList As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/employeeNoString")
                        For Each xn As XmlNode In xnList
                            CardNo = xn.InnerText
                        Next xn
                        If CardNo = "" Then
                            CardNo = "Visitor"
                            If EHomeForm.AlowVisitor = "N" Then
                                Return
                            End If
                        End If
                        If CardNo = "Visitor" Then
                            PuchTimeStr = PuchTime.ToString("yyyy-MM-dd HH:mm:ss")
                        Else
                            PuchTimeStr = PuchTime.ToString("yyyy-MM-dd HH:mm:00")
                            'PuchTimeStr = PuchTime.ToString("yyyy-MM-dd HH:mm:ss")  'smart one of the customer
                        End If
                        'MsgBox("CardNo " & CardNo)
                        Try
                            Dim xnListName As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/name")
                            For Each xn As XmlNode In xnListName
                                name = xn.InnerText
                            Next xn
                        Catch ex As Exception
                            name = ""
                        End Try


                        Dim xnListCT As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/CardType")
                        For Each xn As XmlNode In xnListCT
                            CardType = xn.InnerText
                        Next xn
                        'MsgBox("CardType " & CardType)

                        Dim xnListVM As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/currentVerifyMode")
                        For Each xn As XmlNode In xnListVM
                            verifymode = xn.InnerText
                        Next xn
                        'MsgBox("verifymode " & verifymode)

                        Dim fCurrTemperature As Double
                        Dim thermometryUnit As String = ""
                        Dim Ftemp As String = ""
                        Dim Ctemp As String = ""
                        Dim MaskWear As String = ""
                        Dim TeperatureStatus As String = ""
                        Dim xnListTU As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/thermometryUnit")
                        For Each xn As XmlNode In xnListTU
                            thermometryUnit = xn.InnerText
                        Next xn
                        Dim xnListTempe As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/currTemperature")
                        For Each xn As XmlNode In xnListTempe
                            fCurrTemperature = xn.InnerText
                        Next xn

                        If thermometryUnit.Trim = "celsius" Then
                            Ctemp = fCurrTemperature
                            Ftemp = Math.Round((fCurrTemperature * 9 / 5) + 32, 2)
                        ElseIf thermometryUnit.Trim = "fahrenheit" Then
                            Ftemp = fCurrTemperature
                            Ctemp = Math.Round((fCurrTemperature - 32) * 5 / 9, 2)
                        Else
                            Ftemp = ""
                            Ctemp = ""
                        End If

                        Dim xnListmask As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/mask")
                        For Each xn As XmlNode In xnListmask
                            MaskWear = xn.InnerText
                        Next xn
                        Dim xnListAbNormal As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/isAbnomalTemperature")
                        For Each xn As XmlNode In xnListAbNormal
                            TeperatureStatus = xn.InnerText
                        Next xn

                        Dim io_mode As String = ""
                        If CardNo <> "" And CardType = "1" Then
                            Dim SaveImage As String = "Y"
                            Dim con As New SqlConnection(EHomeForm.ConnectionString)

                            Dim dsM As DataSet = New DataSet
                            Dim sSqlM As String = "select IN_OUT, SaveImage from tblmachine where SerialNumber='" & DeviceID & "'"
                            Dim adpM As SqlDataAdapter = New SqlDataAdapter(sSqlM, con)
                            adpM.Fill(dsM)

                            If dsM.Tables(0).Rows.Count > 0 Then
                                SaveImage = dsM.Tables(0).Rows(0)("SaveImage").ToString.Trim
                                If CardNo <> "Visitor" Then
                                    If dsM.Tables(0).Rows(0)(0).ToString.Trim = "0" Then
                                        'io_mode = "I"
                                        io_mode = "0"
                                    ElseIf dsM.Tables(0).Rows(0)(0).ToString.Trim = "1" Then
                                        'io_mode = "O"
                                        io_mode = "1"
                                    ElseIf dsM.Tables(0).Rows(0)(0).ToString.Trim = "2" Or dsM.Tables(0).Rows(0)(0).ToString.Trim = "A" Then
                                        Try
                                            Dim xnListIN_OUT As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/attendanceStatus")
                                            For Each xn As XmlNode In xnListIN_OUT
                                                io_mode = xn.InnerText
                                            Next xn
                                            If io_mode.ToLower = "checkin" Then
                                                io_mode = "0"
                                            ElseIf io_mode.ToLower = "checkout" Then
                                                io_mode = "1"
                                            End If
                                        Catch ex As Exception
                                            io_mode = ""
                                        End Try
                                        ''LnT
                                        'If io_mode = "" Or io_mode.ToLower = "undefined" Then
                                        '    Dim ds2 As DataSet = New DataSet
                                        '    Dim sSql1 = "select count (UserID) from UserAttendance where UserID = '" & CardNo & "' and convert(varchar, AttDateTime, 23) = '" & PuchTime.ToString("yyyy-MM-dd") & "'"
                                        '    Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql1, con)
                                        '    adap.Fill(ds2)
                                        '    If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                                        '        io_mode = "0"
                                        '    Else
                                        '        If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                                        '            io_mode = "0"
                                        '        Else
                                        '            io_mode = "1"
                                        '        End If
                                        '    End If
                                        'End If
                                        ''end LnT
                                    End If
                                End If
                            End If

                            Dim ds As New DataSet()
                            Dim cmd As SqlCommand
                            Dim sSql As String = ""
                            Dim pIsVU As Boolean = IsValidUserAtt(CardNo, PuchTime, con, EHomeForm.DuplicateCheck, DeviceID)
                            If pIsVU Then    'Sapphire
                                'If CardNo <> "Visitor" Then    'LNT
                                Try

#Region "Smartinfocom"
                                    '    Try
                                    '        sSql = "insert into DumpUserAttendance(DeviceID,UserID,VerifyMode,AttDateTime, CTemp, FTemp,[MaskStatus],[IsAbnomal], UpdatedOn,[io_mode],[AttState]) " &
                                    '"values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTimeStr & "','" & Ctemp & "', '" & Ftemp & "','" & MaskWear & "','" & TeperatureStatus & "',getdate(),'" & io_mode & "','" & io_mode & "')"
                                    '        If con.State <> ConnectionState.Open Then
                                    '            con.Open()
                                    '        End If
                                    '        cmd = New SqlCommand(sSql, con)
                                    '        cmd.ExecuteNonQuery()
                                    '        If con.State <> ConnectionState.Closed Then
                                    '            con.Close()
                                    '        End If
                                    '    Catch ex As Exception
                                    '        If Not ex.Message.ToString.Contains("Violation of PRIMARY KEY constraint") Then
                                    '            'save failed logs text                               
                                    '            Dim failedTxt As String = DeviceID & "," & CardNo & "," & verifymode & "," & PuchTimeStr & "," & Ctemp & ", " & Ftemp & "," & MaskWear & "," & TeperatureStatus & "," & io_mode & "," & ex.Message.ToString & ",RealTime"
                                    '            TraceServiceFailedLogsDump(failedTxt)
                                    '        End If
                                    '    End Try
#End Region

                                    Try
                                        sSql = "select Userid from UserAttendance  with (nolock) where DeviceID='" & DeviceID & "' and UserID='" & CardNo & "' and AttDateTime='" & PuchTimeStr & "' " &
                                " IF @@ROWCOUNT=0 " &
                                "insert into UserAttendance(DeviceID,UserID,VerifyMode,AttDateTime, CTemp, FTemp,[MaskStatus],[IsAbnomal],[LImage], UpdateedOn,[io_mode],[AttState]) " &
                                "values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTimeStr & "','" & Ctemp & "', '" & Ftemp & "','" & MaskWear & "','" & TeperatureStatus & "',@LImage,Getdate(),'" & io_mode & "','" & io_mode & "')"

                                        '        sSql = "insert into UserAttendance(DeviceID,UserID,VerifyMode,AttDateTime, CTemp, FTemp,[MaskStatus],[IsAbnomal],[LImage], UpdateedOn,[io_mode],[AttState]) " &
                                        '"values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTimeStr & "','" & Ctemp & "', '" & Ftemp & "','" & MaskWear & "','" & TeperatureStatus & "',@LImage,Getdate(),'" & io_mode & "','" & io_mode & "')"

                                        'TraceServiceText(sSql)
                                        'Query(sSql)
                                        'MsgBox("sql: " & sSql)


                                        If SaveImage <> "Y" Then
                                            'sSql = sSql.Replace(",[LImage]", "").Replace(",@LImage", "")
                                            bArr = Nothing
                                        End If


                                        If bArr Is Nothing Then
                                            sSql = sSql.Replace(",[LImage]", "").Replace(",@LImage", "")
                                        End If

                                        'If TImage Is Nothing Then
                                        '    sSql = sSql.Replace(",[TImage]", "").Replace(",@TImage", "")
                                        'End If
                                        If con.State <> ConnectionState.Open Then
                                            con.Open()
                                        End If
                                        cmd = New SqlCommand(sSql, con)
                                        If bArr IsNot Nothing Then
                                            'cmd.Parameters.AddWithValue("@LImage", LImage)
                                            Dim sqlParamLogImg As SqlParameter = New SqlParameter("@LImage", SqlDbType.VarBinary)
                                            sqlParamLogImg.Direction = ParameterDirection.Input
                                            sqlParamLogImg.Size = bArr.Length
                                            sqlParamLogImg.Value = bArr
                                            cmd.Parameters.Add(sqlParamLogImg)
                                        End If
                                        cmd.ExecuteNonQuery()
                                        If con.State <> ConnectionState.Closed Then
                                            con.Close()
                                        End If


#Region "Panipath"  'Kapoor Industries
                                        '        Try
                                        '            Dim dsM As DataSet = New DataSet
                                        '            Dim sSqlM As String = "select ID_NO from tblmachine where SerialNumber='" & DeviceID & "'"
                                        '            Dim adpM As SqlDataAdapter = New SqlDataAdapter(sSqlM, con)
                                        '            adpM.Fill(dsM)
                                        '            Dim ID_NO As String = "1"
                                        '            If dsM.Tables(0).Rows.Count > 0 Then
                                        '                ID_NO = (dsM.Tables(0).Rows(0)(0).ToString.Trim) '.ToString("00")
                                        '            End If
                                        '            'TraceServiceTextPanipat(sSqlM & " " & ID_NO)
                                        '            Dim conTmp As New SqlConnection("Data Source=10.20.33.140;Initial Catalog=iDMS;User Id=sa;Password=kil@1234;MultipleActiveResultSets=true;")
                                        '            Dim conTmp1 As New SqlConnection("Data Source=10.20.33.24;Initial Catalog=Payroll;User Id=sa;Password=;MultipleActiveResultSets=true;")
                                        '            Dim Ezone As String = "COMP"
                                        '            If CardNo.Substring(0, 1) = "4" Then
                                        '                Ezone = "B"
                                        '            Else
                                        '                Ezone = "COMP"
                                        '            End If
                                        '            sSql = "insert into DAILY_ATTANDANCE_TEMP(Ezone,CardNo,Att_Date,PunchTime, MacId, Night) " &
                                        '"values('" & Ezone & "','" & CardNo & "','" & PuchTime.ToString("yyyy-MM-dd") & "','" & PuchTime.ToString("HH:mm") & "','" & ID_NO & "', '0')"

                                        '            'TraceServiceTextPanipat(sSql)
                                        '            Try
                                        '                If conTmp.State <> ConnectionState.Open Then
                                        '                    conTmp.Open()
                                        '                End If
                                        '                cmd = New SqlCommand(sSql, conTmp)
                                        '                cmd.ExecuteNonQuery()
                                        '                If conTmp.State <> ConnectionState.Closed Then
                                        '                    conTmp.Close()
                                        '                End If
                                        '            Catch ex As Exception
                                        '                If conTmp.State <> ConnectionState.Closed Then
                                        '                    conTmp.Close()
                                        '                End If
                                        '                TraceServiceTextPanipat("10.20.33.140 " & sSql & " " & ex.Message)
                                        '            End Try
                                        '            Try
                                        '                If conTmp1.State <> ConnectionState.Open Then
                                        '                    conTmp1.Open()
                                        '                End If
                                        '                cmd = New SqlCommand(sSql, conTmp1)
                                        '                cmd.ExecuteNonQuery()
                                        '                If conTmp1.State <> ConnectionState.Closed Then
                                        '                    conTmp1.Close()
                                        '                End If
                                        '            Catch ex As Exception
                                        '                If conTmp1.State <> ConnectionState.Closed Then
                                        '                    conTmp1.Close()
                                        '                End If
                                        '                TraceServiceTextPanipat("10.20.33.24 " & sSql & " " & ex.Message)
                                        '            End Try
                                        '        Catch ex As Exception
                                        '            TraceServiceTextPanipat(ex.Message)
                                        '        End Try
#End Region


                                        ' Image Delete From Folder Ajitesh
                                        Try
                                            If ImageTodelete.Trim <> "" Then
                                                My.Computer.FileSystem.DeleteFile(ImageTodelete)
                                            End If
                                        Catch ex1 As Exception
                                        End Try


                                    Catch ex As Exception
                                        If con.State <> ConnectionState.Closed Then
                                            con.Close()
                                        End If

                                        If Not ex.Message.ToString.Contains("Violation of PRIMARY KEY constraint") Then
                                            'save failed logs text                               
                                            Dim failedTxt As String = DeviceID & "," & CardNo & "," & verifymode & "," & PuchTimeStr & "," & Ctemp & ", " & Ftemp & "," & MaskWear & "," & TeperatureStatus & "," & io_mode & "," & ex.Message.ToString & ",RealTime"
                                            TraceServiceFailedLogs(failedTxt)
                                        End If
                                        EHomeForm.TraceService(ex.Message, "DeviceLogList Line 631")
                                        'If con.State <> ConnectionState.Closed Then
                                        '    con.Close()
                                        'End If
                                    End Try

#Region "check user in DB"

                                    Try 'check user in DB
                                        If EHomeForm.AutoSync = "Y" Then
                                            If CardNo <> "Visitor" Then
                                                sSql = "select * from Userdetail where DeviceID ='" & DeviceID & "' and UserID='" & CardNo & "' " &
                                                        "If @@ROWCOUNT=0 " &
                                                        "Begin " &
                                                        "select Dev_Cmd_Id from DeviceCommands where SerialNumber='" & DeviceID & "' and  TransfertoDevice='" & DeviceID & "' and CommandContent='CHECK' and UserID='" & CardNo & "' and Executed ='0' " &
                                                        "IF @@ROWCOUNT=0 " &
                                                        "insert into Devicecommands (SerialNumber, CommandContent, TransferToDevice, UserId, Executed, IsOnline, CreatedOn) " &
                                                        "values('" & DeviceID & "','CHECK','" & DeviceID & "', '" & CardNo & "',0,1,getdate()) " &
                                                        "End"
                                                If con.State <> ConnectionState.Open Then
                                                    con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, con)
                                                Dim count As Integer = cmd.ExecuteNonQuery()
                                                If con.State <> ConnectionState.Closed Then
                                                    con.Close()
                                                End If
                                            End If
                                        End If
                                    Catch ex As Exception
                                        If con.State <> ConnectionState.Closed Then
                                            con.Close()
                                        End If
                                        EHomeForm.TraceService(ex.Message, "DeviceLogList Line 654")
                                    End Try
#End Region


                                Catch ex As Exception
                                    EHomeForm.TraceService(ex.Message, "DeviceLogList Line 596")
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If
                                End Try
                                'End If   'End LNT
                            End If 'end Sapphire


                            CardNo = GetCardNumber(CardNo)
#Region "tblmachine"
                            Try
                                sSql = "Update tblMachine set UpdatedOn =getdate(), LastLog='" & PuchTime.ToString("yyyy-MM-dd HH:mm:ss") & "' where SerialNumber='" & DeviceID & "' "
                                If con.State <> ConnectionState.Open Then
                                    con.Open()
                                End If
                                cmd = New SqlCommand(sSql, con)
                                Dim count As Integer = cmd.ExecuteNonQuery()
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            Catch ex As Exception
                                EHomeForm.TraceService(ex.Message, "DeviceLogList " & sSql & " Line 663")
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End Try
#End Region
#Region "machinerawpunchandprocess"
                            If EHomeForm.AttProcess = "Y" Then
                                Try
                                    If CardNo <> "Visitor" Then
                                        Dim adpA As New SqlDataAdapter
                                        ds = New DataSet
                                        'sSql = "select TM.id_No,TE.companycode,TE.PRESENTCARDNO,TE.SSN,TE.paycode,TE.DepartmentCode from tblMachine TM  with (nolock) inner join tblemployee TE on Tm.CompanyCode = TE.COMPANYCODE where TM.SerialNumber = '" & DeviceID & "' and TE.PresentCardNo = '" & CardNo & "'"
                                        sSql = "select TES.DUPLICATECHECKMIN, TM.id_No,TE.companycode,TE.PRESENTCARDNO,TE.SSN,TE.paycode,TE.DepartmentCode from tblMachine TM  with (nolock) inner join tblemployee TE on Tm.CompanyCode = TE.COMPANYCODE inner join tblemployeeshiftmaster TES on TES.paycode=TE.paycode where TM.SerialNumber = '" & DeviceID & "' and TE.PresentCardNo = '" & CardNo & "'"
                                        adpA = New SqlDataAdapter(sSql, con)
                                        adpA.Fill(ds)
                                        If ds.Tables(0).Rows.Count > 0 Then
                                            Dim inout As String = ""
                                            'If (status = "0") Then
                                            '    inout = "I"
                                            'Else
                                            '    inout = "O"
                                            'End If
                                            Dim dupMin As Integer = 5
                                            Dim pIsV As Boolean = False
                                            If ds.Tables(0).Rows(0)("DUPLICATECHECKMIN").ToString().Trim() <> "" Then
                                                dupMin = ds.Tables(0).Rows(0)("DUPLICATECHECKMIN").ToString().Trim()
                                            End If
                                            pIsV = IsValid(ds.Tables(0).Rows(0)("PayCode").ToString().Trim(), PuchTime, con, dupMin)
                                            If pIsV Then
                                                Try
                                                    sSql = "insert into MachineRawPunch (CardNo,OfficePunch,P_Day,IsManual,MC_NO,INOUT,PAYCODE,SSN,CompanyCode)values('" & ds.Tables(0).Rows(0)("PresentCardNo").ToString().Trim() & "','" & PuchTime.ToString("yyyy-MM-dd HH:mm") & "','N','N'," & ds.Tables(0).Rows(0)("id_No").ToString().Trim() & ",'" & inout & "','" & ds.Tables(0).Rows(0)("PayCode").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("SSN").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "')"
                                                    If con.State <> ConnectionState.Open Then
                                                        con.Open()
                                                    End If
                                                    cmd = New SqlCommand(sSql, con)
                                                    cmd.ExecuteNonQuery()

                                                    If con.State <> ConnectionState.Closed Then
                                                        con.Close()
                                                    End If
                                                Catch ex As Exception
                                                    EHomeForm.TraceService(ex.Message, "DeviceLogList Line 252")
                                                    If con.State <> ConnectionState.Closed Then
                                                        con.Close()
                                                    End If
                                                End Try

                                                'temp comment
                                                sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup where companycode='" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "') "
                                                Dim ds1 As DataSet = New DataSet
                                                Try
                                                    adpA = New SqlDataAdapter(sSql, con)
                                                    adpA.Fill(ds1)
                                                Catch ex As Exception
                                                    EHomeForm.TraceService(ex.Message, "DeviceLogList Line 1563")
                                                    If con.State <> ConnectionState.Closed Then
                                                        con.Close()
                                                    End If
                                                End Try

                                                Dim SetupID As String = ""
                                                If ds1.Tables(0).Rows.Count > 0 Then
                                                    SetupID = ds1.Tables(0).Rows(0)(0).ToString.Trim
                                                    Try
                                                        If (con.State <> ConnectionState.Open) Then
                                                            con.Open()
                                                        End If
                                                        'Dim tmpdatetime As DateTime = Convert.ToDateTime(atttime.ToString.Trim)
                                                        cmd = New SqlCommand("ProcessBackDate", con)
                                                        cmd.CommandTimeout = 1800
                                                        cmd.CommandType = CommandType.StoredProcedure
                                                        cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                                                        cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                                                        cmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("CompanyCode").ToString.Trim
                                                        cmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("DepartmentCode").ToString.Trim
                                                        cmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("PayCode").ToString.Trim
                                                        cmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID
                                                        cmd.ExecuteNonQuery()
                                                        If (con.State <> ConnectionState.Closed) Then
                                                            con.Close()
                                                        End If
                                                    Catch ex As Exception
                                                        EHomeForm.TraceService(ex.Message, "DeviceLogList Line 293")
                                                        If con.State <> ConnectionState.Closed Then
                                                            con.Close()
                                                        End If
                                                    End Try
                                                End If
                                            End If
                                            'end temp comment
                                        End If
                                    End If
                                Catch ex As Exception
                                    EHomeForm.TraceService(ex.Message, "DeviceLogList Line 302")
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If
                                End Try
                            End If
#End Region
                        End If
                    Catch e As IOException
                        str = String.Format("[ACS ALARM] save file failed![%s]", e.ToString())
                        EHomeForm.TraceService(e.Message, "DeviceLogList Line 322")
                    End Try
                End If
                System.Windows.Forms.Application.DoEvents()
            Catch ex As Exception

            End Try

        End Sub
        Public Function IsValid(ByVal UID As String, PTime As DateTime, conS As SqlConnection, dupMin As Integer) As Boolean
            Dim Valid As Boolean = False
            Try
                Dim sSql As String = ""
                Dim RsVald As DataSet = New DataSet()
                sSql = "select max(officepunch) as LastPunch from machinerawpunch where paycode ='" & UID.Trim & "' "
                'TraceServiceText(sSql)

                'TraceServiceText(PTime)
                Dim SDV As SqlDataAdapter = New SqlDataAdapter(sSql, conS)
                SDV.Fill(RsVald)
                'TraceServiceText(Convert.ToDateTime(RsVald.Tables(0).Rows(0)("LastPunch").ToString.Trim).AddMinutes(dupMin))
                If RsVald.Tables(0).Rows.Count = 0 Or RsVald.Tables(0).Rows(0)(0).ToString().Trim() = "" Or String.IsNullOrEmpty(RsVald.Tables(0).Rows(0)(0).ToString().Trim()) Then
                    Valid = True
                ElseIf Convert.ToDateTime(RsVald.Tables(0).Rows(0)("LastPunch").ToString.Trim).AddMinutes(dupMin) < PTime Then
                    Valid = True
                Else
                    Valid = False
                End If

            Catch ex As Exception

                Return False
            End Try

            Return Valid
        End Function
        Public Function IsValidUserAtt(ByVal UID As String, PTime As DateTime, conS As SqlConnection, dupMin As Integer, DeviceID As String) As Boolean
            Dim Valid As Boolean = False
            If UID = "Visitor" Then
                Return True
            End If
            Dim sSql As String = ""
            Try
                If dupMin = 0 Then
                    Return True
                End If

                Dim RsVald As DataSet = New DataSet()
                'sSql = "select max(AttDateTime) as LastPunch from UserAttendance where Userid ='" & UID.Trim & "' and DeviceID='" & DeviceID & "' "
                sSql = "select AttDateTime from UserAttendance where Userid ='" & UID.Trim & "' and DeviceID='" & DeviceID & "' and AttDateTime between '" & PTime.AddMinutes(-2).ToString("yyyy-MM-dd HH:mm:00") & "' and '" & PTime.ToString("yyyy-MM-dd HH:mm:00") & "'"
                'TraceServiceText(sSql)

                Dim SDV As SqlDataAdapter = New SqlDataAdapter(sSql, conS)
                SDV.Fill(RsVald)
                'TraceServiceText(Convert.ToDateTime(RsVald.Tables(0).Rows(0)("LastPunch").ToString.Trim).AddMinutes(dupMin))

                If RsVald.Tables(0).Rows.Count > 0 Then
                    Valid = False
                Else
                    Valid = True
                End If

                'If RsVald.Tables(0).Rows(0)("LastPunch").ToString.Trim = "" Then
                '    Valid = True
                'ElseIf Convert.ToDateTime(RsVald.Tables(0).Rows(0)("LastPunch").ToString.Trim).AddMinutes(dupMin) > PTime Then
                '    Valid = False
                'Else
                '    Valid = True
                'End If

            Catch ex As Exception
                TraceServiceTextQuery(ex.Message & " " & sSql)
                Return False
            End Try

            Return Valid
        End Function
        Public Function InsertEventLog(SerialNo As String, DeviceID As String, EventDate As DateTime)
            Dim con As New SqlConnection(EHomeForm.ConnectionString)
            Try
                Dim sSql As String = "insert into EventCheck (SerialNo,DeviceID, EventDate) values ('" & SerialNo & "','" & DeviceID & "','" & EventDate.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                If con.State <> ConnectionState.Open Then
                    con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, con)
                cmd.ExecuteNonQuery()
                If con.State <> ConnectionState.Closed Then
                    con.Close()
                End If
            Catch ex As Exception
                If con.State <> ConnectionState.Closed Then
                    con.Close()
                End If
            End Try
        End Function
        'Public Sub ProcessEhomeCid(ByVal pStru As IntPtr, ByVal dwStruLen As UInteger, ByVal pXml As IntPtr, ByVal dwXmlLen As UInteger)
        '    If pStru = Nothing Then
        '        Return
        '    End If

        '    Dim pStruCidInfo As HCEHomeAlarm.NET_EHOME_CID_INFO = New HCEHomeAlarm.NET_EHOME_CID_INFO()
        '    pStruCidInfo.Init()
        '    pStruCidInfo = CType(Marshal.PtrToStructure(pStru, GetType(HCEHomeAlarm.NET_EHOME_CID_INFO)), HCEHomeAlarm.NET_EHOME_CID_INFO)
        '    Dim pStruCidInfoEx As HCEHomeAlarm.NET_EHOME_CID_INFO_INTERNAL_EX = New HCEHomeAlarm.NET_EHOME_CID_INFO_INTERNAL_EX()
        '    pStruCidInfoEx.Init()
        '    pStruCidInfoEx = CType(Marshal.PtrToStructure(pStruCidInfo.pCidInfoEx, GetType(HCEHomeAlarm.NET_EHOME_CID_INFO_INTERNAL_EX)), HCEHomeAlarm.NET_EHOME_CID_INFO_INTERNAL_EX)
        '    Dim cDescribe As String = Nothing
        '    cDescribe = pStruCidInfo.byCIDDescribe
        '    Dim cUUID As String = Nothing

        '    If 1 = pStruCidInfo.byExtend Then
        '        cDescribe = pStruCidInfoEx.byCIDDescribeEx
        '        cUUID = pStruCidInfoEx.byUUID
        '        Dim temp As String = "[CID_EX]uuid" & cUUID & "recheck" & pStruCidInfoEx.byRecheck.ToString() & "Recheck URL" + pStruCidInfoEx.byVideoURL
        '        AddLog(-1, HCEHomePublic.ALARM_INFO_T, 0, temp)
        '    End If

        '    Dim LogInfo As String = Nothing
        '    LogInfo = "[CID]uuid" & cUUID & ",DeviceID:" & pStruCidInfo.byDeviceID & ",CID code:" + pStruCidInfo.dwCIDCode.ToString() & ",CID type:" + pStruCidInfo.dwCIDType.ToString() & ",Subsys No" + pStruCidInfo.dwSubSysNo.ToString() & ",Describe:" & cDescribe
        '    AddLog(-1, HCEHomePublic.ALARM_INFO_T, 0, LogInfo)
        'End Sub
        Public Sub ProcessEhomeAlarmACS(ByVal pXml As IntPtr, ByVal dwXmlLen As UInteger)
            Dim str As String
            Dim strTemp As String = Marshal.PtrToStringAnsi(pXml, CInt(dwXmlLen))
            str = String.Format("[ACS ALARM] save to[" & System.Environment.CurrentDirectory & "]")
            Try
                Dim CardNo As String = ""
                Dim PuchTime As DateTime = Convert.ToDateTime("1900-01-01 00:00:00")
                Dim DeviceID As String = ""
                Dim CardType As String = ""
                Dim verifymode As String = ""
                Dim xml As New XmlDocument()
                xml.LoadXml(strTemp) ' suppose that myXmlString contains "<Names>...</Names>"

                Dim XCommandType As String = ""
                Dim xnListCtype As XmlNodeList = xml.SelectNodes("/PPVSPMessage/CommandType")
                For Each xn As XmlNode In xnListCtype
                    XCommandType = xn.InnerText
                Next xn
                Dim xnListD As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/DeviceID")
                For Each xn As XmlNode In xnListD
                    DeviceID = xn.InnerText
                Next xn

                'for online registration request
                If XCommandType = "REQUEST" Then



                End If
                'end online registration request

                Dim xnList As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/employeeNoString")
                For Each xn As XmlNode In xnList
                    CardNo = xn.InnerText
                Next xn

                Dim xnListT As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/Time")
                For Each xn As XmlNode In xnListT
                    PuchTime = Convert.ToDateTime(xn.InnerText)
                Next xn

                Dim xnListCT As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/CardType")
                For Each xn As XmlNode In xnListCT
                    CardType = xn.InnerText
                Next xn
                Dim xnListVM As XmlNodeList = xml.SelectNodes("/PPVSPMessage/Params/currentVerifyMode")
                For Each xn As XmlNode In xnListCT
                    verifymode = xn.InnerText
                Next xn
                If CardNo <> "" And CardType = "1" Then
                    Dim con As New SqlConnection(EHomeForm.ConnectionString)
                    Dim ds As New DataSet()
                    CardNo = GetCardNumber(CardNo)
#Region "old"
                    'Dim sql As String = "select TM.id_No,TM.companycode,TM.clientid,TE.PRESENTCARDNO,TE.SSN,TE.paycode,TE.DepartmentCode from tblMachine TM  with (nolock) inner join tblemployee TE on Tm.CompanyCode = TE.COMPANYCODE where TM.MAC_ADDRESS = '" & DeviceID & "' and TE.PresentCardNo = '" & CardNo & "'"
                    'Dim adp As New SqlDataAdapter(sql, con)
                    'adp.Fill(ds, "TableName")

                    'If ds.Tables(0).Rows.Count > 0 Then
                    '    'string inout = "";
                    '    'if (status == "0")
                    '    '{
                    '    '    inout = "I";
                    '    '}
                    '    'else
                    '    '{
                    '    '    inout = "O";
                    '    '}
                    '    Dim cmd As SqlCommand
                    '    Try
                    '        If con.State <> ConnectionState.Open Then
                    '            con.Open()
                    '        End If
                    '        Dim sqli As String = "insert into MachineRawPunch (CardNo,OfficePunch,P_Day,IsManual,MC_NO,PAYCODE,SSN,CompanyCode)values('" & ds.Tables(0).Rows(0)("PresentCardNo").ToString().Trim() & "','" & PuchTime.ToString("yyyy/MM/dd HH:mm:ss") & "','N','N'," & ds.Tables(0).Rows(0)("id_No").ToString().Trim() & ",'" & ds.Tables(0).Rows(0)("PayCode").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("SSN").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "')"
                    '        cmd = New SqlCommand(sqli, con)
                    '        cmd.CommandType = CommandType.Text
                    '        cmd.ExecuteNonQuery()
                    '        If con.State <> ConnectionState.Closed Then
                    '            con.Close()
                    '        End If
                    '    Catch ex As Exception
                    '        If con.State <> ConnectionState.Closed Then
                    '            con.Close()
                    '        End If
                    '        'TraceService(ex.Message, "DeviceLogList Line 4849");
                    '    End Try
                    '    Try
                    '        Dim SetupID As String = ""
                    '        Dim Rs As New DataSet()
                    '        Dim sSql As String = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup where companycode='" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "') "
                    '        Dim rsadp As New SqlDataAdapter(sSql, con)
                    '        rsadp.Fill(Rs, "TableName")
                    '        If Rs.Tables(0).Rows.Count > 0 Then
                    '            SetupID = Rs.Tables(0).Rows(0)(0).ToString().Trim()
                    '        End If
                    '        If con.State <> ConnectionState.Open Then
                    '            con.Open()
                    '        End If
                    '        'DateTime tmpdatetime = Convert.ToDateTime(atttime.ToString().Trim());
                    '        cmd = New SqlCommand("ProcessBackDate", con)
                    '        cmd.CommandTimeout = 1800
                    '        cmd.CommandType = CommandType.StoredProcedure
                    '        cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                    '        cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                    '        cmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim()
                    '        cmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("DepartmentCode").ToString().Trim()
                    '        cmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("PayCode").ToString().Trim()
                    '        cmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID
                    '        cmd.ExecuteNonQuery()
                    '        If con.State <> ConnectionState.Closed Then
                    '            con.Close()
                    '        End If
                    '    Catch ex As Exception
                    '        If con.State <> ConnectionState.Closed Then
                    '            con.Close()
                    '        End If
                    '        'TraceService(ex.Message, "DeviceLogList Line 4864");
                    '    End Try
                    'End If
#End Region
                    Dim cmd As SqlCommand
                    Dim sSql As String = ""
                    Try
                        sSql = "select Userid from UserAttendance  with (nolock) where DeviceID='" & DeviceID & "' and UserID='" & CardNo & "' and AttDateTime='" & PuchTime.ToString("yyyy-MM-dd HH:mm:ss") & "' " &
                        " IF @@ROWCOUNT=0 " &
                        "insert into UserAttendance(DeviceID,UserID,VerifyMode,AttDateTime) " &
                        "values('" & DeviceID & "','" & CardNo & "','" & verifymode & "','" & PuchTime.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                        If con.State <> ConnectionState.Open Then
                            con.Open()
                        End If
                        cmd = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    Catch ex As Exception
                        EHomeForm.TraceService(ex.Message, "DeviceLogList Line 221")
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    End Try

                    Try
                        Dim adpA As New SqlDataAdapter
                        ds = New DataSet
                        sSql = "select TM.id_No,TE.companycode,TE.PRESENTCARDNO,TE.SSN,TE.paycode,TE.DepartmentCode from tblMachine TM  with (nolock) inner join tblemployee TE on Tm.CompanyCode = TE.COMPANYCODE where TM.SerialNumber = '" & DeviceID & "' and TE.PresentCardNo = '" & CardNo & "'"
                        adpA = New SqlDataAdapter(sSql, con)
                        adpA.Fill(ds)
                        If ds.Tables(0).Rows.Count > 0 Then
                            Dim inout As String = ""
                            'If (status = "0") Then
                            '    inout = "I"
                            'Else
                            '    inout = "O"
                            'End If
                            Try
                                sSql = "insert into MachineRawPunch (CardNo,OfficePunch,P_Day,IsManual,MC_NO,INOUT,PAYCODE,SSN,CompanyCode)values('" & ds.Tables(0).Rows(0)("PresentCardNo").ToString().Trim() & "','" & PuchTime.ToString("yyyy-MM-dd HH:mm:ss") & "','N','N'," & ds.Tables(0).Rows(0)("id_No").ToString().Trim() & ",'" & inout & "','" & ds.Tables(0).Rows(0)("PayCode").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("SSN").ToString().Trim() & "','" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "')"
                                If con.State <> ConnectionState.Open Then
                                    con.Open()
                                End If
                                cmd = New SqlCommand(sSql, con)
                                cmd.ExecuteNonQuery()

                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            Catch ex As Exception
                                EHomeForm.TraceService(ex.Message, "DeviceLogList Line 252")
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End Try

                            'temp comment
                            sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup where companycode='" & ds.Tables(0).Rows(0)("CompanyCode").ToString().Trim() & "') "
                            Dim ds1 As DataSet = New DataSet
                            Try
                                adpA = New SqlDataAdapter(sSql, con)
                                adpA.Fill(ds1)
                            Catch ex As Exception
                                EHomeForm.TraceService(ex.Message, "DeviceLogList Line 1563")
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End Try

                            Dim SetupID As String = ""
                            If ds1.Tables(0).Rows.Count > 0 Then
                                SetupID = ds1.Tables(0).Rows(0)(0).ToString.Trim
                                Try
                                    If (con.State <> ConnectionState.Open) Then
                                        con.Open()
                                    End If
                                    'Dim tmpdatetime As DateTime = Convert.ToDateTime(atttime.ToString.Trim)
                                    cmd = New SqlCommand("ProcessBackDate", con)
                                    cmd.CommandTimeout = 1800
                                    cmd.CommandType = CommandType.StoredProcedure
                                    cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                                    cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = PuchTime.ToString("yyyy-MM-dd") 'atttime;
                                    cmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("CompanyCode").ToString.Trim
                                    cmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("DepartmentCode").ToString.Trim
                                    cmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = ds.Tables(0).Rows(0)("PayCode").ToString.Trim
                                    cmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID
                                    cmd.ExecuteNonQuery()
                                    If (con.State <> ConnectionState.Closed) Then
                                        con.Close()
                                    End If
                                Catch ex As Exception
                                    EHomeForm.TraceService(ex.Message, "DeviceLogList Line 293")
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If
                                End Try
                            End If
                            'end temp comment
                        End If
                    Catch ex As Exception
                        EHomeForm.TraceService(ex.Message, "DeviceLogList Line 302")
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    End Try

                End If

                Dim directoryPath As String = System.Environment.CurrentDirectory & "\picture"
                Dim filePath As String = m_iIndex.ToString() & ".xml"
                If Not Directory.Exists(directoryPath) Then
                    Directory.CreateDirectory(directoryPath)
                End If
                Dim sw As StreamWriter = New StreamWriter(Path.Combine(directoryPath, filePath))
                sw.WriteLine(strTemp)
                sw.Flush()
                sw.Close()
                m_iIndex += 1
            Catch e As IOException
                str = String.Format("[ACS ALARM] save file failed![%s]", e.ToString())
                EHomeForm.TraceService(e.Message, "DeviceLogList Line 322")
            End Try
            'AddLog(-1, HCEHomePublic.ALARM_INFO_T, 0, str)
        End Sub
        Public Function GetCardNumber(ByVal USerID As String) As String
            If IsNumeric(USerID) Then
                Dim cardno As String = "", zeros As String = ""
                Try
                    Dim UIDLenth As Integer = USerID.Length
                    Dim toloop As Integer = 8 - UIDLenth
                    For i As Integer = 1 To toloop
                        zeros = zeros & "0"
                    Next i
                    cardno = zeros & USerID
                    'TraceService(cardno, "DeviceLogList Line 2376");
                    Return cardno
                Catch ex As Exception
                    'TraceService(ex.Message, "DeviceLogList Line 2382");
                    Return cardno
                End Try
            Else
                Return USerID
            End If
        End Function
        Public Sub ProcessEhomeAlarm(ByVal pStru As IntPtr, ByVal dwStruLen As UInteger, ByVal pXml As IntPtr, ByVal dwXmlLen As UInteger)
            Dim struAlarmInfo As HCEHomeAlarm.NET_EHOME_ALARM_INFO = New HCEHomeAlarm.NET_EHOME_ALARM_INFO()
            struAlarmInfo.Init()
            struAlarmInfo = CType(Marshal.PtrToStructure(pStru, GetType(HCEHomeAlarm.NET_EHOME_ALARM_INFO)), HCEHomeAlarm.NET_EHOME_ALARM_INFO)
            Dim strDevID As String = System.Text.Encoding.UTF8.GetString(struAlarmInfo.szDeviceID).Substring(0, struAlarmInfo.szDeviceID.Length)
            Dim strDevIDLen As Integer = strDevID.IndexOf(vbNullChar)
            Dim strSampTime As String = System.Text.Encoding.UTF8.GetString(struAlarmInfo.szAlarmTime).Substring(0, struAlarmInfo.szAlarmTime.Length)
            Dim strSampTimeLen As Integer = strSampTime.IndexOf(vbNullChar)
            Dim str As String
            str = String.Format("[ALARM]DeviceID:{0},Time:{1},Type:{2},Action:{3},Channel:{4},AlarmIn:{5},DiskNo:{6}", strDevID.Substring(0, strDevIDLen), strSampTime.Substring(0, strSampTimeLen), struAlarmInfo.dwAlarmType, struAlarmInfo.dwAlarmAction, struAlarmInfo.dwVideoChannel, struAlarmInfo.dwAlarmInChannel, struAlarmInfo.dwDiskNumber)
            'AddLog(-1, HCEHomePublic.ALARM_INFO_T, 0, str)
        End Sub
        'Public Sub ProcessEhomeGps(ByVal pStru As IntPtr, ByVal dwStruLen As UInteger, ByVal pXml As IntPtr, ByVal dwXmlLen As UInteger)
        '    Dim struGps As HCEHomeAlarm.NET_EHOME_GPS_INFO = New HCEHomeAlarm.NET_EHOME_GPS_INFO()
        '    struGps.Init()
        '    struGps = CType(Marshal.PtrToStructure(pStru, GetType(HCEHomeAlarm.NET_EHOME_GPS_INFO)), HCEHomeAlarm.NET_EHOME_GPS_INFO)
        '    Dim strDevID As String = System.Text.Encoding.UTF8.GetString(struGps.byDeviceID).Substring(0, struGps.byDeviceID.Length)
        '    Dim strDevIDLen As Integer = strDevID.IndexOf(vbNullChar)
        '    Dim strSampTime As String = System.Text.Encoding.UTF8.GetString(struGps.bySampleTime).Substring(0, struGps.bySampleTime.Length)
        '    Dim strSampTimeLen As Integer = strSampTime.IndexOf(vbNullChar)
        '    Dim str As String
        '    str = String.Format("[GPS]DeviceID:{0},SampleTime:{1},Division:[{2} {3}],Satelites:{4},Precision:{5},Longitude:{6},Latitude:{7},Direction:{8},Speed:{9},Height:{10}", strDevID.Substring(0, strDevIDLen), strSampTime.Substring(0, strSampTimeLen), struGps.byDivision(0), struGps.byDivision(1), struGps.bySatelites, struGps.byPrecision, struGps.dwLongitude, struGps.dwLatitude, struGps.dwDirection, struGps.dwSpeed, struGps.dwHeight)
        '    AddLog(-1, HCEHomePublic.ALARM_INFO_T, 0, str)
        'End Sub
        'Public Sub showList(ByVal iTag As Integer)
        '    Select Case iTag
        '        Case 0
        '            listViewAllLog.Visible = True
        '            listViewAlarmInfo.Visible = False
        '        Case 1
        '            listViewAllLog.Visible = False
        '            listViewAlarmInfo.Visible = True
        '        Case Else
        '            listViewAllLog.Visible = True
        '            listViewAlarmInfo.Visible = False
        '    End Select
        'End Sub
        'Public Sub ClearList(ByVal iTag As Integer)
        '    Select Case iTag
        '        Case 0
        '            listViewAllLog.Items.Clear()
        '        Case 1
        '            listViewAlarmInfo.Items.Clear()
        '        Case Else
        '            listViewAllLog.Items.Clear()
        '            listViewAlarmInfo.Items.Clear()
        '    End Select
        'End Sub
        'Private Delegate Sub AddListCallBack(ByVal struLogInfo As HCEHomePublic.LOCAL_LOG_INFO)
        'Private Sub AddList(ByVal struLogInfo As HCEHomePublic.LOCAL_LOG_INFO)
        '    If Me.InvokeRequired Then
        '        Dim add As AddListCallBack = New AddListCallBack(AddressOf AddList)
        '        Me.BeginInvoke(add, New Object() {struLogInfo})
        '    Else

        '        SyncLock locker

        '            If listViewAllLog.Items.Count >= 5000 Then
        '                listViewAllLog.Items.Clear()
        '            End If

        '            If listViewAlarmInfo.Items.Count >= 5000 Then
        '                listViewAlarmInfo.Items.Clear()
        '            End If

        '            Dim strLogType As String = "FAIL"

        '            Select Case struLogInfo.iLogType
        '                Case HCEHomePublic.ALARM_INFO_T
        '                    strLogType = "Alarm"
        '                    Dim listItem1 As ListViewItem = New ListViewItem()
        '                    listItem1.Text = struLogInfo.strTime
        '                    listItem1.SubItems.Add(struLogInfo.strLogInfo)
        '                    listItem1.SubItems.Add(struLogInfo.strDevInfo)
        '                    listViewAlarmInfo.Items.Insert(0, listItem1)
        '                Case HCEHomePublic.OPERATION_SUCC_T
        '                    strLogType = "SUCC"
        '                    Dim listItem2 As ListViewItem = New ListViewItem()
        '                    listItem2.Text = struLogInfo.strTime
        '                    listItem2.SubItems.Add(strLogType)
        '                    listItem2.SubItems.Add(struLogInfo.strLogInfo)
        '                    listItem2.SubItems.Add(struLogInfo.strDevInfo)
        '                    listItem2.SubItems.Add(struLogInfo.strErrInfo)
        '                    listViewAllLog.Items.Insert(0, listItem2)
        '                Case HCEHomePublic.OPERATION_FAIL_T
        '                    strLogType = "FAIL"
        '                    Dim listItem3 As ListViewItem = New ListViewItem()
        '                    listItem3.Text = struLogInfo.strTime
        '                    listItem3.SubItems.Add(strLogType)
        '                    listItem3.SubItems.Add(struLogInfo.strLogInfo)
        '                    listItem3.SubItems.Add(struLogInfo.strDevInfo)
        '                    listItem3.SubItems.Add(struLogInfo.strErrInfo)
        '                    listViewAllLog.Items.Insert(0, listItem3)
        '                Case HCEHomePublic.PLAY_SUCC_T
        '                    strLogType = "SUCC"
        '                    Dim listItem4 As ListViewItem = New ListViewItem()
        '                    listItem4.Text = struLogInfo.strTime
        '                    listItem4.SubItems.Add(strLogType)
        '                    listItem4.SubItems.Add(struLogInfo.strLogInfo)
        '                    listItem4.SubItems.Add(struLogInfo.strDevInfo)
        '                    listItem4.SubItems.Add(struLogInfo.strErrInfo)
        '                    listViewAllLog.Items.Insert(0, listItem4)
        '                Case HCEHomePublic.PLAY_FAIL_T
        '                    strLogType = "FAIL"
        '                    Dim listItem5 As ListViewItem = New ListViewItem()
        '                    listItem5.Text = struLogInfo.strTime
        '                    listItem5.SubItems.Add(strLogType)
        '                    listItem5.SubItems.Add(struLogInfo.strLogInfo)
        '                    listItem5.SubItems.Add(struLogInfo.strDevInfo)
        '                    listItem5.SubItems.Add(struLogInfo.strErrInfo)
        '                    listViewAllLog.Items.Insert(0, listItem5)
        '                Case Else
        '                    strLogType = "FAIL"
        '                    Dim listItem6 As ListViewItem = New ListViewItem()
        '                    listItem6.Text = struLogInfo.strTime
        '                    listItem6.SubItems.Add(strLogType)
        '                    listItem6.SubItems.Add(struLogInfo.strLogInfo)
        '                    listItem6.SubItems.Add(struLogInfo.strDevInfo)
        '                    listItem6.SubItems.Add(struLogInfo.strErrInfo)
        '                    listViewAllLog.Items.Insert(0, listItem6)
        '            End Select
        '        End SyncLock
        '    End If
        'End Sub
        'Public Sub AddLog(ByVal iDeviceIndex As Integer, ByVal iLogType As Integer, ByVal iComType As Integer, ByVal strFormat As String)
        '    Dim g_deviceTree As DeviceTree = DeviceTree.Instance()
        '    Dim curTime As DateTime = DateTime.Now
        '    Dim strTime As String = Nothing
        '    Dim strlogType As String = "FAIL"
        '    Dim strLogInfo As String = Nothing
        '    Dim strDevInfo As String = Nothing
        '    Dim strErrInfo As String = Nothing
        '    strTime = curTime.ToString()
        '    strLogInfo = strFormat

        '    If iDeviceIndex <> -1 AndAlso iDeviceIndex <512 Then

        '        If DeviceTree.g_struDeviceInfo(iDeviceIndex).byDeviceID(0) = 0 Then
        '            strDevInfo = String.Format("[]")
        '        Else
        '            strDevInfo = System.Text.Encoding.UTF8.GetString(DeviceTree.g_struDeviceInfo(iDeviceIndex).byDeviceID)
        '            Dim strDevInfoLen As Integer = strDevInfo.IndexOf(vbNullChar)
        '            strDevInfo = String.Format("[{0}]", strDevInfo.Substring(0, strDevInfoLen))
        '        End If
        '    End If

        '    Select Case iLogType
        '        Case HCEHomePublic.OPERATION_SUCC_T
        '            strErrInfo = ""
        '            strlogType = "SUCC"
        '        Case HCEHomePublic.PLAY_SUCC_T
        '            strErrInfo = ""
        '            strlogType = "SUCC"
        '        Case HCEHomePublic.PLAY_FAIL_T
        '            strErrInfo = "PLAY_M4 Error!!!"
        '        Case Else

        '            Select Case iComType
        '                Case 1
        '                    strErrInfo = String.Format("CMS_ERR{0}", HCEHomeCMS.NET_ECMS_GetLastError())
        '                Case 2
        '                    strErrInfo = String.Format("PREVIEW_ERR{0}", HCEHomeStream.NET_ESTREAM_GetLastError())
        '                Case 3
        '                    strErrInfo = String.Format("ALARM_ERR{0}", HCEHomeAlarm.NET_EALARM_GetLastError())
        '                Case Else
        '            End Select
        '    End Select

        '    m_struLogInfo.iLogType = iLogType
        '    m_struLogInfo.strTime = strTime
        '    m_struLogInfo.strLogInfo = strLogInfo
        '    m_struLogInfo.strDevInfo = strDevInfo
        '    m_struLogInfo.strErrInfo = strErrInfo
        '    AddList(m_struLogInfo)
        'End Sub
        'Private Sub listViewAllLog_ColumnClick(ByVal sender As Object, ByVal e As ColumnClickEventArgs)
        '    Dim pos As Point = New Point()
        '    pos.X = 5
        '    pos.Y = 4
        '    Dim w As Integer = Me.Parent.Parent.Size.Width
        '    Dim h As Integer = Me.Parent.Parent.Size.Height
        '    If Not m_bEnlarged Then
        '        m_logPanelPos.X = Me.Parent.Location.X
        '        m_logPanelPos.Y = Me.Parent.Location.Y
        '        m_logPanelSize.Width = Me.Parent.Size.Width
        '        m_logPanelSize.Height = Me.Parent.Size.Height
        '        Me.Parent.SetBounds(pos.X, pos.Y, w, h)
        '        Me.Parent.Parent.GetChildAtPoint(pos).Visible = False
        '        Me.Parent.Visible = True
        '        Me.Parent.Show()
        '        m_bEnlarged = True
        '    Else
        '        Me.Parent.SetBounds(m_logPanelPos.X, m_logPanelPos.Y, m_logPanelSize.Width, m_logPanelSize.Height)
        '        Me.Parent.Parent.GetChildAtPoint(pos).Visible = True
        '        Me.Parent.Visible = True
        '        Me.Parent.Show()
        '        m_bEnlarged = False
        '    End If
        'End Sub
        Private Sub DeviceLogList_Load(ByVal sender As Object, ByVal e As EventArgs)
            'MultiLanguage.LoadLanguage(Me)
        End Sub
        Public Function imgToByteArray(ByVal img As Image) As Byte()
            Using mStream As New MemoryStream()
                img.Save(mStream, img.RawFormat)
                Return mStream.ToArray()
            End Using
        End Function
        Public Sub TraceServiceText(ByVal errmessage As String)
            Try
                Dim fs As FileStream = New FileStream("String.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & errmessage)
                sw.Flush()
                sw.Close()
            Catch ex As Exception

            End Try

        End Sub
        Public Sub Query(ByVal errmessage As String)
            Try
                Dim fs As FileStream = New FileStream("Query.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & errmessage)
                sw.Flush()
                sw.Close()
            Catch ex As Exception

            End Try

        End Sub
        Public Sub TraceServiceTextEvents(ByVal errmessage As String)
            Try
                Dim fs As FileStream = New FileStream("Events\String" & Now.ToString("yyyyMMddHH") & ".txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & errmessage & " " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
                sw.Flush()
                sw.Close()
            Catch ex As Exception

            End Try

        End Sub
        Public Sub TraceServiceTextPanipat(ByVal errmessage As String)
            Try
                Dim fs As FileStream = New FileStream("Panipat.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & errmessage & " " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
                sw.Flush()
                sw.Close()
            Catch ex As Exception

            End Try

        End Sub
        Public Sub TraceServiceTextQuery(ByVal errmessage As String)
            Try
                Dim fs As FileStream = New FileStream("Query.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & errmessage)
                sw.Flush()
                sw.Close()
            Catch ex As Exception

            End Try
        End Sub


        Public Sub TraceServiceFailedLogs(ByVal errmessage As String)
            Try
                Dim fs As FileStream = New FileStream("FailedEvents\Failed.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & errmessage)
                sw.Flush()
                sw.Close()
            Catch ex As Exception

            End Try
        End Sub

        Public Sub TraceServiceFailedLogsDump(ByVal errmessage As String)
            Try
                Dim fs As FileStream = New FileStream("FailedEvents\FailedDump.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.[End])
                sw.WriteLine(vbLf & errmessage)
                sw.Flush()
                sw.Close()
            Catch ex As Exception

            End Try
        End Sub

    End Class
End Namespace
